<?php

namespace common\models;

use Yii;
use yii\helpers\Inflector;

/**
 * This is the model class for table "page".
 *
 * @property int $id
 * @property string $slug
 * @property int $status
 *
 * @property PageLocalization[] $pageLocalizations
 */
class Page extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public function behaviors()
    {
        return [
            [
                'class' => \backend\components\behaviors\LanguageBehavior::className(),
                'model' => '\common\models\PageLocalization',
                'field' => 'language',
                'relation' => 'localization'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'required'],
            [['status'], 'integer'],
            [['slug'], 'string', 'max' => 100],
            [['slug'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slug' => 'Slug',
            'status' => 'Статус',
        ];
    }

    public function save_with_slug($slug)
    {
        $this->slug = Inflector::slug($slug);
        return $this->save();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalization()
    {
        return $this->hasMany(PageLocalization::className(), ['page_id' => 'id'])->indexBy('language');
    }

    public function getMainLanguage()
    {
        return $this->hasOne(PageLocalization::className(), ['page_id' => 'id'])->andWhere(['language' => Yii::$app->params['main_language']]);
    }

    public function getActiveLanguage()
    {
        return $this->hasOne(PageLocalization::className(), ['page_id' => 'id'])->where(['language' => Yii::$app->language]);
    }

    static public function findPage($slug)
    {
        $model = self::find()->where([
            'status' => self::STATUS_ACTIVE,
            'slug' => $slug
        ])->one();

        if($model) {
            return $model->translation;
        }

        return false;
    }

    public function getTranslation()
    {
        $model = $this->activeLanguage;

        $name = $model->name ?: $this->mainLanguage->name;

        return [
            'name' => $name,
            'text' => $model->text ?: '',
            'meta_title' => $model->meta_title ?: $name,
            'meta_description' => $model->meta_description ?: '',
            'meta_keywords' => $model->meta_keywords ?: ''
        ];
    }
}
