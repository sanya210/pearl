<?php

namespace common\models;

use Yii;
use sjaakp\illustrated\Illustrated;
use yii\helpers\Html;

/**
 * This is the model class for table "social".
 *
 * @property int $id
 * @property string $name
 * @property string $icon
 * @property string $link
 * @property int $status
 */
class Social extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public $image;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'social';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'name'], 'required'],
            [['priority', 'status'], 'integer'],
            [['name', 'icon', 'link'], 'string', 'max' => 100],
            [['image'], 'file', 'extensions' => 'svg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'icon' => 'Иконка',
            'link' => 'Ссылка',
            'priority' => 'Приоритет',
            'status' => 'Статус',
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->image->saveAs('uploads/social/' . $this->image->baseName . '.' . $this->image->extension);
            return true;
        } else {
            return false;
        }
    }

}
