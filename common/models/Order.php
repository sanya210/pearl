<?php

namespace common\models;

use common\models\shop\Product;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\web\BadRequestHttpException;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property int $created_at
 * @property int $user_id
 * @property string $user_name
 * @property string $user_family_name
 * @property string $user_phone
 * @property string $language
 * @property int $currency
 * @property string $note
 * @property string $delivery_index
 * @property string $delivery_address
 * @property string $status_json
 * @property string $cost
 *
 * @property OrderItem[] $orderItems
 */
class Order extends ActiveRecord
{
    const STATUSES = [
        0 => 'No status',
        1 => 'Waiting for payment',
        2 => 'Paid',
        3 => 'Canceled',
        4 => 'Delivery',
        5 => 'Received'
    ];

    const DELIVERY_TYPES = [
        1 => [
            'name' => 'DHL',
            'url' => 'https://www.dhl.com/',
        ],
        2 => [
            'name' => 'Новая почта',
            'url' => 'https://novaposhta.ua/ru/tracking',
        ],
        3 => [
            'name' => 'Ukrposhta',
            'url' => 'https://track.ukrposhta.ua/tracking_UA.html',
        ],
        4 => [
            'name' => 'EMS',
            'url' => 'https://www.ems.post/en/global-network/tracking',
        ],
        5 => [
            'name' => 'Fedex',
            'url' => 'https://www.fedex.com/apps/fedextrack/?action=track',
        ],
    ];

    const SCENARIO_API = 'api';
    const SCENARIO_DEFAULT = 'default';

    private $status_active;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_API] = ['status', 'delivery_type', 'delivery_number'];
        return $scenarios;
    }

    public function afterFind()
    {
        $this->checkProducts();
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->user_name = strip_tags($this->user_name);
            $this->user_family_name = strip_tags($this->user_family_name);
            $this->user_phone = strip_tags($this->user_phone);
            $this->note = strip_tags($this->note);
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);

        if(array_key_exists('status', $changedAttributes)) {
            $log = new OrderLog();
            $log->status = $this->status;
            $log->link('order', $this);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['language', 'user_id', 'status'], 'required'],
            [['created_at', 'user_id', 'status', 'address_id', 'delivery_type'], 'integer'],
            [['note'], 'string'],
            [['cost'], 'number'],
            [['user_name', 'user_family_name', 'delivery_number'], 'string', 'max' => 255],
            [['user_phone'], 'string', 'max' => 100],
            [['language', 'currency'], 'string', 'max' => 16],
            ['status', 'filter', 'filter' => 'intval'],
            ['status', 'in', 'range' => array_keys(self::STATUSES)],
            ['delivery_type', 'in', 'range' => array_keys(self::DELIVERY_TYPES)],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер заказа',
            'created_at' => 'Дата создания',
            'user_id' => 'Пользователь',
            'user_name' => 'Имя',
            'user_family_name' => 'Фамилия',
            'user_phone' => 'Телефон',
            'language' => 'Язык',
            'currency' => 'Валюта',
            'note' => 'Примечание',
            'address_id' => 'Адрес',
            'status' => 'Статус',
            'cost' => 'Оплачено',
            'delivery_number' => 'Трекинг номер',
            'delivery_type' => 'Тип доставки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::className(), ['order_id' => 'id']);
    }

    public function getAddress()
    {
        return $this->hasOne(Address::className(), ['id' => 'address_id']);
    }

    public function getLogs()
    {
        return $this->hasMany(OrderLog::className(), ['order_id' => 'id'])->orderBy(['date' => SORT_ASC]);
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getDelivery()
    {
        if ($this->delivery_type) {
            return self::DELIVERY_TYPES[$this->delivery_type];
        }
        return null;
    }

    public function getTotal($transfer=true)
    {
        $items = $this->orderItems;
        $total = 0;
        foreach ($items as $item) {
            $total += $item->getTotal($transfer);
        }
        return $total;
    }

    public function getProducts()
    {
        $items = $this->orderItems;
        $products = [];

        foreach ($items as $item) {
            $products[] = [
                'article' => $item->product->article,
                'name' => $item->product->translation['name'],
                'photo' => '/uploads/product/preview/'.$item->product->mainPhoto,
                'slug' => '/'.$item->product->category->slug.'/'.$item->product->slug,
                'sizes' => json_decode($item->sizes, true),
                'price' => $item->total,
                'sizes_translate' => $item->sizesTranslate,
                'size_label' => $item->sizeLabel
            ];
        }

        return $products;
    }

    private function checkProducts()
    {
        $items = $this->orderItems;
        foreach ($items as $item) {
            if($item->product->status != Product::STATUS_ACTIVE) {
                $this->status_active = false;
                break;
            } else {
                $this->status_active = true;
            }
        }
    }

    public function getStatusActive()
    {
        return $this->status_active;
    }

    public static function getUserOrders()
    {
        return self::find()->where([
            'user_id' => Yii::$app->user->identity->id
        ])->andWhere([
            '!=', 'order.status', 3
        ])->joinWith(['orderItems' => function($query) {
            $query->joinWith(['product' => function($query) {
                $query->with(['activeLanguage', 'category']);
            }]);
        }])
        ->orderBy(['created_at' => SORT_DESC]);
    }

    public function toPay($amount, $currency)
    {
        if (!$this->status_active) {
            throw new BadRequestHttpException();
        }
        Yii::$app->currency->set($currency);

        if($this->total <= $amount) {
            foreach ($this->orderItems as $item) {
                $item->setPrice();
            }
            $this->currency = $currency;
            $this->cost = $amount;
            $this->status = 2;
            if($this->save()){
                $this->notify();
                return true;
            }
        }
        return false;
    }

    public function notify()
    {
        $message = '<p><a href="http://admin.pearl.wedding/order/update?id='.$this->id.'">Заказ: #'.$this->id.'</a></p>
                    <p>Статус: '.self::STATUSES[$this->status].'</p>
                    <p>Оплачено: '.$this->cost.' '.$this->currency.'</p>
                    <p>Телефон: '.$this->user_phone.'</p>';

        return Yii::$app->mailer->compose()
            ->setTo(Yii::$app->params['managerEmail'])
            ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
            ->setSubject('Заказ с '.Yii::$app->name)
            ->setHtmlBody($message)
            ->send();
    }

    public static function getCountPaid()
    {
        return self::find()->where(['status' => 2])->count();
    }

    public function cancel()
    {
        if($this->status == 1) {
            $this->status = 3;
            return $this->save();
        }
        return false;
    }

    public static function getDeliveryTypes()
    {
        $types = [];
        foreach (self::DELIVERY_TYPES as $id => $type) {
            $types[$id] = $type['name'];
        }
        return $types;
    }

    public function sendTracking()
    {
        Yii::$app->language = $this->language;
        $session = Yii::$app->session;
        if (!$this->delivery_type || !$this->delivery_number) {
            $session->addFlash('danger', 'Трекинг номер не отправлен! Поля "Тип доставки" и "Трекинг номер" должны быть указаны!');
        } else {
            $mail_res = Yii::$app->mailer
                ->compose(
                    ['html' => 'tracking-html', 'text' => 'tracking-text'],
                    ['order' => $this]
                )
                ->setTo($this->user->email)
                ->setFrom([Yii::$app->params['managerEmail'] => 'Pearl Fashion Group'])
                ->setSubject( Yii::t('order', 'Tracking number for order').' #'.$this->id)
                ->send();

            if ($mail_res) {
                $session->addFlash('success', 'Трекинг номер успешно отправлен!');
            } else {
                $session->addFlash('danger', 'Не удалось отправить трекинг номер.');
            }
        }
    }

    public function sendSuccess()
    {
        Yii::$app->mailer
            ->compose(
                ['html' => 'successOrder-html', 'text' => 'successOrder-text'],
                ['order' => $this]
            )
            ->setTo($this->user->email)
            ->setFrom([Yii::$app->params['managerEmail'] => 'Pearl Fashion Group'])
            ->setSubject( Yii::t('order2', 'Successful order!').' #'.$this->id)
            ->send();
    }
}
