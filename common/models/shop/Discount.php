<?php

namespace common\models\shop;

use backend\models\kasta\Kasta;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "discount".
 *
 * @property int $id
 * @property int $product_id
 * @property string $price
 * @property int $created_at
 * @property int $date_start
 * @property int $date_finish
 * @property int $status
 *
 * @property Product $product
 */
class Discount extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETED = 2;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ]
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'discount';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'created_at', 'status'], 'integer'],
            [['date_start', 'date_finish'], 'date', 'format' => 'yyyy-mm-dd'],
            [['price'], 'number', 'min' => 0],
            [['status', 'price', 'date_start', 'date_finish'], 'required'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'price' => 'Акционная цена',
            'created_at' => 'Дата создания',
            'date_start' => 'Дата начала акции',
            'date_finish' => 'Дата окончания акции',
            'status' => 'Статус',
        ];
    }

    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);
        if ($this->product->status_kasta) {
            $kasta = new Kasta();
            $kasta->productUpdate($this->product->id);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
