<?php

namespace common\models\shop;

use backend\models\kasta\Kasta;
use Yii;
use yii\helpers\Inflector;
use Intervention\Image\ImageManagerStatic as Image;
use Elasticsearch\Client;
use common\models\Wishlist;
use backend\models\CrmModeliType;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property int $category_id
 * @property string $slug
 * @property string $article
 * @property string $price
 * @property int $priority
 * @property int $status
 * @property int $provider_id
 * @property string $provider_name
 * @property string $provider_article
 * @property string $provider_price
 * @property array $translation
 * @property ShopCategory $category
 *
 * @property ProductLocalization[] $productLocalizations
 */
class Product extends \yii\db\ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_AVAILABLE = 2;
    const FIRST_ARTICLE = 'PF-0000';

    public function behaviors()
    {
        return [
            [
                'class' => \backend\components\behaviors\LanguageBehavior::className(),
                'model' => '\common\models\shop\ProductLocalization',
                'field' => 'language',
                'relation' => 'localization'
            ],
            [
                'class' => \voskobovich\linker\LinkerBehavior::className(),
                'relations' => [
                    'pseudoCategories_ids' => 'pseudoCategories',
                    'attributes_ids' => 'attribute0',
                    'productSizes_ids' => 'sizes',
                ],
            ],
        ];
    }

    public $other_color_products;
    public $similar_products;
    public $product_sizes;

    public $files;
    public $files_rozetka;
    public $files_kasta;

    public function afterFind()
    {
        $this->other_color_products = unserialize($this->other_colors);
        $this->similar_products = unserialize($this->similar);
    }

    public function beforeSave($insert) 
    {
        if(parent::beforeSave($insert)) {
            $this->other_colors = serialize($this->other_color_products);
            $this->similar = serialize($this->similar_products);
            return true;
        }
        return false;
    }

    public function afterSave($insert, $changedAttributes){
        parent::afterSave($insert, $changedAttributes);
        if ($this->status_kasta && isset($changedAttributes['price'])) {
            $kasta = new Kasta();
            $kasta->productUpdate($this->id);
        }
    }

    public function reindex(Client $client)
    {
        $name = '';
        $description = '';
        foreach ($this->localization as $item) {
            $name .= ' '.$item->name;
            $description .= ' '.strip_tags($item->description);
        }

        $client->index([
            'index' => 'shop',
            'type' => 'products',
            'id' => $this->id,
            'body' => [
                'name' => $name,
                'description' => $description,
                'article' => $this->article,
            ]
        ]);
    }

    private static function generate_article()
    {
        $last_product = self::find()->orderBy(['id' => SORT_DESC])->one();
        $last_article = $last_product ? $last_product->article : self::FIRST_ARTICLE;
        $num = (int) str_replace('PF-', '', $last_article) + 1;
        
        return 'PF-'.sprintf("%'.04d", $num);
    }

    public function upload()
    {
        $count = count($this->attachment);
        foreach ($this->files as $key => $file) {
            $fileName = $this->slug . ($count + $key) . '_' . time() . '.' . $file->extension;
            $img = Image::make($file->tempName)->widen(470, function($constraint) {
                $constraint->upsize();
            });
            $img->save(Yii::getAlias('@webroot').'/uploads/product/preview/' . $fileName);
            if( $file->saveAs(Yii::getAlias('@webroot').'/uploads/product/' . $fileName) ) {
                $fileManager = new ProductPhoto();
                $fileManager->name = $fileName;
                $fileManager->product_id = $this->id;
                $fileManager->sort = $count + $key;
                $fileManager->save();
                if ($this->etsy_id) {
                    $etsy = new Etsy();
                    $path = Yii::getAlias('@backend/web/uploads/product/'.$fileName);
                    $rank = $fileManager->sort + 1;
                    $etsy->addPhoto($this->etsy_id, $fileManager->id, $path, $rank);
                }
            }
        }
        return true;
    }

    public function uploadRozetka()
    {
        $count = count($this->photoRozetka);

        foreach ($this->files_rozetka as $key => $file) {
            $fileName = $this->slug . ($count + $key) . '_' . time() . '.' . $file->extension;

            if( $file->saveAs(Yii::getAlias('@webroot').'/uploads/product/rozetka/' . $fileName) ) {
                $fileManager = new ProductRozetka();
                $fileManager->name = $fileName;
                $fileManager->product_id = $this->id;
                $fileManager->sort = $count + $key;
                $fileManager->save();
            }
        }
        return true;
    }

    public function uploadKasta()
    {
        $count = count($this->photoKasta);

        foreach ($this->files_kasta as $key => $file) {
            $fileName = $this->slug . ($count + $key) . '_' . time() . '.' . $file->extension;

            if( $file->saveAs(Yii::getAlias('@webroot').'/uploads/product/kasta/' . $fileName) ) {
                $fileManager = new ProductKasta();
                $fileManager->name = $fileName;
                $fileManager->product_id = $this->id;
                $fileManager->sort = $count + $key;
                $fileManager->save();
            }
        }
        return true;
    }

    public function save_with_slug($slug)
    {
        $this->slug = Inflector::slug($slug);
        return $this->save();
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'priority', 'number_purchased', 'status', 'provider_id', 'color_id', 'status_rozetka', 'status_kasta', 'season', 'google_merchants', 'rozetka_id'], 'integer'],
            [['number_purchased', 'status', 'provider_article', 'color_id', 'article'], 'required'],
            [['price', 'provider_price', 'rozetka_price'], 'number', 'min' => 0],
            [['provider_article'], 'string', 'max' => 100],
            [['provider_name', 'video', 'name_rozetka', 'composition_kasta', 'gtin'], 'string', 'max' => 255],
            [['slug', 'article'], 'unique'],
            [['article'], 'filter', 'filter' => 'trim'],
            ['files', 'image', 
                'extensions' => 'jpg',
                'maxFiles' => 10,
                'minWidth' => 1280, 'maxWidth' => 1280,
                'minHeight' => 1920, 'maxHeight' => 1920,
            ],
            ['files_rozetka', 'image', 
                'extensions' => 'jpg',
                'maxFiles' => 10,
                'minWidth' => 850, 'maxWidth' => 850,
                'minHeight' => 850, 'maxHeight' => 850,
            ],
            ['files_kasta', 'image',
                'extensions' => 'jpg',
                'maxFiles' => 10,
            ],
            ['pseudoCategories_ids', 'each', 'rule' => ['integer']],
            ['attributes_ids', 'each', 'rule' => ['integer']],
            ['other_color_products', 'each', 'rule' => ['integer']],
            ['similar_products', 'each', 'rule' => ['integer']],
            ['productSizes_ids', 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'slug' => 'Slug',
            'article' => 'Артикул',
            'price' => 'Цена',
            'priority' => 'Приоритет',
            'number_purchased' => 'Количество купленных',
            'status' => 'Статус',
            'provider_id' => 'Поставщик',
            'provider_name' => 'Название у поставщика',
            'provider_article' => 'Артикул у поставщика',
            'provider_price' => 'Цена у поставщика',
            'color_id' => 'Цвет',
            'other_color_products' => 'Аналогичные товары другого цвета',
            'similar_products' => 'Аналогичные товары',
            'video' => 'Видео',
            'pseudoCategories_ids' => 'Псевдо-категории',
            'attributes_ids' => 'Атрибуты',
            'files' => 'Изображения',
            'files_rozetka' => 'Изображения для ROZETKA',
            'files_kasta' => 'Изображения для KASTA',
            'name_rozetka' => 'Название в ROZETKA',
            'status_rozetka' => 'Публиковать в ROZETKA',
            'status_kasta' => 'Публиковать в Kasta',
            'season' => 'Сезонность',
            'composition_kasta' => 'Состав Kasta (Пример: 80% хлопок, 20% эластан)',
            'productSizes_ids' => 'Доступные размеры',
            'gtin' => 'Gtin',
            'rozetka_id' => 'Категория в Rozetka',
            'rozetka_price' => 'Цена на Rozetka(грн)',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalization()
    {
        return $this->hasMany(ProductLocalization::className(), ['product_id' => 'id'])->indexBy('language');
    }

    public function getActiveLanguage()
    {
        return $this->hasOne(ProductLocalization::className(), ['product_id' => 'id'])->where(['language' => Yii::$app->language]);
    }

    public function getMainLanguage()
    {
        return $this->hasOne(ProductLocalization::className(), ['product_id' => 'id'])->where(['language' => Yii::$app->params['main_language']]);
    }

    public function getCategory()
    {
        return $this->hasOne(ShopCategory::className(), ['id' => 'category_id']);
    }

    public function getRozetkaCategory()
    {
        return $this->hasOne(RozetkaCategory::className(), ['id' => 'rozetka_id']);
    }

    public function getProvider()
    {
        return $this->hasOne(Provider::className(), ['id' => 'provider_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPseudoCategories()
    {
        return $this->hasMany(PseudoCategory::className(), ['id' => 'pseudo_category_id'])
            ->viaTable('product_pseudo_category', ['product_id' => 'id']);
    }

    public function getSizes()
    {
        return $this->hasMany(Size::className(), ['id' => 'size_id'])
            ->viaTable('product_size', ['product_id' => 'id'])->orderBy(['europe' => SORT_ASC]);
    }

    public function getSizeByEurope()
    {
        return $this->hasMany(Size::className(), ['id' => 'size_id'])
            ->viaTable('product_size', ['product_id' => 'id'])->indexBy('europe');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttribute0()
    {
        return $this->hasMany(AttributeValue::className(), ['id' => 'value_id'])
            ->viaTable('product_attribute', ['product_id' => 'id']);
    }

    public function getAttachment()
    {
        return $this->hasMany(ProductPhoto::className(), ['product_id' => 'id'])->orderBy(['sort' => SORT_ASC]);
    }

    public function getImages()
    {
        $images = [];
        foreach ($this->attachment as $attachment) {
            $images[] = 'https://pearl.wedding/uploads/product/preview/'.$attachment->name;
        }
        return $images;
    }


    public function getMainPhotoLink()
    {
        return 'https://pearl.wedding/uploads/product/preview/'.$this->mainPhoto;
    }

    public function getPhotoRozetka()
    {
        return $this->hasMany(ProductRozetka::className(), ['product_id' => 'id'])->orderBy(['sort' => SORT_ASC]);
    }

    public function getPhotoKasta()
    {
        return $this->hasMany(ProductKasta::className(), ['product_id' => 'id'])->orderBy(['sort' => SORT_ASC]);
    }

    public function getWishlist()
    {
        return $this->hasOne(Wishlist::className(), ['product_id' => 'id'])->andWhere(['user_id' => Yii::$app->user->id]);
    }

    public function getColor()
    {
        return $this->hasOne(Color::className(), ['id' => 'color_id']);
    }

    public function getPhoto()
    {
        return $this->hasOne(ProductPhoto::className(), ['product_id' => 'id'])->orderBy(['sort' => SORT_ASC])->limit(1);
    }

    public function getCrmModelType()
    {
        return $this->hasOne(CrmModeliType::className(), ['artikul' => 'article']);
    }

    public function getMainPhoto()
    {
        $photo = $this->photo;

        if($photo) {
            return $photo->name;
        } else {
            return 'default_photo.jpg';
        }
    }

    static function findProduct($category, $product)
    {
        return self::find()
            ->where([
                'product.slug' => $product, 
                'product.status' => [self::STATUS_ACTIVE, self::STATUS_NOT_AVAILABLE],
                'shop_category.slug' => $category, 
                'shop_category.status' => ShopCategory::STATUS_ACTIVE 
            ])
            ->leftJoin('shop_category', 'shop_category.id = product.category_id')
            ->with([
                'attachment' => function($query) {
                    $query->limit(5);
                }
            ])
            ->one();
    }

    static function wishlistProducts()
    {
        return self::find()
            ->rightJoin('wishlist', 'wishlist.product_id=product.id')
            ->where([
                'product.status' => self::STATUS_ACTIVE,
                'wishlist.user_id' => Yii::$app->user->id
            ])
            ->with(['category', 'activeLanguage']);
    }

    public function getTranslation()
    {
        $model = $this->activeLanguage;

        $name = $model->name ?? $this->mainLanguage->name;

        return [
            'name' => $name,
            'description' => $model->description ?? '',
            'meta_title' => $model->meta_title ?? $name,
            'meta_description' => $model->meta_description ?? '',
            'meta_keywords' => $model->meta_keywords ?? ''
        ];
    }

    public function getDiscounts()
    {
        return $this->hasMany(Discount::className(), ['product_id' => 'id'])
            ->where(['!=', 'status', Discount::STATUS_DELETED])
            ->orderBy(['created_at' => SORT_DESC]);
    }

    public function getActiveDiscount()
    {
        return $this->hasOne(Discount::className(), ['product_id' => 'id'])
            ->where(['product_id' => $this->id])
            ->andWhere(['status' => Discount::STATUS_ACTIVE])
            ->andWhere(['<=', 'date_start', date('Y-m-d')])
            ->andWhere(['>=', 'date_finish', date('Y-m-d')])
            ->orderBy(['created_at' => SORT_DESC]);
    }

    public function getPriceValue($rate=false)
    {
        $discount = $this->activeDiscount;
        if($discount) {
            if(!$rate) {
                $discount_price = round(Yii::$app->currency->transfer($discount->price));
            } else {
                $discount_price = round($rate * $discount->price);
            }
        } else {
            $discount_price = null;
        }

        if(!$rate) {
            $price = round(Yii::$app->currency->transfer($this->price));
        } else {
            $price = round($rate * $this->price);
        }
        $active_price = $discount_price ?? $price;

        return [
            'isDiscount' => $discount != false,
            'price' => $active_price,
            'active_price' => number_format((float)$active_price, 2, '. ', ' '),
            'old_price' => number_format((float)$price, 2, '. ', ' '),
            'date_finish' => $discount ? ceil((strtotime($discount->date_finish) - time()) / 86400) + 1 : '',
            'percent' => $discount ? floor(100 * ($price - $discount_price) / $price) : '',
            'start' => $discount ? new \DateTime($discount->date_start) : '',
            'finish' => $discount ? new \DateTime($discount->date_finish . ' 23:59:59') : '',
            'data' => [
                'old_price' => $price
            ]
        ];
    }

    public function getColors()
    {
        $this_color = $this->color;
        $category = $this->category->slug;
        
        $colors = [
            'current_color' => [
                'hex' => $this_color->hex,
                'name' => $this->color->activeLanguage->name,
                'link' => null
            ],
            'other_colors' => []
        ];

        $other_products = self::find()
            ->where([
                'product.id' => $this->other_color_products,
                'product.status' => self::STATUS_ACTIVE
            ])
            ->leftJoin('color', 'color.id = product.color_id')
            ->orderBy(['color.sort' => SORT_DESC])
            ->all();

        foreach($other_products as $product) {
            array_push($colors['other_colors'], [
                'hex' => $product->color->hex,
                'name' => $product->color->activeLanguage->name,
                'link' => '/'.$category.'/'.$product->slug
            ]);
        }
        
        return $colors;
    }

    public function getOtherProducts()
    {
        $products = self::find()
            ->where(['!=', 'id', $this->id])
            ->andWhere(['category_id' => $this->category_id])
            ->with('mainLanguage')
            ->all();

        return $products;
    }

    public function getSimilarProducts()
    {
        return self::find()
            ->where([
                'id' => $this->similar_products,
                'status' => self::STATUS_ACTIVE
            ])
            ->with('activeLanguage')
            ->all();
    }

    public function getActivePseudoCategories()
    {
        return $this->getPseudoCategories()
            ->andWhere([
                'language' => Yii::$app->language,
                'status' => PseudoCategory::STATUS_ACTIVE
            ])
            ->all();
    }

    public static function getTop5()
    {
        return self::find()
            ->where([
                'status' => self::STATUS_ACTIVE
            ])
            ->with('activeLanguage')
            ->orderBy(['number_purchased' => SORT_DESC])
            ->limit(5)
            ->all();
    }

    public static function parentArticle($sku)
    {
        $position = strrpos($sku, '.');
        return mb_strimwidth($sku, 0, $position);
    }

    public function getLink()
    {
        $lang = Yii::$app->language == Yii::$app->params['main_language'] ? '' : Yii::$app->language.'/';
        return 'https://pearl.wedding/'.$lang.$this->category->slug.'/'.$this->slug;
    }

    public function getInternationalSizes()
    {
        return $this->getSizes()->select('international')->column();
    }
}
