<?php

namespace common\models\shop;

use Yii;

/**
 * This is the model class for table "size".
 *
 * @property int $id
 * @property int $europe
 * @property string $international
 * @property string $chest
 * @property string $waist
 * @property string $hips
 * @property string $growth
 * @property int $status
 */
class Size extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'size';
    }

    public function behaviors()
    {
        return [
            [
                'class' => \voskobovich\linker\LinkerBehavior::className(),
                'relations' => [
                    'categories_ids' => 'categories',
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['europe', 'international', 'status'], 'required'],
            [['europe', 'status', 'number_usa', 'ru', 'uk', 'de'], 'integer'],
            [['international', 'chest', 'waist', 'hips', 'dimension', 'waist_inch', 'hips_inch', 'chest_inch'], 'string', 'max' => 10],
            ['categories_ids', 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'international' => 'Letter (USA)',
            'number_usa' => 'Number (USA)',
            'europe' => 'EU',
            'ru' => 'RU',
            'uk' => 'UK',
            'de' => 'DE',
            'chest' => 'Грудь (см)',
            'waist' => 'Талия (см)',
            'hips' => 'Бедра (см)',
            'chest_inch' => 'Грудь (дюймы)',
            'waist_inch' => 'Талия (дюймы)',
            'hips_inch' => 'Бедра (дюймы)',
            'status' => 'Статус',
            'categories_ids' => 'Категории',
            'dimension' => 'Размерность (для розетки)',
        ];
    }

    public function getCategories()
    {
        return $this->hasMany(ShopCategory::className(), ['id' => 'category_id'])
            ->viaTable('size_category', ['size_id' => 'id']);
    }

    static function activeSize($category_id = false)
    {
        if ($category_id) {
            return self::find()
                ->where([
                    'size.status' => self::STATUS_ACTIVE,
                    'size_category.category_id' => $category_id 
                ])
                ->joinWith('categories')
                ->orderBy(['europe' => SORT_ASC])
                ->all();
        }

        return self::find()->where(['status' => self::STATUS_ACTIVE])->orderBy(['europe' => SORT_ASC])->all();
    }
}
