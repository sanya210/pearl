<?php

namespace common\models\shop;

use Yii;

/**
 * This is the model class for table "rozetka_category".
 *
 * @property int $id
 * @property string $name
 * @property int $status
 */
class RozetkaCategory extends \yii\db\ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rozetka_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'status' => 'Статус',
        ];
    }

    static public function getActiveCategory()
    {
        return self::find()->where([
            'status' => self::STATUS_ACTIVE
        ])->all();
    }
}
