<?php

namespace common\models\shop;

use Yii;

/**
 * This is the model class for table "attribute_value".
 *
 * @property int $id
 * @property int $attribute_id
 * @property int $priority
 * @property int $status
 *
 * @property Attribute $attribute0
 * @property AttributeValueLocalization[] $attributeValueLocalizations
 * @property ProductAttribute[] $productAttributes
 */
class AttributeValue extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public function behaviors()
    {
        return [
            [
                'class' => \backend\components\behaviors\LanguageBehavior::className(),
                'model' => '\common\models\shop\AttributeValueLocalization',
                'field' => 'language',
                'relation' => 'localization'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attribute_value';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attribute_id', 'priority', 'status'], 'integer'],
            [['status'], 'required'],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => Attribute::className(), 'targetAttribute' => ['attribute_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'attribute_id' => 'Атрибут',
            'priority' => 'Приоритет',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttribute0()
    {
        return $this->hasOne(Attribute::className(), ['id' => 'attribute_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalization()
    {
        return $this->hasMany(AttributeValueLocalization::className(), ['value_id' => 'id'])->indexBy('language');
    }

    public function getMainLanguage()
    {
        return $this->hasOne(AttributeValueLocalization::className(), ['value_id' => 'id'])->andWhere(['language' => Yii::$app->params['main_language']]);
    }

    public function getActiveLanguage()
    {
        return $this->hasOne(AttributeValueLocalization::className(), ['value_id' => 'id'])->where(['language' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductAttributes()
    {
        return $this->hasMany(ProductAttribute::className(), ['value_id' => 'id']);
    }

    static function findByCategory($category_id)
    {
        return self::find()
            ->joinWith(['attribute0' => function($query) use ($category_id) {
                $query->where([
                    'category_id' => $category_id
                ]);
            }])
            ->all();
    }
}
