<?php

namespace common\models\shop;

use Yii;

/**
 * This is the model class for table "section".
 *
 * @property int $id
 * @property int|null $priority
 * @property int $status
 * @property string|null $categories
 *
 * @property SectionLocalization[] $sectionLocalizations
 */
class Section extends \yii\db\ActiveRecord
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $categories_arr;

    public function behaviors()
    {
        return [
            [
                'class' => \backend\components\behaviors\LanguageBehavior::className(),
                'model' => '\common\models\shop\SectionLocalization',
                'field' => 'language',
                'relation' => 'localization'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'section';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['priority', 'status'], 'integer'],
            [['status'], 'required'],
            [['categories'], 'string'],
            ['categories_arr', 'each', 'rule' => ['integer']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'priority' => 'Приоритет',
            'status' => 'Статус',
            'categories' => 'Категории',
            'categories_arr' => 'Категории'
        ];
    }

    public function afterFind()
    {
        $this->categories_arr = unserialize($this->categories);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->categories = serialize($this->categories_arr);

            return true;
        }
        return false;
    }

    /**
     * Gets query for [[SectionLocalizations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLocalization()
    {
        return $this->hasMany(SectionLocalization::className(), ['section_id' => 'id'])->indexBy('language');
    }

    public function getActiveLanguage()
    {
        return $this->hasOne(SectionLocalization::className(), ['section_id' => 'id'])->where(['language' => Yii::$app->language]);
    }

    public function getMainLanguage()
    {
        return $this->hasOne(SectionLocalization::className(), ['section_id' => 'id'])->where(['language' => Yii::$app->params['main_language']]);
    }

    public function getTranslation()
    {
        $model = $this->activeLanguage;

        $name = $model->name ?? $this->mainLanguage->name;

        return [
            'name' => $name,
        ];
    }

    public function getCategoriesModels()
    {
        return ShopCategory::find()
            ->where([
                'id' => $this->categories_arr,
                'status' => ShopCategory::STATUS_ACTIVE,
                'show_in_menu' => ShopCategory::STATUS_ACTIVE
            ])
            ->with('mainLanguage')
            ->orderBy(['priority' => SORT_DESC])
            ->all();
    }

    static public function getActive()
    {
        return self::find()
            ->where([
                'status' => self::STATUS_ACTIVE
            ])
            ->with('activeLanguage')
            ->orderBy(['priority' => SORT_DESC])
            ->all();
    }
}
