<?php

namespace common\models\shop;

use Yii;

/**
 * This is the model class for table "product_localization".
 *
 * @property int $id
 * @property int $product_id
 * @property string $language
 * @property string $name
 * @property string $description
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 *
 * @property Product $product
 */
class ProductLocalization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_localization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id'], 'integer'],
            [['language'], 'required'],
            ['name', 'required', 'when' => function($model) {
                return $model->language == Yii::$app->params['main_language'];
            }, 
            'whenClient' => 'function (attribute, value) {
                return attribute.name == "['.Yii::$app->params['main_language'].']name";
            }'],
            [['description', 'meta_description'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique', 'targetAttribute' => ['name', 'language']],
            [['meta_title'], 'string', 'max' => 150],
            [['meta_keywords'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'language' => 'Language',
            'name' => 'Название',
            'description' => 'Описание',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
