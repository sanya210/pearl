<?php

namespace common\models\shop;

use Yii;
use backend\models\etsy\Etsy;

/**
 * This is the model class for table "product_photo".
 *
 * @property int $id
 * @property string $name
 * @property int $product_id
 * @property int $main
 *
 * @property Product $product
 */
class ProductPhoto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_photo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['product_id', 'sort', 'etsy_id'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'product_id' => 'Product ID',
            'sort' => 'Sort',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    static function sort($data)
    {
        foreach ($data as $id => $value) {
            $model = self::findOne($id);
            $model->sort = $value;
            $model->save();

            if ($model->listingId && $model->etsy_id) {
                $rank = $value + 1;
                $etsy = new Etsy();
                $etsy->updateRankPhoto($model->listingId, $model->etsy_id, $rank);
            }
        }
    }

    public function getListingId()
    {
        return $this->product->etsy_id;
    }

    static function remove($id)
    {
        $model = self::findOne($id);
        $position = $model->sort;
        $product_id = $model->product_id;
        $listing_id = $model->listingId;
        $listing_image_id = $model->etsy_id;
        if($model->delete()) {
            if ($listing_id && $listing_image_id) {
                $etsy = new Etsy();
                $etsy->removePhoto($listing_id, $listing_image_id);
            }
            self::updateAllCounters(['sort' => -1], ['and', ['product_id' => $product_id], ['>', 'sort', $position]]);
            return true;
        }
        return false;
    }
}
