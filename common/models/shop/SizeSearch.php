<?php

namespace common\models\shop;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\shop\Size;

/**
 * SizeSearch represents the model behind the search form of `common\models\shop\Size`.
 */
class SizeSearch extends Size
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'europe', 'status'], 'integer'],
            [['international', 'chest', 'waist', 'hips'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Size::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'europe' => $this->europe,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'international', $this->international])
            ->andFilterWhere(['like', 'chest', $this->chest])
            ->andFilterWhere(['like', 'waist', $this->waist])
            ->andFilterWhere(['like', 'hips', $this->hips]);

        return $dataProvider;
    }
}
