<?php

namespace common\models\shop;

use Yii;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "pseudo_category".
 *
 * @property int $id
 * @property string $slug
 * @property string $language
 * @property string $name
 * @property int $category_id
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property int $priority
 * @property int $status
 *
 * @property ProductPseudoCategory[] $productPseudoCategories
 * @property ShopCategory $category
 */
class PseudoCategory extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pseudo_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['language', 'status', 'category_id', 'name'], 'required'],
            [['category_id', 'priority', 'status'], 'integer'],
            [['meta_description'], 'string'],
            ['name', 'unique'],
            [['language'], 'string', 'max' => 16],
            [['meta_title'], 'string', 'max' => 150],
            [['meta_keywords'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ShopCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slug' => 'Slug',
            'language' => 'Язык',
            'name' => 'Название',
            'category_id' => 'Основная категория',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'priority' => 'Приоритет',
            'status' => 'Статус',
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])
            ->viaTable('product_pseudo_category', ['pseudo_category_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ShopCategory::className(), ['id' => 'category_id']);
    }

    public function getAttribute0()
    {
        return $this->hasMany(Attribute::className(), ['category_id' => 'category_id'])
            ->where(['attribute.status' => Attribute::STATUS_ACTIVE])
            ->with('activeLanguage')
            ->with('attributeValues')
            ->orderBy(['priority' => SORT_DESC]);
    }

    static function findByCategory($category_id)
    {
        return self::find()
            ->where(['category_id' => $category_id])
            ->all();
    }

    public function getTranslation()
    {
        return [
            'name' => $this->name,
            'meta_title' => $this->meta_title ?: $this->name,
            'meta_description' => $this->meta_description ?: $this->name,
            'meta_keywords' => $this->meta_keywords ?: ''
        ];
    }

    static function findPseudoCategory($slug)
    {
        return PseudoCategory::find()
            ->where([
                'slug' => $slug,
                'language' => Yii::$app->language,
                'status' => PseudoCategory::STATUS_ACTIVE
            ])
            ->one();
    }
}
