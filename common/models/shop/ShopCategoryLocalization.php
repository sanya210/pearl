<?php

namespace common\models\shop;

use Yii;

/**
 * This is the model class for table "shop_category_localization".
 *
 * @property int $id
 * @property int $category_id
 * @property string $language
 * @property string $name
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 *
 * @property ShopCategory $category
 */
class ShopCategoryLocalization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shop_category_localization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id'], 'integer'],
            [['language'], 'required'],
            ['name', 'required', 'when' => function($model) {
                return $model->language == Yii::$app->params['main_language'];
            }, 
            'whenClient' => 'function (attribute, value) {
                return attribute.name == "['.Yii::$app->params['main_language'].']name";
            }'],
            [['meta_description'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['name'], 'string', 'max' => 100],
            [['meta_title'], 'string', 'max' => 150],
            [['meta_keywords'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ShopCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'language' => 'Язык',
            'name' => 'Название',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ShopCategory::className(), ['id' => 'category_id']);
    }
}
