<?php

namespace common\models\shop;

use Yii;

/**
 * This is the model class for table "product_rozetka".
 *
 * @property int $id
 * @property string $name
 * @property int $product_id
 * @property int $sort
 *
 * @property Product $product
 */
class ProductRozetka extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_rozetka';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['product_id', 'sort'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'product_id' => 'Product ID',
            'sort' => 'Sort',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    static function sort($data)
    {
        foreach ($data as $id => $value) {
            $model = self::findOne($id);
            $model->sort = $value;
            $model->save();
        }
    }

    static function remove($id)
    {
        $model = self::findOne($id);
        $position = $model->sort;
        $product_id = $model->product_id;
        if( $model->delete() ){
            self::updateAllCounters(['sort' => -1], ['and', ['product_id' => $product_id], ['>', 'sort', $position]]);
            return true;
        }
        return false;
    }
}
