<?php

namespace common\models\shop;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\shop\Product;

/**
 * ProductSearch represents the model behind the search form of `common\models\shop\Product`.
 */
class ProductSearch extends Product
{
    public function attributes()
    {
        return array_merge(parent::attributes(), ['mainLanguage.name']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'priority', 'status', 'provider_id'], 'integer'],
            [['slug', 'article', 'provider_name', 'provider_article', 'mainLanguage.name'], 'safe'],
            [['price', 'provider_price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find()->orderBy(['id' => SORT_DESC]);

        $query->joinWith(['mainLanguage']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'price' => $this->price,
            'priority' => $this->priority,
            'status' => $this->status,
            'provider_id' => $this->provider_id,
            'provider_price' => $this->provider_price,
        ]);

        $query->andFilterWhere(['LIKE', 'product_localization.name', $this->getAttribute('mainLanguage.name')]);

        $query->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'article', $this->article])
            ->andFilterWhere(['like', 'provider_name', $this->provider_name])
            ->andFilterWhere(['like', 'provider_article', $this->provider_article]);

        return $dataProvider;
    }
}
