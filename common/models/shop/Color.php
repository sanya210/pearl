<?php

namespace common\models\shop;

use Yii;

/**
 * This is the model class for table "color".
 *
 * @property int $id
 * @property string $hex
 * @property int $sort
 * @property int $status
 *
 * @property ColorLocalization[] $colorLocalizations
 * @property ColorLocalization[] $activeLanguage
 */
class Color extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public function behaviors()
    {
        return [
            [
                'class' => \backend\components\behaviors\LanguageBehavior::className(),
                'model' => '\common\models\shop\ColorLocalization',
                'field' => 'language',
                'relation' => 'localization'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'color';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hex', 'status'], 'required'],
            [['sort', 'status'], 'integer'],
            [['hex'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hex' => 'Цвет',
            'sort' => 'Приоритет',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalization()
    {
        return $this->hasMany(ColorLocalization::className(), ['color_id' => 'id'])->indexBy('language');
    }

    public function getMainLanguage()
    {
        return $this->hasOne(ColorLocalization::className(), ['color_id' => 'id'])->andWhere(['language' => Yii::$app->params['main_language']]);
    }

    public function getActiveLanguage()
    {
        return $this->hasOne(ColorLocalization::className(), ['color_id' => 'id'])->where(['language' => Yii::$app->language]);
    }

    public function getName()
    {
        return $this->activeLanguage->name;
    }
    
    static public function getActiveColor()
    {
        return self::find()
            ->where([
                'status' => self::STATUS_ACTIVE
            ])
            ->with('mainLanguage')
            ->orderBy(['sort' => SORT_DESC])
            ->all();
    }

    static public function getColors()
    {
        return self::find()
            ->where([
                'status' => self::STATUS_ACTIVE
            ])
            ->with('activeLanguage')
            ->orderBy(['sort' => SORT_DESC])
            ->all();
    }
}
