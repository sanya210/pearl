<?php

namespace common\models\shop;

use Yii;
use yii\helpers\Inflector;

/**
 * This is the model class for table "shop_category".
 *
 * @property int $id
 * @property string $slug
 * @property int $priority
 * @property int $status
 * @property int $show_in_menu
 * @property array $translation
 *
 * @property ShopCategoryLocalization[] $shopCategoryLocalizations
 */
class ShopCategory extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public function behaviors()
    {
        return [
            [
                'class' => \backend\components\behaviors\LanguageBehavior::className(),
                'model' => '\common\models\shop\ShopCategoryLocalization',
                'field' => 'language',
                'relation' => 'localization'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shop_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'show_in_menu'], 'required'],
            ['slug', 'unique'],
            [['priority', 'status', 'show_in_menu', 'status_rozetka', 'etsy_id', 'etsy_section_id', 'lamoda_id'], 'integer'],
            [['name_rozetka', 'etsy_sizes', 'etsy_properties'], 'string'],
            [['uktzed'], 'string', 'max' => 100],
            [['name_kasta', 'gender'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slug' => 'Slug',
            'priority' => 'Приоритет',
            'status' => 'Статус',
            'show_in_menu' => 'Отображение в меню',
            'name' => 'Название',
            'status_rozetka' => 'Публиковать в ROZETKA',
            'name_rozetka' => 'Название в ROZETKA',
            'status_etsy' => 'Публиковать в Etsy',
            'etsy_id' => 'ID в Etsy',
            'etsy_section_id' => 'Секция в Etsy',
            'etsy_sizes' => 'Размеры в Etsy',
            'etsy_properties' => 'Свойства в Etsy',
            'lamoda_id' => 'ID в Lamoda',
            'uktzed' => 'УКТЗЕД',
            'name_kasta' => 'Название в KASTA',
            'gender' => 'Гендерность',
        ];
    }

    public function save_with_slug($slug)
    {
        $this->slug = Inflector::slug($slug);
        return $this->save();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalization()
    {
        return $this->hasMany(ShopCategoryLocalization::className(), ['category_id' => 'id'])->indexBy('language');
    }

    public function getMainLanguage()
    {
        return $this->hasOne(ShopCategoryLocalization::className(), ['category_id' => 'id'])->andWhere(['language' => Yii::$app->params['main_language']]);
    }

    public function getActiveLanguage()
    {
        return $this->hasOne(ShopCategoryLocalization::className(), ['category_id' => 'id'])->andWhere(['language' => Yii::$app->language]);
    }

    public function getAttribute0()
    {
        return $this->hasMany(Attribute::className(), ['category_id' => 'id'])
            ->where(['attribute.status' => Attribute::STATUS_ACTIVE])
            ->with('activeLanguage')
            ->with('attributeValues')
            ->orderBy(['priority' => SORT_DESC]);
    }

    static public function getForMenu()
    {
        return self::find()
            ->where([
                'status' => self::STATUS_ACTIVE,
                'show_in_menu' => self::STATUS_ACTIVE
            ])
            ->with('activeLanguage')
            ->orderBy(['priority' => SORT_DESC])
            ->all();
    }

    static public function getActiveCategory()
    {
        return self::find()
            ->where([
                'status' => self::STATUS_ACTIVE
            ])
            ->with('mainLanguage')
            ->orderBy(['priority' => SORT_DESC])
            ->all();
    }

    static public function getForRozetka()
    {
        return self::find()
            ->where([
                'status_rozetka' => self::STATUS_ACTIVE
            ])
            ->with('mainLanguage')
            ->orderBy(['priority' => SORT_DESC])
            ->all();
    }

    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }

    static function findCategory($slug)
    {
        return self::find()
            ->where([
                'slug' => $slug,
                'status' => self::STATUS_ACTIVE
            ])
            ->one();
    }

    public function getTranslation()
    {
        $model = $this->activeLanguage;

        $name = $model->name ?: $this->mainLanguage->name;

        return [
            'name' => $name,
            'meta_title' => $model->meta_title ?: $name,
            'meta_description' => $model->meta_description ?: $name,
            'meta_keywords' => $model->meta_keywords ?: $name
        ];
    }

    public function getSizes()
    {
        return $this->hasMany(Size::className(), ['id' => 'size_id'])
            ->viaTable('size_category', ['category_id' => 'id']);
    }

}
