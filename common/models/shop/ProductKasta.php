<?php

namespace common\models\shop;

use Yii;

/**
 * This is the model class for table "product_kasta".
 *
 * @property int $id
 * @property string $name
 * @property int|null $product_id
 * @property int|null $sort
 *
 * @property Product $product
 */
class ProductKasta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_kasta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['product_id', 'sort'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'product_id' => 'Product ID',
            'sort' => 'Sort',
        ];
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    static function sort($data)
    {
        foreach ($data as $id => $value) {
            $model = self::findOne($id);
            $model->sort = $value;
            $model->save();
        }
    }

    static function remove($id)
    {
        $model = self::findOne($id);
        $position = $model->sort;
        $product_id = $model->product_id;
        if( $model->delete() ){
            self::updateAllCounters(['sort' => -1], ['and', ['product_id' => $product_id], ['>', 'sort', $position]]);
            return true;
        }
        return false;
    }
}
