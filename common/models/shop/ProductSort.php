<?php

namespace common\models\shop;

use Yii;
use yii\data\Pagination;

class ProductSort
{
	// const PAGE_SIZE = 16;
	const PAGE_SIZE = 5;

	const ITEMS = [
		'popular' => 'Most popular', //default
		'alphabetical' => 'Alphabetical',
		'price_low_high' => 'Price: Low-High',
		'price_high_low' => 'Price: High-Low'
	];

	private $model;
	private $query;
	public $pages;
	public $products;

	public function __construct($model, $sort)
	{
		$this->model = $model;
		$this->$sort();
	}

	private function popular()
	{
		$this->query = $this->model->orderBy(['number_purchased' => SORT_DESC]);

		return $this->pagination();
	}

	private function alphabetical()
	{
		$this->query = $this->model
			->andWhere(['language' => Yii::$app->language])
			->leftJoin('product_localization', 'product_localization.product_id = product.id')
			->orderBy(['product_localization.name' => SORT_ASC]);

		return $this->pagination();
	}

	private function price($order)
	{
		$discount_query = Discount::find()
			->select('MAX(discount.id)')
			->where('discount.product_id=product.id')
			->andWhere(['status' => Discount::STATUS_ACTIVE])
			->andWhere(['<=', 'date_start', date('Y-m-d')])
			->andWhere(['>=', 'date_finish', date('Y-m-d')]);

		$this->query = $this->model
			->select(['
				discount.price,
				product.price,
				product.category_id,
				product.id, 
				product.slug, 
				IFNULL(discount.price, product.price) AS price_finish'])
			->leftJoin('discount', ['discount.id' => $discount_query])
			->orderBy(['price_finish' => $order]);

		return $this->pagination();
	}

	private function pagination()
	{
		$this->pages = new Pagination([
			'totalCount' => $this->query->count(),
			'pageSize' => self::PAGE_SIZE,
			'forcePageParam' => false,
			'pageSizeParam' => false
		]);

		$this->products = $this->query->offset($this->pages->offset)
		   ->limit($this->pages->limit)
		   ->each();
	}

	private function price_low_high()
	{
		return $this->price(SORT_ASC);
	}

	private function price_high_low()
	{
		return $this->price(SORT_DESC);
	}
}