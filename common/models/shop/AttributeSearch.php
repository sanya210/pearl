<?php

namespace common\models\shop;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\shop\Attribute;

/**
 * AttributeSearch represents the model behind the search form of `common\models\shop\Attribute`.
 */
class AttributeSearch extends Attribute
{
    public function attributes()
    {
        return array_merge(parent::attributes(), ['mainLanguage.name']);
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'priority', 'status'], 'integer'],
            [['mainLanguage.name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Attribute::find()->joinWith(['mainLanguage']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'priority' => $this->priority,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['LIKE', 'attribute_localization.name', $this->getAttribute('mainLanguage.name')]);

        return $dataProvider;
    }
}
