<?php

namespace common\models\shop;

use Yii;
use yii\helpers\Inflector;

/**
 * This is the model class for table "attribute".
 *
 * @property int $id
 * @property int $category_id
 * @property int $priority
 * @property int $status
 *
 * @property ShopCategory $category
 * @property AttributeLocalization[] $attributeLocalizations
 * @property AttributeValue[] $attributeValues
 */
class Attribute extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public function behaviors()
    {
        return [
            [
                'class' => \backend\components\behaviors\LanguageBehavior::className(),
                'model' => '\common\models\shop\AttributeLocalization',
                'field' => 'language',
                'relation' => 'localization'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attribute';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'priority', 'status'], 'integer'],
            [['status'], 'required'],
            [['slug'], 'string', 'max' => 100],
            ['slug', 'unique'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ShopCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'priority' => 'Приоритет',
            'status' => 'Статус',
            'slug' => 'Slug',
        ];
    }

    public function save_with_slug($slug)
    {
        $this->slug = Inflector::slug($slug);
        return $this->save();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ShopCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalization()
    {
        return $this->hasMany(AttributeLocalization::className(), ['attribute_id' => 'id'])->indexBy('language');
    }

    public function getMainLanguage()
    {
        return $this->hasOne(AttributeLocalization::className(), ['attribute_id' => 'id'])->andWhere(['language' => Yii::$app->params['main_language']]);
    }

    public function getActiveLanguage()
    {
        return $this->hasOne(AttributeLocalization::className(), ['attribute_id' => 'id'])->where(['language' => Yii::$app->language]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttributeValues()
    {
        return $this->hasMany(AttributeValue::className(), ['attribute_id' => 'id'])->where(['status' => AttributeValue::STATUS_ACTIVE]);
    }

    public function getAttributeValuesAll()
    {
        return $this->hasMany(AttributeValue::className(), ['attribute_id' => 'id']);
    }

    static function getAttributesByCategory($category_id)
    {
        return self::find()
            ->select('slug')
            ->where([
                'status' => self::STATUS_ACTIVE,
                'category_id' => $category_id
            ])
            ->column();
    }

    static function getAttributesByPseudoCategory($category_id)
    {
        return self::find()
            ->select('attribute.slug, attribute.category_id')
            ->leftJoin('pseudo_category', 'pseudo_category.category_id=attribute.category_id')
            ->where([
                'attribute.status' => self::STATUS_ACTIVE,
                'pseudo_category.id' => $category_id
            ])
            ->column();
    }
}
