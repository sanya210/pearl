<?php

namespace common\models\shop;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\shop\Section;

/**
 * SectionSearch represents the model behind the search form of `common\models\shop\Section`.
 */
class SectionSearch extends Section
{
    public function attributes()
    {
        return array_merge(parent::attributes(), ['mainLanguage.name']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'priority', 'status'], 'integer'],
            [['categories', 'mainLanguage.name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Section::find();

        $query->joinWith(['mainLanguage']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'priority' => $this->priority,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['LIKE', 'section_localization.name', $this->getAttribute('mainLanguage.name')]);

        $query->andFilterWhere(['like', 'categories', $this->categories]);

        return $dataProvider;
    }
}
