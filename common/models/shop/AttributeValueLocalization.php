<?php

namespace common\models\shop;

use Yii;

/**
 * This is the model class for table "attribute_value_localization".
 *
 * @property int $id
 * @property int $value_id
 * @property string $language
 * @property string $name
 *
 * @property AttributeValue $value
 */
class AttributeValueLocalization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attribute_value_localization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value_id'], 'integer'],
            [['language'], 'required'],
            [['language'], 'string', 'max' => 16],
            [['name'], 'string', 'max' => 100],
            ['name', 'match', 'pattern' => '~^(\p{L}|\p{Zs})+$~u'],
            ['name', 'required', 'when' => function($model) {
                return $model->language == Yii::$app->params['main_language'];
            }, 
            'whenClient' => 'function (attribute, value) {
                return attribute.name == "['.Yii::$app->params['main_language'].']name";
            }'],
            [['value_id'], 'exist', 'skipOnError' => true, 'targetClass' => AttributeValue::className(), 'targetAttribute' => ['value_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'value_id' => 'Value ID',
            'language' => 'Language',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValue()
    {
        return $this->hasOne(AttributeValue::className(), ['id' => 'value_id']);
    }
}
