<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property int $id
 * @property string $symbol
 * @property string $rate
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['symbol', 'rate'], 'required'],
            [['rate'], 'number'],
            ['symbol', 'unique'],
            [['symbol'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'symbol' => 'Валюта',
            'rate' => 'Курс',
        ];
    }

    public static function getRate($symbol)
    {
        $rate = self::find()->where(['symbol' => $symbol])->one();
        return $rate ? $rate->rate : 1;
    }

    public static function setRates($data)
    {
        foreach (Yii::$app->params['currencies'] as $key => $value) {
            if ($key != Yii::$app->params['main_currency'] && isset($data[$key])) {
                $currency = self::find()->where(['symbol' => $key])->one();
                $currency->rate = $data[$key];
                if ($currency->save()) {
                    CurrencyLog::AddLog($currency->id, $currency->rate);
                }
            }
        }
    }

    public function getActive()
    {
        $currencies = array_keys(Yii::$app->params['currencies']);
        return self::find()->select(['symbol', 'rate'])->where(['symbol' => $currencies])->asArray()->indexBy('symbol')->all();
    }
}
