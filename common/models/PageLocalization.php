<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "page_localization".
 *
 * @property int $id
 * @property int $page_id
 * @property string $language
 * @property string $name
 * @property string $text
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 *
 * @property Page $page
 */
class PageLocalization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'page_localization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_id'], 'integer'],
            [['language'], 'required'],
            [['text', 'meta_description'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['name', 'meta_keywords'], 'string', 'max' => 255],
            ['name', 'required', 'when' => function($model) {
                return $model->language == Yii::$app->params['main_language'];
            }, 
            'whenClient' => 'function (attribute, value) {
                return attribute.name == "['.Yii::$app->params['main_language'].']name";
            }'],
            [['meta_title'], 'string', 'max' => 150],
            [['page_id'], 'exist', 'skipOnError' => true, 'targetClass' => Page::className(), 'targetAttribute' => ['page_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Страница',
            'language' => 'Язык',
            'name' => 'Название',
            'text' => 'Текст',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }
}
