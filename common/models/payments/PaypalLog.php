<?php

namespace common\models\payments;

use Yii;
use common\models\Order;
use common\models\payments\Pay;

/**
 * This is the model class for table "paypal_log".
 *
 * @property int $id
 * @property string $mc_gross
 * @property string $invoice
 * @property string $protection_eligibility
 * @property string $payer_id
 * @property string $payment_date
 * @property string $payment_status
 * @property string $charset
 * @property string $first_name
 * @property string $mc_fee
 * @property string $notify_version
 * @property string $custom
 * @property string $payer_status
 * @property string $business
 * @property string $quantity
 * @property string $verify_sign
 * @property string $payer_email
 * @property string $txn_id
 * @property string $payment_type
 * @property string $last_name
 * @property string $receiver_email
 * @property string $payment_fee
 * @property string $shipping_discount
 * @property string $receiver_id
 * @property string $insurance_amount
 * @property string $txn_type
 * @property string $item_name
 * @property string $discount
 * @property string $mc_currency
 * @property string $item_number
 * @property string $residence_country
 * @property string $test_ipn
 * @property string $shipping_method
 * @property string $transaction_subject
 * @property string $payment_gross
 * @property string $ipn_track_id
 */
class PaypalLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paypal_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['mc_gross', 'invoice', 'protection_eligibility', 'payer_id', 'payment_date', 'payment_status', 'charset', 'first_name', 'mc_fee', 'notify_version', 'custom', 'payer_status', 'business', 'quantity', 'verify_sign', 'payer_email', 'txn_id', 'payment_type', 'last_name', 'receiver_email', 'payment_fee', 'shipping_discount', 'receiver_id', 'insurance_amount', 'txn_type', 'item_name', 'discount', 'mc_currency', 'item_number', 'residence_country', 'test_ipn', 'shipping_method', 'transaction_subject', 'payment_gross', 'ipn_track_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mc_gross' => 'Mc Gross',
            'invoice' => 'Invoice',
            'protection_eligibility' => 'Protection Eligibility',
            'payer_id' => 'Payer ID',
            'payment_date' => 'Payment Date',
            'payment_status' => 'Payment Status',
            'charset' => 'Charset',
            'first_name' => 'First Name',
            'mc_fee' => 'Mc Fee',
            'notify_version' => 'Notify Version',
            'custom' => 'Custom',
            'payer_status' => 'Payer Status',
            'business' => 'Business',
            'quantity' => 'Quantity',
            'verify_sign' => 'Verify Sign',
            'payer_email' => 'Payer Email',
            'txn_id' => 'Txn ID',
            'payment_type' => 'Payment Type',
            'last_name' => 'Last Name',
            'receiver_email' => 'Receiver Email',
            'payment_fee' => 'Payment Fee',
            'shipping_discount' => 'Shipping Discount',
            'receiver_id' => 'Receiver ID',
            'insurance_amount' => 'Insurance Amount',
            'txn_type' => 'Txn Type',
            'item_name' => 'Item Name',
            'discount' => 'Discount',
            'mc_currency' => 'Mc Currency',
            'item_number' => 'Item Number',
            'residence_country' => 'Residence Country',
            'test_ipn' => 'Test Ipn',
            'shipping_method' => 'Shipping Method',
            'transaction_subject' => 'Transaction Subject',
            'payment_gross' => 'Payment Gross',
            'ipn_track_id' => 'Ipn Track ID',
        ];
    }

    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'invoice']);
    }

    public function add($data)
    {
        if(!self::find()->where(['txn_id' => $data['txn_id']])->exists()){
            $msg = json_encode($data);
            $this->Loger($msg, 'PayPal');
            
            foreach($this as $name => $value) {
                if(isset($data[$name])) {
                    $this->$name = $data[$name];
                }
            }
            if($this->save()) {
                $order = $this->order;
                $order->toPay($data['mc_gross'], $data['mc_currency']);
                $this->addPay();
            }
        }
    }

    public function addPay()
    {
        $pay_model = new Pay();
        $pay_model->order_id = $this->invoice;
        $pay_model->user_id = $this->order->user_id;
        $pay_model->amount = $this->payment_gross;
        $pay_model->currency = $this->mc_currency;
        $pay_model->date = strtotime($this->payment_date);
        $pay_model->system = 'PayPal';
        $pay_model->system_id = $this->id;
        $pay_model->status = $this->payment_status;
        $pay_model->save();
    }

    private function Loger($msg, $folder){        
        $fd = fopen(Yii::getAlias('@app/logs/'.$folder.'/'.$folder.'_Log_'.date('Y-m-d', time()).'.txt'), 'a');
        fwrite($fd, "\r\n\r\n--------------------------".date('Y-m-d\TH:i:s', time())."-------------------------------\r\n\r\n".$msg);
        fclose($fd);
    }

}
