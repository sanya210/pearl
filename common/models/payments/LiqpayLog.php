<?php

namespace common\models\payments;

use Yii;
use common\models\Order;
use common\models\payments\Pay;

/**
 * This is the model class for table "liqpay_log".
 *
 * @property int $id
 * @property int $order_id
 * @property string $status
 * @property string $payment_id
 * @property string $public_key
 * @property string $liqpay_order_id
 * @property string $description
 * @property string $sender_phone
 * @property string $sender_card_mask2
 * @property string $sender_card_country
 * @property string $ip
 * @property string $amount
 * @property string $currency
 * @property string $sender_commission
 * @property string $receiver_commission
 * @property string $agent_commission
 * @property string $amount_debit
 * @property string $amount_credit
 * @property string $commission_debit
 * @property string $commission_credit
 * @property string $currency_debit
 * @property string $currency_credit
 * @property string $sender_bonus
 * @property string $amount_bonus
 * @property string $language
 * @property int $create_date
 * @property int $end_date
 * @property int $transaction_id
 */
class LiqpayLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'liqpay_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'create_date', 'end_date', 'transaction_id'], 'integer'],
            [['amount', 'sender_commission', 'receiver_commission', 'agent_commission', 'amount_debit', 'amount_credit', 'commission_debit', 'commission_credit', 'sender_bonus', 'amount_bonus', 'payment_id', ], 'number'],
            [['status', 'currency', 'currency_debit', 'currency_credit', 'language'], 'string', 'max' => 20],
            [['public_key', 'sender_phone', 'sender_card_mask2'], 'string', 'max' => 100],
            [['liqpay_order_id', 'description'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 50],
            ['sender_card_country', 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'status' => 'Status',
            'payment_id' => 'Payment ID',
            'public_key' => 'Public Key',
            'liqpay_order_id' => 'Liqpay Order ID',
            'description' => 'Description',
            'sender_phone' => 'Sender Phone',
            'sender_card_mask2' => 'Sender Card Mask2',
            'sender_card_country' => 'Sender Card Country',
            'ip' => 'Ip',
            'amount' => 'Amount',
            'currency' => 'Currency',
            'sender_commission' => 'Sender Commission',
            'receiver_commission' => 'Receiver Commission',
            'agent_commission' => 'Agent Commission',
            'amount_debit' => 'Amount Debit',
            'amount_credit' => 'Amount Credit',
            'commission_debit' => 'Commission Debit',
            'commission_credit' => 'Commission Credit',
            'currency_debit' => 'Currency Debit',
            'currency_credit' => 'Currency Credit',
            'sender_bonus' => 'Sender Bonus',
            'amount_bonus' => 'Amount Bonus',
            'language' => 'Language',
            'create_date' => 'Create Date',
            'end_date' => 'End Date',
            'transaction_id' => 'Transaction ID',
        ];
    }

    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    public function add($signature, $data)
    {
        $json = base64_decode($data);
        $msg = "\r\n data: ".$data."\r\n signature: ".$signature."\r\n json: ".$json;
        $this->Loger($msg, 'LiqPay');

        $result = json_decode($json, true);

        $public_key = $result['public_key'];
        $private_key = LiqPay::PRIVATE_KEY;

        $sign = base64_encode( sha1($private_key . $data . $private_key, 1) );
        
        if($signature == $sign) {
            foreach ($this as $name => $value) {
                if(isset($result[$name])) {
                    $this->$name = $result[$name];
                }
            }
            if($this->save() && $this->status == 'success') {
                $order = $this->order;
                $amount = $result['amount'] - $result['sender_commission'];
                $order->toPay($amount, $result['currency']);
                $this->addPay();
            }
        }
    }

    public function addPay()
    {
        $pay_model = new Pay();
        $pay_model->order_id = $this->order_id;
        $pay_model->user_id = $this->order->user_id;
        $pay_model->amount = $this->amount - $this->sender_commission;
        $pay_model->currency = $this->currency;
        $pay_model->date = $this->end_date;
        $pay_model->system = 'LiqPay';
        $pay_model->system_id = $this->id;
        $pay_model->status = $this->status;
        $pay_model->save();
    }

    private function Loger($msg, $folder){        
        $fd = fopen(Yii::getAlias('@app/logs/'.$folder.'/'.$folder.'_Log_'.date('Y-m-d', time()).'.txt'), 'a');
        fwrite($fd, "\r\n\r\n--------------------------".date('Y-m-d\TH:i:s', time())."-------------------------------\r\n\r\n".$msg);
        fclose($fd);
    }
}
