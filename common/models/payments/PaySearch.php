<?php

namespace common\models\payments;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\payments\Pay;

/**
 * PaySearch represents the model behind the search form of `common\models\payments\Pay`.
 */
class PaySearch extends Pay
{
    public function attributes()
    {
        return array_merge(parent::attributes(), ['user.name', 'user.family_name']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'user_id', 'date', 'system_id'], 'integer'],
            [['amount'], 'number'],
            [['currency', 'system', 'status', 'user.name', 'user.family_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pay::find()->orderBy(['id' => SORT_DESC]);

        $query->joinWith(['user']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'user_id' => $this->user_id,
            'amount' => $this->amount,
            'date' => $this->date,
            'system_id' => $this->system_id,
        ]);

        $query->andFilterWhere(['LIKE', 'user.name', $this->getAttribute('user.name')]);
        $query->andFilterWhere(['LIKE', 'user.family_name', $this->getAttribute('user.family_name')]);

        $query->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'system', $this->system])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
