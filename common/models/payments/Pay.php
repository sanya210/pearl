<?php

namespace common\models\payments;

use common\models\User;
use Yii;

/**
 * This is the model class for table "pay".
 *
 * @property int $id
 * @property int $order_id
 * @property int $user_id
 * @property string $amount
 * @property string $currency
 * @property int $date
 * @property string $system
 * @property int $system_id
 * @property int $status
 */
class Pay extends \yii\db\ActiveRecord
{
    const PAGE_SIZE = 10;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pay';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'currency', 'date', 'system', 'system_id', 'status'], 'required'],
            [['order_id', 'user_id', 'date', 'system_id'], 'integer'],
            [['amount'], 'number'],
            [['currency'], 'string', 'max' => 16],
            [['system', 'status'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ',
            'user_id' => 'Пользователь',
            'amount' => 'Сумма',
            'currency' => 'Валюта',
            'date' => 'Дата',
            'system' => 'Система',
            'system_id' => 'System ID',
            'status' => 'Статус',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function getUserPay()
    {
        return self::find()
            ->where(['user_id' => Yii::$app->user->identity->id])
            ->orderBy(['date' => SORT_DESC]);
    }
}
