<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "text_localization".
 *
 * @property int $id
 * @property string $language
 * @property int $text_id
 * @property string $text
 *
 * @property Text $text0
 */
class TextLocalization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'text_localization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['language'], 'required'],
            [['text_id'], 'integer'],
            [['text'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['text_id'], 'exist', 'skipOnError' => true, 'targetClass' => Text::className(), 'targetAttribute' => ['text_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'language' => 'Language',
            'text_id' => 'Text ID',
            'text' => 'Text',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getText0()
    {
        return $this->hasOne(Text::className(), ['id' => 'text_id']);
    }
}
