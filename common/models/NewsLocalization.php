<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "news_localization".
 *
 * @property int $id
 * @property int $news_id
 * @property string $language
 * @property string $name
 * @property string $text
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 *
 * @property News $news
 */
class NewsLocalization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news_localization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['news_id'], 'integer'],
            [['language'], 'required'],
            [['text', 'meta_description'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['name', 'meta_keywords'], 'string', 'max' => 255],
            ['name', 'required', 'when' => function($model) {
                return $model->language == Yii::$app->params['main_language'];
            }, 
            'whenClient' => 'function (attribute, value) {
                return attribute.name == "['.Yii::$app->params['main_language'].']name";
            }'],
            [['meta_title'], 'string', 'max' => 150],
            [['news_id'], 'exist', 'skipOnError' => true, 'targetClass' => News::className(), 'targetAttribute' => ['news_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'news_id' => 'News ID',
            'language' => 'Language',
            'name' => 'Name',
            'text' => 'Text',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['id' => 'news_id']);
    }
}
