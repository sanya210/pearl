<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "main_slider_localization".
 *
 * @property int $id
 * @property int $slider_id
 * @property string $language
 * @property string $header
 * @property string $text
 * @property string $link
 *
 * @property MainSlider $slider
 */
class MainSliderLocalization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'main_slider_localization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['slider_id'], 'integer'],
            [['language'], 'required'],
            [['text'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['header'], 'string', 'max' => 255],
            ['link', 'url'],
            [['slider_id'], 'exist', 'skipOnError' => true, 'targetClass' => MainSlider::className(), 'targetAttribute' => ['slider_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slider_id' => 'Слайдер',
            'language' => 'Язык',
            'header' => 'Заголовок',
            'text' => 'Текст',
            'link' => 'Ссылка',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSlider()
    {
        return $this->hasOne(MainSlider::className(), ['id' => 'slider_id']);
    }
}
