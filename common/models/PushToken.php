<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "push_token".
 *
 * @property int $id
 * @property string $token
 * @property int $user_id
 */
class PushToken extends \yii\db\ActiveRecord
{
    const URL = 'https://fcm.googleapis.com/fcm/send';
    const API_KEY = 'AAAA8Br65fI:APA91bERZat3_5YcWsTIXNCz1YJOj9Iqq_Xj560pPtGOy1jccbys1DyjwVWuqf_9-qp79YR8jEDasK-N-gdgeeunkG9-74wshwqk6lfhwRT9SarzbJdhs6c-MHJqTvZu9_w7pkpEEllk';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'push_token';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['token'], 'required'],
            [['user_id'], 'integer'],
            [['token'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'token' => 'Token',
            'user_id' => 'User ID',
        ];
    }

    public static function add($token)
    {
        if ($token) {
            $model = new self();
            $model->token = $token;
            $model->user_id = Yii::$app->user->isGuest ? 0 : Yii::$app->user->identity->id;
            return $model->save();
        }
        return false;
    }

    private static function send($tokens, $message)
    {
        $token_groups = array_chunk($tokens, 998);
        foreach ($token_groups as $tokens) {
            $request_body = [
                'registration_ids' => $tokens,
                'notification' => [
                    'title' => $message['title'],
                    'body' => $message['body'],
                    'click_action' => $message['link'],
                    'icon' => 'https://pearl.wedding/img/logo.png',
                ],
            ];
            $fields = json_encode($request_body);

            $request_headers = [
                'Content-Type: application/json',
                'Authorization: key=' . self::API_KEY,
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, self::URL);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $response = curl_exec($ch);
            curl_close($ch);
        }
    }

    private static function getUserTokens($user_id)
    {
        return self::find()->select('token')->where(['user_id' => $user_id])->column();
    }

    public static function sendToUser($user_id, $message)
    {
        $tokens = self::getUserTokens($user_id);
        if ($tokens) {
            self::send($tokens, $message);
        }
    }

    public static function sendToUsers($message)
    {
        $tokens = self::find()->select('token')->column();
        if ($tokens) {
            self::send($tokens, $message);
        }
    }

    public static function updateToken($token)
    {
        $model = self::find()->where(['token' => $token])->one();
        if ($model) {
            $model->user_id = Yii::$app->user->isGuest ? 0 : Yii::$app->user->identity->id;
            $model->save();
        }
    }
}
