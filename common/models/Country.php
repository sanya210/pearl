<?php

namespace common\models;

use frontend\models\Geo;
use Yii;

/**
 * This is the model class for table "country".
 *
 * @property int $id
 * @property string $code
 * @property int $status
 */
class Country extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'required'],
            [['status', 'priority'], 'integer'],
            [['code'], 'string', 'max' => 10],
            [['name'], 'string'],
            [['size_type'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'code' => 'Код',
            'status' => 'Статус',
            'priority' => 'Приоритет',
            'size_type' => 'Тип размерной сетки',
        ];
    }

    public function getName()
    {
        return Yii::t('country', $this->code);
    }

    public static function getCountries()
    {
        $models = self::find()->where([
            'status' => self::STATUS_ACTIVE
        ])->orderBy(['priority' => SORT_DESC])->all();

        $countries = [];

        foreach ($models as $model) {
            $countries[$model->id] = Yii::t('country', $model->code); 
        }

        return $countries;
    }

    public static function getSizeType()
    {
        $country = new Geo();
        $code = $country->getCountry();
        if ($code) {
            $size = self::find()->select('size_type')->where(['code' => $code])->column();
            return $size ? $size[0] : null;
        } else {
            return null;
        }
    }
}
