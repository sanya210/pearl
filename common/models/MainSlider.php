<?php

namespace common\models;

use Yii;
use yii\helpers\Inflector;

/**
 * This is the model class for table "main_slider".
 *
 * @property int $id
 * @property string $img_lg
 * @property string $img_xs
 * @property int $priority
 * @property int $status
 *
 * @property MainSliderLocalization[] $mainSliderLocalizations
 */
class MainSlider extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public $image_lg;
    public $image_xs;

    public function behaviors()
    {
        return [
            [
                'class' => \backend\components\behaviors\LanguageBehavior::className(),
                'model' => '\common\models\MainSliderLocalization',
                'field' => 'language',
                'relation' => 'localization'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'main_slider';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'required'],
            [['priority', 'status'], 'integer'],
            [['image_lg'], 'image', 
                'extensions' => 'jpg',
                'skipOnEmpty' => !$this->isNewRecord,
                'maxFiles' => 1,
                'minWidth' => 1920, 'maxWidth' => 1920,
                'minHeight' => 660, 'maxHeight' => 660,
            ],
            [['image_xs'], 'image', 
                'extensions' => 'jpg',
                'skipOnEmpty' => !$this->isNewRecord,
                'maxFiles' => 1,
                'minWidth' => 768, 'maxWidth' => 768,
                'minHeight' => 256, 'maxHeight' => 256,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image_lg' => 'Картинка LG(1920×660)',
            'image_xs' => 'Картинка XS(768×256)',
            'img_lg' => 'Картинка LG',
            'img_xs' => 'Картинка XS',
            'priority' => 'Приоритет',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalization()
    {
        return $this->hasMany(MainSliderLocalization::className(), ['slider_id' => 'id'])->indexBy('language');
    }

    public function getMainLanguage()
    {
        return $this->hasOne(MainSliderLocalization::className(), ['slider_id' => 'id'])->andWhere(['language' => Yii::$app->params['main_language']]);
    }

    public function getActiveLanguage()
    {
        return $this->hasOne(MainSliderLocalization::className(), ['slider_id' => 'id'])->where(['language' => Yii::$app->language]);
    }

    public function upload()
    {
        if($this->validate()) {
            $this->image_lg->saveAs('uploads/slider/' . Inflector::slug($this->img_lg));
            $this->image_xs->saveAs('uploads/slider/' . Inflector::slug($this->img_xs));
            return true;
        } else {
            return false;
        }
    }

    public static function getActiveSlides()
    {
        return self::find()->where([
            'status' => self::STATUS_ACTIVE
        ])
        ->with('activeLanguage')
        ->orderBy(['priority' => SORT_DESC])
        ->all();
    }
}
