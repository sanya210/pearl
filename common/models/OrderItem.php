<?php

namespace common\models;

use common\models\shop\Size;
use Yii;
use common\models\shop\Product;
use common\models\Currency;

/**
 * This is the model class for table "order_item".
 *
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property string $sizes
 * @property string $quantity
 * @property string $price
 * @property string $size_type
 *
 * @property Order $order
 * @property Product $product
 */
class OrderItem extends \yii\db\ActiveRecord
{
    private $_rate;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id'], 'required'],
            [['order_id', 'product_id'], 'integer'],
            [['sizes', 'quantity'], 'string'],
            [['size_type'], 'string', 'max' => 15],
            [['price', 'price_main'], 'number'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'product_id' => 'Product ID',
            'sizes' => 'Sizes',
            'quantity' => 'Quantity',
            'price' => 'Price',
            'price_main' => 'Price main',
            'size_type' => 'Size type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function getRate()
    {
        if (!$this->_rate) {
            $this->_rate = Currency::getRate($this->order->currency);
        }
        return $this->_rate;
    }

    public function getTotal($transfer=true)
    {
        if($this->price && $this->price_main) {
            if($transfer) {
                return $this->price;
            } else {
                return $this->price_main;
            }
        } else {
            if($transfer) {
                return $this->quantity * $this->product->getPriceValue($this->rate)['price'];
            } else {
                return $this->quantity * $this->product->getPriceValue(1)['price'];
            }
        }
    }

    public function setPrice()
    {
        $this->price = $this->quantity * $this->product->getPriceValue($this->rate)['price'];
        $this->price_main = $this->quantity * $this->product->getPriceValue(1)['price'];
        $this->save();
    }

    public function getSizeType()
    {
        return $this->size_type ?: 'europe';
    }

    public function getSizesTranslate()
    {
        $sizes = json_decode($this->sizes, true);
        $sizes_translate = [];
        foreach ($sizes as $size => $quantity) {
            $sizes_translate[$size] = [
                'size' => $this->product->sizeByEurope[$size][$this->sizeType],
                'quantity' => $quantity
            ];
        }
        return $sizes_translate;
    }

    public function getSizeLabel()
    {
        $model_size = new Size();
        return $model_size->attributeLabels()[$this->sizeType];
    }
}
