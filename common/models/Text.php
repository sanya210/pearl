<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "text".
 *
 * @property int $id
 * @property string $name
 *
 * @property TextLocalization[] $textLocalizations
 */
class Text extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => \backend\components\behaviors\LanguageBehavior::className(),
                'model' => '\common\models\TextLocalization',
                'field' => 'language',
                'relation' => 'localization'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'text';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalization()
    {
        return $this->hasMany(TextLocalization::className(), ['text_id' => 'id'])->indexBy('language');
    }

    public function getMainLanguage()
    {
        return $this->hasOne(TextLocalization::className(), ['text_id' => 'id'])->andWhere(['language' => Yii::$app->params['main_language']]);
    }

    public function getActiveLanguage()
    {
        return $this->hasOne(TextLocalization::className(), ['text_id' => 'id'])->where(['language' => Yii::$app->language]);
    }

    public static function getText($name)
    {
        $model = self::find()->where(['name' => $name])->with('activeLanguage')->one();
        if($model) {
            if($model->activeLanguage) return $model->activeLanguage->text;
            return $model->mainLanguage->text;
        }
        return '';
    }
}
