<?php

namespace common\models;

use Yii;
use yii\helpers\Inflector;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 * @property string $slug
 * @property string $img
 * @property string $img_preview
 * @property int $status
 *
 * @property NewsLocalization[] $newsLocalizations
 */
class News extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const PAGE_SIZE = 12;

    public $image;
    public $image_preview;

    public function behaviors()
    {
        return [
            [
                'class' => \backend\components\behaviors\LanguageBehavior::className(),
                'model' => '\common\models\NewsLocalization',
                'field' => 'language',
                'relation' => 'localization'
            ],
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'status'], 'integer'],
            [['status'], 'required'],
            [['slug', 'img', 'img_preview'], 'string', 'max' => 255],
            ['slug', 'unique'],
            [['image'], 'image', 
                'extensions' => 'jpg',
                'skipOnEmpty' => !$this->isNewRecord,
                'maxFiles' => 1,
            ],
            [['image_preview'], 'image', 
                'extensions' => 'jpg',
                'skipOnEmpty' => !$this->isNewRecord,
                'maxFiles' => 1,
                'minWidth' => 776, 'maxWidth' => 776,
                'minHeight' => 552, 'maxHeight' => 552,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
            'slug' => 'Slug',
            'img' => 'Картинка',
            'img_preview' => 'Превью',
            'status' => 'Статус',
            'image' => 'Главная картинка',
            'image_preview' => 'Превью картинка(776×552)',
        ];
    }

    public function save_with_slug($slug)
    {
        $this->slug = Inflector::slug($slug);
        return $this->save();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalization()
    {
        return $this->hasMany(NewsLocalization::className(), ['news_id' => 'id'])->indexBy('language');
    }

    public function getMainLanguage()
    {
        return $this->hasOne(NewsLocalization::className(), ['news_id' => 'id'])->andWhere(['language' => Yii::$app->params['main_language']]);
    }

    public function getActiveLanguage()
    {
        return $this->hasOne(NewsLocalization::className(), ['news_id' => 'id'])->where(['language' => Yii::$app->language]);
    }

    public function upload()
    {
        if($this->validate()) {
            $this->image->saveAs('uploads/news/' . $this->img);
            $this->image_preview->saveAs('uploads/news/' . $this->img_preview);
            return true;
        } else {
            return false;
        }
    }

    public static function getNews()
    {
        return self::find()->where([
            'status' => self::STATUS_ACTIVE
        ])
        ->with('activeLanguage')
        ->orderBy(['updated_at' => SORT_DESC]);
    }

}
