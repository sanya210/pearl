<?php
namespace common\bootstrap;

use yii\base\BootstrapInterface;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;

class SetUp implements BootstrapInterface
{
	public function bootstrap($app)
	{
		$container = \Yii::$container;

		$container->setSingleton(Client::class, function() {
			return ClientBuilder::create()
				->setHosts(['192.168.8.162:9200'])
				->build();
		});
	}
}









