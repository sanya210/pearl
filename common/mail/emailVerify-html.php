<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$verifyLink = Url::to(['verify-email', 'token' => $user->verification_token], true);
?>
<div class="verify-email">
	<p><?=Yii::t('pages', 'Hello')?> <?= Html::encode($user->name) ?>,</p>

	<p><?=Yii::t('pages', 'Follow the link below to verify your email:')?></p>

	<p><?= Html::a(Html::encode($verifyLink), $verifyLink) ?></p>
</div>
