<h2><?=Yii::t('order2', 'Good afternoon')?>, <?=$order->user->name?></h2>

<p><?=Yii::t('order2', 'You just ordered at pearl.wedding. Thank you for your trust!')?></p>
<p><?=Yii::t('order2', 'Your order will be shipped as soon as possible after payment.')?></p>

<h3><?=Yii::t('order2', 'Your order')?> № <?=$order->id?> (<?=Yii::t('order2', 'from')?> <?=date('Y.m.d H:i', $order->created_at)?>)</h3>

<table style="border:1px solid #ead3d3;margin-top: 15px;width: 100%;">
    <thead style="background-color: #ead3d3;margin: 0;padding:10px;">
    <th style="padding:10px;border:1px solid #000;"><?=Yii::t('order2', 'Product')?></th>
    <th style="padding:10px;border:1px solid #000;text-align: center;"><?=Yii::t('order2', 'Size (quantity)')?></th>
    <th style="padding:10px;border:1px solid #000;text-align: center;"><?=Yii::t('order2', 'Amount')?></th>
    </thead>
    <?php foreach ($order->products as $product) { ?>
        <tr>
            <td style="padding:10px;border:1px solid #000;"><?=$product['name']?></td>
            <td style="padding:10px;border:1px solid #000;text-align: center;">
                <?php foreach ($product['sizes_translate'] as $size) { ?>
                    <div><?= $size['size'] ?> (<b><?= $size['quantity'] ?></b>);</div>
                <?php } ?>
            </td>
            <td style="padding:10px;border:1px solid #000;text-align: center;"><?=$product['price']?> <?=$order->currency?></td>
        </tr>
    <?php } ?>
</table>

<table style="border:1px solid #000;margin-top: 25px;width: 100%;">
    <thead style="border:1px solid #000;">
    <th style="padding:10px;border:1px solid #000;"><?=Yii::t('order2', 'The payer')?></th>
    <th style="padding:10px;border:1px solid #000;"><?=Yii::t('order2','Address for delivery')?></th>
    </thead>
    <tr>
        <td style="padding:10px;border:1px solid #000;"><?=$order->user->name?> <?=$order->user->family_name?></td>
        <td style="padding:10px;border:1px solid #000;">
            <?=Yii::t('country', $order->address->country->code)?>, <?=$order->address->delivery_address?>, <?=$order->address->delivery_index?>
        </td>
    </tr>
</table>

<p style="margin-top: 20px;"><?=Yii::t('order2', 'The most important for us is your satisfaction. Our manager will be glad to answer all your questions and help you.  You can find all the ways to contact us on <a href="https://pearl.wedding/contacts">the contact page</a>.')?></p>
<p><?=Yii::t('order2', 'Share with us your opinion about our dresses and service quality! We are waiting for your feedback!')?></p>
<p><?=Yii::t('order2', 'Thank you for the order!')?></p>
<p><?=Yii::t('order2', 'Best regards, the Pearl Fashion Group team.')?></p>
