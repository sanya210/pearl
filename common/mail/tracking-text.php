<?=Yii::t('order', 'Good afternoon')?>, <?=$order->user->name?>!
<?=Yii::t('order', 'We are pleased to inform you of the dispatch of your order')?> № <?=$order->id?>.
<?=Yii::t('order', 'Using this number, you will be able to track the whole process of parcel delivery with your order.')?>

<?=Yii::t('order', 'You can track your parcel follow the link:')?> <?=$order->delivery['url']?> <?=Yii::t('order', 'и вставить ваш трэкинг')?> № <?=$order->delivery_number?>.

<?=Yii::t('order', 'Thank you for the order!')?> <?=Yii::t('order', 'Best regards, the Pearl Fashion Group')?>.
