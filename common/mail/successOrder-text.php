<?=Yii::t('order2', 'Good afternoon')?>, <?=$order->user->name?>!

<?=Yii::t('order2', 'You just ordered at pearl.wedding. Thank you for your trust!')?>

<?=Yii::t('order2', 'Your order will be shipped as soon as possible after payment.')?>

<?=Yii::t('order2', 'Your order')?> № <?=$order->id?> (<?=Yii::t('order2', 'from')?> <?=date('Y.m.d H:i', $order->created_at)?>).

<?=Yii::t('order2', 'The most important for us is your satisfaction. Our manager will be glad to answer all your questions and help you.  You can find all the ways to contact us on <a href="https://pearl.wedding/contacts">the contact page</a>.')?>

<?=Yii::t('order2', 'Share with us your opinion about our dresses and service quality! We are waiting for your feedback!')?>

<?=Yii::t('order2', 'Thank you for the order!')?>

<?=Yii::t('order2', 'Best regards, the Pearl Fashion Group team.')?>
