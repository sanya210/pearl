<?php
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$verifyLink = Url::to(['verify-email', 'token' => $user->verification_token], true);
?>

<?=Yii::t('pages', 'Hello')?>, <?=$user->name?>,

<?=Yii::t('pages', 'Follow the link below to verify your email:')?>

<?= $verifyLink ?>
