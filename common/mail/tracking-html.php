<h2><?=Yii::t('order', 'Good afternoon')?>, <?=$order->user->name?></h2>

<p><?=Yii::t('order', 'We are pleased to inform you of the dispatch of your order')?> <b>№ <?=$order->id?>.</b></p>
<p><?=Yii::t('order', 'Your order number is')?> <b>№ <?=$order->id?></b>. <?=Yii::t('order', 'Using this number, you will be able to track the whole process of parcel delivery with your order.')?></p>

<h3><?=Yii::t('order', 'Sending your order')?> № <?=$order->id?></h3>

<table style="border:1px solid #ead3d3;margin-top: 15px;width: 100%;">
    <thead style="background-color: #ead3d3;margin: 0;padding:10px;">
    <th style="padding:10px;border:1px solid #000;"><?=Yii::t('order', 'Product')?></th>
    <th style="padding:10px;border:1px solid #000;"><?=Yii::t('order', 'Size (quantity)')?></th>
    <th style="padding:10px;border:1px solid #000;"><?=Yii::t('order', 'Amount')?></th>
    </thead>
    <?php foreach ($order->products as $product) { ?>
        <tr>
            <td style="padding:10px;border:1px solid #000;"><?=$product['name']?></td>
            <td style="padding:10px;border:1px solid #000;text-align: center;">
                <?php foreach ($product['sizes_translate'] as $size) { ?>
                    <div><?= $size['size'] ?> (<b><?= $size['quantity'] ?></b>);</div>
                <?php } ?>
            </td>
            <td style="padding:10px;border:1px solid #000;text-align: center;"><?=$product['price']?> <?=$order->currency?></td>
        </tr>
    <?php } ?>
</table>

<table style="border:1px solid #000;margin-top: 25px;width: 100%;">
    <thead style="border:1px solid #000;">
    <th style="padding:10px;border:1px solid #000;"><?=Yii::t('order','Address for delivery')?></th>
    <th style="padding:10px;border:1px solid #000;"><?=Yii::t('order', 'Method of delivery')?></th>
    <th style="padding:10px;border:1px solid #000;"><?=Yii::t('order', 'Tracking number')?></th>
    </thead>
    <tr>
        <td style="padding:10px;border:1px solid #000;">
            <?=Yii::t('country', $order->address->country->code)?>, <?=$order->address->delivery_address?>, <?=$order->address->delivery_index?>
        </td>
        <td style="padding:10px;border:1px solid #000;text-align: center;"><?=$order->delivery['name'] ?? ''?></td>
        <td style="padding:10px;border:1px solid #000;text-align: center;"><?=$order->delivery_number?></td>
    </tr>
</table>

<p style="margin-top: 20px;"><?=Yii::t('order', 'You can track your parcel follow the link:')?> <a href="<?=$order->delivery['url']?>"><?=$order->delivery['url']?></a> <?=Yii::t('order', 'и вставить ваш трэкинг')?> № <?=$order->delivery_number?>.</p>

<p><?=Yii::t('order', 'The most important thing for us is for you to be satisfied. Our account manager will be happy to answer all your questions and help you.  You can find all the ways to get in touch with us on <a href="https://pearl.wedding/contacts">the contact page</a>.')?></p>
<p><?=Yii::t('order', 'Thank you for the order!')?></p>
<p><?=Yii::t('order', 'Best regards, the Pearl Fashion Group')?></p>
