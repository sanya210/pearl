<?php
return [
	'adminEmail' => 'sales@pearl.wedding',
	'marketingEmail' => 'marketing@pearl.wedding',
	'managerEmail' => 'order@pearl.wedding',
	'supportEmail' => 'sales@pearl.wedding',
	'senderEmail' => 'sales@pearl.wedding',
	'senderName' => 'pearl.wedding mailer',
	'phone' => '+380935290909',
	'user.passwordResetTokenExpire' => 3600,
	'main_language' => 'en',
	'languages' => [
		'en' => 'English',
		'ru' => 'Русский',
		'es' => 'Español',
		'de' => 'Deutsch',
		'it' => 'Italiano',
		'fr' => 'Le français',
	],
	'languages_active' => [
		'en' => 'English',
		'ru' => 'Русский',
		'fr' => 'Le français',
		'de' => 'Deutsch',
	],
	'texts' => [
		'main_page' => 'Текст на главной',
		'main_title' => 'Title главной',
		'main_description' => 'Description главной',
		'product' => 'Текст на странице товара',
		'pay_success' => 'Текст успешной оплаты',
	],
	'currencies' => [
		'USD' => '$',
		'EUR' => '€',
		'UAH' => '₴'
	],
	'main_currency' =>'USD',
];
