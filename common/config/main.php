<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => [
        'common\bootstrap\SetUp'
    ], 
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
          'class' => 'yii\rbac\DbManager',
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'google' => [
                    'class' => 'yii\authclient\clients\Google',
                    'clientId' => '1031244801522-ermcd6dfdinhfqrvn8de7lcnqb1k11np.apps.googleusercontent.com',
                    'clientSecret' => 'ZB3kDXjxEAhEwmXIilve1sIy',
                    // 'clientId' => '160742252212-0lp2kstfldnsfb1kdjntb2iua0tkjkmo.apps.googleusercontent.com',
                    // 'clientSecret' => 'O8_F3SkoqXQUsden07HyJrpk',
                ],
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '510887626406131',
                    'clientSecret' => 'baf31efe65531c2327cee39c0744ffa7',
                ],
                'vkontakte' => [
                    'class' => 'yii\authclient\clients\VKontakte',
                    'clientId' => '7010356',
                    'clientSecret' => 'Kdzq5cDoMtS9eWEh5l13',
                    'scope' => 'email'
                ],
            ],
        ],
          'i18n' => [
            'translations' => [
                'country' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'fileMap' => [
                        'frontend' => 'country.php',
                    ],
                ]
            ],
        ],
    ],
];
