<?php
namespace api\models;

class Order extends \common\models\Order
{
    public function afterFind()
    {
        parent::afterFind();
        $this->scenario = self::SCENARIO_API;
    }

    public static function getAll()
    {
        return self::find()
            ->with('address')
            ->with(['orderItems' => function($query) {
                return $query->with('product');
            }])->asArray();
    }

    public static function view($id)
    {
        $model = self::find()->where(['id' => $id])
            ->with('address')
            ->with(['orderItems' => function($query) {
                return $query->with('product');
            }])

            ->asArray()->one();
        return $model;
    }

    public static function getStatuses()
    {
        $statuses = [];
        foreach (self::STATUSES as $id => $status) {
            $statuses[] = [
                'id' => $id,
                'value' => $status
            ];
        }
        return $statuses;
    }

    public static function getDeliveryTypes()
    {
        $delivery_types = [];
        foreach (self::DELIVERY_TYPES as $id => $delivery) {
            $delivery_types[] = [
                'id' => $id,
                'name' => $delivery['name'],
                'url' => $delivery['url']
            ];
        }
        return $delivery_types;
    }
}