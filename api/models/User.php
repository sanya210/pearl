<?php
namespace api\models;

use common\models\User as UserModel;

class User extends UserModel
{
    public static function getAll()
    {
        return self::find()
            ->select(['id', 'email', 'name', 'family_name', 'phone'])
            ->where(['status' => 10])
            ->with('addresses')
            ->asArray();
    }

    public static function getOne($id)
    {
        return self::find()
            ->select(['id', 'email', 'name', 'family_name', 'phone'])
            ->where(['status' => 10, 'id' => $id])
            ->with('addresses')
            ->asArray()
            ->one();
    }
}