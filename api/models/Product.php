<?php
namespace api\models;

use common\models\shop\Product as ProductCommon;
use Yii;

class Product extends ProductCommon
{
    public static function get($language, $id=null)
    {
        Yii::$app->language = $language;
        $query = self::find()
            ->select(['id', 'category_id', 'slug', 'article', 'price', 'status', 'gtin'])
            ->with(['activeLanguage' => function($query) {
                return $query->select(['language', 'product_id', 'name']);
            }])
            ->with(['category' => function($query) {
                return $query->select(['id', 'slug'])->with(['activeLanguage' => function($query) {
                    return $query->select(['language', 'category_id', 'name']);
                }]);
            }]);

        if ($id) {
            $query->where(['product.id' => $id]);
        }

        return $query->asArray();
    }
}