<?php
namespace api\controllers;

use api\models\Order;
use common\models\User;
use developeruz\db_rbac\behaviors\AccessBehavior;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\Cors;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;

class OrderController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['authorization', 'content-type']
            ],
        ];

        $behaviors['authenticator']['class'] = HttpBasicAuth::className();
        $behaviors['authenticator']['auth'] = function ($email, $password) {
            return User::findByEmailAndPassword($email, $password);
        };

        $behaviors['access'] = [
            'class' => AccessBehavior::className(),
        ];

        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'index' => ['GET'],
                'view' => ['GET'],
                'get-statuses' => ['GET'],
                'get-delivery' => ['GET'],
                'update' => ['PATCH']
            ],
        ];
        return $behaviors;
    }

    public function actionIndex()
    {
//        return new ActiveDataProvider([
//            'query' => Order::getAll(),
//        ]);
        return Order::getAll()->all();
    }

    public function actionView($id)
    {
        if (($model = Order::view($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetStatuses()
    {
        return Order::getStatuses();
    }

    public function actionGetDelivery()
    {
        return Order::getDeliveryTypes();
    }

    public function actionUpdate($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            if ($model->load(Yii::$app->request->getBodyParams())) {
                return $model->save() ?: $model->errors;
            }
            return false;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}