<?php
namespace api\controllers;

use common\models\payments\Pay;
use common\models\User;
use developeruz\db_rbac\behaviors\AccessBehavior;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\Cors;
use yii\filters\VerbFilter;
use yii\rest\Controller;

class PayController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['authorization', 'content-type']
            ],
        ];

        $behaviors['authenticator']['class'] = HttpBasicAuth::className();
        $behaviors['authenticator']['auth'] = function ($email, $password) {
            return User::findByEmailAndPassword($email, $password);
        };

        $behaviors['access'] = [
            'class' => AccessBehavior::className(),
        ];

        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'view' => ['find-by-order-id'],
            ],
        ];
        return $behaviors;
    }

    public function actionFindByOrderId($order_id)
    {
        return Pay::find()->where(['order_id' => $order_id])->all();
    }
}