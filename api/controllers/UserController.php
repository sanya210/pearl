<?php
namespace api\controllers;

use common\models\User as UserCommon;
use api\models\User;
use developeruz\db_rbac\behaviors\AccessBehavior;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\Cors;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;

class UserController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['authorization', 'content-type']
            ],
        ];

        $behaviors['authenticator']['class'] = HttpBasicAuth::className();
        $behaviors['authenticator']['auth'] = function ($email, $password) {
            return UserCommon::findByEmailAndPassword($email, $password);
        };

        $behaviors['access'] = [
            'class' => AccessBehavior::className(),
        ];

        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'index' => ['GET'],
                'view' => ['GET'],
            ],
        ];
        return $behaviors;
    }

    public function actionIndex()
    {
//        return new ActiveDataProvider([
//            'query' => User::getAll(),
//        ]);
        return User::getAll()->all();
    }

    public function actionView($id)
    {
        if (($model = User::getOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}