<?php
namespace api\controllers\shop;

use api\models\Product;
use common\models\User;
use developeruz\db_rbac\behaviors\AccessBehavior;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\Cors;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class ProductController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['corsFilter'] = [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['authorization', 'content-type']
            ],
        ];

        $behaviors['authenticator']['class'] = HttpBasicAuth::className();
        $behaviors['authenticator']['auth'] = function ($email, $password) {
            return User::findByEmailAndPassword($email, $password);
        };

        $behaviors['access'] = [
            'class' => AccessBehavior::className(),
        ];

        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'index' => ['GET'],
                'view' => ['GET'],
            ],
        ];
        return $behaviors;
    }

    public function actionIndex($language)
    {
        $this->checkLanguage($language);

        return new ActiveDataProvider([
            'query' => Product::get($language),
        ]);
    }

    public function actionView($language, $id)
    {
        $this->checkLanguage($language);

        if (($model = Product::get($language, $id)->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function checkLanguage($language)
    {
        if (!array_key_exists($language, Yii::$app->params['languages_active'])) {
            throw new BadRequestHttpException('This language is not supported.');
        }
    }
}