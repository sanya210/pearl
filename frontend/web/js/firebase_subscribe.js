function isTokenSentToServer() {
	return window.localStorage.getItem('sentFirebaseMessagingToken');
}

if (!isTokenSentToServer()) {
	firebase.initializeApp({
		messagingSenderId: '1031244801522'
	});

	if ('Notification' in window) {
		var messaging = firebase.messaging();
		setTimeout(function() {
			subscribe();
		}, 2000);
	}
} else {
	auth();
}

function subscribe() {
	messaging.requestPermission()
		.then(function () {
			messaging.getToken()
				.then(function (currentToken) {
					if (currentToken) {
						sendTokenToServer(currentToken);
					} else {
						console.warn('Failed to get token.');
						setTokenSentToServer(false);
					}
				})
				.catch(function (err) {
					console.warn('An error occurred while getting the token.', err);
					setTokenSentToServer(false);
				});
		})
		.catch(function (err) {
			console.warn('Failed to get permission to show notifications.', err);
		});
}

function sendTokenToServer(currentToken) {
	var url = '/add-push-token';
	$.post(url, {
		token: currentToken
	});
	setTokenSentToServer(currentToken);
	auth();
}

function setTokenSentToServer(currentToken) {
	window.localStorage.setItem(
		'sentFirebaseMessagingToken',
		currentToken ? currentToken : ''
	);
}

function auth() {
	let token = isTokenSentToServer();
	$('input[name="LoginForm[push_token]"]').val(token);
	$('#logout_link a').attr('data-params', JSON.stringify({
		token: token
	}));
}
