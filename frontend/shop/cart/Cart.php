<?php

namespace frontend\shop\cart;

use Yii;
use common\models\shop\Product;

class Cart
{
    private $items;
    private $session;

    public function __construct()
    {
        $this->session = Yii::$app->session;
    }

    public function getItems()
    {
        $this->loadItems();
        $ids = [];
        $data = [];
        $total_price = 0;
        foreach ($this->items as $current) {
            $id = $current->getProduct_id();
            $ids[] = $id;
            $data[$id]['sizes'] = $current->getSizes();
            $data[$id]['size_type'] = $current->getSizeType();
        }

        $products = Product::find()
            ->where([
                'id' => $ids,
                'status' => Product::STATUS_ACTIVE
            ])
            ->with(['activeDiscount', 'activeLanguage', 'category', 'sizeByEurope'])
            ->indexBy('id')
            ->all();

        $active_ids = array_column($products, 'id');
        $not_active_ids = array_diff($ids, $active_ids);
        foreach ($not_active_ids as $id) {
            unset($data[$id]);
        }

        foreach ($products as $id => $product) {
            $data[$id]['category_id'] = $product->category_id;
            $data[$id]['photo'] = $product->mainPhoto;
            $data[$id]['name'] = $product->activeLanguage->name;
            $data[$id]['link'] = '/'.$product->category->slug.'/'.$product->slug;
            $data[$id]['article'] = $product->article;
            $data[$id]['price'] = $product->priceValue['active_price'];

            $sizes_translate = [];
            foreach ($data[$id]['sizes'] as $size => $quantity) {
                $sizes_translate[$size] = [
                    'size' => $product->sizeByEurope[$size][$data[$id]['size_type']],
                    'quantity' => $quantity
                ];
            }

            $data[$id]['sizes_translate'] = $sizes_translate;
            $data[$id]['size_label'] = $product->sizeByEurope[$size]->attributeLabels()[$data[$id]['size_type']];

            $total = array_sum($data[$id]['sizes']) * $product->priceValue['price'];
            $total_price += $total;

            $data[$id]['total'] = number_format((float)$total, 2, '. ', ' ');
        }

        return [
            'products' => $data,
            'total' => number_format((float)$total_price, 2, '. ', ' ')
        ];
    }

    public function add(CartItem $item)
    {
        $this->loadItems();
        foreach ($this->items as $i => $current) {
            if($current->getProduct_id() == $item->getProduct_id()) {
                $current->plus($item->getSizes(), $item->getSizeType());
                return;
            }
        }
        $this->items[] = $item;
        $this->saveItems();
    }

    public function update(CartItem $item)
    {
        $this->loadItems();
        foreach ($this->items as $i => $current) {
            if($current->getProduct_id() == $item->getProduct_id()) {
                $this->items[$i] = $item;
                $this->saveItems();
                return;
            }
        }
        $this->items[] = $item;
        $this->saveItems();
    }

    public function remove($product_id)
    {
        $this->loadItems();
        foreach ($this->items as $i => $current) {
            if($current->getProduct_id() == $product_id) {
                unset($this->items[$i]);
                $this->saveItems();
                return;
            }
        }
    }

    public function clear()
    {
        $this->session->set('cart', []);
    }

    private function loadItems()
    {
        if($this->items === null) {
            $this->items = $this->session->get('cart', []);
        }
    }

    public function saveItems()
    {
        $this->session->set('cart', $this->items);
    }
}






















