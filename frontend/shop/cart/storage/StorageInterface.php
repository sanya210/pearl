<?php

namespace frontend\shop\cart\storage;

interface StorageInterface
{
   /**
     * @return CartItem[]
     */
   public function load(): array;
   /**
     * @param CartItem[] $items
     */
   public function save(array $items);
}