<?php

namespace frontend\shop\cart;

class CartItem
{
	private $product_id;
	private $sizes;
	private $size_type;

	public function __construct($product_id, $sizes, $size_type)
	{
		$this->product_id = $product_id;
		$this->sizes = $sizes;
		$this->size_type = $size_type;
	}

	public function getProduct_id()
	{
		return $this->product_id;
	}

	public function getSizes()
	{
		return $this->sizes;
	}

    public function getSizeType()
    {
        return $this->size_type;
    }

	public function plus($sizes, $size_type)
	{
        foreach ($sizes as $size => $current) {
            if (isset($this->sizes[$size])) {
                $this->sizes[$size] += $current;
            } else {
                $this->sizes[$size] = $current;
            }
        }
        $this->size_type = $size_type;
	}
}





























