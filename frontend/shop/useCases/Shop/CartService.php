<?php

namespace frontend\shop\useCases\Shop;

use frontend\shop\cart\Cart;
use frontend\shop\cart\CartItem;
class CartService
{
	private $cart;

	public function __construct(Cart $cart)
	{
		$this->cart = $cart;
	}

	public function getCart()
	{
		return $this->cart;
	}

	public function add($product_id, $sizes, $size_type)
	{
		$this->cart->add(new CartItem($product_id, $sizes, $size_type));
	}

	public function update($product_id, $sizes, $size_type)
	{
		$remove = true;
		foreach ($sizes as $size) {
			if($size > 0) {
				$remove = false;
				break;
			}
		}
		if($remove) {
			$this->cart->remove($product_id);
		} else {
			$this->cart->update(new CartItem($product_id, $sizes, $size_type));
		}
	}

	public function clear()
	{
		$this->cart->clear();
	}
}



