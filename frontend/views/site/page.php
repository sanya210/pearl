<?php
use yii\widgets\Breadcrumbs;

$this->title = $model['meta_title'];
$this->registerMetaTag(['name' => 'description', 'content' => $model['meta_description']]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $model['meta_keywords']]);

$this->params['breadcrumbs'][] = $model['name'];

?>

<?=Breadcrumbs::widget([
   'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]) ?>

<div class="page-index">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="news_h1">
					<span><?=$model['name']?></span>
					<h1><?=$model['name']?></h1>
				</div>
				<?=$model['text']?>
			</div>
		</div>
	</div>
</div>































