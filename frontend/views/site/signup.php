<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

$this->title = Yii::t('pages', 'Signup');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <?=Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>

    <div class="site-signup main-form">
        <h1 class="main-form__h1"><?= Html::encode($this->title) ?></h1>

        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

        <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => $model->getAttributeLabel('name').' *'])->label(false) ?>

        <?= $form->field($model, 'email')->textInput(['placeholder' => $model->getAttributeLabel('email').' *'])->label(false) ?>

        <?= $form->field($model, 'password')->passwordInput(['placeholder' => $model->getAttributeLabel('password').' *'])->label(false) ?>

        <?= $form->field($model, 'password_repeat')->passwordInput(['placeholder' => Yii::t('pages', 'Confirm password').' *'])->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('pages', 'Create account'), ['class' => 'btn main-form__btn', 'name' => 'signup-button']) ?>
        </div>

        <p class="signup-terms-text"><?=Yii::t('pages', 'By registering, you agree to the ')?> <?=Html::a(Yii::t('pages', 'user agreement'), ['/'])?></p>

        <?php ActiveForm::end(); ?>

    </div>

</div>
