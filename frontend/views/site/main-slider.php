<?php
use kv4nt\owlcarousel\OwlCarouselWidget;
use common\models\MainSlider;
use yii\helpers\Html;

?>

<div class="main-slider">
	<?php OwlCarouselWidget::begin([
		'container' => 'div',
		'pluginOptions' => [
			'autoplay' => true,
			'autoplayTimeout' => 5000,
			'items' => 1,
	      'loop' => true,
			'responsiveClass' => true,
			'dots' => true,
			'autoHeight' => true
		]
	]); ?>

	<?php foreach (MainSlider::getActiveSlides() as $slide) { ?>
		<div class="main-slider__item">
			<picture>
			  <source media="(max-width: 767px)" srcset="/uploads/slider/<?=$slide->img_xs?>">
			  <img src="/uploads/slider/<?=$slide->img_lg?>" class="main-slider__img" alt="<?=$slide->activeLanguage['header']??''?>">
			</picture>
						
			<div class="main-slider__wrapper">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="main-slider__header">
								<?=$slide->activeLanguage['header']??''?>
							</div>
							<div class="main-slider__text">
								<?=$slide->activeLanguage['text']??''?>
							</div>
							<a href="<?=$slide->activeLanguage['link']??''?>" class="main-slider__link"><?=Yii::t('pages', 'Interesting')?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>

	<?php OwlCarouselWidget::end(); ?>
</div>






























