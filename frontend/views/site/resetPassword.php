<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Breadcrumbs;

$this->title = Yii::t('pages', 'Reset password');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <?=Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>

    <div class="site-reset-password main-form">
        <h1 class="main-form__h1"><?= Html::encode($this->title) ?></h1>

        <p><?=Yii::t('pages', 'Please choose your new password:')?></p>

        <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

        <?= $form->field($model, 'password')->passwordInput(['placeholder' => $model->getAttributeLabel('password').' *'])->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('pages', 'Save'), ['class' => 'btn main-form__btn', 'name' => 'signup-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
