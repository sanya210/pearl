<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Breadcrumbs;

$this->title = Yii::t('pages', 'Resend verification email');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <?=Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>

    <div class="site-resend-verification-email main-form">
        <h1 class="main-form__h1"><?= Html::encode($this->title) ?></h1>

        <p><?=Yii::t('pages', 'Please fill out your email. A verification email will be sent there.')?></p>

        <?php $form = ActiveForm::begin(['id' => 'resend-verification-email-form']); ?>

        <?= $form->field($model, 'email')->textInput(['placeholder' => $model->getAttributeLabel('email').' *'])->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('pages', 'Send'), ['class' => 'btn main-form__btn']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
