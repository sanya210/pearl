<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$this->title = $name;
$this->params['breadcrumbs'][] = Yii::t('pages', 'Error');
?>

<div class="site-error">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?=Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>

                <?php if($exception->statusCode == '404') { ?>
                    <div class="error-404">
                        <div class="error-404__text">
                            <div class="error-404__text1">Oops!</div>
                            <div class="error-404__text2">Error 404 - Page Not Found</div>
                            <div class="error-404__text3"><?= nl2br(Html::encode($message)) ?></div>
                        </div>
                        <img src="/img/404.jpg" class="img-404" alt="404">
                    </div>
                <?php } else { ?>
                    <h1 class="error__h1"><?= Html::encode($this->title) ?></h1>

                    <div class="alert alert-danger">
                        <?= nl2br(Html::encode($message)) ?>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>
</div>
