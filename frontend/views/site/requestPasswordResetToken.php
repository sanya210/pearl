<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Breadcrumbs;

$this->title = Yii::t('pages', 'Request password reset');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <?=Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>

    <div class="site-request-password-reset main-form">
        <h1 class="main-form__h1"><?= Html::encode($this->title) ?></h1>

        <p><?=Yii::t('pages', 'Please fill out your email. A link to reset password will be sent there.')?></p>

        <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

        <?= $form->field($model, 'email')->textInput(['placeholder' => $model->getAttributeLabel('email')])->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('pages', 'Send'), ['class' => 'btn main-form__btn']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
