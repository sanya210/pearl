<?php
use yii\widgets\Breadcrumbs;
use frontend\components\PaginationWidget;
use frontend\models\shop\ProductElasticSearch;

$this->title = 'Pearl - ' . Yii::t('pages', 'Searching results') . ': "'.$text.'" | ' . Yii::t('pages', 'Search');
$this->registerMetaTag(['name' => 'description', 'content' => $this->title]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $this->title]);
$this->registerMetaTag(['name' => 'robots', 'content' => 'noindex,nofollow']);

$this->params['breadcrumbs'][] = Yii::t('pages', 'Search');

?>

<?=Breadcrumbs::widget([
	'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]) ?>

<div class="search-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="search-page__count">
					<?=Yii::t('pages', 'Found {number} product(s)', [
						'number' => $pages->totalCount
					]) ?>
				</div>
				<?php if($text) { ?>
					<h1 class="search-page__h1"><?=$text?></h1>
				<?php } ?>

				<?php if($pages->totalCount === 0) { ?>
					<div class="search-page__not-found">
						<?=Yii::t('pages', 'No results found') ?>
					</div>
				<?php } ?>
			</div>	
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="category_list">
					<?php
					foreach ($products as $product) {
						echo $this->render('/product/product', [
							'product' => $product
						]);		
					}
					?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<?=PaginationWidget::widget([
					'pagination' => $pages,
					'page_size' => ProductElasticSearch::PAGE_SIZE
				]) ?>
			</div>
		</div>
	</div>
</div>



















