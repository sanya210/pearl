<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\Breadcrumbs;

$this->title = Yii::t('header', 'Contacts').' '.Yii::$app->name;
$this->registerMetaTag(['name' => 'description', 'content' => Yii::t('header', 'Contacts').' '.Yii::$app->name]);
$this->params['breadcrumbs'][] = Yii::t('header', 'Contacts');
?>

<div class="site-contact">
    <div class="container">
        <div class="row">
            <div class="col-md-12">        
                <?=Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="contact__wrapper">
                    <h1 class="contact__h1"><?=Yii::t('header', 'Contacts')?></h1>
                        
                    <div class="contact__label">
                        <label>Email</label>
                        <a href="mailto:<?=Yii::$app->params['supportEmail']?>"><?=Yii::$app->params['supportEmail']?></a>
                    </div>

                    <div class="contact__label">
                        <label><?=Yii::t('pages', 'Germany')?></label>
                        <p>Xantener str.2, Berlin, Germany 10707, PFG GmbH</p>
                        <a href="tel:‎+4917216329362">‎+4917216329362</a>
                    </div>

                    <div class="contact__label">
                        <label><?=Yii::t('pages', 'Ukraine')?></label>
                        <p>Shkil'na St, 3, Ilichanka, Odessa Oblast, 67582</p>
                        <a href="tel:<?=Yii::$app->params['phone']?>"><?=Yii::$app->params['phone']?></a>
                    </div>

                    <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>

                        <?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => Yii::t('pages', 'Name')])->label(false) ?>

                        <?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->label(false) ?>

                        <?= $form->field($model, 'subject')->textInput(['placeholder' => Yii::t('pages', 'Subject')])->label(false) ?>

                        <?= $form->field($model, 'body')->textarea(['rows' => 6, 'placeholder' => Yii::t('pages', 'Message')])->label(false) ?>

                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('pages', 'Submit'), ['class' => 'btn main-form__btn', 'name' => 'contact-button']) ?>
                        </div>

                    <?php ActiveForm::end(); ?>
                </div>

            </div>
        </div>
    </div>
</div>


