<?php
use frontend\components\NewsWidget;
use common\models\Text;
use yii\helpers\Url;

$this->title = strip_tags(Text::getText('main_title'));
$this->registerMetaTag(['name' => 'description', 'content' => strip_tags(Text::getText('main_description'))]);

$this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
$this->registerMetaTag(['property' => 'og:url', 'content' => Url::current([], true)]);
$this->registerMetaTag(['property' => 'og:description', 'content' => strip_tags(Text::getText('main_description'))]);

?>

<div class="main-index">
	<?=$this->render('main-slider')?>

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?=$this->render('/product/top5')?>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<?=NewsWidget::widget([
					'header' => Yii::t('pages', 'Blog')
				]) ?>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<?=Text::getText('main_page')?>
			</div>
		</div>
	</div>
</div>



