<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use common\models\Text;

$this->title = Yii::t('pages', 'Successful payment');

$this->params['breadcrumbs'][] = $this->title;

?>

<div class="payment-index">
	<div class="container">
		<?=Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>
		<div class="payment-success">		
			<h1 class="payment-success__h1">
				<?=Yii::t('pages', 'Successful payment')?>
			</h1>
			<div class="payment-success__text">
				<?=Text::getText('pay_success')?>
			</div>
			<img src="/img/success.jpg" alt="">
		</div>
	</div>
</div>


























