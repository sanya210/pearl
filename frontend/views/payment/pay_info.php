<div class="pay_info">
	<div class="pay_info__number">
		<?=Yii::t('pages', 'Order')?> #<?=$order->id?>
	</div>

	<div class="pay_info__text">
		<?=Yii::t('pages', 'Payment currency')?>: <span><?=$order->currency?></span>
	</div>

	<div class="pay_info__text">
		<?=Yii::t('pages', 'Amount of payment')?>: <span><?=$order->total?> <?=$order->currency?></span>
	</div>
</div>