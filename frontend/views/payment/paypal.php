<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;

$this->title = Yii::t('pages', 'Payment').' LiqPay';

$this->params['breadcrumbs'][] = $this->title;

?>

<div class="payment-index">
	<div class="container">
		<?=Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>
		<div class="row">
			<div class="col-md-12">
				<?=$this->render('pay_info', ['order' => $order])?>
				<?=Html::a('', ['/paypal-send'], [
					'class' => 'paypal',
					'data' => [
						'method' => 'post',
						'params' => [
							'order_id' => $order->id
						]
					]
				]) ?>
			</div>
		</div>
	</div>
</div>







