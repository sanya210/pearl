<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;

$this->title = Yii::t('pages', 'Payment');

$this->params['breadcrumbs'][] = $this->title;

?>

<div class="payment-index">
	<div class="container">
		<?=Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>
		<div class="row">
			<div class="col-md-12">
				<h4><?=Yii::t('pages', 'Select a Payment Method')?>:</h4>
				<ul class="payment__items">
					<?php if($currency != 'UAH') { ?>
					<li class="payment__item">
						<?=Html::a('', ['/paypal'], [
							'class' => 'payment__btn btn-paypal',
							'data' => [
								'method' => 'post',
								'params' => [
									'order_id' => $order_id
								]
							]
						]) ?>
					</li>
					<?php } ?>
					<li class="payment__item">
						<?=Html::a('', ['/liqpay'], [
							'class' => 'payment__btn btn-liqpay',
							'data' => [
								'method' => 'post',
								'params' => [
									'order_id' => $order_id
								]
							]
						]) ?>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>















