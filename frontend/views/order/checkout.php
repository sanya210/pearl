<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = Yii::t('pages', 'Checkout');
$this->params['breadcrumbs'][] = $this->title;



?>

<div class="order-index">
	<div class="container">
		<?=Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>

		<div class="main-form">
			<h1 class="main-form__h1"><?= Html::encode($this->title) ?></h1>

			<?php $form = ActiveForm::begin(['id' => 'form-checkout']); ?>

			<?= $form->field($model, 'user_name')->textInput([
				'value' => !Yii::$app->user->isGuest ? Yii::$app->user->identity->name : '', 
				'autofocus' => true, 
				'placeholder' => $model->getAttributeLabel('user_name').' *'
			])->label(false) ?>
			
			<?= $form->field($model, 'user_family_name')->textInput([
				'value' => !Yii::$app->user->isGuest ? Yii::$app->user->identity->family_name : '', 
				'autofocus' => true, 
				'placeholder' => $model->getAttributeLabel('user_family_name').' *'
			])->label(false) ?>

			<?= $form->field($model, 'user_phone')->textInput([
				'value' => !Yii::$app->user->isGuest ? Yii::$app->user->identity->phone : '', 
				'autofocus' => true, 
				'placeholder' => $model->getAttributeLabel('user_phone').' *'
			])->label(false) ?>

			<?= $form->field($model, 'address_id')->dropDownList(
				ArrayHelper::map(Yii::$app->user->identity->addresses, 'id', 'delivery_address')
			)->label(false) ?>

			<?= $form->field($model, 'note')->textarea(['rows' => 2, 'placeholder' => Yii::t('pages', 'Note')])->label(false) ?>

			<ul class="checkout__items">
			<?php foreach($model->products as $product) { ?>
				<li class="checkout__item">
					<div class="checkout__wrapper">
						<img src="/uploads/product/preview/<?=$product['photo']?>" class="checkout__photo" alt="<?=$product['name']?>">
						<?=Html::a($product['name'], [$product['link']], ['class' => 'checkout__link']); ?>
                        <div class="cart-sizes">
                            <?=Yii::t('product', 'Sizes and quantity')?> (<?=$product['size_label']?>):
                            <?php foreach ($product['sizes_translate'] as $size) { ?>
                                <div><?=$size['size']?> - <span><?=$size['quantity']?></span>;</div>
                            <?php } ?>
                        </div>
					</div>
					<div class="checkout__price"><?=Yii::$app->currency->sign?> <?=$product['total']?></div>
				</li>
			<?php } ?>
			</ul>

			<div class="checkout__total">
				<?=Yii::t('pages', 'Total')?>: <span><?=Yii::$app->currency->sign?> <?=$model->total?></span>
			</div>

			<div class="form-group">
				<?= Html::submitButton(Yii::t('pages', 'Confirm checkout'), ['class' => 'btn main-form__btn', 'name' => 'signup-button']) ?>
			</div>

			<?php ActiveForm::end(); ?>

		</div>
	   
	</div>
</div>

















