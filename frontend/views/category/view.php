<?php
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\bootstrap\Dropdown;
use frontend\components\PaginationWidget;
use frontend\models\shop\ProductSearch;
use frontend\components\SortProductWidget;
use frontend\components\FilterWidget;
use frontend\components\RangeWidget;
use common\models\shop\Color;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;

$this->title = $translation['meta_title'];
$this->registerMetaTag(['name' => 'description', 'content' => $translation['meta_description']]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $translation['meta_keywords']]);
if($canonical) {
	Yii::$app->view->registerLinkTag(['rel' => 'canonical', 'href' => $canonical]);
}

$this->params['breadcrumbs'][] = $translation['name'];

?>

<?=Breadcrumbs::widget([
	'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]) ?>

<?php Pjax::begin(['id' => 'pjax-category']); ?>
	<div class="category">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="category__top">
						<div class="row">
							<div class="col-md-3 col-sm-4 col-xs-8">
								<?=SortProductWidget::widget([
									'data' => ProductSearch::SORT_ITEMS,
									'slug' => $model->slug
								]) ?>
							</div>

							<div class="col-md-9 col-sm-8 col-xs-4">
								<div class="category__mosaic">
									<button type="button" class="btn-filter"><i class="fa fa-filter"></i> <?=Yii::t('category', 'Filter')?></button>
									<ul class="category__mosaic-list">
										<li>
											<button class="category__mosaic-btn" data-column="3"></button>
										</li>
										<li>
											<button class="category__mosaic-btn" data-column="2"></button>
										</li>
									</ul>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

			<div class="row wrapper-main-category">
				<div class="col-md-3">
					<div class="wrapper-filter">
						<?php
						foreach ($model->attribute0 as $attribute) {
							echo FilterWidget::widget([
								'name' => $attribute->activeLanguage->name,
								'slug' => $attribute->slug,
								'items' => ArrayHelper::map($attribute->attributeValues, 'id', 'activeLanguage.name')
							]);
						}
						?>

						<?=FilterWidget::widget([
							'name' => Yii::t('category', 'Color'),
							'slug' => 'color',
							'items' => ArrayHelper::map(Color::getColors(), 'id', 'activeLanguage.name')
						]) ?>

						<?=RangeWidget::widget([
							'name' => 'category-price'
						]) ?>
					</div>
				</div>

				<div class="col-md-9 col-sm-12">
					<div class="category_list">
						<?php
						foreach ($products as $product) {
							echo $this->render('/product/product', [
								'product' => $product
							]);		
						}
						?>
					</div>

					<?=PaginationWidget::widget([
						'pagination' => $pages,
						'page_size' => ProductSearch::PAGE_SIZE
					]) ?>

				</div>
			</div>
		</div>
	</div>
<?php Pjax::end(); ?>









