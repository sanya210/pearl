<?php
use common\models\shop\Product;

$products = Product::getTop5();
?>

<div class="top5">
	<div class="news-header">
		<span><?=Yii::t('product', 'Top 5 dresses')?></span>
		<h4><?=Yii::t('product', 'Top 5 dresses')?></h4>
	</div>

	<div class="top5__items">
		<div class="top5__top5__double-item">
		<?php foreach ($products as $key => $product) { ?>
			<?php if($key == 2) { ?>
			</div>
			<?php } ?>
			<div class="top5__item">
			<?=$this->render('product', [
				'product' => $product
			]) ?>
			</div>
			<?php if($key == 2) { ?>
			<div class="top5__top5__double-item">
			<?php } ?>
		<?php } ?>
		</div>
	</div>
</div>



















