<?php
use yii\widgets\Breadcrumbs;
use frontend\assets\ProductAsset;
use frontend\components\ColorWidget;
use frontend\components\SizeTableWidget;
use frontend\components\SliderWidget;
use yii\helpers\Html;
use yii\helpers\Url;

ProductAsset::register($this);

$this->title = $translation['meta_title'];
$this->registerMetaTag(['name' => 'description', 'content' => $translation['meta_description']]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $translation['meta_keywords']]);

$this->registerMetaTag(['property' => 'og:type', 'content' => 'product']);
$this->registerMetaTag(['property' => 'og:title', 'content' => $this->title]);
$this->registerMetaTag(['property' => 'og:url', 'content' => Url::current([], true)]);
$this->registerMetaTag(['property' => 'og:description', 'content' => $translation['meta_description']]);
$this->registerMetaTag(['property' => 'og:image', 'content' => '/uploads/product/preview/'.$model->mainPhoto]);

$this->params['breadcrumbs'][] = ['label' => $model->category->translation['name'], 'url' => ['/'.$model->category->slug]];
$this->params['breadcrumbs'][] = $translation['name'];

?>

<?=Breadcrumbs::widget([
   'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]) ?>

<div class="container">
	<div class="product">
		<div class="row">
			<div class="col-md-6">
				<div class="product__wrapper">
					<?php if(!Yii::$app->user->isGuest) { ?>
						<button class="card-wishlist" data-status="<?=$model->wishlist ? 'in' : 'out'?>" data-product_id="<?=$model->id?>"><i class="fa <?=$model->wishlist ? 'fa-heart' : 'fa-heart-o'?>"></i></button>
						<div class="whishlist-success"></div>
					<?php } ?>
					<?php if($model->attachment) { ?>
					<div class="sp-wrap">
						<?php foreach ($model->attachment as $image) { ?>
						<a href="/uploads/product/<?=$image->name?>">
							<img src="/uploads/product/preview/<?=$image->name?>" alt="<?=$translation['name']?>">	
						</a>
						<?php } ?>
					</div>
					<?php } else { ?>
						<img src="/uploads/product/preview/<?=$model->mainPhoto?>" class="product__default" alt="">
					<?php } ?>
				</div>
			</div>

			<div class="col-md-6">
				<div class="wrap-product">
					<h1 class="product__h1"><?=$translation['name']?></h1>
					<div class="product__info">
						<span><?=Yii::t('product', 'Article')?>: <?=$model->article?></span>
						<span><?=Yii::t('product', 'Bought more than')?> <?=$model->number_purchased?></span>
					</div>
					<div class="product__price">
						<span class="product__price-value"><?=Yii::$app->currency->sign?> <?=$price['active_price']?></span>
						<?php if($price['isDiscount']) { ?>
						<span class="product__price-old">$<?=$model->priceValue['old_price']?></span>
						<span class="product__date-finish"><?=Yii::t('product', 'Left')?> <span><?=$model->priceValue['date_finish']?></span> <?=Yii::t('product', 'days')?></span>
						<?php } ?>
					</div>

                    <div class="product__shipping">
                        <p><?=Yii::t('product', 'Free shipping worldwide')?></p>
                    </div>

                    <div class="product__colors">
						<label><?=Yii::t('product', 'Color')?>:</label>
						<?= ColorWidget::widget(['colors' => $model->colors]) ?>
					</div>

					<?php if ($model->status == $model::STATUS_ACTIVE) { ?>
					<div class="product__size">
						<div class="product__size-top">
							<label><?=Yii::t('product', 'Size')?>:</label>
							<?= SizeTableWidget::widget([
							    'sizes' => $model->sizes
							]) ?>
						</div>

						<?=$this->render('_form-cart.php', [
							'product_id' => $model->id
						])?>
					</div>
					<?php } else { ?>
					<p class="product__not-available"><?=Yii::t('product', 'Not available')?></p>
					<?php } ?>

				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
	
			<div class="product__main">
				<ul class="nav nav-tabs product__tabs" role="tablist">
					<li role="presentation" class="product__tab active">
						<a href="#description" aria-controls="description" role="tab" data-toggle="tab">
							<?=Yii::t('product', 'Description')?>
						</a>
					</li>

					<li role="presentation" class="product__tab">
						<a href="#categories" aria-controls="categories" role="tab" data-toggle="tab">
							<?=Yii::t('product', 'Categories')?>
						</a>
					</li>
				</ul>

				<div class="tab-content product__tab-content">
					<div role="tabpanel" class="tab-pane active" id="description">
						<div class="row">
							<div class="col-md-6">
								<?=$translation['description']?>
							</div>
							
							<?php if($model->video) { ?>
							<div class="col-md-6">
								<div class="product__video">
									<?=$model->video ?>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
<!-- 					<div role="tabpanel" class="tab-pane" id="reviews">
						reviews
					</div>
 -->					<div role="tabpanel" class="tab-pane" id="categories">
						<?php 
						foreach ($pseudoCategories as $key => $category) {
							$length = count($pseudoCategories);
							echo Html::a($category->name, ['/'.$category->slug], ['class' => 'product__category-link']);
							if($length > $key + 1) echo ', ';
						}
						?>
					</div>
				</div>
			</div>

		</div>
	</div>

	<?php if($model->similarProducts) { ?>
	<div class="row">
		<div class="col-md-12">
			<?=SliderWidget::widget([
				'models' => $model->similarProducts,
				'header' => Yii::t('product', 'Similar products')
			]) ?>
		</div>
	</div>
	<?php } ?>
	
</div>
