<?php
use yii\helpers\Html;
use yii\helpers\Url;

$price = $product->priceValue;

?>

<div class="card">
	<?php if(!Yii::$app->user->isGuest) { ?>
	<button class="card-wishlist" data-status="<?=$product->wishlist ? 'in' : 'out'?>" data-product_id="<?=$product->id?>"><i class="fa <?=$product->wishlist ? 'fa-heart' : 'fa-heart-o'?>"></i></button>
	<div class="whishlist-success"></div>
	<?php } ?>
	<?php if($price['isDiscount']) { ?>
		<span class="card__sale">-<?=$price['percent']?>%</span>
	<?php } ?>
	<a href="<?=Url::to(['/'.$product->category->slug.'/'.$product->slug])?>">
		<img src="/uploads/product/preview/<?=$product->mainPhoto ?>" alt="<?=$product->translation['name']?>">
	</a>
	<div class="card__bottom">
		<?=Html::a($product->translation['name'], ['/'.$product->category->slug.'/'.$product->slug], ['class' => 'card__link']); ?>

		<div class="card__price">
			<span class="card__price-value"><?=Yii::$app->currency->sign?> <?=$price['active_price']?></span>
			<?php if($price['isDiscount']) { ?>
				<span class="card__price-old">$<?=$price['old_price']?></span>
			<?php } ?>
		</div>

		<?=Html::a('', ['/'.$product->category->slug.'/'.$product->slug], ['class' => 'card__btn-backet']); ?>
	</div>
</div>