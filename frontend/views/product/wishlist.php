<?php
use yii\widgets\Breadcrumbs;
use frontend\models\shop\ProductSearch;
use frontend\components\PaginationWidget;

$this->title = Yii::t('pages', 'Wishlist');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="wishlist-index">
	<div class="container">
		<?=Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>

		<div class="category_list">
			<?php foreach ($products as $product) {
				echo $this->render('/product/product', [
					'product' => $product
				]);		
			} ?>
		</div>

		<?=PaginationWidget::widget([
			'pagination' => $pages,
			'page_size' => ProductSearch::PAGE_SIZE
		]) ?>
	</div>
</div>































