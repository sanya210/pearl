<?php

use frontend\components\SizeWidget;
use yii\widgets\ActiveForm;
use frontend\models\shop\AddToCartForm;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\Text;

$model = new AddToCartForm($product_id);
?>

<?php $form = ActiveForm::begin([
    'action' => Url::to(['/cart/add', 'product_id' => $product_id]),
    'method' => 'post',
    'options' => [
        'class' => 'cart-add-form',
    ],
    'fieldConfig' => [
        'options' => [
            'tag' => false,
        ],
    ],
]) ?>

<?=SizeWidget::widget([
    'form' => $form,
    'model' => $model,
    'sizes' => $model->sizes_value,
])?>

<p class="product__warning">
    <?= strip_tags(Text::getText('product')) ?>
</p>

<div class="product__btn-line">
    <div class="product__cart-note">
        <?= Yii::t('product', 'You must choose a size.') ?>
    </div>
    <?= Html::submitButton(Yii::t('product', 'In bag'), ['class' => 'product__btn product__btn-in-bag']) ?>
</div>

<?php ActiveForm::end(); ?>


<div class="modal fade" tabindex="-1" role="dialog" id="modal-cart" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div class="modal-cart-img"></div>
                <div class="modal-cart-name"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-close"
                        data-dismiss="modal"><?= Yii::t('pages', 'Continue shopping') ?></button>
                <?= Html::a(Yii::t('pages', 'Go to cart'), ['/cart'], ['class' => 'btn']); ?>
            </div>
        </div>
    </div>
</div>












