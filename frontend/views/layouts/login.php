<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\models\LoginForm;
use yii\helpers\Url;
use yii\authclient\widgets\AuthChoice;

$model = new LoginForm();
?>

<div class="modal fade" id="modal-login" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?=Yii::t('pages', 'Welcome, <span>Please log in to continue</span>')?></h4>
            </div>
            <div class="modal-body">
                <?=AuthChoice::widget([
                 'baseAuthUrl' => ['/auth'],
                 'popupMode' => false,
                 ]) ?>

                <div class="modal-login__or"><?=Yii::t('pages', 'or')?></div>

            	<?php $form = ActiveForm::begin([
            		'id' => 'login-form',
            		'enableAjaxValidation' => true,
                    'validateOnBlur' => false,
                    'validateOnChange' => false,
                    'action' => Url::to(['/login'])
            	]); ?>

            	<?= $form->field($model, 'email')->textInput(['placeholder' => $model->getAttributeLabel('email')])->label(false) ?>

                <?= $form->field($model, 'password')->passwordInput(['placeholder' => $model->getAttributeLabel('password')])->label(false) ?>

                <?= $form->field($model, 'push_token')->hiddenInput()->label(false) ?>

            	<div class="form-group">
            		<?= Html::submitButton(Yii::t('pages', 'Sign in'), ['class' => 'btn site-login__btn', 'name' => 'signup-button']) ?>
            	</div>

                <div class="login-terms-text">
                    <p><?=Html::a(Yii::t('pages', 'To register'), ['/signup'])?>.</p>
                    <p><?=Yii::t('pages', 'If you forgot your password you can')?> <?=Html::a(Yii::t('pages', 'reset it'), ['/request-password-reset'])?>.</p>
                    <p><?=Yii::t('pages', 'Need new verification email?')?> <?=Html::a(Yii::t('pages', 'Resend'), ['/resend-verification-email'])?>.</p>
                </div>


            	<?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>