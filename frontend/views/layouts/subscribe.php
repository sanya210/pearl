<?php
use common\models\Email;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;

$model = new Email();
?>


<div class="subscribe">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<h3 class="subscribe__h3"><?=Yii::t('pages', 'Subscribe to our newsletter')?></h3>
				<?php $form = ActiveForm::begin([
					'action' => Url::to(['/subscribe']),
					'options' => ['class' => 'subscribe__form']
				]); ?>
				<div class="success"><span></span></div>

				<?=$form->field($model, 'email')->textInput(['placeholder' => Yii::t('pages', 'Your e-mail')])->label(false)?>
				
				<?= Html::submitButton(Yii::t('pages', 'Send'), ['class' => 'btn btn-subscribe']) ?>
				<?php ActiveForm::end(); ?>
			</div>
			<div class="col-sm-6">
				<h3 class="subscribe__h3"><?=Yii::t('pages', 'Our contacts')?></h3>
				<div class="subscribe__contact-wrapper">
					<div class="subscribe__contact"><?=Yii::t('pages', 'Phone in Germany')?>: <a href="tel:‎+4917216329362">‎+4917216329362</a></div>
					<div class="subscribe__contact"><?=Yii::t('pages', 'Phone in Ukraine')?>: <a href="tel:<?=Yii::$app->params['phone']?>"><?=Yii::$app->params['phone']?></a></div>
					<div class="subscribe__contact">E-mail: <a href="mailto:<?=Yii::$app->params['supportEmail']?>"><?=Yii::$app->params['supportEmail']?></a></div>
				</div>
				<?=Html::a(Yii::t('pages', 'Write'), ['/contacts'], ['class' => 'btn btn-subscribe btn-subscribe-link'])?>
			</div>
		</div>
	</div>
</div>


























