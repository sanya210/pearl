<?php

/* @var $this \yii\web\View */
/* @var $content string */

use common\models\shop\Section;
use yii\helpers\Html;
use frontend\assets\AppAsset;

AppAsset::register($this);

$menu_items = [
	[
		'label' => Yii::t('header', 'Home'),
		'url' => '/'
	],
	[
		'label' => Yii::t('header', 'Blog'),
		'url' => '/news'
	],
	[
		'label' => Yii::t('pages', 'Delivery'),
		'url' => '/site/delivery1',
	],
	[
		'label' => 'FAQ',
		'url' => '/site/faq',
	],
	[
		'label' => Yii::t('header', 'Contacts'),
		'url' => '/contacts'
	]
];

$footer_items = [
	[
		'label' => Yii::t('header', 'Home'),
		'url' => '/'
	],
	[
		'label' => Yii::t('header', 'Blog'),
		'url' => '/news'
	],
	[
		'label' => Yii::t('pages', 'Privacy policy'),
		'url' => '/site/privacy-policy'
	],
	[
		'label' => Yii::t('pages', 'Public offer agreement'),
		'url' => '/site/public-offer-agreement'
	],
    [
        'label' => Yii::t('pages', 'Return the goods'),
        'url' => '/site/how-to-return-the-goods'
    ],
	[
		'label' => 'FAQ',
		'url' => '/site/faq',
	],
	[
		'label' => Yii::t('header', 'Contacts'),
		'url' => '/contacts'
	]
];

$sections = [];
foreach (Section::getActive() as $section) {
    $categories = [];
    if ($categories_models = $section->categoriesModels) {
        foreach ($categories_models as $category) {
            $categories[] = [
                'label' => $category->translation['name'],
                'url' => '/'.$category->slug
            ];
        }
    }
    $sections[] = [
        'label' => $section->translation['name'],
        'categories' => $categories
    ];
}

$sections[] = [
    'label' => Yii::t('header', 'Sales'),
    'url' => '/wedding_dresses?discount=15'
];
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" prefix="og: http://ogp.me/ns#">
<head>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-PSNBW87');</script>
	<!-- End Google Tag Manager -->

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-143189464-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('set', {'user_id': 'USER_ID'});
		gtag('config', 'UA-143189464-1');
	</script>
	
	<?php
	if(Yii::$app->params['translate']) {
		$host = Yii::$app->request->hostInfo;
		foreach (Yii::$app->params['languages_active'] as $lang => $name) {
			$language = $lang == Yii::$app->params['main_language'] ? '' : '/'.$lang;
			$path = Yii::$app->request->pathInfo ? '/'.Yii::$app->request->pathInfo : '';
			$this->registerLinkTag(['rel' => 'alternate', 'hreflang' => $lang, 'href' => $host.$language.$path]);
		}
	}
	?>

	<meta charset="<?= Yii::$app->charset ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?=$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => '/img/icon.png'])?>
	<?php $this->registerCsrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?=$this->registerMetaTag(['property' => 'og:site_name', 'content' => 'pearl.wedding'])?>
	<?php $this->head() ?>

	<!-- Facebook Pixel Code -->
	<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t,s)}(window, document,'script',
				'https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '442292683360529');
			fbq('track', 'PageView');
	</script>
	<noscript>
		<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=442292683360529&ev=PageView&noscript=1"/>
	</noscript>
	<!-- End Facebook Pixel Code -->
	<script data-ad-client="ca-pub-8167319275174012" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>
<body>
	<?php $this->beginBody() ?>
	<!-- Load Facebook SDK for JavaScript -->
	<div id="fb-root"></div>
	<script>
		window.fbAsyncInit = function() {
			FB.init({
				xfbml            : true,
				version          : 'v5.0'
			});
		};

		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>

		<!-- Your customer chat code -->
		<div class="fb-customerchat"
		attribution=setup_tool
		page_id="2067411150234981"
		theme_color="#d696bb">
	</div>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PSNBW87"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<div class="wrapper">
		<div class="content">
			<?=$this->render('header', [
				'menu_items' => $menu_items,
				'sections' => $sections
			])?>
			<?=$content ?>
		</div>
		
		<?=$this->render('footer', [
			'menu_items' => $footer_items,
		])?>
	</div>
	<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
