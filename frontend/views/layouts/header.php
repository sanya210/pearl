<?php
use yii\helpers\Html;
use frontend\components\LanguageWidget;
use frontend\components\CurrencyWidget;
use frontend\components\LeftMenuWidget;
use frontend\components\MenuWidget;
use frontend\components\CategoryWidget;
use yii\helpers\Url;
use yii\bootstrap\Dropdown;
use common\widgets\Alert;

?>


<?= LeftMenuWidget::widget([
	'items' => $menu_items,
	'sections' => $sections
]) ?>

<header class="header">
	<div class="header__top">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="header__top-wrapper">
						<button type="button" class="hamburger" id="main-hamburger">
							<span></span>
						</button>

						<div class="header__dropdowns">
							<?= LanguageWidget::widget() ?>
							<?= CurrencyWidget::widget() ?>
						</div>
						
						<nav class="main-nav">
							<?= MenuWidget::widget(['items' => $menu_items]) ?>
						</nav>

						<div class="header__top-right">
		
							<?php if(Yii::$app->user->isGuest) { ?>
							<button type="button" class="header__link header__user-link" data-toggle="modal" data-target="#modal-login"></button>
							<?php } else { ?>
							<?=Html::a('', ['/wishlist'], ['class' => 'header__link header__wishlist-link'])?>
							<div class="dropdown header__user-dropdown">
								<a href="#" data-toggle="dropdown" class="header__link header__user-link dropdown-toggle"></a>
								<?=Dropdown::widget([
									'items' => [
										[
											'label' => Yii::t('pages', 'Person area'),
											'url' => Url::to(['/profile'])
										],
										[
											'label' => Yii::t('pages', 'Sign out'),
											'url' => Url::to(['/logout']),
											'options' => [
												'id' => 'logout_link',
											],
											'linkOptions' => ['data-method' => 'post']
										],
									]
								])?>
							</div>
							<?php } ?>

							<?=Html::a('', ['/cart'], ['class' => 'header__link header__cart-link'])?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<?=\frontend\components\AlertWidget::widget() ?>
	</div>
	
	<div class="header__main">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<?=Html::a('', ['/'], ['class' => 'logo'])?>
				</div>
			</div>
		</div>

		<div class="header__category">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="header__category-wrapper">
							<?=CategoryWidget::widget(['menuItems' => $sections]) ?>

							<div class="header__category-search">
								<form class="header__category-search-form" action="<?=Url::to(['/search'])?>">
									<button type="button" class="header__category-search-close">×</button>
									<input type="text" name="text" class="header__category-search-input" placeholder="<?=Yii::t('header', 'Search') ?>">
									<button type="submit" class="header__category-search-btn"></button>
								</form>
								<button class="header__category-search-btn"></button>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		
	</div>

	<div id="loader">
		<div class="range"></div>
	</div>
</header>










