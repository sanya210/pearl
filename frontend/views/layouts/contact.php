<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\models\ContactForm;
use yii\helpers\Url;

$model = new ContactForm();
?>

<div class="modal fade" id="modal-contact" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?=Yii::t('pages', 'Ask a question')?></h4>
            </div>
            <div class="modal-body">

            	<?php $form = ActiveForm::begin([
            		'id' => 'contact-form-modal',
                    'validateOnBlur' => false,
                    'validateOnChange' => false,
                    'action' => Url::to(['/modal-contacts'])
            	]); ?>

            	<div class="success"><span></span></div>

            	<?= $form->field($model, 'name')->textInput(['autofocus' => true, 'placeholder' => Yii::t('pages', 'Name')])->label(false) ?>

            	<?= $form->field($model, 'email')->textInput(['placeholder' => 'Email'])->label(false) ?>

            	<?= $form->field($model, 'body')->textarea(['rows' => 6, 'placeholder' => Yii::t('pages', 'Message')])->label(false) ?>

            	<div class="form-group">
            		<?= Html::submitButton(Yii::t('pages', 'Submit'), ['class' => 'btn main-form__btn', 'name' => 'contact-button']) ?>
            	</div>

            	<?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>

































