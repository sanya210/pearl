<?php
use common\models\Social;
use frontend\components\MenuWidget;
use yii\helpers\Html;

$socials = Social::find()->where(['status' => Social::STATUS_ACTIVE])->orderBy(['priority' => SORT_DESC])->all();
?>

<?=$this->render('subscribe')?>

<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<?=Html::a('', ['/'], ['class' => 'logo logo-white'])?>
				<ul class="footer__social">
					<?php foreach ($socials as $social) { ?>
					<li class="footer__social-item">
						<a href="<?=$social->link?>" class="footer__social-link" target="_blank">
							<img src="/uploads/social/<?=$social->icon?>" alt="<?=$social->name?>" class="footer__social-img">
						</a>
					</li>
					<?php } ?>
				</ul>

				<div class="footer__menu">
					<?= MenuWidget::widget(['items' => $menu_items]) ?>
				</div>
			</div>
		</div>
	</div>
</footer>

<div class="cookie">
	<p class="cookie__text"><?=Yii::t('pages', 'By continuing to use the site, you agree to our use of')?> <?=Html::a('cookies', ['/site/privacy-policy'], ['class' => 'cookie__link'])?>.</p>
	<button class="btn cookie__btn"><?=Yii::t('pages', 'Accept')?></button>
</div>

<?php if (Yii::$app->controller->route != 'site/contacts') { ?>
	<button id="contact_btn" data-toggle="modal" data-target="#modal-contact"><span><?=Yii::t('pages', 'Ask a question')?></span></button>
	<?=$this->render('contact')?>
<?php } ?>

<?php if(Yii::$app->user->isGuest) { 
	echo $this->render('login');
} ?>








