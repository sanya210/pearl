<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use common\models\Order;
use frontend\components\PaginationWidget;

$this->title = Yii::t('pages', 'My orders');

$this->params['breadcrumbs'][] = Yii::t('pages', 'Person area');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="person">
	<div class="container">
		<?=Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>

		<div class="person__wrapper">

			<div class="row">
				<div class="col-md-3">
					<?=$this->render('menu')?>
				</div>

				<div class="col-md-9">
					<ul class="order__items">
					<?php foreach ($orders as $order) { ?>
						<li class="order__item">
							<div class="order__number">
								<?=Yii::t('pages', 'Order')?> #<?=$order->id?>
							</div>
							<?php if($order->status == 1) { ?>
	    						<?php if($order->statusActive) { ?>
		    						<?=Html::a(Yii::t('pages', 'To pay'), ['/pay', 'order_id' => $order->id], ['class' => 'btn-pay']) ?>
			    					<?=Html::a(Yii::t('pages', 'Delete'), ['/delete-order', 'order_id' => $order->id], ['class' => 'btn-pay btn-pay-delete']) ?>
    							<?php } else { ?>
                                    <?=Yii::t('pages', 'Not all products are in stock')?>
    							<?php } ?>
							<?php } ?>
							<ul class="order__info">
								<li>
									<?=Yii::t('pages', 'Total')?>:
									<span><?=$order->total?> <?=$order->currency?></span>
								</li>
								<li>
									<?=Yii::t('pages', 'Date of creation')?>:
									<span><?=date('d.m.Y H:i', $order->created_at)?></span>
								</li>
								<li>
									<?=Yii::t('pages', 'Status')?>:
									<span><?=Yii::t('pages', Order::STATUSES[$order->status])?></span>
								</li>
								<li>
									<?=Yii::t('pages', 'Address')?>:
									<span><?=$order->address->delivery_address ?? ''?></span>
								</li>
							</ul>

							<ul class="order__products">
								<?php foreach ($order->products as $product) { ?>
								<li class="checkout__item">
									<div class="checkout__wrapper">
										<img src="<?=$product['photo']?>" class="checkout__photo" alt="<?=$product['name']?>">
										<?=Html::a($product['name'], [$product['slug']], ['class' => 'checkout__link']); ?>
										<div class="checkout__price"><?=Yii::t('pages', 'Sum')?>: <?=$product['price']?> <?=$order->currency?></div>
                                        <div class="cart-sizes">
                                            <?=Yii::t('product', 'Sizes and quantity')?> (<?=$product['size_label']?>):
                                            <?php foreach ($product['sizes_translate'] as $size) { ?>
                                                <div><?=$size['size']?> - <span><?=$size['quantity']?></span>;</div>
                                            <?php } ?>
                                        </div>
									</div>
								</li>
								<?php } ?>
							</ul>
						</li>
					<?php } ?>
					</ul>

					<?=PaginationWidget::widget([
						'pagination' => $pages,
						'page_size' => $pages->pageSize
					]) ?>
				</div>
			</div>

		</div>
	</div>
</div>








