<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('pages', 'Profile');

$this->params['breadcrumbs'][] = Yii::t('pages', 'Person area');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="person">
	<div class="container">
		<?=Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>

		<div class="person__wrapper">

			<div class="row">
				<div class="col-md-3">
					<?=$this->render('menu')?>
				</div>

				<div class="col-md-9">
					<div class="profile-form main-form">
						<h1 class="main-form__h1"><?= Html::encode($this->title) ?></h1>
						
						<?php $form = ActiveForm::begin(); ?>

						<?= $form->field($model, 'name')->textInput(['placeholder' => $model->getAttributeLabel('name')])->label(false) ?>
						
						<?= $form->field($model, 'email')->textInput(['placeholder' => $model->getAttributeLabel('email')])->label(false) ?>

						<?= $form->field($model, 'phone')->textInput(['placeholder' => $model->getAttributeLabel('phone')])->label(false) ?>

						<div class="form-group">
							<?= Html::submitButton(Yii::t('pages', 'Save'), ['class' => 'btn main-form__btn']) ?>
						</div>
						<?php ActiveForm::end(); ?>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>























