<?php
use frontend\components\PersonAreaMenuWidget;

$menu_items = [
	[
		'label' => Yii::t('pages', 'Profile'),
		'url' => '/profile'
	],
	[
		'label' => Yii::t('pages', 'My addresses'),
		'url' => '/addresses'
	],
	[
		'label' => Yii::t('pages', 'My orders'),
		'url' => '/orders'
	],
	[
		'label' => Yii::t('pages', 'My pays'),
		'url' => '/pays'
	],
	[
		'label' => Yii::t('pages', 'My wishlist'),
		'url' => '/wishlist'
	],
];

?>

<div class="person__menu-wrapper">
	<button type="button" class="hamburger person__menu-toggle" id="pearson-hamburger">
		<span></span>
	</button>
	<?=PersonAreaMenuWidget::widget(['menuItems' => $menu_items]) ?>
</div>














