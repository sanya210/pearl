<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\TabularInput;
use kartik\select2\Select2;
use common\models\Country;

$this->title = Yii::t('pages', 'My addresses');

$this->params['breadcrumbs'][] = Yii::t('pages', 'Person area');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="person">
	<div class="container">
		<?=Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>

		<div class="person__wrapper">

			<div class="row">
				<div class="col-md-3">
					<?=$this->render('menu')?>
				</div>

				<div class="col-md-9">
					<div class="addresses-form main-form">
						<?php $form = ActiveForm::begin([
							'enableAjaxValidation' => true,
							'enableClientValidation' => false,
							'validateOnChange' => false,
							'validateOnSubmit' => true,
							'validateOnBlur' => false,
						]); ?>

						<?= TabularInput::widget([
							'models' => $models,
							'addButtonOptions' => [
								'class' => 'btn btn-multiple'
							],
							'removeButtonOptions' => [
								'class' => 'btn btn-multiple'
							],
							'attributeOptions' => [
								'enableAjaxValidation' => true,
								'enableClientValidation' => false,
								'validateOnChange' => false,
								'validateOnSubmit' => true,
								'validateOnBlur' => false,
							],
							'columns' => [
								[
									'name' => 'id',
									'type' => 'hiddenInput'
								],
								[
									'name'  => 'country_id',
									'type' => Select2::className(),
									'options' => [
										'data' => Country::getCountries(),
										'options' => [
											'placeholder' => Yii::t('pages', 'Select a country...'),
										],
										'maintainOrder' => false
									]
								],
								[
									'name'  => 'delivery_address',
									'options' => [
										'placeholder' => Yii::t('pages', 'Delivery address')
									]
								],
								[
									'name'  => 'delivery_index',
									'options' => [
										'placeholder' => Yii::t('pages', 'Zip code (Postal code)')
									]
								],
							],
						]) ?>

						<div class="form-group">
							<?= Html::submitButton(Yii::t('pages', 'Save'), ['class' => 'btn main-form__btn']) ?>
						</div>
						<?php ActiveForm::end(); ?>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>




































