<?php
use yii\widgets\Breadcrumbs;
use kartik\grid\GridView;
use frontend\components\PaginationWidget;
use common\models\payments\Pay;

$this->title = Yii::t('pages', 'My pays');

$this->params['breadcrumbs'][] = Yii::t('pages', 'Person area');
$this->params['breadcrumbs'][] = $this->title;
date_default_timezone_set('Europe/Kiev');
?>

<div class="person">
	<div class="container">
		<?=Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]) ?>

		<div class="person__wrapper">

			<div class="row">
				<div class="col-md-3">
					<?=$this->render('menu')?>
				</div>

				<div class="col-md-9">
					<div class="pays-index">
						<?= GridView::widget([
							'dataProvider'=> $dataProvider,
							'layout' => '{items}',
							'columns' => [
								[
									'attribute' => 'order_id',
									'label' => Yii::t('pages', 'Order'),
									'hAlign' => 'center',
									'value' => function($model) {
										return '#'.$model->order_id;
									}
								],
								[
									'attribute' => 'amount',
									'label' => Yii::t('pages', 'Sum'),
									'hAlign' => 'center',
									'value' => function($model) {
										return $model->amount.' '.$model->currency;
									}
								],
								[
									'attribute' => 'date',
									'label' => Yii::t('pages', 'Date'),
									'hAlign' => 'center',
									'format' => ['date', 'php:Y.m.d H:i:s']
								],
								[
									'attribute' => 'system',
									'label' => Yii::t('pages', 'System'),
									'hAlign' => 'center',
								],
								[
									'attribute' => 'status',
									'label' => Yii::t('pages', 'Status'),
									'hAlign' => 'center',
								]
							]
						]) ?>

						<?=PaginationWidget::widget([
							'pagination' => $pages,
							'page_size' => Pay::PAGE_SIZE
						]) ?>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>


