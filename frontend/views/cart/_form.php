<?php
use yii\widgets\ActiveForm;
use frontend\components\InputWidget;
use frontend\models\shop\AddToCartForm;
use yii\helpers\Html;

$model = new AddToCartForm($category_id);
$model->setAttributes([
	'product_id' => $id,
	'sizes' => $sizes
]);
?>

<?php $form = ActiveForm::begin([
	'options' => [
		'class' => 'size-form',
		'data-pjax' => true
	],
	'method' => 'post',
	'id' => 'cart-form-'.$id,
	'fieldConfig' => [
      'options' => [
      	'tag' => false,
   	],
   ],
]) ?>

	<?=$form->field($model, 'product_id', ['template' => "{label}\n{input}"])->hiddenInput([
		'value' => $id,
		'id' => 'addtocartform-product_id-'.$id,
	])->label(false)?>

	<?php	foreach ($model->sizes_value as $size) {
		echo InputWidget::widget([
			'label' => $size,
			'id' => 'product-'.$id.'-size-'.$size,
			'form' => $form,
			'model' => $model,
			'name' => 'sizes['.$size.']'
		]);
	} ?>

<?php ActiveForm::end(); ?>









