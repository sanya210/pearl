<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use frontend\components\InputWidget;
use yii\widgets\Pjax;

$this->title = Yii::t('pages', 'Cart');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="cart-index">
	<div class="container">
		<?=Breadcrumbs::widget([
	      'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	   ]) ?>

		<?php Pjax::begin(['id' => 'pjax-cart']); ?>
		<div class="cart-wrapper"></div>
		
		<?php if($products) { ?>
			<ul class="cart-list">
				<?php foreach ($products as $id => $product) { ?>
				<li class="cart-item">
					<img src="/uploads/product/preview/<?=$product['photo']?>" class="cart-photo" alt="<?=$product['name']?>">
					<div class="cart-info">
						<?=Html::a($product['name'], [$product['link']], ['class' => 'card__link']); ?>
						<div class="cart-article"><?=Yii::t('product', 'Article')?>: <?=$product['article']?></div>
						<div class="card-price"><?=Yii::$app->currency->sign?> <?=$product['price']?></div>
					</div>

                    <div class="cart-sizes">
                        <?=Yii::t('product', 'Sizes and quantity')?> (<?=$product['size_label']?>):
                        <?php foreach ($product['sizes_translate'] as $size) { ?>
                            <div><?=$size['size']?> - <span><?=$size['quantity']?></span>;</div>
                        <?php } ?>
                    </div>

					<div class="cart-total">
						<span class="total-label"><?=Yii::t('pages', 'Sum')?></span>
						<?=Yii::$app->currency->sign?> <?=$product['total']?>
					</div>

					<?=Html::a('<i class="fa fa-trash-o"></i>', [''], [
						'class' => 'card-remove',
						'data-method' => 'post',
						'data-params' => [
							'AddToCartForm[product_id]' => $id
	 					]
					]); ?>
					<div class="clear"></div>
				</li>
				<?php } ?>
			</ul>
		

		<div class="cart-bottom">
			<?=Html::a(Yii::t('pages', 'Continue shopping'), ['/'], ['class' => 'btn-cart btn-cart_grey']); ?>
			<div>			
				<div class="cart-bottom__total">
					<?=Yii::t('pages', 'Total')?>: <span><?=Yii::$app->currency->sign?> <?=$total?></span>
				</div>
				<?php if(Yii::$app->user->isGuest) { ?>
					<a class="btn-cart" data-toggle="modal" data-target="#modal-login"><?=Yii::t('pages', 'Checkout')?></a>
				<?php } else { ?>
					<?=Html::a(Yii::t('pages', 'Checkout'), ['/checkout'], ['class' => 'btn-cart']); ?>
				<?php } ?>
			</div>
		</div>
		
		<?php } else { ?>
			<p><?=Yii::t('pages', 'Your cart is empty')?></p>
		<?php } ?>

		<?php Pjax::end(); ?>

	</div>
</div>





















