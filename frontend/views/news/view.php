<?php
use yii\widgets\Breadcrumbs;
use frontend\components\NewsWidget;

$this->title = $model->activeLanguage->meta_title;
$this->registerMetaTag(['name' => 'description', 'content' => $model->activeLanguage->meta_description]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $model->activeLanguage->meta_keywords]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('pages', 'Blog'), 'url' => ['/news']];

?>

<?=Breadcrumbs::widget([
   'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]) ?>


<div class="news-view">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<img src="/uploads/news/<?=$model->img?>" class="news-view__img" alt="<?=$model->activeLanguage->name?>">
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<h1 class="news-view__h1"><?=$model->activeLanguage->name?></h1>
				<div class="news-view__date"><?=Yii::t('pages', 'Date')?>: <?=Yii::$app->formatter->asDate($model->updated_at)?></div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<?=$model->activeLanguage->text?>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<?=NewsWidget::widget([
					'cancel' => $model->id,
					'header' => Yii::t('pages', 'Other articles')
				]) ?>
			</div>
		</div>
	</div>
</div>




























