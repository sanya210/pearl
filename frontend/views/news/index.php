<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use frontend\components\PaginationWidget;
use common\models\News;
use yii\helpers\Url;
use yii\helpers\StringHelper;

$this->title = Yii::t('pages', 'Blog').' '.Yii::$app->name;
$this->registerMetaTag(['name' => 'description', 'content' => Yii::t('pages', 'Blog').' '.Yii::$app->name]);

$this->params['breadcrumbs'][] = Yii::t('pages', 'Blog');

?>

<?=Breadcrumbs::widget([
   'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]) ?>

<div class="news-index">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="news_h1">
					<span><?=Yii::t('pages', 'Blog')?></span>
					<h1><?=Yii::t('pages', 'Blog')?></h1>
				</div>

				<ul class="news__items">
					<?php foreach ($news as $item) { ?>
						<li class="news__item">
 							<div class="news__wrapper1">
								<a href="<?=Url::to(['/news/'.$item->slug])?>">
									<img src="/uploads/news/<?=$item->img_preview?>" class="news__img" alt="<?=$item->activeLanguage->name?>">
								</a>
								<div class="news__bottom">
									<?=Html::a($item->activeLanguage->name, ['/news/'.$item->slug], ['class' => 'news__link'])?>
								</div>
							</div>
							<div class="news__wrapper2">
								<h2 class="news__link"><?=$item->activeLanguage->name?></h2>
								<div class="news__text">
									<p><?=StringHelper::truncate(strip_tags($item->activeLanguage->text), 250)?></p>
								</div>
								<?=Html::a(Yii::t('pages', 'Read'), ['/news/'.$item->slug], ['class' => 'news__btn'])?>		
							</div>
							<div class="news__date"><?=Yii::t('pages', 'Date')?>: <?=Yii::$app->formatter->asDate($item->updated_at)?></div>
						</li>
					<?php } ?>
				</ul>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<?=PaginationWidget::widget([
					'pagination' => $pages,
					'page_size' => News::PAGE_SIZE
				]) ?>
			</div>
		</div>
	</div>
</div>

