<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\shop\Product;
use yii\web\NotFoundHttpException;
use common\models\shop\Size;
use common\models\Wishlist;
use yii\data\Pagination;
use frontend\models\shop\ProductSearch;

/**
 * Category controller
 */
class ProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex($category, $product)
    {
        $model = $this->findModel($category, $product);

        return $this->render('view', [
            'model' => $model,
            'translation' => $model->translation,
            'price' => $model->priceValue,
            'sizes' => Size::activeSize($model->category_id),
            'pseudoCategories' => $model->activePseudoCategories,
        ]);
    }

    public function actionWishlist($page = false)
    {
        $query = Product::wishlistProducts();

        $pages = new Pagination([
            'totalCount' => $query->count(),
            'pageSize' => ProductSearch::PAGE_SIZE,
            'forcePageParam' => false,
            'pageSizeParam' => false
        ]);

        if ($page > $pages->getPageCount() || $page == 1) {
            throw new \yii\web\NotFoundHttpException(404);
        }

        $products = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->each();

        return $this->render('wishlist', [
            'products' => $products,
            'pages' => $pages
        ]);
    }

    public function actionUpdateWishlist()
    {
        $product_id = Yii::$app->request->post('product_id');

        $item = Wishlist::find()->where([
            'product_id' => $product_id,
            'user_id' => Yii::$app->user->id
        ])->one();

        if ($item) {
            $item->delete();
            return Yii::t('product', 'Dress removed from wishlist');
        }

        if ($product_id) {
            $whishlist = new Wishlist();
            $whishlist->product_id = (int)$product_id;
            $whishlist->user_id = Yii::$app->user->id;
            if ($whishlist->save()) {
                return Yii::t('product', 'Dress added to wishlist');
            }
        }
        return Yii::t('product', 'Error');
    }

    protected function findModel($category, $product)
    {
        if (($model = Product::findProduct($category, $product)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}










