<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use frontend\shop\useCases\Shop\CartService;
use yii\filters\AccessControl;
use common\models\Order;
use common\models\Address;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\filters\VerbFilter;
use common\models\User;
use common\models\payments\Pay;
use yii\helpers\ArrayHelper;
use frontend\models\Model;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Category controller
 */
class PersonAreaController extends Controller
{
   /**
     * {@inheritdoc}
     */
   public function actions()
   {
   	return [
   		'error' => [
   			'class' => 'yii\web\ErrorAction',
   		],
   		'captcha' => [
   			'class' => 'yii\captcha\CaptchaAction',
   			'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
   		],
   	];
   }

   public function behaviors()
   {
   	return [
   		'access' => [
   			'class' => AccessControl::className(),
   			'rules' => [
   				[
   					'allow' => true,
   					'roles' => ['@'],
   				],
   			],
   		],
   	];
   }

   public function actionProfile()
   {
   	$model = User::findOne(Yii::$app->user->identity->id);
   	$model->scenario = User::SCENARIO_USER;

   	if($model->load(Yii::$app->request->post())) {
         if($model->save()) {
            Yii::$app->session->addFlash('success', Yii::t('pages', 'Data successfully saved'));
         } else {
            Yii::$app->session->addFlash('danger', Yii::t('pages', 'Failed to save data'));
         }
         return $this->redirect('profile');
      }

      return $this->render('profile', ['model' => $model]);
   }

   public function actionAddresses()
   {
   	$user = Yii::$app->user->identity;

   	$models = empty($user->addresses) ? [new Address] : $user->addresses;

   	$request = Yii::$app->getRequest();
   	if ($request->isPost && $request->post('ajax') !== null) {
   		$models_addresses = [new Address()];
   		$data = Yii::$app->request->post('Address', []);
   		foreach (array_keys($data) as $index) {
   			$models_addresses[$index] = new Address();
   		}
   		Model::loadMultiple($models_addresses, Yii::$app->request->post());
   		Yii::$app->response->format = Response::FORMAT_JSON;
   		$result = ActiveForm::validateMultiple($models_addresses);
   		return $result;
   	}

   	if(Yii::$app->request->post()) {
   		$oldIDs = ArrayHelper::map($models, 'id', 'id');
   		$models = Model::createMultiple(Address::classname(), $models);

   		Model::loadMultiple($models, Yii::$app->request->post());

   		$deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($models, 'id', 'id')));

   		if(Model::validateMultiple($models)) {
   			$transaction = \Yii::$app->db->beginTransaction();
   			try {
               if(!empty($deletedIDs)) {
                  Address::deleteAll(['id' => $deletedIDs]);
               }
               
               foreach ($models as $model) {
                  $model->user_id = $user->id;
                  if(!($flag = $model->save(false))) {
                     $transaction->rollBack();
                     Yii::$app->session->addFlash('danger', Yii::t('pages', 'Failed to save data'));
                     break;
                  }
               }
               if($flag) {
                  $transaction->commit();
                  Yii::$app->session->addFlash('success', Yii::t('pages', 'Data successfully saved'));
               }
            } catch (Exception $e) {
               $transaction->rollBack();
               Yii::$app->session->addFlash('danger', Yii::t('pages', 'Failed to save data'));
            }
         }
         return $this->redirect('addresses');
      }

      return $this->render('addresses', [
        'models' => $models,
      ]);
   }

   public function actionOrders($page=false)
   {
      $query = Order::getUserOrders();

      $pages = new Pagination([
         'totalCount' => $query->count(),
         'pageSize' => 10,
         'forcePageParam' => false,
         'pageSizeParam' => false
      ]);

      if($page > $pages->getPageCount() || $page == 1) {
         throw new \yii\web\NotFoundHttpException(404);
      }

      $orders = $query->offset($pages->offset)
         ->limit($pages->limit)
         ->each();

      return $this->render('orders', [
         'orders' => $orders,
         'pages' => $pages
      ]);
   }

   public function actionPays($page=false)
   {
      $query = Pay::getUserPay();
      $dataProvider = new ActiveDataProvider([
         'query' => $query,
         'pagination' => [
            'totalCount' => $query->count(),
            'pageSize' => Pay::PAGE_SIZE,
            'forcePageParam' => false,
            'pageSizeParam' => false
         ]
      ]);

      if($page > $dataProvider->pagination->getPageCount() || $page == 1) {
         throw new \yii\web\NotFoundHttpException(404);
      }

      return $this->render('pays', [
         'dataProvider' => $dataProvider,
         'pages' => $dataProvider->pagination
      ]);
   }

}










