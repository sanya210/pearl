<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use common\models\payments\PayPal;
use common\models\payments\PaypalLog;
use common\models\payments\LiqPay;
use common\models\payments\LiqpayLog;
use common\models\Order;
use yii\helpers\Url;

/**
 * Category controller
 */
class PaymentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['pay', 'liqpay', 'paypal', 'pay-success', 'paypal-send'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['callback-liqpay', 'callback-paypal'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'liqpay' => ['post'],
                    'pay-send' => ['post']
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id == 'callback-liqpay' || $action->id == 'callback-paypal') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionPay($order_id)
    {
        $order = Order::find()->where(['id' => $order_id, 'user_id' => Yii::$app->user->identity->id])->one();
        if ($order && $order->statusActive) {
            return $this->render('pay', [
                'order_id' => $order_id,
                'currency' => $order->currency
            ]);
        }
        throw new BadRequestHttpException(Yii::t('pages', 'This order cannot be paid'));
    }

    public function actionPaypal()
    {
        $order_id = Yii::$app->request->post('order_id');

        if ($order_id) {
            $order = Order::find()->where(['id' => $order_id, 'user_id' => Yii::$app->user->identity->id])->one();

            if ($order) {
                return $this->render('paypal', [
                    'order' => $order
                ]);
            }
        }

        throw new BadRequestHttpException();
    }

    public function actionPaypalSend()
    {
        $order_id = Yii::$app->request->post('order_id');

        if ($order_id) {
            $order = Order::find()->where(['id' => $order_id, 'user_id' => Yii::$app->user->identity->id])->one();

            if ($order) {

                $products = '';
                foreach ($order->products as $product) {
                    $sizes = '';
                    foreach ($product['sizes'] as $size => $count) {
                        $sizes .= $size . ': ' . $count . ', ';
                    }
                    $sizes = substr($sizes, 0, -2);
                    $products .= $product['name'] . '; ' . Yii::t('pages', 'Sizes') . ': ' . $sizes . '. ';
                }

                $data = [
                    'cmd' => '_xclick',
                    'business' => 'vmindak@westele.com.ua',
                    'amount' => $order->total,
                    'currency_code' => $order->currency,
                    'lc' => Yii::$app->language . '-' . strtoupper(Yii::$app->language),
                    'return' => Url::toRoute('/pay-success', 'https'),
                    'invoice' => $order_id,
                    'no_shipping' => 1,
                    'item_name' => $products
                ];

                $query = '';
                foreach ($data as $key => $value) {
                    $query .= $key . '=' . $value . '&';
                }
                $query = substr($query, 0, -1);

                return $this->redirect('https://www.paypal.com/cgi-bin/websc?' . $query);
            }
        }

        throw new BadRequestHttpException();
    }

    public function actionCallbackPaypal()
    {
        $data = Yii::$app->request->post();
        $ipn = new PayPal();

        $ipn->useSandbox();
        $verified = $ipn->verifyIPN();
        if ($verified) {
            $model = new PaypalLog();
            $model->add($data);
        }

        header("HTTP/1.1 200 OK");
    }

    public function actionLiqpay()
    {
        $order_id = Yii::$app->request->post('order_id');

        if ($order_id) {
            $order = Order::find()->where(['id' => $order_id, 'user_id' => Yii::$app->user->identity->id])->one();

            if ($order) {
                $liqpay = new LiqPay();
                $html = $liqpay->cnb_form(array(
                    'action' => 'pay',
                    'amount' => $order->total,
                    'currency' => $order->currency,
                    'description' => Yii::t('pages', 'Payment for the purchase in the store') . ' ' . Yii::$app->name,
                    'language' => Yii::$app->language,
                    'order_id' => $order_id,
                    'version' => '3',
                    'paytypes' => 'liqpay',
                    'result_url' => Url::toRoute('/pay-success', 'https'),
                    'server_url' => 'https://pearl.wedding/callback-liqpay'
                ));

                return $this->render('liqpay', [
                    'html' => $html,
                    'order' => $order
                ]);
            }
        }

        throw new BadRequestHttpException();
    }

    public function actionCallbackLiqpay()
    {
        $signature = Yii::$app->request->post('signature');
        $data = Yii::$app->request->post('data');

        if ($signature && $data) {
            $model = new LiqpayLog();
            $model->add($signature, $data);
            return true;
        }
        throw new BadRequestHttpException();
    }

    public function actionPaySuccess()
    {
        return $this->render('success');
    }

}










