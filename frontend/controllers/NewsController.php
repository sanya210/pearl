<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\News;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\helpers\Url;
use yii\data\Pagination;

/**
 * News controller
 */
class NewsController extends Controller
{
   /**
     * {@inheritdoc}
     */
   public function actions()
   {
   	return [
   		'error' => [
   			'class' => 'yii\web\ErrorAction',
   		],
   		'captcha' => [
   			'class' => 'yii\captcha\CaptchaAction',
   			'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
   		],
   	];
   }

   public function actionIndex($page=false)
   {
      $query = News::getNews();

      $pages = new Pagination([
         'totalCount' => $query->count(),
         'pageSize' => News::PAGE_SIZE,
         'forcePageParam' => false,
         'pageSizeParam' => false
      ]);

      if($page > $pages->getPageCount() || $page == 1) {
         throw new \yii\web\NotFoundHttpException(404);
      }

      $news = $query->offset($pages->offset)
         ->limit($pages->limit)
         ->each();

      return $this->render('index', [
         'news' => $news,
         'pages' => $pages
      ]);
   }

   public function actionView($slug)
   {
      $model = News::find()->where([
         'status' => News::STATUS_ACTIVE,
         'slug' => $slug
      ])
      ->with('activeLanguage')
      ->one();

      if($model) {
         return $this->render('view', [
            'model' => $model
         ]);
      }

      throw new \yii\web\NotFoundHttpException(404);      
   }
   
}










