<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\shop\ShopCategory;
use common\models\shop\PseudoCategory;
use common\models\shop\Product;
use frontend\models\shop\ProductSearch;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\helpers\Url;

/**
 * Category controller
 */
class CategoryController extends Controller
{
   /**
     * {@inheritdoc}
     */
   public function actions()
   {
   	return [
   		'error' => [
   			'class' => 'yii\web\ErrorAction',
   		],
   		'captcha' => [
   			'class' => 'yii\captcha\CaptchaAction',
   			'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
   		],
   	];
   }

   public function actionView($category, $page=false)
   {
      if(($model = ShopCategory::findCategory($category)) !== null) {
         $scenario = 'category';
      } elseif(($model = PseudoCategory::findPseudoCategory($category)) !== null) {
         $scenario = 'pseudo_category';
         Yii::$app->urlManager->languages = [$model->language];
         Yii::$app->params['translate'] = false;
      } else {
         throw new NotFoundHttpException('The requested page does not exist.');
      }

      $price = Yii::$app->request->get('price');
      if($price) {
         $price_values = explode(',', $price);         
            if( count($price_values) != 2 || !is_numeric($price_values[0]) || !is_numeric($price_values[1]) ) {
               throw new BadRequestHttpException();
            }
      }

      $searchModel = new ProductSearch();
      $dataProvider = $searchModel->search($model->id, $scenario,[
         'ProductSearch' => Yii::$app->request->queryParams,
      ]);

      $pages = $dataProvider->pagination;
      if($page > $pages->getPageCount() || $page == 1) {
         throw new \yii\web\NotFoundHttpException(404);
      }

      return $this->render('view', [
         'products' => $dataProvider->getModels(),
         'pages' => $pages,
         'model' => $model,
         'translation' => $model->translation,
         'canonical' => $page ? Url::to(['/'.$model->slug], true) : null
      ]);
   }
   
}










