<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\shop\Product;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use yii\helpers\Url;
use frontend\shop\useCases\Shop\CartService;
use frontend\models\shop\AddToCartForm;

/**
 * Cart controller
 */
class CartController extends Controller
{
    private $service;

    public function __construct($id, $module, CartService $service, $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->service = $service;
    }

    public function actionIndex()
    {
        $cart = $this->service->getCart();

        if($post = Yii::$app->request->post()) {
            $form = new AddToCartForm($post['AddToCartForm']['product_id']);
            if($form->load($post) && $form->validate()) {
                $this->service->update($form->product_id, $form->sizes, $form->size_type);
            }
        }

        $data = $cart->getItems();

        return $this->render('index', [
            'products' => $data['products'],
            'total' => $data['total']
        ]);
    }

    public function actionAdd($product_id)
    {
        $form = new AddToCartForm($product_id);

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $product = Product::find()->where([
                'id' => $form->product_id,
                'status' => Product::STATUS_ACTIVE
            ])->one();

            if (!$product) {
                Yii::$app->session->setFlash('danger', Yii::t('pages', 'Product not found'));
                $result = [
                    'text' => Yii::t('pages', 'Product not found'),
                    'name' => '',
                    'img' => ''
                ];
                return json_encode($result);
            }
            try {
                $this->service->add($form->product_id, $form->sizes, $form->size_type);
                $result = [
                    'text' => Yii::t('pages', 'Product added to cart'),
                    'name' => $product->activeLanguage->name . ' ' . $product->article,
                    'img' => '/uploads/product/preview/' . $product->mainPhoto
                ];
                return json_encode($result);
            } catch (\DomainExeption $e) {
                $result = [
                    'text' => Yii::t('pages', 'Could not add product to cart'),
                    'name' => '',
                    'img' => ''
                ];
                return json_encode($result);
            }
        }
    }
}










