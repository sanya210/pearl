<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\Order;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use frontend\models\CheckoutForm;
use frontend\shop\useCases\Shop\CartService;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * Category controller
 */
class OrderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    public function actionCheckout()
    {
        if (!Yii::$app->user->identity->addresses) {
            Yii::$app->session->setFlash('warning', Yii::t('pages', 'To place an order, you must add an address'));
            return $this->redirect('/addresses');
        }

        $model = Yii::$container->get('frontend\models\CheckoutForm');

        if ($model->load(Yii::$app->request->post()) && $order_id = $model->makeOrder()) {
            return $this->redirect(['/pay', 'order_id' => $order_id]);
        }

        return $this->render('checkout', ['model' => $model]);
    }

    public function actionDeleteOrder($order_id)
    {
        $order = Order::find()->where(['id' => $order_id, 'user_id' => Yii::$app->user->identity->id])->one();

        if ($order->cancel()) {
            Yii::$app->session->setFlash('success', Yii::t('pages', 'Order removed'));
        } else {
            Yii::$app->session->setFlash('danger', Yii::t('pages', 'Could not delete order'));
        }
        return $this->redirect(Yii::$app->request->referrer ?: Url::to(['/']));
    }
}










