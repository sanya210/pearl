<?php
namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\shop\ProductElasticSearch;
use yii\helpers\Url;
use common\models\User;
use common\models\UserNetwork;
use common\models\Page;
use yii\web\NotFoundHttpException;
use common\models\Email;
use frontend\models\Sitemap;
use frontend\models\Rozetka;
use common\models\PushToken;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'subscribe' => ['post'],
                    'add-push-token' => ['post']
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
                'successUrl' => Url::to(['/'])
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if(Yii::$app->request->isAjax) {
            $model = new LoginForm();
            if ($model->load(Yii::$app->request->post())) {
                if ($model->login()) {
                    return $this->redirect(Yii::$app->request->referrer ?: Url::to(['/']));
                } else {
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return \yii\widgets\ActiveForm::validate($model);
                }
            }
        } else {
            throw new \yii\web\NotFoundHttpException(404);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout(false);
        $token = Yii::$app->request->post();

        if ($token) {
            PushToken::updateToken($token);
        }

        return $this->redirect(Url::to(['/']));
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContacts()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', Yii::t('pages', 'Thank you for contacting us. We will respond to you as soon as possible'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('pages', 'There was an error sending your message.'));
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionModalContacts()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail([Yii::$app->params['marketingEmail'], Yii::$app->params['managerEmail']])) {
                return Yii::t('pages', 'Thank you for contacting us. We will respond to you as soon as possible');
            }
        }
        return Yii::t('pages', 'There was an error sending your message.');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', Yii::t('pages', 'Thank you for registration. Please check your inbox for verification email.'));
            return $this->redirect(Url::to(['/']));
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', Yii::t('pages', 'Check your email for further instructions.'));

                return $this->redirect(Url::to(['/']));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('pages', 'Sorry, we are unable to reset password for the provided email address.'));
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', Yii::t('pages', 'New password saved.'));

            return $this->redirect(Url::to(['/']));
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    public function actionVerifyEmail($token)
    {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if ($user = $model->verifyEmail()) {
            if (Yii::$app->user->login($user)) {
                Yii::$app->session->setFlash('success', Yii::t('pages', 'Your email has been confirmed!'));
                return $this->redirect(Url::to(['/']));
            }
        }

        Yii::$app->session->setFlash('error', Yii::t('pages', 'Sorry, we are unable to verify your account with provided token.'));
        return $this->redirect(Url::to(['/']));
    }

    /**
     * Resend verification email
     *
     * @return mixed
     */
    public function actionResendVerificationEmail()
    {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', Yii::t('pages', 'Check your email for further instructions.'));
                return $this->redirect(Url::to(['/']));
            }
            Yii::$app->session->setFlash('error', Yii::t('pages', 'Sorry, we are unable to resend verification email for the provided email address.'));
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }

    public function actionSearch($page=false, $text)
    {
        $searchModel = new ProductElasticSearch();
        $dataProvider = Yii::$container->invoke([$searchModel, 'search'], [$text]);

        $pages = $dataProvider->pagination;
        if($page > $pages->getPageCount() || $page == 1) {
           throw new \yii\web\NotFoundHttpException(404);
        }

        return $this->render('search', [
           'products' => $dataProvider->getModels(),
           'pages' => $pages,
           'text' => $text
        ]);
    }

    public function onAuthSuccess($client)
    {
        $source = $client->getId();
        $attributes = $client->getUserAttributes();

        /* @var $auth Auth */
        $auth = UserNetwork::find()->where([
            'source' => $client->getId(),
            'source_id' => $attributes['id'],
        ])->one();
        
        if(Yii::$app->user->isGuest) {
            if($auth) { // авторизация
                $user = $auth->user;
                Yii::$app->user->login($user);
            } else { // регистрация
                if(isset($attributes['email']) && User::find()->where(['email' => $attributes['email']])->exists()) {
                    Yii::$app->getSession()->setFlash('error', [
                        Yii::t('pages', 'User with the same email as in {client} account already exists but isn\'t linked to it. Login using email first to link it.', ['client' => $client->getTitle()]),
                    ]);
                } else {
                    $password = Yii::$app->security->generateRandomString(6);
                    if($source == 'google') {
                        $user = new User([
                            'email' => $attributes['email'],
                            'password' => $password,
                            'name' => $attributes['name'],
                            'family_name' => $attributes['family_name'],
                            'status' => User::STATUS_ACTIVE
                        ]);
                    } elseif($source == 'facebook') {
                        $names_str = preg_replace('| +|', ' ', $attributes['name']);
                        $name = explode(' ', $names_str);
                        $user = new User([
                            'email' => $attributes['email'],
                            'password' => $password,
                            'name' => $name[0],
                            'family_name' => $name[1] ?? '',
                            'status' => User::STATUS_ACTIVE
                        ]);
                    } elseif($source == 'vkontakte') {
                        $user = new User([
                            'email' => $attributes['email'],
                            'password' => $password,
                            'name' => $attributes['first_name'],
                            'family_name' => $attributes['last_name'],
                            'status' => User::STATUS_ACTIVE
                        ]);
                    }

                    $user->generateAuthKey();
                    $user->generatePasswordResetToken();
                    $transaction = $user->getDb()->beginTransaction();
                    if($user->save()) {
                        $auth = new UserNetwork([
                            'user_id' => $user->id,
                            'source' => $client->getId(),
                            'source_id' => (string)$attributes['id'],
                        ]);
                        if($auth->save()) {
                            $transaction->commit();
                            Yii::$app->user->login($user);
                        } else {
                            print_r($auth->getErrors());
                        }
                    } else {
                        print_r($user->getErrors());
                    }
                }
            }
        } else { // Пользователь уже зарегистрирован
            if(!$auth) { // добавляем внешний сервис аутентификации
                $auth = new UserNetwork([
                    'user_id' => Yii::$app->user->id,
                    'source' => $client->getId(),
                    'source_id' => $attributes['id'],
                ]);
                $auth->save();
            }
        }
    }

    public function actionPage($slug)
    {
        $model = Page::findPage($slug);
        if($model) {
            return $this->render('page', [
                'model' => $model
            ]);
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionSubscribe()
    {
        $model = new Email();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return Yii::t('pages', 'Thanks for subscribing!');
        }

        return Yii::t('pages', 'Error, try again later.');
    }

    public function actionChangeCurrency($currency)
    {
        Yii::$app->currency->set($currency);

        return $this->redirect(Yii::$app->request->referrer ?: Url::to(['/']));
    }

    public function actionSitemap()
    {
        $sitemap = new Sitemap();
        $sitemap->showXml();
    }

    public function actionRozetka()
    {
        $sitemap = new Rozetka();
        $sitemap->showXml();
    }

    public function actionAddPushToken()
    {
        $token = Yii::$app->request->post('token');
        PushToken::add($token);
    }

}
