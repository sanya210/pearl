<?php
return [
    'Sort by' => 'Sortieren nach',
    'Most popular' => 'am beliebtensten',
    'Alphabetical' => 'alphabetisch',
    'Price: Low-High' => 'von billig bis teuer',
    'Price: High-Low' => 'von teuer  bis billig ',
    'items' => 'position',
    'Showing {page}-{pages} of {all} items' => 'gezeigt {page}-{pages} из {all} positionen',
    'Color' => 'Farbe',
    'Price, $' => 'Preiß, $',
    'Filter' => 'Filter'
];


