<?php
return [
    'Successful order!' => 'Erfolgreiche Bestellung!',
    'Good afternoon' => 'Guten Tag',
    'You just ordered at pearl.wedding. Thank you for your trust!' => 'Sie haben gerade eine Bestellung  bei pearl.wedding aufgegeben .Vielen Dank für Ihr Vertrauen!',
    'Your order will be shipped as soon as possible after payment.' => 'Ihre Bestellung wird möglichst schnell nach Zahlungseingang versandt.',
    'Your order' => 'Ihre Bestellungs',
    'from' => 'vom',
    'Product' => 'Ware',
    'Size (quantity)' => 'Größe (menge)',
    'Amount' => 'Betrag',
    'The payer' => 'Zahler',
    'Address for delivery' => 'Empfängeradresse',
    'The most important for us is your satisfaction. Our manager will be glad to answer all your questions and help you.  You can find all the ways to contact us on <a href="https://pearl.wedding/contacts">the contact page</a>.' => 'Die allerwichtigste Hauptsache für uns ist, dass Sie zufrieden bleiben. Unser Kundendienstmanager beantwortet gerne alle Ihre Fragen und entsprechend hilft Ihnen weiter. Auf der <a href="https://pearl.wedding/de/contacts">Kontaktseite</a> finden Sie alle Möglichkeit uns zu kontaktieren.',
    'Share with us your opinion about our dresses and service quality! We are waiting for your feedback!' => 'Teilen Sie uns Ihre Meinung zu unseren Kleidern und der Servicequalität mit!',
    'Thank you for the order!' => 'Danke für Ihre Bestellung!',
    'Best regards, the Pearl Fashion Group team.' => 'Mit freundlichen  Grüßen  Pearl Fashion Group.',
];
