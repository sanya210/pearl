<?php
return [
    'Successful order!' => 'Успешный заказ!',
    'Good afternoon' => 'Добрый день',
    'You just ordered at pearl.wedding. Thank you for your trust!' => 'Вы только что сделали заказ на сайте pearl.wedding. Спасибо Вам за оказанное доверие!',
    'Your order will be shipped as soon as possible after payment.' => 'Ваш заказ будет отправлен в кратчайший срок после оплаты.',
    'Your order' => 'Ваш заказ',
    'from' => 'от',
    'Product' => 'Товар',
    'Size (quantity)' => 'Размер (количество)',
    'Amount' => 'Сумма',
    'The payer' => 'Плательщик',
    'Address for delivery' => 'Адрес получателя',
    'The most important for us is your satisfaction. Our manager will be glad to answer all your questions and help you.  You can find all the ways to contact us on <a href="https://pearl.wedding/contacts">the contact page</a>.' => 'Для нас главное, чтобы вы остались довольны. Наш менеджер с радостью ответит на все вопросы и окажет вам помощь.  Все способы связи с нами вы найдете на странице <a href="https://pearl.wedding/ru/contacts">контакты</a>.',
    'Share with us your opinion about our dresses and service quality! We are waiting for your feedback!' => 'Поделитесь с нами вашим мнением о наших платьях  и качестве обслуживания! Мы ждем вашего отзыва!',
    'Thank you for the order!' => 'Спасибо за заказ!',
    'Best regards, the Pearl Fashion Group team.' => 'С уважением команда Pearl Fashion Group.',
];
