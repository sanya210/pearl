<?php
return [
	'Sort by' => 'Сортировать по',
	'Most popular' => 'По популярности',
	'Alphabetical' => 'По алфавиту',
	'Price: Low-High' => 'От дешевых к дорогим',
	'Price: High-Low' => 'От дорогих к дешевым',
	'items' => 'позиций',
	'Showing {page}-{pages} of {all} items' => 'Показано {page}-{pages} из {all} позиций',
	'Color' => 'Цвет',
	'Price, $' => 'Цена, $',
	'Filter' => 'Фильтр'
];


