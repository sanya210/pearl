<?php
return [
	'Article' => 'Артикул',
	'Bought more than' => 'Купили больше чем',
	'Description' => 'Описание',
	'Reviews' => 'Отзывы',
	'Left' => 'Осталось',
	'days' => 'дн.',
	'Color' => 'Цвет',
	'Size' => 'Размер',
	'Size table' => 'Таблица размеров',
	'Chest, cm' => 'Грудь, см',
	'Waist, cm' => 'Талия, см',
	'Hips, cm' => 'Бедра, см',
	'Growth, cm' => 'Рост, см',
	'All size' => 'Все размеры',
	'Enter' => 'Ввод',
	'In bag' => 'В корзину',
	'Order by size' => 'Заказ по размеру',
	'Constructor' => 'Конструктор',
	'Share' => 'Поделиться',
	'Free shipping' => 'Бесплатная доставка',
	'Delivery to any city in the world' => 'Доставка в любой город мира',
	'Individual order' => 'Индивидуальный заказ',
	'We will sew this dress for you in any size to order' => 'Мы пошьем для вас это платье в любом размере на заказ',
	'Constructor dress' => 'Конструктор платьев',
	'You can create your own dream dress' => 'Вы можете создать собственное платье мечты',
	'Description' => 'Описание',
	'Reviews' => 'Отзывы',
	'Categories' => 'Категории',
	'Similar products' => 'Похожие товары',
	'Dress added to wishlist' => 'Платье добавлено в список желаний',
	'Dress removed from wish list' => 'Платье удалено из списка желаний',
	'Error' => 'Ошибка',
	'Top 5 dresses' => 'Топ 5 платьев',
///////////////////////////////////
	'You must choose a size.' => 'Вы должны выбрать размер.',

    'Not available' => 'Нет в наличии',
    'Choose a dimensional grid: ' => 'Выберите размерную сетку',
    'Choose size: ' => 'Выберите размер',
    'Free shipping worldwide' => 'Бесплатная доставка по всему миру',
    'Amount' => 'Количество',
];


