<?php
return [
	'Article' => 'Article',
	'Bought more than' => 'Acheté plus que',
	'Description' => 'La description',
	'Reviews' => 'Avis',
	'Left' => 'Il en reste',
	'days' => 'jours',

	'Color' => 'Couleur',

	'Size' => 'Taille',

	'Size table' => 'Tableau des tailles',

	'Chest, cm' => 'Buste, cm',

	'Waist, cm' => 'Taille, cm',


	'Hips, cm' => 'Hanches, cm',

	'Growth, cm' => 'Hauteur, cm',

	'All size' => 'Toutes tailles',

	'Enter' => 'Entrer',

	'In bag' => 'Dans le panier',

	'Order by size' => 'Commander par taille',

	'Constructor' => 'Constructeur',

	'Share' => 'Partager',

	'Free shipping' => 'Livraison gratuite',

	'Delivery to any city in the world' => 'Livraison dans n\'importe quelle ville du monde',

	'Individual order' => 'Personnalisation',

	'We will sew this dress for you in any size to order' => 'Nous allons coudre cette robe pour vous dans n\'importe quelle taille sur commande',
	'Constructor dress' => 'Constructeur de robe',

	'You can create your own dream dress' => 'Vous pouvez créer votre propre robe de rêve',
	'Description' => 'La description',

	'Reviews' => 'Avis',

	'Categories' => 'Catégories',

	'Similar products' => 'Produits similaires',
	
	'Dress added to wishlist' => 'Robe ajoutée à la liste de souhaits',
	'Dress removed from wish list' => 'Robe retirée de la liste de souhaits',
	'Error' => 'Erreur',
	'Top 5 dresses' => 'Top 5 robes',
///////////////////////////////////
	'You must choose a size.' => 'Vous devez choisir une taille.',

    'Not available' => 'Indisponible',

    'Choose a dimensional grid: ' => 'Choisissez une grille dimensionnelle',
    'Choose size: ' => 'Choisissez la taille',
    'Free shipping worldwide' => 'Envoi gratuit dans le monde entier',
    'Amount' => 'Nombre',
];


