<?php
return [
	'Home' => 'Accueil',
	'Events' => 'Événements',
	'Blog' => 'Le blog',
	'Our brides' => 'Nos mariées',
	'Salons' => 'Salons',
	'Cooperation' => 'La coopération',
	'Contacts' => 'Contacts',
	'SALE' => 'RABAIS',
	'Constructor dress' => 'Constructeur des robes',
	'Search' => 'Chercher',
	////////////////////
	'Charm 2019' => 'Charme 2019',
	'Tenderness 2019' => 'Tendresse 2019',
	'Princess 2019' => 'Princesse 2019',
	'Sales' => 'Rabais'
];


