<?php
return [
	'Sort by' => 'Trier par',
	'Most popular' => 'Les plus populaire',
	'Alphabetical' => 'Alphabétique',
	'Price: Low-High' => 'Prix du moins cher au plus cher',
	'Price: High-Low' => 'Prix du plus cher au moins cher',
	'items' => 'articles',
	'Showing {page}-{pages} of {all} items' => 'Affichage de {page} - {pages} de {all} éléments',
	'Color' => 'Couleur',
	'Price, $' => 'Prix, $',
	'Filter' => 'Filtre'
];


