<?php
return [
    'Successful order!' => 'Commande réussie!',
    'Good afternoon' => 'Bonjour',
    'You just ordered at pearl.wedding. Thank you for your trust!' => 'Vous venez de passer commande sur notre boutique pearl.wedding et nous vous remercions de votre confiance!',
    'Your order will be shipped as soon as possible after payment.' => 'Votre commande sera expédiée dans les meilleurs délais à la réception du règlement.',
    'Your order' => 'Votre commande',
    'from' => 'datant du',
    'Product' => 'Article',
    'Size (quantity)' => 'Taille (quantité)',
    'Amount' => 'Valeur',
    'The payer' => 'Payeur',
    'Address for delivery' => 'Information Livraison',
    'The most important for us is your satisfaction. Our manager will be glad to answer all your questions and help you.  You can find all the ways to contact us on <a href="https://pearl.wedding/contacts">the contact page</a>.' => 'Votre satisfaction est au coeur de nos priorités. Notre service client est à votre écoute, pour toute question, rendez-vous <a href="https://pearl.wedding/fr/contacts">sur notre page contact</a>.',
    'Share with us your opinion about our dresses and service quality! We are waiting for your feedback!' => 'Partagez votre opinion avec nous sur nos robes et la qualité de service! Nous attendons vos commentaires!',
    'Thank you for the order!' => 'Merci de votre commande!',
    'Best regards, the Pearl Fashion Group team.' => 'Cordialement vôtre, l\'équipe du Pearl Fashion Group.',
];
