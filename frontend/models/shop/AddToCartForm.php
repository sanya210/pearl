<?php

namespace frontend\models\shop;

use common\models\shop\Product;
use Yii;
use yii\base\Model;
use common\models\shop\Size;

class AddToCartForm extends Model
{
	public $product_id;
    public $sizes_value;
    public $sizes = [];
    public $size_type;

	const TYPES = ['international', 'number_usa', 'europe', 'ru', 'uk', 'de'];

	public function __construct($product_id, array $config = [])
    {
        $this->product_id = $product_id;
        $this->sizes_value = Product::findOne($product_id)->sizes;
        parent::__construct($config);
    }

	public function rules()
	{
		return [
			[['product_id'], 'integer'],
			['sizes', 'each', 'rule' => ['integer']],
            [['size_type'], 'string'],
		];
	}

	public static function getTypes()
    {
        $size_model = new Size();
        $size_labels = $size_model->attributeLabels();
        $types = array_intersect_key($size_labels, array_flip(self::TYPES));
        array_walk($types, function(&$item) {
            $item .= ' ' . Yii::t('product', 'Size table');
        });

        return $types;
    }
}



















