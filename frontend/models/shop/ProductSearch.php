<?php

namespace frontend\models\shop;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\shop\Product;
use common\models\shop\Discount;
use common\models\shop\Attribute;
use common\models\shop\AttributeValue;
use common\models\shop\ProductAttribute;

/**
 * ProductSearch represents the model behind the search form of `common\models\shop\Product`.
 */
class ProductSearch extends Product
{
    const PAGE_SIZE = 12;

    const SORT_ITEMS = [
        'popular' => 'Most popular', //default
        'alphabetical' => 'Alphabetical',
        'price' => 'Price: Low-High',
        '-price' => 'Price: High-Low'
    ];

    public $color;

    public $price;

    private $attr = [];

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['color', 'price'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($category_id, $scenario, $params)
    {
        switch ($scenario) {
            case 'category':
                $query = Product::find()
                    ->where([
                        'product.category_id' => $category_id,
                        'product.status' => self::STATUS_ACTIVE
                    ])
                    ->orderBy(['priority' => SORT_DESC])
                    ->with('category');

                $attributes = Attribute::getAttributesByCategory($category_id);
                break;
            case 'pseudo_category':
                $query = Product::find()
                    ->leftJoin('product_pseudo_category', 'product_pseudo_category.product_id=product.id')
                    ->where([
                        'product_pseudo_category.pseudo_category_id' => $category_id,
                        'product.status' => self::STATUS_ACTIVE
                    ])
                    ->orderBy(['priority' => SORT_DESC])
                    ->with('category');

                $attributes = Attribute::getAttributesByPseudoCategory($category_id);
                break;
        }

        if(!Yii::$app->user->isGuest) {
            $query->with('wishlist');
        }

        foreach ($attributes as $attribute) {
            if(isset($params['ProductSearch'][$attribute])) {
                $values = explode(',', $params['ProductSearch'][$attribute]);
                foreach ($values as $value) {
                    array_push($this->attr, $value);
                }
            }
        }

        if($this->attr) {
            $number_groups = AttributeValue::find()->where(['id' => $this->attr])->select('attribute_id')->distinct()->count();

            $sub_product_groups = ProductAttribute::find()
                ->select(['product_attribute.product_id', 'attribute_value.attribute_id'])
                ->leftJoin('attribute_value', 'attribute_value.id=product_attribute.value_id')
                ->where(['product_attribute.value_id' => $this->attr])
                ->distinct();

            $product_groups = (new \yii\db\Query())
                ->select(['groups.product_id', 'COUNT(groups.product_id) as count'])
                ->from(['groups' => $sub_product_groups])
                ->groupBy('groups.product_id')
                ->all();

            $product_attr = (new \yii\db\Query())
                ->select(['groups.product_id', 'COUNT(groups.product_id) as counter'])
                ->from(['groups' => $sub_product_groups])
                ->having(['counter' => $number_groups])
                ->groupBy('groups.product_id')
                ->all();

            $query->andWhere(['in', 'product.id', array_column($product_attr, 'product_id')]);
        }


        if(isset($params['ProductSearch']['price']) ||
           (isset($params['ProductSearch']['sort']) && 
           ($params['ProductSearch']['sort'] == 'price' ||
           $params['ProductSearch']['sort'] == '-price'))
        ) {
            $discount_query = Discount::find()
                ->select('MAX(discount.id)')
                ->where('discount.product_id=product.id')
                ->andWhere(['status' => Discount::STATUS_ACTIVE])
                ->andWhere(['<=', 'date_start', date('Y-m-d')])
                ->andWhere(['>=', 'date_finish', date('Y-m-d')]);

            $query->select(['
                discount.price, 
                product.price, 
                product.category_id,
                product.id, 
                product.slug, 
                IFNULL(discount.price, product.price) AS price_finish'
            ])
            ->leftJoin('discount', ['discount.id' => $discount_query]);
        }

        $query->joinWith('activeLanguage');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'totalCount' => $query->count(),
                'pageSize' => self::PAGE_SIZE,
                'forcePageParam' => false,
                'pageSizeParam' => false
            ]
        ]);

        $this->load($params);

        $dataProvider->setSort([
            'attributes' => array_merge($dataProvider->getSort()->attributes, [
                'popular' => [
                    'asc' => ['number_purchased' => SORT_DESC],
                    'desc' => ['number_purchased' => SORT_ASC]
                ],
                'alphabetical' => [
                    'asc' => ['product_localization.name' => SORT_ASC],
                    'desc' => ['product_localization.name' => SORT_DESC]
                ],
                'price' => [
                    'asc' => ['price_finish' => SORT_ASC],
                    'desc' => ['price_finish' => SORT_DESC]
                ]
            ])
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if( isset($params['ProductSearch']['price']) ) {
            $value_min = null;
            $value_max = null;
            $values = explode(',', $this->price);
            if(count($values) === 2){
                $value_min = Yii::$app->currency->backTransfer($values[0]);
                $value_max = Yii::$app->currency->backTransfer($values[1]);
            }
            $query->andFilterWhere(['between', 'IFNULL(discount.price, product.price)', $value_min, $value_max]);
        }

        // // grid filtering conditions
        $query->andFilterWhere([
            'color_id' => $this->color ? explode(',', $this->color) : null
        ]);

        return $dataProvider;
    }
}
