<?php
namespace frontend\models\shop;

use Elasticsearch\Client;
use common\models\shop\Product;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
* 
*/
class ProductElasticSearch extends ActiveRecord
{
	const PAGE_SIZE = 12;

	public function search($text, Client $client)
	{
		$query = Product::find()
			->where(['status' => Product::STATUS_ACTIVE]);

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'totalCount' => $query->count(),
				'pageSize' => self::PAGE_SIZE,
				'forcePageParam' => false,
				'pageSizeParam' => false
			]
		]);

		$response = $client->search([
			'index' => 'shop',
			'type' => 'products',
			'body' => [
				'_source' => ['id'],
				'query' => [
					'bool' => [
						'must' => [
							'multi_match' => [
								'query' => $text,
								'fuzziness' => 'auto',
								'operator' => 'and',
								'fields' => ['name', 'description', 'article']
							]
						]
					]
				]
			]
		]);

		$ids = ArrayHelper::getColumn($response['hits']['hits'], '_id');
		
		if($ids) {		
			$query->andFilterWhere([
				'id' => $ids
			])
			->orderBy([new \yii\db\Expression('FIELD (id, ' . implode(',', $ids) . ')')]);
		} else {
			$query->andFilterWhere([
				'id' => 0
			]);
		}

		return $dataProvider;

	}


}




























