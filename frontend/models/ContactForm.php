<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('pages', 'Name'),
            'subject' => Yii::t('pages', 'Subject'),
            'body' => Yii::t('pages', 'Message'),
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return bool whether the email was sent
     */
    public function sendEmail($email)
    {
        $subject = $this->subject ?: 'Сообщение из обратной связи';
        $message = '<p>Имя: '.$this->name.'</p>
                    <p>Email: '.$this->email.'</p>
                    <p>Тема: '.$subject.'</p>
                    <p>Сообщение: '.$this->body.'</p>';

        return Yii::$app->mailer->compose()
            ->setTo($email)
            ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
            // ->setReplyTo([$this->email => $this->name])
            ->setSubject($subject)
            ->setHtmlBody($message)
            ->send();
    }
}
