<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\Order;
use common\models\OrderItem;
use frontend\shop\useCases\Shop\CartService;

/**
 * Checkout Form
 */
class CheckoutForm extends Model
{
    public $user_name;
    public $user_family_name;
    public $user_phone;
    public $address_id;
    public $note;

    public $products;
    public $total;

    private $cart_service;

    public function __construct(CartService $cart_service, $config = [])
    {
        $this->cart_service = $cart_service;
        $this->loadCart();

        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['user_name', 'trim'],
            ['user_name', 'required'],
            ['user_name', 'string', 'min' => 2, 'max' => 255],

            ['user_family_name', 'trim'],
            ['user_family_name', 'required'],
            ['user_family_name', 'string', 'min' => 2, 'max' => 255],

            ['user_phone', 'trim'],
            ['user_phone', 'required'],
            ['user_phone', 'string', 'min' => 2, 'max' => 255],

            ['address_id', 'integer'],
            ['address_id', 'required'],

            ['note', 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_name' => Yii::t('pages', 'Name'),
            'user_family_name' => Yii::t('pages', 'Last name'),
            'user_phone' => Yii::t('pages', 'Phone number'),
            'address_id' => Yii::t('pages', 'Address'),
            'note' => Yii::t('pages', 'Note'),
        ];
    }

    public function loadCart()
    {
        $cart = $this->cart_service->getCart()->getItems();
        $this->products = $cart ? $cart['products'] : [];
        $this->total = $cart ? $cart['total'] : 0;
    }

    public function makeOrder()
    {
        $order = new Order();
        $order->user_id = Yii::$app->user->identity->id;
        $order->user_name = $this->user_name;
        $order->user_family_name = $this->user_family_name;
        $order->user_phone = $this->user_phone;
        $order->language = Yii::$app->language;
        $order->currency = Yii::$app->currency->symbol;
        $order->note = $this->note;
        $order->address_id = $this->address_id;
        $order->status = 1;

        $session = Yii::$app->session;
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if($order->save()) {        
                foreach ($this->products as $product_id => $product) {
                    $sizes = array_filter($product['sizes']);

                    $order_item = new OrderItem();
                    $order_item->product_id = $product_id;
                    $order_item->sizes = json_encode($sizes);
                    $order_item->quantity = array_sum($sizes);
                    $order_item->size_type = $product['size_type'];
                    $order_item->link('order', $order);
                }
                $transaction->commit();
                $this->cart_service->clear();
                $session->addFlash('success', Yii::t('pages', 'Checkout successfully issued!'));
                $order->sendSuccess();
                return $order->id;
            }
        } catch(\Throwable $e) {
            $transaction->rollBack();
        }
        $session->addFlash('danger', Yii::t('pages', 'Failed to place an checkout'));
        return false;
    }
}
