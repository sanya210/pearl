<?php
namespace frontend\models;


use Yii;
use yii\httpclient\Client;
use yii\web\Cookie;

class Geo extends Client
{
    const URL = 'https://api.sypexgeo.net/json/';
    private $ip;
    private $geo_data;

    private function setIp()
    {
        $this->ip = Yii::$app->request->userIP;
    }

    private function getGeoInfo()
    {
        $this->setIp();
        $response = $this->createRequest()->setUrl(self::URL.$this->ip)->send();

        if ($response->isOk && isset($response->data['country'])) {
            $this->geo_data = $response->data;
        }
    }

    private function getCoockieCountry()
    {
        $cookies = Yii::$app->request->cookies;
        return $cookies->getValue('country');
    }

    private function setCoockieCountry($country)
    {
        $cookies = Yii::$app->response->cookies;
        $cookies->add(new Cookie([
            'name' => 'country',
            'value' => $country,
            'expire' => time() + 3600 * 24 * 365,
            'httpOnly' => true
        ]));
    }

    public function getCountry()
    {
        if ($this->getCoockieCountry()) {
            return $this->getCoockieCountry();
        } else {
            $this->getGeoInfo();
            if ($this->geo_data) {
                $country = $this->geo_data['country']['iso'];
                $this->setCoockieCountry($country);
            } else {
                $country = null;
            }
            return $country;
        }
    }
}