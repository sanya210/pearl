<?php

namespace frontend\models;

use common\models\shop\RozetkaCategory;
use yii\base\Model;
use Yii;
use common\models\shop\ShopCategory;
use common\models\shop\Product;
use common\models\shop\Size;
use common\models\Currency;
use yii\web\Response;
use yii\helpers\Url;

/**
* 
*/
class Rozetka extends Model
{
	const NAME = 'Deo fashion';
	const COMPANY = 'ТОВ ПЕРЛ ФЕШН ГРУП';
	const VENDOR = 'Pearl';

	private function getCurrencies()
	{
		$currencies = Currency::getActive();
		$main_currency = $currencies[Yii::$app->params['main_currency']]['rate'];
		$uah = $currencies['UAH']['rate'];

		$data = '<currency id="UAH" rate="1"/>';

		foreach ($currencies as $currency => $value) {
			$rate = round($uah / $value['rate'], 5);
			if ($currency != 'UAH') {
				$data .= '<currency id="'.$currency.'" rate="'.$rate.'"/>';
			}
		}
		return $data;
	}

	private function getCategories()
	{
		$categories = RozetkaCategory::getActiveCategory();

		$data = '';

		foreach ($categories as $category) {
			$data .= '<category id="'.$category->id.'">'.$category->name.'</category>';
		}
		return $data;
	}

	private function getProducts()
	{
		$products = Product::find()->where([
			'product.status' => Product::STATUS_ACTIVE,
			'product.status_rozetka' => Product::STATUS_ACTIVE,
			'rozetka_category.status' => RozetkaCategory::STATUS_ACTIVE
		])->joinWith('rozetkaCategory')->all();
		$host = Yii::$app->request->hostInfo;
		$data = '';
		
		foreach ($products as $product) {
			foreach ($product->sizes as $size) {
				$url = Url::to(['/'.$product->category->slug.'/'.$product->slug], true);
				$price = $product->rozetka_price ?? $product->priceValue['price'];

				$pictures = '';
				foreach ($product->photoRozetka as $photo) {
					$pictures .= '<picture>'.$host.'/uploads/product/rozetka/'.$photo->name.'</picture>';
				}

				$name = $product->name_rozetka ?: $product->translation['name'];
				$description = $product->translation['description'];
				$color = $product->color->activeLanguage->name;
				$article = $product->article.'.'.$size->europe;

				$params = '';
				$params_arr = [];
				foreach ($product->attribute0 as $attribute) {
					$params_arr[$attribute->attribute0->activeLanguage->name][] = $attribute->activeLanguage->name;
				}
				foreach ($params_arr as $name_attribute => $attribute) {
					$params .= '<param name="'.$name_attribute.'">'.implode('; ', $attribute).'</param>';
				}

				$data .= '<offer id="'.$product->id.'.'.$size->europe.'" available="true">
								<url>'.$url.'</url>
							 	<price>'.$price.'</price>
							 	<currencyId>UAH</currencyId>
							 	<categoryId>'.$product->rozetka_id.'</categoryId>
							 	'.$pictures.'
							 	<vendor>'.self::VENDOR.'</vendor>
							 	<stock_quantity>100</stock_quantity>
							 	<name>'.$name.' '.$size->europe.' '.$size->dimension.' Цвет '.$color.' '.$article.'</name>
							 	<description>
									<![CDATA['.$description.']]>
							 	</description>
							 	<param name="Артикул">'.$article.'</param>
							 	<param name="Цвет">'.$color.'</param>
							 	<param name="Размер">'.$size->europe.' '.$size->dimension.'</param>
							 	<param name="Страна-производитель">Украина</param>
							 	'.$params.'
							 </offer>';
			}
		}
		return $data;
	}

	private function generateXml()
	{
		$host = Yii::$app->request->hostInfo;

		$xml = '<?xml version="1.0" encoding="UTF-8"?>
				  <yml_catalog date="'.date("Y-m-d H:i").'">
				  		<shop>
				  			<name>'.self::NAME.'</name>
				  			<company>'.self::COMPANY.'</company>
				  			<url>'.$host.'</url>
				  			<currencies>
								'.$this->getCurrencies().'
				  			</currencies>
				  			<categories>
				  				'.$this->getCategories().'
				  			</categories>
				  			<offers>
				  				'.$this->getProducts().'
				  			</offers>';

		$xml .= '</shop>
						</yml_catalog>';
		return $xml;
   }

   public function showXml()
   {
		Yii::$app->language = 'ru';
		Yii::$app->currency->set('UAH');

   	$xml = $this->generateXml();

   	Yii::$app->response->format = Response::FORMAT_XML;

   	header("Content-type: text/xml");
   	ob_start();
   	echo $xml;
   	Yii::$app->end();
   }

}















