<?php

namespace frontend\models;

use yii\base\Model;
use Yii;
use common\models\shop\Product;
use common\models\shop\ShopCategory;
use common\models\shop\PseudoCategory;
use common\models\News;
use common\models\Page;
use yii\web\Response;

/**
* 
*/
class Sitemap extends Model
{
	const TAGS = ['lastmod', 'changefreq', 'priority'];

	private $urls = [];
	private $xml;

	private function getUrl()
	{
		$this->generateUrl([
			'loc' => '',
			'changefreq' => 'monthly',
			'priority' => 1
		], true);

		$products = Product::find()->where(['status' => Product::STATUS_ACTIVE])->with('category')->all();
		foreach ($products as $product) {
			$this->generateUrl([
				'loc' => '/'.$product->category->slug.'/'.$product->slug,
				'changefreq' => 'monthly',
				'priority' => 1
			], true);
		}


		$categories = ShopCategory::find()->where(['status' => ShopCategory::STATUS_ACTIVE])->all();
		foreach ($categories as $category) {
			$this->generateUrl([
				'loc' => '/'.$category->slug,
				'changefreq' => 'yearly',
				'priority' => 0.8
			], true);
		}

		$pseudo_categories = PseudoCategory::find()->where(['status' => PseudoCategory::STATUS_ACTIVE])->all();
		foreach ($pseudo_categories as $category) {
			$this->generateUrl([
				'loc' => '/'.$category->language.'/'.$category->slug,
				'changefreq' => 'yearly',
				'priority' => 0.8
			], false);
		}

		$this->generateUrl([
			'loc' => '/news',
			'changefreq' => 'yearly',
			'priority' => 0.5
		], true);

		$news = News::find()->where(['status' => News::STATUS_ACTIVE])->all();
		foreach ($news as $news_item) {
			$this->generateUrl([
				'loc' => '/news/'.$news_item->slug,
				'lastmod' => date(DATE_W3C, $news_item->updated_at),
				'changefreq' => 'monthly',
				'priority' => 0.6
			], true);
		}

		$pages = Page::find()->where(['status' => Page::STATUS_ACTIVE])->all();
		foreach ($pages as $page) {
			$this->generateUrl([
				'loc' => '/site/'.$page->slug,
				'changefreq' => 'yearly',
				'priority' => 0.5
			], true);
		}

		$this->generateUrl([
			'loc' => '/contacts',
			'changefreq' => 'yearly',
			'priority' => 0.5
		], true);
	}

	private function generateUrl($tags, $translate)
	{
		$host = Yii::$app->request->hostInfo;

		if($translate) {
			$link = '';
			foreach (Yii::$app->params['languages_active'] as $lang => $name) {
				$language = $lang == Yii::$app->params['main_language'] ? '' : '/'.$lang;
				$link .= '<xhtml:link 
			               rel="alternate"
			               hreflang="'.$lang.'"
			               href="'.$host.$language.$tags['loc'].'"/>';
			}

			foreach (Yii::$app->params['languages_active'] as $lang => $name) {
				if($lang == Yii::$app->params['main_language']) {
					$lang = '';
				} else {
					$lang = '/'.$lang;
				}
				$this->urls[] = [
					'loc' => $host.$lang.$tags['loc'],
					'changefreq' => $tags['changefreq'],
					'priority' => $tags['priority'],
					'link' => $link
				];
			}
		} else {
			$this->urls[] = [
				'loc' => $host.$tags['loc'],
				'changefreq' => $tags['changefreq'],
				'priority' => $tags['priority'],
			];
		}
	}

	private function generateXml()
	{
		$this->getUrl();

		$xml = '<?xml version="1.0" encoding="UTF-8"?>
		<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">';

		foreach($this->urls as $url) {
			$xml .= '<url><loc>'.$url['loc'].'</loc>';
			$xml .= $url['link'] ?? '';
			foreach (self::TAGS as $tag) {
				$xml .=	isset($url[$tag]) ? '<'.$tag.'>'.$url[$tag].'</'.$tag.'>' : '';
			}
			$xml .= '</url>';
		}

		$xml .= '</urlset>';
		return $xml;
   }

   private function cacheXml()
   {
   	$cache = Yii::$app->cache;
   	$this->xml = $cache->getOrSet('sitemap', function() {
   		return $this->generateXml();
   	});
   }

   public function showXml()
   {
   	// $this->cacheXml();

   	Yii::$app->response->format = Response::FORMAT_XML;

   	header("Content-type: text/xml");
   	ob_start();
   	// echo $this->xml;
   	echo $this->generateXml();
   	Yii::$app->end();
   }

}















