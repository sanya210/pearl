'use strict';

function getAllUrlParams(url) {
	var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

	var obj = {};

	if (queryString) {
		queryString = queryString.split('#')[0];

		var arr = queryString.split('&');

		for (var i = 0; i < arr.length; i++) {
			var a = arr[i].split('=');

			var paramNum = undefined;
			var paramName = a[0].replace(/\[\d*\]/, function (v) {
				paramNum = v.slice(1, -1);
				return '';
			});

			var paramValue = typeof a[1] === 'undefined' ? true : a[1];

			paramName = paramName.toLowerCase();
			paramValue = paramValue.toLowerCase();

			if (obj[paramName]) {
				if (typeof obj[paramName] === 'string') {
					obj[paramName] = [obj[paramName]];
				}

				if (typeof paramNum === 'undefined') {
					obj[paramName].push(paramValue);
				} else {
					obj[paramName][paramNum] = paramValue;
				}
			} else {
				obj[paramName] = paramValue;
			}
		}
	}
	return obj;
}
'use strict';

$(function () {
	$('#main-hamburger').click(function () {
		var hamburger = $(this);
		if (!hamburger.hasClass('is-active')) {
			$('#left-menu').animate({ left: '0' }, 500);
			hamburger.addClass('is-active');
		}
	});

	$('body').on('click', '.btn-filter', function () {
		$('.wrapper-filter').slideToggle(500);
	});

	$('.left-menu__close').click(function () {
		$('#left-menu').animate({ left: '-300px' }, 500);
		$('#main-hamburger').removeClass('is-active');
	});

	$('#pearson-hamburger').click(function () {
		$('.pearson__menu-items').slideToggle(300);
	});

	$(window).resize(function () {
		if ($(window).width() > 991) {
			$('.pearson__menu-items').removeAttr('style');
		}
	});

	$('body').on('click', '.arrow-btn__max', function () {
		var input = $(this).parent('.arrow-btn').siblings('.number__form-input');
		var val = input.val();
		input.val(+val + 1).change();
	});

	$('body').on('click', '.arrow-btn__min', function () {
		var input = $(this).parent('.arrow-btn').siblings('.number__form-input');
		var val = input.val();
		if (val > 0) input.val(+val - 1).change();
	});

	$('body').on('change', '.cart-index .number__form-input', function () {
		$(this).parents('form').submit();
	});

	$('body').on('click', '.category__mosaic-btn', function () {
		$('.category__mosaic-btn').removeClass('active');
		var num_column = +$(this).data('column');
		$('.card').css('width', 'calc(100% / ' + num_column + ' - 24px)');

		$(this).addClass('active');
	});

	$('body').on('change', '.filter__form', function () {
		var values = [];
		$(this).find('input[type=checkbox]:checked').each(function () {
			values.push($(this).val());
		});

		var name_param = $(this).attr('data-name');
		sendFilter(name_param, values.join(','));
	});

	function sendFilter(name_param, value) {
		var url_params = getAllUrlParams(window.location.href);
		url_params[name_param] = value;

		var new_url = window.location.pathname.split('/page')[0];
		var counter = 0;
		for (var param in url_params) {
			if (url_params[param]) {
				new_url += counter++ > 0 ? '&' : '?';
				new_url += param + '=' + url_params[param];
			}
		}

		$.pjax.reload({
			container: '#pjax-category',
			url: new_url,
			timeout: 5000
		});
	}

	$(document).on('pjax:start', function () {
		$('#loader').fadeIn(200);
	}).on('pjax:end', function () {
		$('#loader').fadeOut(200);
	});

	$('body').on('slide', '.slider', function (value) {
		$(this).parent('.range').find('.range__input-min').val(value.value[0]);
		$(this).parent('.range').find('.range__input-max').val(value.value[1]);
	});

	$('body').on('change', '.range__input', function () {
		var elem = $('#' + $(this).attr('name').slice(4));

		var value_min = $('.range__input-min').val();
		var value_max = $('.range__input-max').val();

		var min = elem.slider('getAttribute', 'min');
		var max = elem.slider('getAttribute', 'max');

		if (!isNaN(value_min) && +value_min >= min && !isNaN(value_max) && +value_max <= max) {
			elem.slider('setValue', [+value_min, +value_max]);
		}
	});

	$('body').on('click', '.range__btn', function () {
		var price = $('#category-price').slider('getValue');
		sendFilter('price', price);
	});

	$('.header__category-search-btn').click(function () {
		var container = $(this).siblings('.header__category-search-form');
		container.addClass('active');
		container.removeClass('no-active');
	});

	$('.header__category-search-close').click(function () {
		var container = $(this).parent('.header__category-search-form');
		container.removeClass('active');
		container.addClass('no-active');
	});

	$('.cart-add-form').on('beforeSubmit', function () {
		var form = $(this);

		var size = form.find('#select-size').val();

		if (size) {
			$.ajax({
				url: form.attr('action'),
				type: form.attr('method'),
				data: form.serialize(),
				success: function success(res) {
					var result = JSON.parse(res);
					$('#modal-cart').find('.modal-header h4').text(result.text);
					if (result.img) {
						$('#modal-cart').find('.modal-cart-img').html('<img src="' + result.img + '" alt="' + result.name + '" />');
					}
					if (result.name) {
						$('#modal-cart').find('.modal-cart-name').text(result.name);
					}
					$('#modal-cart').modal('show');
				},
				error: function error() {
					$('#modal-cart').find('.modal-header h4').text('Error');
					$('#modal-cart').modal('show');
				}
			});
		} else {
			$('.product__cart-note').show();
			setTimeout(function () {
				$('.product__cart-note').hide();
			}, 1500);
		}

		return false;
	});

	$('.subscribe__form').on('beforeSubmit', function () {
		var form = $(this);
		$.ajax({
			url: form.attr('action'),
			type: form.attr('method'),
			data: form.serialize(),
			success: function success(res) {
				form.find('.success span').text(res);
				form.find('.success').addClass('active').css('display', 'flex').hide().fadeIn();
				setTimeout(function () {
					form.find('.success').removeClass('active').fadeOut();
					form.trigger('reset');
				}, 3000);
			},
			error: function error(err) {
				form.find('.success span').text(err.statusText);
				form.find('.success').addClass('active').css('display', 'flex').hide().fadeIn();
				setTimeout(function () {
					form.find('.success').removeClass('active').fadeOut();
					form.trigger('reset');
				}, 3000);
			}
		});
		return false;
	});

	$('body').on('click', '.card-wishlist', function () {
		var th = $(this);
		var product_id = th.data('product_id');
		var status = th.data('status');

		if (status == 'in') {
			th.html('<i class="fa fa-heart-o"></i>');
			th.data('status', 'out');
		} else if (status == 'out') {
			th.html('<i class="fa fa-heart"></i>');
			th.data('status', 'in');
		}
		$.ajax({
			url: '/update-wishlist',
			type: 'POST',
			data: 'product_id=' + product_id,
			success: function success(res) {
				th.siblings('.whishlist-success').text(res).show();
				setTimeout(function () {
					th.siblings('.whishlist-success').hide();
				}, 1500);
			}
		});
	});

	function getCookie(name) {
		var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
		return matches ? decodeURIComponent(matches[1]) : undefined;
	}

	if (!getCookie('cookie')) {
		setTimeout(function () {
			$('.cookie').slideToggle();
		}, 1000);
	}

	$('.cookie__btn').click(function () {
		document.cookie = 'cookie=true; max-age=5184000';
		$(this).parent('.cookie').fadeOut();
	});

	$('#contact-form-modal').on('beforeSubmit', function () {
		var form = $(this);
		$.ajax({
			url: form.attr('action'),
			type: 'POST',
			data: form.serialize(),
			beforeSend: function beforeSend() {
				form.find('.success').addClass('active').css('display', 'flex').hide().fadeIn();
			},
			success: function success(res) {
				form.find('.success span').text(res);
				setTimeout(function () {
					form.find('.success').removeClass('active').fadeOut();
					form.trigger('reset');
					$('#modal-contact').modal('hide');
				}, 3000);
			}
		});
		return false;
	});

	$('#addtocartform-size_type').change(function () {
		var type = $(this).val();
		if (type) {
			var options = '';
			var _iteratorNormalCompletion = true;
			var _didIteratorError = false;
			var _iteratorError = undefined;

			try {
				for (var _iterator = window.sizes[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
					var size = _step.value;

					if (size[type]) {
						options += '<option value="' + size.id + '">' + size[type] + '</option>';
					}
				}
			} catch (err) {
				_didIteratorError = true;
				_iteratorError = err;
			} finally {
				try {
					if (!_iteratorNormalCompletion && _iterator.return) {
						_iterator.return();
					}
				} finally {
					if (_didIteratorError) {
						throw _iteratorError;
					}
				}
			}

			$('#select-size').html(options).show();
		} else {
			$('#select-size').html('').hide();
		}
		onChangeSize();
	});

	$('#select-size').change(function () {
		onChangeSize();
	});

	onChangeSize();

	function onChangeSize() {
		var type = $('#addtocartform-size_type').val();
		var size_id = $('#select-size').val();
		if (size_id) {
			var size = window.sizes.find(function (size) {
				return size.id == size_id;
			});
			var name = 'AddToCartForm[sizes][' + size.europe + ']';
			$('#input-number-size').attr('name', name);
			$('.number-size').show();

			$('#table__size').text(size[type]);
			$('#table__chest').text(size.chest);
			$('#table__waist').text(size.waist);
			$('#table_hips').text(size.hips);
			$('.table-size').show();
		} else {
			$('.table-size').hide();
			$('.number-size').hide();
		}
	}
});