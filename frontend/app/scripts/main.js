$(function() {
	$('#main-hamburger').click(function() {
		var hamburger = $(this);
		if(!hamburger.hasClass('is-active')) {
			$('#left-menu').animate({left: '0'}, 500);
			hamburger.addClass('is-active');
		}
	});

	$('body').on('click', '.btn-filter', function() {
		$('.wrapper-filter').slideToggle(500);
	});

	$('.left-menu__close').click(function() {
		$('#left-menu').animate({left: '-300px'}, 500);
		$('#main-hamburger').removeClass('is-active');
	});

	$('#pearson-hamburger').click(function() {
		$('.pearson__menu-items').slideToggle(300);
	});

	$(window).resize(function() {
		if($(window).width() > 991) {
			$('.pearson__menu-items').removeAttr('style');
		}
	});

	$('body').on('click', '.arrow-btn__max', function() {
		let input = $(this).parent('.arrow-btn').siblings('.number__form-input');
		let val = input.val();
		input.val(+val + 1).change();
	});

	$('body').on('click', '.arrow-btn__min', function() {
		let input = $(this).parent('.arrow-btn').siblings('.number__form-input');
		let val = input.val();
		if(val > 0) input.val(+val - 1).change();
	});

	$('body').on('change', '.cart-index .number__form-input', function() {
		$(this).parents('form').submit();
	});

	$('body').on('click', '.category__mosaic-btn', function(){
		$('.category__mosaic-btn').removeClass('active');
		let num_column = +$(this).data('column');
		$('.card').css('width', `calc(100% / ${num_column} - 24px)`);

		$(this).addClass('active');
	});

	$('body').on('change', '.filter__form', function() {
		var values = [];
		$(this).find('input[type=checkbox]:checked').each(function() {
			values.push($(this).val());
		});

		let name_param = $(this).attr('data-name');		
		sendFilter(name_param, values.join(','));
	})

	function sendFilter(name_param, value) {
		let url_params = getAllUrlParams(window.location.href);
		url_params[name_param] = value;

		let new_url = window.location.pathname.split('/page')[0];
		let counter = 0;
		for(let param in url_params) {
			if(url_params[param]) {
				new_url += (counter++ > 0) ? '&' : '?';
				new_url += param + '=' + url_params[param];
			}
		}

		$.pjax.reload({
			container: '#pjax-category',
			url: new_url,
			timeout: 5000,
		});
	}

	$(document).on('pjax:start', function() {
		$('#loader').fadeIn(200);
	}).on('pjax:end', function() {
		$('#loader').fadeOut(200);
	});

	$('body').on('slide', '.slider', function(value) {
		$(this).parent('.range').find('.range__input-min').val(value.value[0]);
		$(this).parent('.range').find('.range__input-max').val(value.value[1]);
	});

	$('body').on('change', '.range__input', function() {
		let elem = $( '#'+$(this).attr('name').slice(4) );
		
		let value_min = $('.range__input-min').val();
		let value_max = $('.range__input-max').val();
		
		let min = elem.slider('getAttribute', 'min');
		let max = elem.slider('getAttribute', 'max');

		if( !isNaN(value_min) && +value_min >= min && !isNaN(value_max) && +value_max <= max ) {
			elem.slider('setValue', [+value_min, +value_max]);
		}
	});

	$('body').on('click', '.range__btn', function() {
		let price = $('#category-price').slider('getValue');
		sendFilter('price', price);
	});

	$('.header__category-search-btn').click(function() {
		let container = $(this).siblings('.header__category-search-form');
		container.addClass('active');
		container.removeClass('no-active');
	});

	$('.header__category-search-close').click(function() {
		let container = $(this).parent('.header__category-search-form');
		container.removeClass('active');
		container.addClass('no-active');
	});

	$('.cart-add-form').on('beforeSubmit', function () {
		let form = $(this);

		const size = form.find('#select-size').val();

		if(size) {
			$.ajax({
				url: form.attr('action'),
				type: form.attr('method'),
				data: form.serialize(),
				success: function(res) {
					let result = JSON.parse(res);
					$('#modal-cart').find('.modal-header h4').text(result.text);
					if(result.img) {				
						$('#modal-cart').find('.modal-cart-img').html(
							`<img src="${result.img}" alt="${result.name}" />`
						);
					}
					if(result.name) {	
						$('#modal-cart').find('.modal-cart-name').text(result.name);
					}
					$('#modal-cart').modal('show');
				},
				error: function() {
					$('#modal-cart').find('.modal-header h4').text('Error');
					$('#modal-cart').modal('show');
				}
			});
		} else {
			$('.product__cart-note').show();
			setTimeout(function() {
				$('.product__cart-note').hide();
			}, 1500);
		}

		return false;
	});

	$('.subscribe__form').on('beforeSubmit', function () {
		let form = $(this);
		$.ajax({
			url: form.attr('action'),
			type: form.attr('method'),
			data: form.serialize(),
			success: function(res) {
				form.find('.success span').text(res);
				form.find('.success').addClass('active').css('display', 'flex').hide().fadeIn();
				setTimeout(function() {
					form.find('.success').removeClass('active').fadeOut();
					form.trigger('reset');
				}, 3000);
			},
			error: function(err) {
				form.find('.success span').text(err.statusText);
				form.find('.success').addClass('active').css('display', 'flex').hide().fadeIn();
				setTimeout(function() {
					form.find('.success').removeClass('active').fadeOut();
					form.trigger('reset');
				}, 3000);
			}
		});
		return false;
	});

	$('body').on('click', '.card-wishlist', function() {
		let th = $(this);
		let product_id = th.data('product_id');
		let status = th.data('status');

		if(status == 'in') {
			th.html('<i class="fa fa-heart-o"></i>');
			th.data('status', 'out');
		} else if(status == 'out') {
			th.html('<i class="fa fa-heart"></i>');
			th.data('status', 'in');
		}
		$.ajax({
			url: '/update-wishlist',
			type: 'POST',
			data: 'product_id='+product_id,
			success: function(res) {
				th.siblings('.whishlist-success').text(res).show();
				setTimeout(function() {
					th.siblings('.whishlist-success').hide();
				}, 1500);
			}
		});
	});

    function getCookie(name) {
        let matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

	if (!getCookie('cookie')) {
		setTimeout(function() {
			$('.cookie').slideToggle();
		}, 1000);
	}

	$('.cookie__btn').click(function() {
		document.cookie = 'cookie=true; max-age=5184000';
		$(this).parent('.cookie').fadeOut();
	})


	$('#contact-form-modal').on('beforeSubmit', function() {
		let form = $(this);
		$.ajax({
			url: form.attr('action'),
			type: 'POST',
			data: form.serialize(),
			beforeSend: function() {
				form.find('.success').addClass('active').css('display', 'flex').hide().fadeIn();
			},
			success: function(res) {
				form.find('.success span').text(res);
				setTimeout(function() {
					form.find('.success').removeClass('active').fadeOut();
					form.trigger('reset');
					$('#modal-contact').modal('hide');
				}, 3000);
			}
		});
		return false;
	});

	$('#addtocartform-size_type').change(function() {
		const type = $(this).val();
		if (type) {
            let options = '';
            for(let size of window.sizes) {
                if (size[type]) {
                    options += `<option value="${size.id}">${size[type]}</option>`;
                }
            }
            $('#select-size').html(options).show();
        } else {
            $('#select-size').html('').hide();
        }
        onChangeSize();
	});

    $('#select-size').change(function() {
        onChangeSize();
    });

    onChangeSize();

    function onChangeSize() {
        const type = $('#addtocartform-size_type').val();
        const size_id = $('#select-size').val();
        if (size_id) {
            const size = window.sizes.find(size => size.id == size_id);
            const name = `AddToCartForm[sizes][${size.europe}]`;
            $('#input-number-size').attr('name', name);
            $('.number-size').show();

            $('#table__size').text(size[type]);
            $('#table__chest').text(size.chest);
            $('#table__waist').text(size.waist);
            $('#table_hips').text(size.hips);
            $('.table-size').show();
        } else {
            $('.table-size').hide();
            $('.number-size').hide();
        }
    }
});