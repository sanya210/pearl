<?php
namespace frontend\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use kv4nt\owlcarousel\OwlCarouselWidget;

class SliderWidget extends Widget
{
    public $models;
    public $header;

    public function run()
    {
        echo '<div class="carousel">
                <div class="carousel__h2">
                    <span>'.$this->header.'</span>
                    <h2>'.$this->header.'</h2>
                </div>';

        OwlCarouselWidget::begin([
            'container' => 'div',
            'pluginOptions' => [
                'autoplay' => true,
                'autoplayTimeout' => 3000,
                // 'items' => 4,
                // 'loop' => true,
                'responsiveClass' => true,
                'margin' => 30,
                'nav' => true,
                'navText' => [
                    '&#8592;',
                    '&#8594;'
                ],
                'responsive' => [
                    320 => [
                        'items' => 1
                    ],
                    480 => [
                        'items' => 2
                    ],
                    768 => [
                        'items' => 3
                    ],
                    992 => [
                        'items' => 4
                    ]
                ]
            ]
        ]);

        foreach ($this->models as $model) {
            echo '<div class="slider-item">';
            echo $this->render('/product/product', [
                'product' => $model
            ]);
            echo '</div>';
        }

        OwlCarouselWidget::end();

        echo '</div>';
    }
}