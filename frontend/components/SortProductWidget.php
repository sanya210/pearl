<?php
namespace frontend\components;

use Yii;
use yii\bootstrap\Dropdown;
use yii\helpers\Url;

class SortProductWidget extends Dropdown
{
    public $data;

    public $slug;

    private $active;

    public function init()
    {
        $this->active = Yii::$app->request->get('sort') ?: array_keys($this->data)[0];
        foreach ($this->data as $param => $label) {
            if($param != $this->active) {
                array_push($this->items, [
                    'label' => Yii::t('category', $label),
                    'url' => Url::current(['sort' => $param])
                ]);
            }
        }

        parent::init();
    }

    public function run()
    {
        return '<div class="dropdown category__sort">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                        '. Yii::t('category', $this->data[$this->active]) .'
                    </a>'.
                    parent::run()
               .'</div>';
    }
}