<?php
namespace frontend\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class MenuWidget extends Widget
{
    public $items;

    public function run()
    {
        $html = '<ul class="main-nav__items">';
        foreach ($this->items as $item) {
            $html .= '<li class="main-nav__item">
            '. Html::a($item['label'], [$item['url']], ['class' => 'main-nav__link']) .'
            </li>';
        }
        $html .= '</ul>';
        return $html;
    }
}