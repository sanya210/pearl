<?php
namespace frontend\components;

use Yii;
use kartik\slider\Slider;

class RangeWidget extends Slider
{
    private $min;
    private $max;
    
    public function init()
    {
        $this->min = Yii::$app->params['min_price'];
        $this->max = Yii::$app->params['max_price'];

        $values = Yii::$app->request->get('price') ?: null;

        $this->value = $values;
        $this->id = $this->name;
        $this->sliderColor = self::TYPE_GREY;
        $this->pluginOptions = [
            'min' => $this->min,
            'max' => $this->max,
            'step' => 1,
            'range' => true,
            'tooltip' => 'hide'
        ];

        parent::init();
    }

    public function run()
    {
        $value_min = null;
        $value_max = null;

        $values = explode(',', $this->value);
        if(count($values) === 2){
            $value_min = $values[0];
            $value_max = $values[1];
        }

        echo '<div class="range">
                <div class="filter__name">
                    '.Yii::t('category', 'Price, '.Yii::$app->currency->sign).'
                </div>
                <div class="range__wrapper">
                    <input type="text" class="range__input range__input-min" name="min-'.$this->name.'" value="'.$value_min.'">
                    -
                    <input type="text" class="range__input range__input-max"  name="max-'.$this->name.'" value="'.$value_max.'">
                    <button class="range__btn">OK</button>
                </div>';
        parent::run();
        echo '</div>';
    }
}