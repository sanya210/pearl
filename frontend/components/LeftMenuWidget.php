<?php
namespace frontend\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class LeftMenuWidget extends Widget
{
    public $items;
    public $sections;

    public function run()
    {
        $html = '<div id="left-menu">
                    <button class="left-menu__close">×</button>
                    <div class="left-menu__logo">
                        <img src="/img/icon/logo_white.svg" alt="'.Yii::$app->name.'">
                    </div>
                    <ul class="left-menu__items">';

        foreach ($this->items as $item) {
            $html .= '<li class="left-menu__item">
            '. Html::a($item['label'], [$item['url']], ['class' => 'left-menu__link']) .'
            </li>';
        }

        $html .= '</ul><ul class="left-menu__category-items">';

        foreach ($this->sections as $section) {
            $html .= '<li class="left-menu__item">';
            if (isset($section['url']) && $section['url']) {
                $html .= Html::a($section['label'], [$section['url']], ['class' => 'left-menu__link']);
            } else {
                $html .= '<span class="left-menu__link">' . $section['label'] . '</span>';
                if (isset($section['categories']) && $section['categories']) {
                    $html .= '<ul class="left-menu__submenu">';
                    foreach ($section['categories'] as $category) {
                        $html .= '<li>';
                        $html .= Html::a($category['label'], [$category['url']], ['class' => 'left-menu__link']);
                        $html .= '</li>';
                    }
                    $html .= '</ul>';
                }
            }
            $html .= '</li>';
        }

        $html .= '</ul></div>';

        return $html;
    }
}