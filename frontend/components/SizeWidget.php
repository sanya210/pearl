<?php
namespace frontend\components;

use common\models\Country;
use yii\base\Widget;
use frontend\models\shop\AddToCartForm;

class SizeWidget extends Widget
{
    public $form;
    public $model;
    public $sizes;

    public function run()
    {
        $default_type = Country::getSizeType();
        echo $this->render('size', [
            'form' => $this->form,
            'model' => $this->model,
            'sizes' => $this->sizes,
            'default_type' => $default_type,
        ]);
    }
}