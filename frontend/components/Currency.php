<?php
namespace frontend\components;

use yii\base\BaseObject;
use Yii;
use common\models\Currency as Rate;

/**
* 
*/
class Currency extends BaseObject
{
	public $currencies;

	public $symbol;
	public $sign;

	private $rate;

	function __construct($config = [])
	{
		$this->currencies = Yii::$app->params['currencies'];

		if(Yii::$app->session['currency']) {
			$this->symbol = Yii::$app->session['currency'];
			$this->sign = $this->currencies[Yii::$app->session['currency']];
		} else {
			$this->symbol = 'USD';
			$this->sign = $this->currencies['USD'];
		}

		$this->rate = Rate::getRate($this->symbol);

		parent::__construct($config);
	}

	public function set($symbol)
	{
      Yii::$app->session->set('currency', $symbol);
		$this->symbol = $symbol;
		$this->sign = $this->currencies[$symbol];
		$this->rate = Rate::getRate($this->symbol);
	}

	public function transfer($sum)
	{
		return $this->rate * $sum;
	}

	public function backTransfer($sum)
	{
		return $sum / $this->rate;
	}

}






































