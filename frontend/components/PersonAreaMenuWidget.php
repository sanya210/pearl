<?php
namespace frontend\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;

/**
* 
*/
class PersonAreaMenuWidget extends Widget
{
	public $menuItems;
	public $url;

	public function init()
	{
		$this->url = explode('/', Yii::$app->request->pathInfo)[0];
	}

	public function run()
	{
		$menu = '<div class="pearson__menu-items">';

		foreach ($this->menuItems as $item) {
			$isActive = false;
			if($item['url']{0} === '/') {
				$url = explode('/', $item['url']);
				$isActive = $url[1] == $this->url;
			}

			$menu .= $isActive ? '<div class="pearson__menu-item active">' : '<div class="pearson__menu-item">';
			$menu .= Html::a($item['label'], [$item['url']], ['class' => 'pearson__menu-link']);
			
			$menu .= '</div>';
		}
		$menu .= '</div>';
		return $menu;
	}

}