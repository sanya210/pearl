<?php
namespace frontend\components;


use Yii;
use yii\bootstrap\Modal;
use yii\bootstrap\Widget;

class AlertWidget extends Widget
{
    public $alertTypes = [
        'error'   => 'alert-danger',
        'danger'  => 'alert-danger',
        'success' => 'alert-success',
        'info'    => 'alert-info',
        'warning' => 'alert-warning'
    ];

    public function run()
    {
        $session = Yii::$app->session;
        $flashes = $session->getAllFlashes();

        if ($flashes) {
            Modal::begin([
                'options' => [
                    'class' => 'alert-modal',
                ],
                'clientOptions' => ['show' => true],
                'toggleButton' => false,
            ]);

            $appendClass = isset($this->options['class']) ? ' ' . $this->options['class'] : '';

            foreach ($flashes as $type => $flash) {
                if (!isset($this->alertTypes[$type])) {
                    continue;
                }

                foreach ((array) $flash as $i => $message) {
                    echo \yii\bootstrap\Alert::widget([
                        'body' => $message,
                        'closeButton' => false,
                        'options' => array_merge($this->options, [
                            'id' => $this->getId() . '-' . $type . '-' . $i,
                            'class' => $this->alertTypes[$type] . $appendClass,
                        ]),
                    ]);
                }

                $session->removeFlash($type);
            }

            Modal::end();
        }
    }
}