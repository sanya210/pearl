<?php
namespace frontend\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use common\models\News;

class NewsWidget extends Widget
{
    public $cancel;
    public $header;
    private $news;

    public function init()
    {
        $this->news = News::find()->where([
            'status' => News::STATUS_ACTIVE,
        ])
        ->andWhere(['!=', 'id', $this->cancel ?: false])
        ->orderBy(['updated_at' => SORT_DESC])
        ->limit(3)
        ->all();
    }

    public function run()
    {
        if($this->news) {        
            echo $this->render('news', [
                'news' => $this->news,
                'header' => $this->header
            ]);
        }
    }
}