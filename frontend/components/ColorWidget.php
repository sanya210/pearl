<?php
namespace frontend\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class ColorWidget extends Widget
{
    public $colors;

    public function run()
    {
        $html = '<ul class="color-list">';
        $html .= '<li class="color-item active">
                    <span style="background-color:'.$this->colors['current_color']['hex'].'"></span>
                    <div class="color-name">'.$this->colors['current_color']['name'].'</div>
                  </li>';

        foreach ($this->colors['other_colors'] as $color) {
            $html .= '<li class="color-item">
                        <a href="'.Url::to([$color['link']]).'" class="color-link">
                            <span style="background-color:'.$color['hex'].'"></span>
                            <div class="color-name">'.$color['name'].'</div>
                        </a>
                      </li>';
        }

        $html .= '</ul>';

        return $html;
    }
}