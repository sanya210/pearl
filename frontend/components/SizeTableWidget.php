<?php
namespace frontend\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use common\models\shop\Size;

class SizeTableWidget extends Widget
{
    public $sizes;

    public function run()
    {
        Modal::begin([
            'header' => Yii::t('product', 'Size table'),
            'toggleButton' => [
                'label' => Yii::t('product', 'Size table'),
                'class' => 'size__link'
            ],
            'size' => 'modal-lg'
        ]);

        echo $this->render('size_table', [
            'sizes' => $this->sizes
        ]);

        Modal::end();
    }
}