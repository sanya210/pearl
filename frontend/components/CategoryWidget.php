<?php
namespace frontend\components;

use Yii;
use yii\base\Widget;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;

/**
* 
*/
class CategoryWidget extends Widget
{
	public $menuItems;

    public function run()
    {
        $menu = '<div class="header__category-items">';

        foreach ($this->menuItems as $section) {
            $menu .= '<div class="header__category-dot"></div>';
            $menu .= '<div class="header__category-item">';
            if (isset($section['url'])) {
                $menu .= Html::a($section['label'], [$section['url']], ['class' => 'header__category-link']);
            } else {
                $menu .= Html::a($section['label'], ['#'], [
                    'data-toggle' => 'dropdown',
                    'class' => 'header__category-link'
                ]);
                if (isset($section['categories']) && $section['categories']) {
                    $menu .= Dropdown::widget([
                        'options' => [
                            'class' => 'header__category-submeny',
                        ],
                        'items' => $section['categories']
                    ]);
                }
            }
            $menu .= '</div>';
        }
        $menu .= '<div class="header__category-dot"></div>';
        $menu .= '</div>';
        return $menu;
    }

}