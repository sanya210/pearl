<?php
namespace frontend\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class InputWidget extends Widget
{
    public $label;
    public $name;
    public $value;
    public $id;

    public $form;
    public $model;

    public function run()
    {
        $input = $this->form->field($this->model, $this->name)
            ->textInput([
                'class' => 'number__form-input',
                'type' => 'number',
                'value' => $this->value,
                'min' => 0,
                'id' => $this->id ?? 'addtocartform-sizes-'.$this->name
            ])
            ->label($this->label ?? false);
        
        return '<div class="number__form-control">
                    '.$input.'
                    <div class="arrow-btn">
                        <button type="button" class="arrow-btn__max arrow-input">
                            <i class="fa fa-angle-up"></i>
                        </button>
                        <button type="button" class="arrow-btn__min arrow-input">
                            <i class="fa fa-angle-down"></i>
                        </button>
                    </div>
                </div>';
    }
}