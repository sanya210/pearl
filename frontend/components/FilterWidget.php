<?php
namespace frontend\components;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class FilterWidget extends Widget
{
    private $param;
    public $name;
    public $slug;
    public $items;

    public function init()
    {
        $this->param = explode(',', Yii::$app->request->get($this->slug));
    }

    public function run()
    {
        $html = '<div class="filter">
                    <form id="filter__'.$this->slug.'" class="filter__form" data-name="'.$this->slug.'">
                        <div class="filter__name">'.$this->name.'</div>';

        foreach ($this->items as $id => $item) {
            $checked = in_array($id, $this->param) ? 'checked' : '';
            $html .= '<div class="check">
                        <input type="checkbox" name="'.$this->slug.'" class="filter__input" id="'.$this->slug.$id.'" value="'.$id.'" '.$checked.'>
                        <label for="'.$this->slug.$id.'" class="check__wrapper"></label>
                        <label for="'.$this->slug.$id.'">'.$item.'</label>
                      </div>';
        }

        $html .= '</form></div>';

        return $html;
    }
}