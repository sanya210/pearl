<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\StringHelper;

?>

<div class="news-widget">
	<div class="news-header">
		<span><?=$header?></span>
		<h4><?=$header?></h4>
	</div>

	<ul class="news__items">
		<?php foreach ($news as $item) { ?>
		<li class="news__item">
			<div class="news__wrapper1">
				<a href="<?=Url::to(['/news/'.$item->slug])?>">
					<img src="/uploads/news/<?=$item->img_preview?>" class="news__img" alt="<?=$item->activeLanguage->name?>">
				</a>
				<div class="news__bottom">
					<?=Html::a($item->activeLanguage->name, ['/news/'.$item->slug], ['class' => 'news__link'])?>
				</div>
			</div>
			<div class="news__wrapper2">
				<h2 class="news__link"><?=$item->activeLanguage->name?></h2>
				<div class="news__text">
					<p><?=StringHelper::truncate(strip_tags($item->activeLanguage->text), 250)?></p>
				</div>
				<?=Html::a(Yii::t('pages', 'Read'), ['/news/'.$item->slug], ['class' => 'news__btn'])?>		
			</div>
			<div class="news__date"><?=Yii::t('pages', 'Date')?>: <?=Yii::$app->formatter->asDate($item->updated_at)?></div>
		</li>
		<?php } ?>
	</ul>
</div>




