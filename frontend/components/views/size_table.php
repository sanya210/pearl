<table class="table table-bordered size__table">
	<thead>
        <tr>
            <td colspan="6"><?=Yii::t('product', 'Size') ?></td>
            <td colspan="2"><?=Yii::t('product', 'Chest') ?></td>
            <td colspan="2"><?=Yii::t('product', 'Waist') ?></td>
            <td colspan="2"><?=Yii::t('product', 'Hips') ?></td>
        </tr>
		<tr>
			<td>Letter (USA)</td>
			<td>Number (USA)</td>
			<td>EU</td>
			<td>RU</td>
			<td>UK</td>
			<td>DE</td>

			<td><?=Yii::t('product', 'Cm') ?></td>
			<td><?=Yii::t('product', 'Inch') ?></td>
			<td><?=Yii::t('product', 'Cm') ?></td>
			<td><?=Yii::t('product', 'Inch') ?></td>
			<td><?=Yii::t('product', 'Cm') ?></td>
			<td><?=Yii::t('product', 'Inch') ?></td>
		</tr>
	</thead>

	<tbody>
		<?php foreach ($sizes as $size) { ?>
		<tr>
			<td><?=$size->international?></td>
			<td><?=$size->number_usa?></td>
			<td><?=$size->europe?></td>
			<td><?=$size->ru?></td>
			<td><?=$size->uk?></td>
			<td><?=$size->de?></td>

            <td><?=$size->chest?></td>
            <td><?=$size->chest_inch?></td>
			<td><?=$size->waist?></td>
			<td><?=$size->waist_inch?></td>
			<td><?=$size->hips?></td>
			<td><?=$size->hips_inch?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>