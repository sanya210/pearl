<?php
use frontend\models\shop\AddToCartForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;

$this->registerJs('window.sizes = '.Json::encode($sizes).';', View::POS_BEGIN);
if ($default_type) {
    $default_sizes = array_filter(ArrayHelper::map($sizes, 'id', $default_type));
} else {
    $default_sizes = [];
}
?>

<div class="container_size">
    <div class="type-size">
        <?=$form->field($model, 'size_type')->dropDownList(AddToCartForm::getTypes(), [
            'class' => 'type-size__input',
            'prompt' => Yii::t('product', 'Choose a dimensional grid: '),
            'value' => $default_type
        ])->label(false)?>
    </div>

    <div class="select-size">
        <?=Html::dropDownList('size', null, $default_sizes, [
            'class' => 'type-size__input',
            'id' => 'select-size',
            'style' => $default_sizes ? '' : 'display:none;',
        ])?>
    </div>

    <table class="table table-size">
        <thead>
        <tr>
            <td><?=Yii::t('product', 'Size') ?></td>
            <td><?=Yii::t('product', 'Chest, cm') ?></td>
            <td><?=Yii::t('product', 'Waist, cm') ?></td>
            <td><?=Yii::t('product', 'Hips, cm') ?></td>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td id="table__size"></td>
            <td id="table__chest"></td>
            <td id="table__waist"></td>
            <td id="table_hips"></td>
        </tr>
        </tbody>
    </table>

    <div class="number-size">
        <div><?=Yii::t('product', 'Amount')?>:</div>
        <div class="number__form-control">
            <input type="number" class="number__form-input" min="1" id="input-number-size" value="1">
            <div class="arrow-btn">
                <button type="button" class="arrow-btn__max arrow-input">
                    <i class="fa fa-angle-up"></i>
                </button>
                <button type="button" class="arrow-btn__min arrow-input">
                    <i class="fa fa-angle-down"></i>
                </button>
            </div>
        </div>
    </div>
</div>