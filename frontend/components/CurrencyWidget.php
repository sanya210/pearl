<?php
namespace frontend\components;

use Yii;
use yii\bootstrap\Dropdown;
use common\models\Currency;

class CurrencyWidget extends Dropdown
{
    public function init()
    {
        $currencies = Yii::$app->params['currencies'];

        foreach ($currencies as $symbol => $sign) {
            $this->items[] = [
                'label' => $sign.' '.$symbol,
                'url' => ['/change-currency', 'currency' => $symbol]
            ];
        }

        parent::init();
    }

    public function run()
    {
        return '<div class="dropdown dropdown-currency">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                        '.Yii::$app->currency->sign.' '.Yii::$app->currency->symbol.'
                    </a>'.
                    parent::run()
               .'</div>';
    }
}