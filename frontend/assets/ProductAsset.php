<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ProductAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/smoothproducts.css',
    ];
    public $js = [
        'js/smoothproducts.min.js',
        'js/product.js'
    ];
    public $depends = [
        'frontend\assets\AppAsset',
    ];
}
