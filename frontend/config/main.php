<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'name' => 'Pearl Fashion Group',
    'language' => $params['main_language'],
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'currency' => [
            'class' => 'frontend\components\Currency'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'class' => 'codemix\localeurls\UrlManager',
            'languages' => array_keys($params['languages_active']),
            'enableLanguageDetection' => false,
            'enableLanguagePersistence' => false,
            'rules' => [
                '' => 'site/index',
                '/site/<slug>' => '/site/page',
                '/contacts' => '/site/contacts',
                '/modal-contacts' => '/site/modal-contacts',

                '/sitemap.xml' => '/site/sitemap',
                '/rozetka.xml' => '/site/rozetka',
                
                '/add-push-token' => '/site/add-push-token',

                '/change-currency' => '/site/change-currency',

                '/profile' => '/person-area/profile',
                '/addresses' => '/person-area/addresses',
                '/orders/page/<page:\d+>' => '/person-area/orders',
                '/orders' => '/person-area/orders',

                '/pays/page/<page:\d+>' => '/person-area/pays',
                '/pays' => '/person-area/pays',

                '/pay' => '/payment/pay',
                '/paypal' => '/payment/paypal',
                '/paypal-send' => '/payment/paypal-send',
                '/liqpay' => '/payment/liqpay',
                '/pay-success' => '/payment/pay-success',
                '/callback-liqpay' => '/payment/callback-liqpay',
                '/callback-paypal' => '/payment/callback-paypal',

                '/checkout' => 'order/checkout',
                '/delete-order' => 'order/delete-order',

                '/cart' => 'cart/index',
                '/cart/add' => 'cart/add',
                '/cart/remove' => 'cart/remove',

                '/wishlist/page/<page:\d+>' => '/product/wishlist',
                '/wishlist' => '/product/wishlist',
                '/update-wishlist' => 'product/update-wishlist',

                '/news/page/<page:\d+>' => '/news/index',
                '/news/<slug>' => '/news/view',
                '/news' => '/news/index',

                '/subscribe' => 'site/subscribe',
                '/auth' => 'site/auth',
                '/login' => 'site/login',
                '/logout' => 'site/logout',
                '/signup' => 'site/signup',
                '/verify-email' => 'site/verify-email',
                '/request-password-reset' => 'site/request-password-reset',
                '/reset-password' => 'site/reset-password',
                '/resend-verification-email' => 'site/resend-verification-email',

                '/search' => 'site/search',

                '/<category>/page/<page:\d+>' => '/category/view',
                '/<category>/<product>' => '/product/index',
                '/<category>' => '/category/view',
            ],
        ],
    ],
    'params' => $params,
    'on beforeRequest' => function () {
        $pathInfo = Yii::$app->request->pathInfo;
        if (!empty($pathInfo) && substr($pathInfo, -1) === '/') {
            Yii::$app->response->redirect('/' . substr(rtrim($pathInfo), 0, -1), 301)->send();
        }
    },
];
