<?php
namespace backend\controllers;

use backend\models\etsy\Etsy;
use Yii;
use yii\helpers\Url;
use yii\web\ConflictHttpException;
use yii\web\Controller;
use backend\models\etsy\Product;
use yii\web\NotFoundHttpException;

class EtsyController extends Controller
{
    public function actionRefreshToken()
    {
        return $this->render('refresh-token');
    }

    public function actionEtsyAuth()
    {
        $login_url = Etsy::auth();
        if ($login_url) {
            return $this->redirect(Url::to($login_url));
        } else {
            Yii::$app->session->addFlash('danger', 'Не удалось обновить токен!');
            return $this->redirect(Yii::$app->request->referrer ?: Url::to(['/']));
        }
    }

    public function actionGetEtsyToken()
    {
        $request_token = Yii::$app->request->get('oauth_token');
        $verifier = Yii::$app->request->get('oauth_verifier');
        $result = Etsy::getToken($request_token, $verifier);
        if ($result) {
            Yii::$app->session->addFlash('success', 'Токен успешно обновлён!');
        } else {
            Yii::$app->session->addFlash('danger', 'Не удалось обновить токен!');
        }
        return $this->redirect('refresh-token');
    }

    public function actionIndex($id)
    {
        $model = $this->findModel($id);

        if (!$model->category->etsy_id) {
            throw new ConflictHttpException('Отсутствует связка категории товара с etsy');
        }

        if ($model->load(Yii::$app->request->post())) {
            $session = Yii::$app->session;
            if ($model->save()) {
                $session->addFlash('success', 'Вы успешно редактировали товар!');
            } else {
                $session->addFlash('danger', 'Не удалось редактировать товар!');
            }
            return $this->redirect(['index', 'id' => $id, [
                'model' => $model
            ]]);
        }

        return $this->render('index', [
            'model' => $model
        ]);
    }

    public function actionCreate()
    {
        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        return $model->createEtsy();
    }

    public function actionUpdate()
    {
        $id = Yii::$app->request->post('id');
        $model = $this->findModel($id);
        return $model->updateEtsy();
    }

    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionFavorite()
    {
        $model = new Etsy();
        $res = $model->findAllShopReceipts();
        echo '<pre>';
        print_r($res);
        echo '</pre>';
    }
}