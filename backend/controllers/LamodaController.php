<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use backend\models\lamoda\Lamoda;
use common\models\shop\Product;

/**
 * LamodaController
 */
class LamodaController extends Controller
{
	public function actionIndex($id)
	{
		$model = Lamoda::findOne($id);

		if (!$model) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}

		$post = Yii::$app->request->post();
		if ($post && isset($post['DynamicModel'])) {
			$res = $model->send($post['DynamicModel']);
			Yii::$app->session->addFlash($res['status'], $res['message']);
			return $this->redirect(['index', 'id' => $id]);
		}

		$properties = $model->getProperties();

		return $this->render('index', [
			'model' => $model,
			'properties_model' => $properties['model'],
			'properties_values' => $properties['values'],
		]);
	}
}
