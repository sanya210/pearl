<?php

namespace backend\controllers;

use Yii;
use common\models\News;
use common\models\NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();

        $model->initLanguages();

        $post = Yii::$app->request->post();
        if( $post && $model->load($post) ) {
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->image_preview = UploadedFile::getInstance($model, 'image_preview');
            if( $model->validate() && 
                Model::loadMultiple($model->languages, $post) &&
                Model::validateMultiple($model->languages) ) {
                    $session = Yii::$app->session;
                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        $model->img = $model->image->baseName . '_' . time() . '.' . $model->image->extension;
                        $model->img_preview = $model->image_preview->baseName . '_' . time() . '.' . $model->image_preview->extension;

                        $model->save_with_slug($model->languages[Yii::$app->params['main_language']]['name']);

                        $model->image->saveAs('uploads/news/' . $model->img);
                        $model->image_preview->saveAs('uploads/news/' . $model->img_preview);
                        
                        $model->saveLanguages();
                        $transaction->commit();
                        return $this->redirect(['update', 'id' => $model->id]);
                    } catch(\Throwable $e) {
                        $transaction->rollBack();
                        $session->addFlash('danger', 'Не удалось создать новость!');
                    }
            }
        }

        return $this->render('create', ['model' => $model]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->initLanguages();

        $post = Yii::$app->request->post();
        if( $post && $model->load($post) ) {
            $model->image = UploadedFile::getInstance($model, 'image');
            $model->image_preview = UploadedFile::getInstance($model, 'image_preview');
         
            if( $model->validate() && 
                Model::loadMultiple($model->languages, $post) && 
                Model::validateMultiple($model->languages) ) {
                    $session = Yii::$app->session;
                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        if($model->image) {
                            $model->img = $model->image->baseName . '_' . time() . '.' . $model->image->extension;
                        }
                        if($model->image_preview) {
                            $model->img_preview = $model->image_preview->baseName . '_' . time() . '.' . $model->image_preview->extension;
                        }

                        $model->save();

                        if($model->image) {
                            $model->image->saveAs('uploads/news/' . $model->img);
                        }
                        if($model->image_preview) {
                            $model->image_preview->saveAs('uploads/news/' . $model->img_preview);
                        }

                        $model->saveLanguages();
                        $transaction->commit();
                        $session->addFlash('success', 'Вы успешно редактировали новость!');
                    } catch(\Throwable $e) {
                        $transaction->rollBack();
                        $session->addFlash('danger', 'Не удалось редактировать новость!');
                    }
                    return $this->redirect(['update', 'id' => $model->id]);
            }
        }

        return $this->render('update', ['model' => $model]);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
