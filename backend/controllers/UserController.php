<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\PushForm;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if( $model->load(Yii::$app->request->post()) ) {
            $session = Yii::$app->session;
            if( $model->save() ) {
                $session->addFlash('success', 'Данные изменены!');
            } else {
                $session->addFlash('danger', 'Не удалось изменить данные!');
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            $model->scenario = User::SCENARIO_ADMIN;
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionPush()
    {
        $model = new PushForm();
        if( $model->load(Yii::$app->request->post()) ) {
            $session = Yii::$app->session;
            if( $model->send() ) {
                $session->addFlash('success', 'Уведомление отправлено!');
            } else {
                $session->addFlash('danger', 'Не удалось отправить уведомление!');
            }
        };
        return $this->redirect(Yii::$app->request->referrer ?: Url::to(['/']));
    }
}
