<?php

namespace backend\controllers;

use Yii;
use common\models\MainSlider;
use common\models\MainSliderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\base\Model;
use yii\helpers\Inflector;

/**
 * MainSliderController implements the CRUD actions for MainSlider model.
 */
class MainSliderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MainSlider models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MainSliderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new MainSlider model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MainSlider();

        $model->initLanguages();

        $post = Yii::$app->request->post();
        if( $post && $model->load($post) ) {
            $model->image_lg = UploadedFile::getInstance($model, 'image_lg');
            $model->image_xs = UploadedFile::getInstance($model, 'image_xs');
            if( $model->validate() && 
                Model::loadMultiple($model->languages, $post) &&
                Model::validateMultiple($model->languages) ) {
                    $session = Yii::$app->session;
                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        $model->img_lg = Inflector::slug($model->image_lg->baseName) . '_' . time() . '.' . $model->image_lg->extension;
                        $model->img_xs = Inflector::slug($model->image_xs->baseName) . '_' . time() . '.' . $model->image_xs->extension;

                        $model->save();

                        $model->image_xs->saveAs('uploads/slider/' . $model->img_xs);
                        $model->image_lg->saveAs('uploads/slider/' . $model->img_lg);
                        
                        $model->saveLanguages();
                        $transaction->commit();
                        return $this->redirect(['update', 'id' => $model->id]);
                    } catch(\Throwable $e) {
                        $transaction->rollBack();
                        $session->addFlash('danger', 'Не удалось создать слайдер!');
                    }
            }
        }

        return $this->render('create', ['model' => $model]);
    }

    /**
     * Updates an existing MainSlider model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->initLanguages();

        $post = Yii::$app->request->post();
        if( $post && $model->load($post) ) {
            $model->image_lg = UploadedFile::getInstance($model, 'image_lg');
            $model->image_xs = UploadedFile::getInstance($model, 'image_xs');
         
            if( $model->validate() && 
                Model::loadMultiple($model->languages, $post) && 
                Model::validateMultiple($model->languages) ) {
                    $session = Yii::$app->session;
                    $transaction = Yii::$app->db->beginTransaction();
                    try {
                        if($model->image_lg) {
                            $model->img_lg = Inflector::slug($model->image_lg->baseName) . '_' . time() . '.' . $model->image_lg->extension;
                        }
                        if($model->image_xs) {
                            $model->img_xs = Inflector::slug($model->image_xs->baseName) . '_' . time() . '.' . $model->image_xs->extension;
                        }

                        $model->save();

                        if($model->image_lg) {
                            $model->image_lg->saveAs('uploads/slider/' . $model->img_lg);
                        }
                        if($model->image_xs) {
                            $model->image_xs->saveAs('uploads/slider/' . $model->img_xs);
                        }

                        $model->saveLanguages();
                        $transaction->commit();
                        $session->addFlash('success', 'Вы успешно редактировали слайдер!');
                    } catch(\Throwable $e) {
                        $transaction->rollBack();
                        $session->addFlash('danger', 'Не удалось редактировать слайдер!');
                    }
                    return $this->redirect(['update', 'id' => $model->id]);
            }
        }

        return $this->render('update', ['model' => $model]);
    }

    /**
     * Deletes an existing MainSlider model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MainSlider model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MainSlider the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MainSlider::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
