<?php
namespace backend\controllers;

use backend\models\kasta\Kasta;
use backend\models\kasta\KastaExel;
use yii\web\Controller;

class KastaController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionPrice()
    {
        $file="kasta.xls";
        header("Content-type: application/vnd.ms-excel; charset=UTF-8");
        header("Content-Disposition: attachment; filename=$file");

        $model = new KastaExel();
        $model->generate();
    }
}