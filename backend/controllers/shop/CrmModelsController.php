<?php

namespace backend\controllers\shop;

use Yii;
use backend\models\CrmModeliType;
use backend\models\CrmModeliTypeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CrmModeliTypeController implements the CRUD actions for CrmModeliType model.
 */
class CrmModelsController extends Controller
{
    /**
     * Lists all CrmModeliType models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CrmModeliTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
