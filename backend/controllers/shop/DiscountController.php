<?php

namespace backend\controllers\shop;

use Yii;
use common\models\shop\Discount;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class DiscountController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Discount();

        $session = Yii::$app->session;
        if($model->load(Yii::$app->request->post()) && 
           $model->date_start <= $model->date_finish &&
           $model->save()
        ) {
            $session->addFlash('success', 'Акция успешно добавлена!');
        } else {
            $session->addFlash('danger', 'Не удалось добавить акцию!');
        }

        return $this->redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if(Yii::$app->request->isAjax) {
            return $this->renderAjax('/shop/product/discount/_form', [
                'model' => $model
            ]);
        }

        $session = Yii::$app->session;
        if($model->load(Yii::$app->request->post()) && 
           $model->date_start <= $model->date_finish &&
           $model->save()
        ) {
            $session->addFlash('success', 'Акция успешно изменена!');
        } else {
            $session->addFlash('danger', 'Не удалось изменить акцию!');
        }

        return $this->redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = Discount::STATUS_DELETED;
        $model->save();

        return $this->redirect($_SERVER['HTTP_REFERER']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Discount::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
