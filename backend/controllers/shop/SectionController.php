<?php

namespace backend\controllers\shop;

use Throwable;
use Yii;
use common\models\shop\Section;
use common\models\shop\SectionSearch;
use yii\base\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SectionController implements the CRUD actions for Section model.
 */
class SectionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Section models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SectionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Section model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Section();
        $model->initLanguages();

        $post = Yii::$app->request->post();
        if( $post &&
            $model->load($post) &&
            $model->validate() &&
            Model::loadMultiple($model->languages, $post) &&
            Model::validateMultiple($model->languages) ) {
            $session = Yii::$app->session;
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $model->save();
                $model->saveLanguages();
                $transaction->commit();
                return $this->redirect(['update', 'id' => $model->id]);
            } catch(Throwable $e) {
                $transaction->rollBack();
                $session->addFlash('danger', 'Не удалось создать раздел!');
            }
        }

        return $this->render('create', ['model' => $model]);
    }

    /**
     * Updates an existing Section model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->initLanguages();

        $post = Yii::$app->request->post();
        if( $post &&
            $model->load($post) &&
            $model->validate() &&
            Model::loadMultiple($model->languages, $post) &&
            Model::validateMultiple($model->languages) ) {
            $session = Yii::$app->session;
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $model->save();
                $model->saveLanguages();
                $transaction->commit();
                $session->addFlash('success', 'Вы успешно редактировали раздел!');
            } catch(\Throwable $e) {
                $transaction->rollBack();
                $session->addFlash('danger', 'Не удалось редактировать раздел!');
            }
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', ['model' => $model]);
    }

    /**
     * Deletes an existing Section model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Section model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Section the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Section::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
