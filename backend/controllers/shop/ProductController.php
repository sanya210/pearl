<?php

namespace backend\controllers\shop;

use Yii;
use common\models\shop\Product;
use common\models\shop\ProductPhoto;
use common\models\shop\ProductRozetka;
use common\models\shop\ProductKasta;
use common\models\shop\ProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'create-etsy' => ['POST'],
                    'update-etsy' => ['POST'],
                    'rozetka-mail' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        $model->load(Yii::$app->request->get());

        $model->initLanguages();

        $post = Yii::$app->request->post();
        if( $post && 
            $model->load($post) && 
            $model->validate() && 
            Model::loadMultiple($model->languages, $post) &&
            Model::validateMultiple($model->languages) ) {
                $session = Yii::$app->session;
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $model->save_with_slug($model->languages[Yii::$app->params['main_language']]['name']);
                    $model->saveLanguages();
                    $model->files = UploadedFile::getInstances($model, 'files');
                    $model->upload();
                    $model->files_rozetka = UploadedFile::getInstances($model, 'files_rozetka');
                    $model->uploadRozetka();
                    $model->files_kasta = UploadedFile::getInstances($model, 'files_kasta');
                    $model->uploadKasta();
                    Yii::$container->invoke([$model, 'reindex']);
                    $transaction->commit();
                    return $this->redirect(['update', 'id' => $model->id]);
                } catch(\Throwable $e) {
                    $transaction->rollBack();
                    $session->addFlash('danger', 'Не удалось создать товар!');
                }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->initLanguages();

        $post = Yii::$app->request->post();
        if( $post && 
            $model->load($post) && 
            $model->validate() && 
            Model::loadMultiple($model->languages, $post) && 
            Model::validateMultiple($model->languages) ) {
                $session = Yii::$app->session;
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $model->save();
                    $model->saveLanguages();
                    $model->files = UploadedFile::getInstances($model, 'files');
                    $model->files_rozetka = UploadedFile::getInstances($model, 'files_rozetka');
                    $model->files_kasta = UploadedFile::getInstances($model, 'files_kasta');
                    $model->upload();
                    $model->uploadRozetka();
                    $model->uploadKasta();
                    $transaction->commit();
                    Yii::$container->invoke([$model, 'reindex']);
                    $session->addFlash('success', 'Вы успешно редактировали товар!');
                } catch(\Throwable $e) {
                    $transaction->rollBack();
                    $session->addFlash('danger', 'Не удалось редактировать товар!');
                }
                return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'discounts' => new ActiveDataProvider([
                'query' => $model->getDiscounts()
            ])
        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionRemoveFile()
    {
        $id = Yii::$app->request->post('key');
        if($id) return ProductPhoto::remove($id);
        return false;
    }

    public function actionSortPhotos()
    {
        $data = Yii::$app->request->post('data');
        ProductPhoto::sort(json_decode($data, true));
        return true;
    }

    public function actionRozetkaRemoveFile()
    {
        $id = Yii::$app->request->post('key');
        if($id) return ProductRozetka::remove($id);
        return false;
    }

    public function actionRozetkaSortPhotos()
    {
        $data = Yii::$app->request->post('data');
        ProductRozetka::sort(json_decode($data, true));
        return true;
    }

    public function actionKastaRemoveFile()
    {
        $id = Yii::$app->request->post('key');
        if($id) return ProductKasta::remove($id);
        return false;
    }

    public function actionKastaSortPhotos()
    {
        $data = Yii::$app->request->post('data');
        ProductKasta::sort(json_decode($data, true));
        return true;
    }

    public function actionRozetkaMail()
    {
        $mail = Yii::$app->mailer->compose()
            ->setTo('alekseenko@rozetka.com.ua')
            ->setFrom(['marketing@pearl.wedding' => 'Deo fashion'])
            ->setSubject('Deo fashion. Добавление новых товаров')
            ->setHtmlBody('
                <p>Добрый день, Тарас!</p>
                <p>Хочу уведомить Вас, что у нас добавлены новые товары.</p>
                <p>С уважением, Яна Лобачева.</p>
            ')
            ->send();

        $session = Yii::$app->session;
        if ($mail) {
            $session->addFlash('success', 'Письмо успешно отправлено!');
        } else {
            $session->addFlash('danger', 'Не удалось отправить письмо!');
        }
        return $this->redirect('index');
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
