<?php

namespace backend\controllers\shop;

use Yii;
use common\models\shop\ShopCategory;
use common\models\shop\ShopCategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Model;

/**
 * ShopCategoryController implements the CRUD actions for ShopCategory model.
 */
class ShopCategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ShopCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShopCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new ShopCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ShopCategory();
        $model->initLanguages();

        $post = Yii::$app->request->post();
        if( $post && 
            $model->load($post) && 
            $model->validate() && 
            Model::loadMultiple($model->languages, $post) &&
            Model::validateMultiple($model->languages) ) {
                $session = Yii::$app->session;
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $model->save_with_slug($model->languages[Yii::$app->params['main_language']]['name']);
                    $model->saveLanguages();
                    $transaction->commit();
                    return $this->redirect(['update', 'id' => $model->id]);
                } catch(\Throwable $e) {
                    $transaction->rollBack();
                    $session->addFlash('danger', 'Не удалось создать категорию!');
                }
        }

        return $this->render('create', ['model' => $model]);
    }

    /**
     * Updates an existing ShopCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->initLanguages();

        $post = Yii::$app->request->post();
        if( $post && 
            $model->load($post) && 
            $model->validate() && 
            Model::loadMultiple($model->languages, $post) && 
            Model::validateMultiple($model->languages) ) {
                $session = Yii::$app->session;
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $model->save();
                    $model->saveLanguages();
                    $transaction->commit();
                    $session->addFlash('success', 'Вы успешно редактировали категорию!');
                } catch(\Throwable $e) {
                    $transaction->rollBack();
                    $session->addFlash('danger', 'Не удалось редактировать категорию!');
                }
                return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', ['model' => $model]);
    }

    /**
     * Deletes an existing ShopCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        try {
            $this->findModel($id)->delete();
        } catch(\Throwable $e) {
            $session = Yii::$app->session;
            $session->addFlash('danger', 'Ошибка удаления!');
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the ShopCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ShopCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShopCategory::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
