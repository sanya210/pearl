<?php

namespace backend\controllers\shop;

use Yii;
use common\models\shop\Attribute;
use common\models\shop\AttributeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AttributeController implements the CRUD actions for Attribute model.
 */
class AttributeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Attribute models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AttributeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Attribute model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Attribute();

        $model->initLanguages();

        $post = Yii::$app->request->post();
        if( $post && 
            $model->load($post) && 
            $model->validate() && 
            Model::loadMultiple($model->languages, $post) &&
            Model::validateMultiple($model->languages) ) {
                $session = Yii::$app->session;
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $model->save_with_slug($model->languages[Yii::$app->params['main_language']]['name']);
                    $model->saveLanguages();
                    $transaction->commit();
                    return $this->redirect(['update', 'id' => $model->id]);
                } catch(\Throwable $e) {
                    $transaction->rollBack();
                    $session->addFlash('danger', 'Не удалось создать атрибут!');
                }
        }

        return $this->render('create', ['model' => $model]);
    }

    /**
     * Updates an existing Attribute model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->initLanguages();

        $post = Yii::$app->request->post();
        if( $post && 
            $model->load($post) && 
            $model->validate() && 
            Model::loadMultiple($model->languages, $post) && 
            Model::validateMultiple($model->languages) ) {
                $session = Yii::$app->session;
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $model->save();
                    $model->saveLanguages();
                    $transaction->commit();
                    $session->addFlash('success', 'Вы успешно редактировали атрибут!');
                } catch(\Throwable $e) {
                    $transaction->rollBack();
                    $session->addFlash('danger', 'Не удалось редактировать атрибут!');
                }
                return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'values' => new ActiveDataProvider([
                'query' => $model->getAttributeValuesAll()->orderBy(['priority' => SORT_DESC])
            ])
        ]);
    }

    /**
     * Deletes an existing Attribute model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Attribute model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Attribute the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Attribute::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
