<?php

namespace backend\controllers;

use backend\models\Merchants;
use Yii;
use yii\web\Controller;

class MerchantsController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionInsertProducts()
    {
        $merchants = new Merchants();
        $products = $merchants->custombatch();
        $session = Yii::$app->session;

        if ($products) {
            $session->addFlash('success', 'Товары успешно отправлены!');
        } else {
            $session->addFlash('danger', 'Не удалось отправить товары.');
        }
        return $this->redirect('index');
    }
}