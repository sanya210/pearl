<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MainSlider */

$this->title = 'Добавление слайдера';
$this->params['breadcrumbs'][] = ['label' => 'Слайдер на главной', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-slider-create">

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
