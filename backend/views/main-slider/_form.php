<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\components\widgets\LanguageWidget;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\MainSlider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="main-slider-form">

	<?php $form = ActiveForm::begin([
		'options' => [
			'enctype' => 'multipart/form-data'
		]
	]); ?>

	<?=$form->field($model, 'status')->checkbox() ?>

	<div class="row">
		<div class="col-md-2">
			<?= $form->field($model, 'priority')->textInput() ?>
		</div>
	</div>

	<?= $form->field($model, 'image_lg')->widget(FileInput::classname(), [
		'options' => [
			'accept' => 'image/*'
		],
		'pluginOptions' => [
			'initialPreview' => $model->img_lg ? '/uploads/slider/'.$model->img_lg : false,
			'initialPreviewAsData' => true,
		],
	]) ?>

	<?= $form->field($model, 'image_xs')->widget(FileInput::classname(), [
		'options' => [
			'accept' => 'image/*'
		],
		'pluginOptions' => [
			'initialPreview' => $model->img_xs ? '/uploads/slider/'.$model->img_xs : false,
			'initialPreviewAsData' => true
		],
	]) ?>

	<?= LanguageWidget::widget([
		'form' => $form,
		'model' => $model,
		'field' => 'header',
		'color' => 'default',
		'label' => 'Заголовок',
	]) ?>

	<?= LanguageWidget::widget([
		'form' => $form,
		'model' => $model,
		'field' => 'link',
		'color' => 'success',
		'label' => 'Ссылка'
	]) ?>

	<?= LanguageWidget::widget([
		'form' => $form,
		'model' => $model,
		'field' => 'text',
		'color' => 'primary',
		'label' => 'Текст',
		'text' => true
	]) ?>

	<div class="form-group">
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
