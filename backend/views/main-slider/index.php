<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\models\MainSlider;

/* @var $this yii\web\View */
/* @var $searchModel common\models\MainSliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Слайдер на главной';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-slider-index">

    <p>
        <?= Html::a('Добавить слайдер', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'img_lg',
                'filter' => false,
                'format' => 'raw',
                'hAlign' => 'center',
                'width' => '90px',
                'value' => function($model){
                    if($model->img_lg){
                        return Html::img('/uploads/slider/'.$model->img_lg, ['width' => '250']);
                    } else {
                        return '';
                    }
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'html',
                'filter' => [
                    MainSlider::STATUS_ACTIVE => 'Активный',
                    MainSlider::STATUS_INACTIVE => 'Не активный'
                ],
                'options' => ['width' => '100'],
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'value' => function($model){
                    $status = $model['status'] == $model::STATUS_ACTIVE ? 'check' : 'ban';
                    return '<i class="fa fa-'.$status.'"></i>'; 
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>


</div>
