<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MainSlider */

$this->title = $model->localization[Yii::$app->params['main_language']]['header'];
$this->params['breadcrumbs'][] = ['label' => 'Слайдер', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->id;
?>
<div class="main-slider-update">

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
