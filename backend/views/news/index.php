<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <p>
        <?= Html::a('Добавить новость', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'img',
                'filter' => false,
                'format' => 'raw',
                'hAlign' => 'center',
                'width' => '50px',
                'value' => function($model){
                    if($model->img){
                        return Html::img('/uploads/news/'.$model->img, ['width' => '250']);
                    } else {
                        return '';
                    }
                }
            ],
            [
                'attribute' => 'mainLanguage.name',
                'value' => function ($model) {
                    return StringHelper::truncate($model->mainLanguage->name, 50);
                }
            ],
            'updated_at:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>


</div>
