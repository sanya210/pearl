<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\components\widgets\LanguageWidget;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=$form->field($model, 'status')->checkbox() ?>

    <?php if(!$model->isNewRecord) { ?>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>
        <div class="col-md-3">
            <label class="control-label">Дата создания</label>
            <div class="form-control"><?=Yii::$app->formatter->asDatetime($model->created_at)?></div>
        </div>
        <div class="col-md-3">
            <label class="control-label">Дата редактирования</label>
            <div class="form-control"><?=Yii::$app->formatter->asDatetime($model->updated_at)?></div>
        </div>
    </div>
    <?php } ?>

    <?= LanguageWidget::widget([
        'form' => $form,
        'model' => $model,
        'field' => 'name',
        'color' => 'default',
        'label' => 'Название'
    ]) ?>

    <?= $form->field($model, 'image')->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'image/*'
        ],
        'pluginOptions' => [
            'initialPreview' => $model->img ? '/uploads/news/'.$model->img : false,
            'initialPreviewAsData' => true,
        ],
    ]) ?>

    <?= $form->field($model, 'image_preview')->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'image/*'
        ],
        'pluginOptions' => [
            'initialPreview' => $model->img_preview ? '/uploads/news/'.$model->img_preview : false,
            'initialPreviewAsData' => true
        ],
    ]) ?>

    <?= LanguageWidget::widget([
        'form' => $form,
        'model' => $model,
        'field' => 'text',
        'color' => 'info',
        'label' => 'Текст',
        'text' => true
    ]) ?>

    <?= LanguageWidget::widget([
        'form' => $form,
        'model' => $model,
        'field' => 'meta_title',
        'color' => 'success',
        'label' => 'Meta Title'
    ]) ?>

    <?= LanguageWidget::widget([
        'form' => $form,
        'model' => $model,
        'field' => 'meta_description',
        'color' => 'primary',
        'label' => 'Meta Description'
    ]) ?>

    <?= LanguageWidget::widget([
        'form' => $form,
        'model' => $model,
        'field' => 'meta_keywords',
        'color' => 'warning',
        'label' => 'Meta Keywords'
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
