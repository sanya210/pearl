<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = $model->localization[Yii::$app->params['main_language']]['name'];
$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['index']];
?>
<div class="news-update">

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
