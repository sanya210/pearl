<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\components\widgets\LanguageWidget;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Text */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="text-form">

   <?php $form = ActiveForm::begin(); ?>

	<div class="row">
		<div class="col-md-5">
   		<?= $form->field($model, 'name')->dropDownList(Yii::$app->params['texts']) ?>
		</div>
	</div>

	<?= LanguageWidget::widget([
		'form' => $form,
		'model' => $model,
		'field' => 'text',
		'color' => 'default',
		'label' => 'Название',
		'text' => true
	]) ?>

   <div class="form-group">
      <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
   </div>

   <?php ActiveForm::end(); ?>

</div>
