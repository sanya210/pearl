<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TextSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Тексты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="text-index">

    <p>
        <?= Html::a('Добавить текст', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'name',
                'filter' => Yii::$app->params['texts'],
                'value' => function($model) {
                    return Yii::$app->params['texts'][$model->name];
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>

</div>