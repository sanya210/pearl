<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Text */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Тексты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
?>

<div class="text-update">

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
