<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Currency */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="currency-form">

	<?php $form = ActiveForm::begin(); ?>

	<div class="row">
		<div class="col-md-3">
			<?php
			$currencies = [];
			foreach(Yii::$app->params['currencies'] as $symbol => $currency) {
				$currencies[$symbol] = $currency.' '.$symbol;
			}
			?>
			<?= $form->field($model, 'symbol')->dropDownList($currencies) ?>
		</div>
		<div class="col-md-3">
			<?= $form->field($model, 'rate')->textInput(['maxlength' => true]) ?>
		</div>
	</div>


	<div class="form-group">
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
