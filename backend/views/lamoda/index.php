 <?php
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

$this->title = ($model->properties_arr ? 'Редактирование' : 'Добавление').' товара на Lamoda';
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['/shop/product/index']];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncate($model->localization[Yii::$app->params['main_language']]['name'], 25), 'url' => ['/shop/product/update', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;

?>



<?php $form = ActiveForm::begin(); ?>

	<div class="row">
		<div class="col-md-12">
			<div class="box box-solid box-danger">
				<div class="box-header">Атрибуты</div>
				<div class="box-body">
					<?php foreach ($properties_values as $attribute) {
						if ($attribute['type'] == 'value') {
							echo $form->field($properties_model, $attribute['name'])->textInput(['placeholder' => $attribute['description']])->label($attribute['label']);
						} else {
							echo $form->field($properties_model, $attribute['name'])->widget(Select2::classname(), [
								'data' => $attribute['options'],
								'options' => [
									'prompt' => $attribute['description'],
									'multiple' => $attribute['type'] == 'multi_option'
								],
							])->label($attribute['label']);
						}
					} ?>
				</div>
			</div>
		</div>
	</div>

	<div class="form-group">
		<?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
	</div>

<?php ActiveForm::end(); ?>