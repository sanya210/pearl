<?php
use backend\components\widgets\MenuWidget;
use common\models\Order;
?>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="text-center image">
                <img src="/img/icon/logo_white.svg"/>
            </div>
        </div>

        <?= MenuWidget::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'data' => [
                    ['label' => 'Меню', 'options' => ['class' => 'header']],
                    ['label' => 'Социальные сети', 'icon' => 'commenting-o', 'url' => '/social'],
                    [
                        'label' => 'Магазин',
                        'icon' => 'shopping-bag',
                        'url' => '/shop',
                        'items' => [
                            ['label' => 'Разделы', 'icon' => 'folder', 'url' => '/shop/section'],
                            ['label' => 'Категории', 'icon' => 'sitemap', 'url' => '/shop/shop-category'],
                            ['label' => 'Псевдо-категории', 'icon' => 'american-sign-language-interpreting', 'url' => '/shop/pseudo-category'],
                            ['label' => 'Товары', 'icon' => 'shopping-basket', 'url' => '/shop/product'],
                            ['label' => 'Поставщики', 'icon' => 'user-secret', 'url' => '/shop/provider'],
                            ['label' => 'Цвета', 'icon' => 'paint-brush', 'url' => '/shop/color'],
                            ['label' => 'Размеры', 'icon' => 'table', 'url' => '/shop/size'],
                            ['label' => 'Атрибуты', 'icon' => 'cubes', 'url' => '/shop/attribute'],
                            ['label' => 'Категории розетки', 'icon' => 'check-circle-o', 'url' => '/shop/rozetka-category'],
                            ['label' => 'Etsy', 'icon' => 'etsy', 'url' => '/etsy/refresh-token'],
                            ['label' => 'Kasta', 'icon' => 'check-circle-o', 'url' => '/kasta/index'],
                            ['label' => 'Google shopping', 'icon' => 'google', 'url' => '/merchants/index'],
                        ]
                    ],
                    ['label' => 'Слайдер на главной', 'icon' => 'sliders', 'url' => '/main-slider'],
                    ['label' => 'Страницы', 'icon' => 'file-text-o', 'url' => '/page'],
                    ['label' => 'Новости', 'icon' => 'newspaper-o', 'url' => '/news'],
                    ['label' => 'Тексты', 'icon' => 'font', 'url' => '/text'],
                    ['label' => 'Заказы', 'icon' => 'paper-plane-o', 'url' => '/order'],
                    ['label' => 'Платежи', 'icon' => 'money', 'url' => '/pay'],
                    ['label' => 'Курсы валют', 'icon' => 'line-chart', 'url' => '/currency'],
                    ['label' => 'Страны', 'icon' => 'globe', 'url' => '/country'],
                    [
                        'label' => 'RBAG',
                        'icon' => 'users',
                        'url' => '/permit',
                        'items' => [
                            ['label' => 'Пользователи', 'icon' => 'user-o', 'url' => '/user'],
                            ['label' => 'Роли', 'icon' => 'id-card-o', 'url' => '/permit/access/role'],
                            ['label' => 'Права доступа', 'icon' => 'universal-access', 'url' => '/permit/access/permission'],
                        ]
                    ],
                    ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => '/gii'],
                    ['label' => 'Debug', 'icon' => 'dashboard', 'url' => '/debug'],
                    ['label' => 'Login', 'url' => 'site/login', 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

    </section>

</aside>
