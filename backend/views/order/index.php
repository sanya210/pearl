<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\models\Order;

/* @var $this yii\web\View */
/* @var $searchModel common\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function ($model, $index, $widget, $grid) {
            if($model->status == 2) {
                return ['class' => 'danger'];
            } elseif($model->status == 4) {
                return ['class' => 'warning'];
            } elseif($model->status == 5) {
                return ['class' => 'success'];
            }
        },
        'columns' => [
            'id',
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:Y.m.d H:i:s']
            ],
            [
                'attribute' => 'status',
                'filter' => Order::STATUSES,
                'value' => function($model) {
                    return Order::STATUSES[$model->status]; 
                }
            ],
            [
                'label' => 'Сумма заказа',
                'hAlign' => 'center',
                'value' => function($model) {
                    return $model->getTotal(false).' '.'у.е.';
                }
            ],
            [
                'label' => 'Сумма в валюте заказа',
                'hAlign' => 'center',
                'value' => function($model) {
                    return $model->total.' '.$model->currency;
                }
            ],
            [
                'attribute' => 'cost',
                'hAlign' => 'center',
                'value' => function($model) {
                    return ($model->cost ? $model->cost : 0) .' '.$model->currency;
                }
            ],
            'user_name',
            'user_family_name',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}'
            ],
        ],
    ]); ?>


</div>
