<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = 'Заказ #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => '#'.$model->id, 'url' => ['view', 'id' => $model->id]];

?>
<div class="order-update">
	<div class="order__date">
		<i class="fa fa-clock-o"></i>
		<span><?=date("Y-m-d H:i:s", $model->created_at)?></span>
	</div>

	<ul class="order__logs">
		<?php foreach ($model->logs as $log) { ?>
			<li class="order__log">
				<div class="order__log-status">
					<?=$model::STATUSES[$log->status]?>
				</div>
				<div class="order__date">
					<?=date('Y.m.d H:i:s', $log->date)?>
				</div>
			</li>
		<?php } ?>
	</ul>

	<span class="order__head">Сменить статус:</span>
 	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

	<div class="box box-info">
		<div class="order__index">
			<dl class="row">
				<dt class="col-sm-2">
					<span class="order__head">Оплачено:</span>
				</dt>
				<dt class="col-sm-10">
					<span class="order__value"><?=($model->cost ? $model->cost : 0) .' '.$model->currency?></span>
				</dt>
			</dl>

			<dl class="row">
				<dt class="col-sm-2">
					<span class="order__head">Сумма заказа:</span>
				</dt>
				<dt class="col-sm-10">
					<span class="order__value"><?=$model->getTotal(false).' '.'у.е.'?></span>
				</dt>
			</dl>

			<dl class="row">
				<dt class="col-sm-2">
					<span class="order__head">Сумма в валюте заказа:</span>
				</dt>
				<dt class="col-sm-10">
					<span class="order__value"><?=$model->total.' '.$model->currency?></span>
				</dt>
			</dl>

			<dl class="row">
				<dt class="col-sm-2">
					<span class="order__head">Имя:</span>
				</dt>
				<dt class="col-sm-10">
					<span class="order__value"><?=$model->user_name?></span>
				</dt>
			</dl>

			<dl class="row">
				<dt class="col-sm-2">
					<span class="order__head">Фамилия:</span>
				</dt>
				<dt class="col-sm-10">
					<span class="order__value"><?=$model->user_family_name?></span>
				</dt>
			</dl>

            <dl class="row">
                <dt class="col-sm-2">
                    <span class="order__head">Email:</span>
                </dt>
                <dt class="col-sm-10">
                    <span class="order__value"><?=$model->user->email?></span>
                </dt>
            </dl>

			<dl class="row">
				<dt class="col-sm-2">
					<span class="order__head">Телефон:</span>
				</dt>
				<dt class="col-sm-10">
					<span class="order__value"><?=$model->user_phone?></span>
				</dt>
			</dl>

			<dl class="row">
				<dt class="col-sm-2">
					<span class="order__head">Индекс:</span>
				</dt>
				<dt class="col-sm-10">
					<span class="order__value"><?=$model->address->delivery_index?></span>
				</dt>
			</dl>

			<dl class="row">
				<dt class="col-sm-2">
					<span class="order__head">Адрес:</span>
				</dt>
				<dt class="col-sm-10">
					<span class="order__value"><?=Yii::t('country', $model->address->country->code)?>, <?=$model->address->delivery_address?></span>
				</dt>
			</dl>
			<?php if($model->note) { ?>
			<div class="order__note">
				<span class="order__head">Примечание:</span>
				<?=$model->note?>
			</div>
			<?php } ?>

			<div class="order__products">
				<span class="order__head">Товары:</span>
				<ul class="order__items">
					<?php foreach ($model->products as $product) { ?>
					<li class="order__item">
						<div class="order__wrapper">
							<img src="<?=$product['photo']?>" class="order__photo" alt="<?=$product['name']?>">
							<a href="https://pearl.wedding<?=$product['slug']?>" class="order__link" target="_blank"><?=$product['name']?></a>
							<div class="order__article">Артикул: <?=$product['article']?></div>
							<div class="order__price">Сумма: <?=$product['price']?> <?=$model->currency?></div>
							<div class="order__sizes">
								<?php foreach($product['sizes'] as $size => $number) { ?>
                                    <div class="cart-sizes">
                                        Размеры: (<?= $product['size_label'] ?>):
                                        <?php foreach ($product['sizes_translate'] as $size) { ?>
                                            <div><?= $size['size'] ?> - <span><?= $size['quantity'] ?></span>;</div>
                                        <?php } ?>
                                    </div>
								<?php } ?>
							</div>
						</div>
					</li>
					<?php } ?>
				</ul>
			</div>

		</div>
	</div>

</div>
