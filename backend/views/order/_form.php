<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'status')->dropDownList($model::STATUSES)->label(false) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'delivery_type')->dropDownList($model::getDeliveryTypes(), ['prompt' => 'Выберите тип']) ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'delivery_number')->textInput() ?>
        </div>

        <?php if ($model->delivery_type && $model->delivery_number) { ?>
            <?= Html::a('<i class="fa fa-paper-plane"></i> Отправить клиенту', ['/order/send-tracking', 'order_id' => $model->id], [
                'data-method' => 'POST',
                'id' => 'tracking-btn',
                'class' => 'btn btn-info btn-sm',
            ]) ?>
        <?php } ?>
    </div>

    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
