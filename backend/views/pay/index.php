<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\payments\PaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Платежи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pay-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'date',
                'format' => ['date', 'php:Y.m.d H:i:s']
            ],
            [
                'attribute' => 'order_id',
                'format' => 'raw',
                'hAlign' => 'center',
                'width' => '70px',
                'value' => function($model) {
                    return Html::a($model->order_id, ['/order/update', 'id' => $model->order_id]);
                }
            ],
            'user.name',
            'user.family_name',
            'amount',
            'currency',
            'system',
            'status',
        ],
    ]); ?>
</div>
