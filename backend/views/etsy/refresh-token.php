<?php
use yii\helpers\Html;

$this->title = 'Etsy';
$this->params['breadcrumbs'][] = $this->title;

?>

<p>
    <?= Html::a('Обновить токен', ['etsy-auth'], ['class' => 'btn btn-success']) ?>
</p>