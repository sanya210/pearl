<?php

use unclead\multipleinput\MultipleInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->title = ($model->etsy_id ? 'Редактирование' : 'Добавление').' товара на Etsy';
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['/shop/product/index']];
$this->params['breadcrumbs'][] = ['label' => StringHelper::truncate($model->localization[Yii::$app->params['main_language']]['name'], 25), 'url' => ['/shop/product/update', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;

?>

<?php $form = ActiveForm::begin(); ?>
	<div class="row">
		<div class="col-md-12">
			<?=$form->field($model, 'etsy_description')->textArea()?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="box box-solid box-danger">
				<div class="box-header">Теги Etsy</div>
				<div class="box-body">
					<?=$form->field($model, 'arr_etsy')->widget(MultipleInput::className(), [
						'max' => 13,
						'min' => 1,
					])->label(false); ?>
				</div>
			</div>
		</div>
	</div>

	<?php if ($model->category->etsy_properties) { ?>
	<div class="row">
		<div class="col-md-12">
			<div class="box box-solid box-danger">
				<div class="box-header">Свойства Etsy</div>
				<div class="box-body">
					<?php if(!$model->isNewRecord) {
						$properties = json_decode($model->category->etsy_properties, true);
						foreach ($properties as $property) {
							echo $form->field($model->propertiesModel, $property['property_id'])->dropDownList(ArrayHelper::map($property['possible_values'], 'value_id', 'name'))->label($property['display_name']);
						}
					} ?>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>

	<div class="note"></div>
	<?php Pjax::begin(['id' => 'pjax-etsy']); ?>
	<div class="row">
		<div class="col-md-12">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-sm btn-success']) ?>
			<?php if ($model->etsy_id) { ?>
			<button type="button" class="btn btn-sm btn-success" id="updateEtsyBtn" data-id="<?=$model->id?>">Обновить товар на Etsy</button>
			<br><br>
			<a href="https://www.etsy.com/your/shops/PearlFashionGroup/tools/listings/<?=$model->etsy_id?>" target="_blank">Перейти на Etsy</a>
			<?php } else { ?>
			<button type="button" class="btn btn-sm btn-info" id="createEtsyBtn" data-id="<?=$model->id?>">Импортировать товар на Etsy</button>
			<?php } ?>
		</div>
	</div>
	<?php Pjax::end(); ?>

<?php ActiveForm::end(); ?>

<div class="preload">
    <div class="lds-circle"><div></div></div>
</div>
