<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\shop\Attribute */

$this->title = $model->localization[Yii::$app->params['main_language']]['name'];
$this->params['breadcrumbs'][] = ['label' => 'Атрибуты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attribute-update">

	<?= $this->render('_form', [
		'model' => $model,
		'values' => $values
	]) ?>

</div>
