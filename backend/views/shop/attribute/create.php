<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\shop\Attribute */

$this->title = 'Добавление атрибута';
$this->params['breadcrumbs'][] = ['label' => 'Атрибуты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attribute-create">

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
