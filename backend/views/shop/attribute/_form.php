<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\shop\ShopCategory;
use backend\components\widgets\LanguageWidget;
use yii\bootstrap\Modal;
use common\models\shop\AttributeValue;

/* @var $this yii\web\View */
/* @var $model common\models\shop\Attribute */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="attribute-form">

	<?php $form = ActiveForm::begin(); ?>
	
	<?=$form->field($model, 'status')->checkbox() ?>

	<div class="row">
		<div class="col-md-3">
	   	<?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(ShopCategory::getActiveCategory(), 'id', 'mainLanguage.name'), ['disabled' => $model->isNewRecord ? false : true]) ?>
		</div>

		<?php if(!$model->isNewRecord) { ?>
			<div class="col-md-3">
				<?= $form->field($model, 'slug')->textInput(['maxlength' => true, 'disabled' => true]) ?>
			</div>
		<?php } ?>

		<div class="col-md-2">
			<?= $form->field($model, 'priority')->textInput() ?>
		</div>
	</div>

	<?= LanguageWidget::widget([
		'form' => $form,
		'model' => $model,
		'field' => 'name',
		'color' => 'default',
		'label' => 'Название'
	]) ?>

	<?php if(!$model->isNewRecord) { ?>
	<div class="row">
		<div class="col-md-12">
			<?=$this->render('value/index', [
				'values' => $values,
				'attribute_id' => $model->id
			])?>
		</div>
	</div>
	<?php } ?>

	<div class="form-group">
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>


<div class="modal fade" id="update-value-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Редактирование значения</h4>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>

<?php Modal::begin([
    'header' => '<h4> <i class="fa fa-certificate l2g" aria-hidden="true"></i> Добавление значения</h4>',
    'id' => 'add-value',
    'size' => 'modal-lg'
]);
$value_model = new AttributeValue;
$value_model->initLanguages();
echo $this->render ('value/_form', [
   'model' => $value_model,
   'attribute_id' => $model->id
]);
Modal::end(); ?>
