<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\shop\Attribute;
use yii\helpers\ArrayHelper;
use common\models\shop\ShopCategory;

/* @var $this yii\web\View */
/* @var $searchModel common\models\shop\AttributeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Атрибуты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attribute-index">

    <p>
        <?= Html::a('Добавить атрибут', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'mainLanguage.name',
                'options' => ['width' => '300'],
            ],
            [
                'attribute' => 'category_id',
                'options' => ['width' => '300'],
                'filter' => ArrayHelper::map(ShopCategory::getActiveCategory(), 'id', 'mainLanguage.name'),
                'value' => function($model){
                    return $model->category->mainLanguage->name;
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'html',
                'filter' => [
                    Attribute::STATUS_ACTIVE => 'Активный',
                    Attribute::STATUS_INACTIVE => 'Не активный'
                ],
                'options' => ['width' => '100'],
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'value' => function($model){
                    $status = $model['status'] == $model::STATUS_ACTIVE ? 'check' : 'ban';
                    return '<i class="fa fa-'.$status.'"></i>'; 
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>


</div>
