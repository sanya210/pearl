<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use backend\components\widgets\LanguageWidget;

/* @var $this yii\web\View */
/* @var $model common\models\shop\Product */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="discount-form">

    <?php $form = ActiveForm::begin([
        'action' => $model->isNewRecord ? '/shop/attribute-value/create' : '/shop/attribute-value/update?id='.$model->id
    ]); ?>
    
    <?=$form->field($model, 'status')->checkbox() ?>

    <?= LanguageWidget::widget([
        'form' => $form,
        'model' => $model,
        'field' => 'name',
        'color' => 'default',
        'label' => 'Название'
    ]) ?>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'priority')->textInput() ?>
        </div>
    </div>

    <?php if($model->isNewRecord) { ?>
        <?=$form->field($model, 'attribute_id')->hiddenInput(['value' => $attribute_id])->label(false) ?>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
