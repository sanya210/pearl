<?php
use yii\grid\GridView;
use common\models\shop\AttributeValue;
use yii\helpers\Html;

?>


	<div class="card card-body">
		<button type="button" class="btn btn-link btn-xs" data-toggle="modal" data-target="#add-value"><i class="fa fa-plus"></i>  Добавить</button>

		<?=GridView::widget([
			'dataProvider' => $values,
			'columns' => [
				[
                'attribute' => 'mainLanguage.name',
                'options' => ['width' => '300'],
            ],
				[
					'attribute' => 'status',
					'format' => 'html',
					'options' => ['width' => '100'],
					'contentOptions' => ['class' => 'text-center'],
					'headerOptions' => ['class' => 'text-center'],
					'value' => function($model){
						$status = $model['status'] == $model::STATUS_ACTIVE ? 'check' : 'ban';
						return '<i class="fa fa-'.$status.'"></i>'; 
					}
				],
				[
					'class' => 'yii\grid\ActionColumn',
					'template' => '{update} {delete}',
					'buttons' => [
						'update' => function($url, $model) {
							return Html::button('', [
								'class' => 'glyphicon glyphicon-pencil btn-link btn-value',
								'data-id' => $model->id,
							]);
						},
						'delete' => function($url, $model) {
							return Html::a('', ['/shop/attribute-value/delete', 'id' => $model->id], [
								'class' => 'glyphicon glyphicon-trash',
								'data-confirm' => 'Вы уверены, что хотите удалить значение?'
							]);
						}
					]
				],
			]
		]) ?>

	</div>


<?php $this->registerJs("
	$('.btn-value').click(function() {
		let id = $(this).attr('data-id');
		let modal = $('#update-value-modal');
		modal.find('.modal-body').load('/shop/attribute-value/update?id=' + id, function() {
			modal.find('.nav li a').each(function(i) {
				let id = '#value-tav' + i;
				$(this).attr('href', id);
			});
			modal.find('.tab-pane').each(function(i) {
				let id = 'value-tav' + i;
				$(this).attr('id', id);
			});
		});
		modal.modal('show');
		return false;
	});
"); ?>

