<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\shop\Color;

/* @var $this yii\web\View */
/* @var $searchModel common\models\shop\ColorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Цвета';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="color-index">

    <p>
        <?= Html::a('Добавить цвет', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'hex',
                'format' => 'raw',
                'options' => ['width' => 120],
                'value' => function($model) {
                    return Html::tag('div', '', [
                        'class' => 'color',
                        'style' => 'background-color:'.$model->hex
                    ]);
                }
            ],
            [
                'attribute' => 'mainLanguage.name',
                'options' => ['width' => '300'],
            ],
            [
                'attribute' => 'status',
                'format' => 'html',
                'filter' => [
                    Color::STATUS_ACTIVE => 'Активный',
                    Color::STATUS_INACTIVE => 'Не активный'
                ],
                'options' => ['width' => '100'],
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'value' => function($model){
                    $status = $model['status'] == $model::STATUS_ACTIVE ? 'check' : 'ban';
                    return '<i class="fa fa-'.$status.'"></i>'; 
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}'
            ],
        ],
    ]); ?>


</div>
