<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\shop\Color */

$this->title = $model->localization[Yii::$app->params['main_language']]['name'];
$this->params['breadcrumbs'][] = ['label' => 'Цвета', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="color-update">

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
