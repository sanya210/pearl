<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\color\ColorInput;
use backend\components\widgets\LanguageWidget;

/* @var $this yii\web\View */
/* @var $model common\models\shop\Color */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="color-form">

	<?php $form = ActiveForm::begin(); ?>

   <?=$form->field($model, 'status')->checkbox() ?>

	<div class="row">
		<div class="col-md-4">
			<?= $form->field($model, 'hex')->widget(ColorInput::classname(), [
				'options' => ['placeholder' => 'Выберите цвет ...'],
			]) ?>			
		</div>

		<div class="col-md-2">
			<?= $form->field($model, 'sort')->textInput() ?>
		</div>
	</div>

	<?= LanguageWidget::widget([
		'form' => $form,
		'model' => $model,
		'field' => 'name',
		'color' => 'default',
		'label' => 'Название'
	]) ?>

	<div class="form-group">
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
