<?php

use backend\components\widgets\LanguageWidget;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\shop\ShopCategory;

/* @var $this yii\web\View */
/* @var $model common\models\shop\Section */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="section-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=$form->field($model, 'status')->checkbox() ?>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'priority')->textInput() ?>
        </div>
    </div>

    <?= LanguageWidget::widget([
        'form' => $form,
        'model' => $model,
        'field' => 'name',
        'color' => 'default',
        'label' => 'Название'
    ]) ?>

    <?= $form->field($model, 'categories_arr')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(ShopCategory::getActiveCategory(), 'id', 'mainLanguage.name'),
        'options' => [
            'placeholder' => 'Выберите товары...',
            'multiple' => true
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
