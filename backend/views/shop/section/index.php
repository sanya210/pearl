<?php

use common\models\shop\Section;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\shop\SectionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Разделы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="section-index">

    <p>
        <?= Html::a('Добавить раздел', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'mainLanguage.name',
                'value' => function($model) {
                    return StringHelper::truncate($model->mainLanguage->name, 25);
                }
            ],
            [
                'attribute' => 'categories',
                'value' => function($model) {
                    return implode(array_column(array_column($model->categoriesModels, 'mainLanguage'), 'name'), ', ');
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'html',
                'filter' => [
                    Section::STATUS_ACTIVE => 'Активный',
                    Section::STATUS_INACTIVE => 'Не активный'
                ],
                'options' => ['width' => '100'],
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'value' => function($model){
                    $status = $model['status'] == $model::STATUS_ACTIVE ? 'check' : 'ban';
                    return '<i class="fa fa-'.$status.'"></i>';
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>


</div>
