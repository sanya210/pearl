<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\shop\ShopCategory */

$this->title = $model->localization[Yii::$app->params['main_language']]['name'];
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-category-update">

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
