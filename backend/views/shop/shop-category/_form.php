<?php

use backend\models\kasta\Product;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\components\widgets\LanguageWidget;

/* @var $this yii\web\View */
/* @var $model common\models\shop\ShopCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="shop-category-form">

	<?php $form = ActiveForm::begin(); ?>

	<div class="row">
		<div class="col-md-2">
			<?=$form->field($model, 'status')->checkbox() ?>
		</div>
		<div class="col-md-3">
			<?= $form->field($model, 'show_in_menu')->checkbox() ?>
		</div>
	</div>

	<div class="row">
		<?php if(!$model->isNewRecord) { ?>
		<div class="col-md-3">
			<?= $form->field($model, 'slug')->textInput(['maxlength' => true, 'disabled' => true]) ?>
		</div>
		<?php } ?>
		<div class="col-md-2">
			<?= $form->field($model, 'priority')->textInput() ?>
		</div>
		<div class="col-md-2">
			<?= $form->field($model, 'etsy_id')->textInput() ?>
		</div>
		<div class="col-md-2">
			<?= $form->field($model, 'etsy_section_id')->textInput() ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<?= $form->field($model, 'etsy_sizes')->textInput() ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<?= $form->field($model, 'etsy_properties')->textInput() ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-2">
			<?= $form->field($model, 'lamoda_id')->textInput() ?>
		</div>

        <div class="col-md-2">
            <?= $form->field($model, 'uktzed')->textInput() ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'name_kasta')->textInput() ?>
        </div>

        <div class="col-md-2">
            <?= $form->field($model, 'gender')->dropDownList(Product::GENDER) ?>
        </div>
	</div>

	<?= LanguageWidget::widget([
		'form' => $form,
		'model' => $model,
		'field' => 'name',
		'color' => 'default',
		'label' => 'Название'
	]) ?>

	<?= LanguageWidget::widget([
		'form' => $form,
		'model' => $model,
		'field' => 'meta_title',
		'color' => 'success',
		'label' => 'Meta Title'
	]) ?>

	<?= LanguageWidget::widget([
		'form' => $form,
		'model' => $model,
		'field' => 'meta_description',
		'color' => 'primary',
		'label' => 'Meta Description'
	]) ?>

	<?= LanguageWidget::widget([
		'form' => $form,
		'model' => $model,
		'field' => 'meta_keywords',
		'color' => 'warning',
		'label' => 'Meta Keywords'
	]) ?>

	<div class="form-group">
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>


