<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\shop\ShopCategory;

/* @var $this yii\web\View */
/* @var $searchModel common\models\shop\ShopCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shop-category-index">

    <p>
        <?= Html::a('Добавить категории', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'slug',
            'mainLanguage.name',
            [
                'attribute' => 'status',
                'format' => 'html',
                'filter' => [
                    ShopCategory::STATUS_ACTIVE => 'Активный',
                    ShopCategory::STATUS_INACTIVE => 'Не активный'
                ],
                'options' => ['width' => '100'],
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'value' => function($model){
                    $status = $model['status'] == $model::STATUS_ACTIVE ? 'check' : 'ban';
                    return '<i class="fa fa-'.$status.'"></i>'; 
                }
            ],
            [
                'attribute' => 'show_in_menu',
                'format' => 'html',
                'filter' => [
                    ShopCategory::STATUS_ACTIVE => 'Активный',
                    ShopCategory::STATUS_INACTIVE => 'Не активный'
                ],
                'options' => ['width' => '100'],
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'value' => function($model){
                    $status = $model['show_in_menu'] == $model::STATUS_ACTIVE ? 'check' : 'ban';
                    return '<i class="fa fa-'.$status.'"></i>'; 
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>


</div>
