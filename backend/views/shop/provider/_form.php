<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\shop\Provider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="provider-form">

	<?php $form = ActiveForm::begin(); ?>

	<div class="form-group">
		<div class="row">
			<div class="col-md-5">
				<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
			</div>
		</div>
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
