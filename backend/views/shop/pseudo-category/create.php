<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\shop\PseudoCategory */

$this->title = 'Добавление псевдо-категории';
$this->params['breadcrumbs'][] = ['label' => 'Псевдо-категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pseudo-category-create">

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
