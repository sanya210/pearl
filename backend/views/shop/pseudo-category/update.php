<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\shop\PseudoCategory */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Псевдо-категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
?>
<div class="pseudo-category-update">

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
