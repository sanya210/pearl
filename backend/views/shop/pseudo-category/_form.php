<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\shop\ShopCategory;

/* @var $this yii\web\View */
/* @var $model common\models\shop\PseudoCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pseudo-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=$form->field($model, 'status')->checkbox() ?>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'language')->dropDownList(Yii::$app->params['languages']) ?>
        </div>
        <?php if(!$model->isNewRecord) { ?>
        <div class="col-md-3">
            <?= $form->field($model, 'slug')->textInput(['maxlength' => true, 'disabled' => true]) ?>
        </div>
        <?php } ?>
        <div class="col-md-3">
            <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(ShopCategory::getActiveCategory(), 'id', 'mainLanguage.name'), ['disabled' => $model->isNewRecord ? false : true]) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'priority')->textInput() ?>    
        </div>
    </div>

    <div class="box box-solid box-default">
        <div class="box-header">Название</div>
        <div class="box-body">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label(false) ?>
        </div>
    </div>


    <div class="box box-solid box-success">
        <div class="box-header">Meta Title</div>
        <div class="box-body">
            <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true])->label(false) ?>
        </div>
    </div>

    <div class="box box-solid box-primary">
        <div class="box-header">Meta Description</div>
        <div class="box-body">
            <?= $form->field($model, 'meta_description')->textarea(['rows' => 6])->label(false) ?>
        </div>
    </div>

    <div class="box box-solid box-warning">
        <div class="box-header">Meta Keywords</div>
        <div class="box-body">
            <?= $form->field($model, 'meta_keywords')->textInput(['maxlength' => true])->label(false) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
