<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\shop\PseudoCategory;
use yii\helpers\ArrayHelper;
use common\models\shop\ShopCategory;

/* @var $this yii\web\View */
/* @var $searchModel common\models\shop\PseudoCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Псевдо-категории';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pseudo-category-index">

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'name',
            [
                'attribute' => 'language',
                'options' => ['width' => '80'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'attribute' => 'category_id',
                'filter' => ArrayHelper::map(ShopCategory::getActiveCategory(), 'id', 'mainLanguage.name'),
                'value' => function($model){
                    return $model->category->mainLanguage->name;
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'html',
                'filter' => [
                    PseudoCategory::STATUS_ACTIVE => 'Активный',
                    PseudoCategory::STATUS_INACTIVE => 'Не активный'
                ],
                'options' => ['width' => '100'],
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'value' => function($model){
                    $status = $model['status'] == $model::STATUS_ACTIVE ? 'check' : 'ban';
                    return '<i class="fa fa-'.$status.'"></i>'; 
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>


</div>
