<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\shop\Size */

$this->title = $model->europe .' / '. $model->international;
$this->params['breadcrumbs'][] = ['label' => 'Размеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="size-update">

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
