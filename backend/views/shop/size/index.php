<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\shop\Size;

/* @var $this yii\web\View */
/* @var $searchModel common\models\shop\SizeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Размеры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="size-index">

    <p>
        <?= Html::a('Добавить размер', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'europe',
            'international',
            [
                'attribute' => 'status',
                'format' => 'html',
                'filter' => [
                    Size::STATUS_ACTIVE => 'Активный',
                    Size::STATUS_INACTIVE => 'Не активный'
                ],
                'options' => ['width' => '100'],
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'value' => function($model){
                    $status = $model['status'] == $model::STATUS_ACTIVE ? 'check' : 'ban';
                    return '<i class="fa fa-'.$status.'"></i>'; 
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>


</div>
