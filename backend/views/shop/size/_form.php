<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\shop\ShopCategory;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\shop\Size */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="size-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?=$form->field($model, 'status')->checkbox() ?>
    
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'categories_ids')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(ShopCategory::find()->all(), 'id', 'mainLanguage.name'),
                'options' => [
                    'placeholder' => 'Выберите категории...',
                    'multiple' => true
                ],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'number_usa')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'international')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'europe')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'ru')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'uk')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'de')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'chest')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'waist')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'hips')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'chest_inch')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'waist_inch')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'hips_inch')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'dimension')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
