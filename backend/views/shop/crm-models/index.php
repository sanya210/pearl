<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CrmModeliTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Продукция из CRM';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crm-modeli-type-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'img',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::img('http://crm.wedding/uploads/modeli/'.$model->img, ['width' => '100px']);
                }
            ],
            'artikul',
            [
                'attribute' => 'name',
                'value' => function($model) {
                    return $model->model ? $model->model->name : '' . ' ' . $model->name;
                }
            ],
            [
                'attribute' => 'color',
                'format' => 'raw',
                'options' => ['width' => 120],
                'value' => function($model) {
                    return Html::tag('div', '', [
                        'class' => 'color',
                        'style' => 'background-color:'.$model->color
                    ]);
                }
            ],
            [
                'format' => 'raw',
                'value' => function($model) {
                    return Html::a('+ Добавить', [
                        '/shop/product/create',
                        'Product[article]' => $model->artikul,
                        'Product[provider_article]' => $model->artikul,
                        'Product[provider_name]' => $model->model ? $model->model->name : '' . ' ' . $model->name,
                        'Product[provider_id]' => 4
                    ],
                    [
                        'class' => 'btn btn-xs btn-success',
                        'target' => '_blank'
                    ]);
                }
            ]
        ],
    ]); ?>


</div>
