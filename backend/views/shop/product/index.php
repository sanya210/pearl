<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\shop\Product;
use yii\helpers\ArrayHelper;
use common\models\shop\ShopCategory;
use common\models\shop\Provider;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\shop\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <p>
        <?= Html::a('Добавить товар', ['create'], ['class' => 'btn btn-success btn-sm']) ?>
        <?= Html::a('Импорт из CRM', ['/shop/crm-models'], ['class' => 'btn btn-warning btn-sm']) ?>
        <?= Html::a('Уведомить розетку о новых товарах', ['/shop/product/rozetka-mail'], [
            'class' => 'btn btn-primary btn-sm',
            'data-method' => 'post',
            'data-confirm' => 'Вы уверены что хотите отправить письмо в розетку о добавление новых товарах?',
        ]) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'mainPhoto',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::img('/uploads/product/preview/' . $model->mainPhoto, ['width' => 70]);
                }
            ],
            'article',
            [
                'attribute' => 'mainLanguage.name',
                'value' => function($model) {
                    return StringHelper::truncate($model->mainLanguage->name, 25);
                }
            ],
            [
                'attribute' => 'provider_id',
                'filter' => ArrayHelper::map(Provider::find()->all(), 'id', 'name'),
                'value' => function($model){
                    return $model->provider->name;
                }
            ],
            [
                'attribute' => 'category_id',
                'filter' => ArrayHelper::map(ShopCategory::getActiveCategory(), 'id', 'mainLanguage.name'),
                'value' => function($model){
                    return $model->category->mainLanguage->name;
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'html',
                'filter' => [
                    Product::STATUS_ACTIVE => 'Активный',
                    Product::STATUS_INACTIVE => 'Не активный',
                    Product::STATUS_NOT_AVAILABLE => 'Не доступен'
                ],
                'options' => ['width' => '100'],
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'value' => function($model){
                    $status = $model['status'] == $model::STATUS_ACTIVE ? 'check' : 'ban';
                    return '<i class="fa fa-'.$status.'"></i>'; 
                }
            ],
            [
                'label' => 'Скидка',
                'format' => 'html',
                'filter' => [
                    Product::STATUS_ACTIVE => 'Есть',
                    Product::STATUS_INACTIVE => 'Нету'
                ],
                'value' => function($model) {
                    $status = $model->activeDiscount ? 'check' : 'ban';
                    return '<i class="fa fa-'.$status.'"></i>'; 
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {etsy} {lamoda} {delete}',
                'buttons' => [
                    'etsy' => function($url, $model, $key) {
                        return Html::a('Etsy', ['/etsy/index', 'id' => $model->id], ['class' => 'btn btn-xs btn-warning']);
                    },
                    'lamoda' => function($url, $model, $key) {
                        return Html::a('Lamoda', ['/lamoda/index', 'id' => $model->id], ['class' => 'btn btn-xs btn-success']);
                    }
                ]
            ],
        ],
    ]); ?>


</div>
