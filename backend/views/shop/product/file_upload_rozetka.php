<?php
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
?>

<?= $form->field($model, 'files_rozetka[]')->widget(FileInput::classname(),
    [
        'language' => 'ru',
        'options' => [
            'accept' => 'image/*',
            'multiple' => true,
        ],
        'pluginOptions' => [
            'deleteUrl' => Url::toRoute('/shop/product/rozetka-remove-file'),
            'initialPreview' => ArrayHelper::getColumn($model->photoRozetka, function($file) {
                return '/uploads/product/rozetka/'.$file['name'];
            }),
            'initialPreviewAsData' => true,
            'overwriteInitial' => false,
            'initialPreviewConfig' => ArrayHelper::getColumn($model->photoRozetka, function($attachment){
                $file = '/uploads/product/'.$attachment['name'];
                $type = substr(strrchr($file, '.'), 1);
                $key = $attachment['id'];
                return [
                    'caption' => $file,
                    'type' => ($type == 'pdf') ? 'pdf' : 'image',
                    'key' => $key,
                    'downloadUrl' => $file
                ];
            }),
            'maxFileSize' => 27000,
            'showUpload' => true,
        ],
        'pluginEvents' => [
            'filesorted' => 'function(event, params) {
                let data = {};
                for(let key in params.stack) {
                    data[params.stack[key].key] = key;
                }

                $.ajax({
                    url: "/shop/product/rozetka-sort-photos",
                    type: "POST",
                    data: "data=" + JSON.stringify(data),
                })
            }',
        ]
    ]
);
?>