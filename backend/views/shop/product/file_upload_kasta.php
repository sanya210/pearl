<?php
use kartik\file\FileInput;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
?>

<?= $form->field($model, 'files_kasta[]')->widget(FileInput::classname(),
    [
        'language' => 'ru',
        'options' => [
            'accept' => 'image/*',
            'multiple' => true,
        ],
        'pluginOptions' => [
            'deleteUrl' => Url::toRoute('/shop/product/kasta-remove-file'),
            'initialPreview' => ArrayHelper::getColumn($model->photoKasta, function($file) {
                return '/uploads/product/kasta/'.$file['name'];
            }),
            'initialPreviewAsData' => true,
            'overwriteInitial' => false,
            'initialPreviewConfig' => ArrayHelper::getColumn($model->photoKasta, function($attachment){
                $file = '/uploads/product/'.$attachment['name'];
                $type = substr(strrchr($file, '.'), 1);
                $key = $attachment['id'];
                return [
                    'caption' => $file,
                    'type' => ($type == 'pdf') ? 'pdf' : 'image',
                    'key' => $key,
                    'downloadUrl' => $file
                ];
            }),
            'maxFileSize' => 27000,
            'showUpload' => true,
        ],
        'pluginEvents' => [
            'filesorted' => 'function(event, params) {
                let data = {};
                for(let key in params.stack) {
                    data[params.stack[key].key] = key;
                }

                $.ajax({
                    url: "/shop/product/kasta-sort-photos",
                    type: "POST",
                    data: "data=" + JSON.stringify(data),
                })
            }',
        ]
    ]
);
?>