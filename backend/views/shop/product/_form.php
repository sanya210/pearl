<?php

use common\models\shop\RozetkaCategory;
use common\models\shop\Size;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\shop\ShopCategory;
use common\models\shop\Color;
use common\models\shop\Provider;
use yii\helpers\ArrayHelper;
use backend\components\widgets\LanguageWidget;
use yii\bootstrap\Modal;
use common\models\shop\Discount;
use kartik\select2\Select2;
use common\models\shop\Product;
use common\models\shop\PseudoCategory;
use common\models\shop\AttributeValue;
use backend\models\kasta\Product as ProductKasta;

/* @var $this yii\web\View */
/* @var $model common\models\shop\Product */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="product-form">

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'validateOnChange' => false,
        'options' => [
            'enctype' => 'multipart/form-data'
        ]
    ]); ?>

    <div class="row">
        <div class="col-md-2">
            <?= $form->field($model, 'status')->dropDownList([
                Product::STATUS_ACTIVE => 'Активный',
                Product::STATUS_INACTIVE => 'Не активный',
                Product::STATUS_NOT_AVAILABLE => 'Не доступен'
            ]) ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'status_rozetka')->checkbox() ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'status_kasta')->checkbox() ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'google_merchants')->checkbox() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'rozetka_id')->dropDownList(ArrayHelper::map(RozetkaCategory::getActiveCategory(), 'id', 'name'), ['prompt' => '']) ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'name_rozetka')->textInput() ?>
        </div>

		<div class="col-md-2">
            <?= $form->field($model, 'rozetka_price')->textInput() ?>
		</div>

        <div class="col-md-2">
            <?= $form->field($model, 'priority')->textInput() ?>
        </div>

        <div class="col-md-2">
            <?= $form->field($model, 'gtin')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-10">
            <?= $form->field($model, 'composition_kasta')->textInput() ?>
        </div>

        <div class="col-md-2">
            <?= $form->field($model, 'season')->dropDownList(ProductKasta::SEASONS) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(ShopCategory::getActiveCategory(), 'id', 'mainLanguage.name'), ['disabled' => $model->pseudoCategories ? true : false]) ?>
        </div>

        <?php if (!$model->isNewRecord) { ?>
            <div class="col-md-3">
                <?= $form->field($model, 'slug')->textInput(['maxlength' => true, 'disabled' => true]) ?>
            </div>

        <?php } ?>

        <div class="col-md-3">
            <?= $form->field($model, 'article')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'price')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'color_id')->dropDownList(ArrayHelper::map(Color::getActiveColor(), 'id', 'mainLanguage.name')) ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'number_purchased')->textInput() ?>
        </div>

        <div class="col-md-6">
            <?= $form->field($model, 'video')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'provider_id')->dropDownList(ArrayHelper::map(Provider::find()->all(), 'id', 'name')) ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'provider_name')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'provider_article')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'provider_price')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <?php if (!$model->isNewRecord) { ?>
        <div class="row">
            <div class="col-md-12">
                <?= $this->render('discount/index', [
                    'discounts' => $discounts,
                    'product_id' => $model->id
                ]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'productSizes_ids')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Size::activeSize($model->category_id), 'id', 'europe'),
                    'options' => [
                        'placeholder' => 'Выберите размеры...',
                        'multiple' => true
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'other_color_products')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map($model->otherProducts, 'id', 'mainLanguage.name'),
                    'options' => [
                        'placeholder' => 'Выберите товары...',
                        'multiple' => true
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'similar_products')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map($model->otherProducts, 'id', 'mainLanguage.name'),
                    'options' => [
                        'placeholder' => 'Выберите товары...',
                        'multiple' => true
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'pseudoCategories_ids')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(PseudoCategory::findByCategory($model->category_id), 'id', 'name'),
                    'options' => [
                        'placeholder' => 'Выберите псевдо-категорию...',
                        'multiple' => true
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?php
                $attribute_value = [];
                foreach (AttributeValue::findByCategory($model->category_id) as $value) {
                    $attribute_value[$value->id] = $value->mainLanguage->name . ' (' . $value->attribute0->mainLanguage->name . ')';
                }
                ?>
                <?= $form->field($model, 'attributes_ids')->widget(Select2::classname(), [
                    'data' => $attribute_value,
                    'options' => [
                        'placeholder' => 'Выберите атрибуты...',
                        'multiple' => true
                    ],
                ]); ?>
            </div>
        </div>

    <?php } ?>

    <?= LanguageWidget::widget([
        'form' => $form,
        'model' => $model,
        'field' => 'name',
        'color' => 'default',
        'label' => 'Название'
    ]) ?>

    <?= $this->render('file_upload', [
        'model' => $model,
        'form' => $form
    ]); ?>

    <?= $this->render('file_upload_rozetka', [
        'model' => $model,
        'form' => $form
    ]); ?>

    <?= $this->render('file_upload_kasta', [
        'model' => $model,
        'form' => $form
    ]); ?>

    <?= LanguageWidget::widget([
        'form' => $form,
        'model' => $model,
        'field' => 'description',
        'color' => 'info',
        'label' => 'Описание',
        'text' => true
    ]) ?>

    <?= LanguageWidget::widget([
        'form' => $form,
        'model' => $model,
        'field' => 'meta_title',
        'color' => 'success',
        'label' => 'Meta Title'
    ]) ?>

    <?= LanguageWidget::widget([
        'form' => $form,
        'model' => $model,
        'field' => 'meta_description',
        'color' => 'primary',
        'label' => 'Meta Description'
    ]) ?>

    <?= LanguageWidget::widget([
        'form' => $form,
        'model' => $model,
        'field' => 'meta_keywords',
        'color' => 'warning',
        'label' => 'Meta Keywords'
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<div class="modal fade" id="update-discount-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Редактирование акции</h4>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>

<?php Modal::begin([
    'header' => '<h4> <i class="fa fa-bookmark-o l2g" aria-hidden="true"></i> Добавление акции</h4>',
    'id' => 'add-discount',
]);
echo $this->render('discount/_form', [
    'model' => new Discount,
    'product_id' => $model->id
]);
Modal::end(); ?>



