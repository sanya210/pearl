<?php

use yii\helpers\Html;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\shop\Product */

$this->title = StringHelper::truncate($model->localization[Yii::$app->params['main_language']]['name'], 25);
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="product-update">

	<?= $this->render('_form', [
		'model' => $model,
		'discounts' => $discounts
	]) ?>

</div>

