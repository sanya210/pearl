<?php
use yii\grid\GridView;
use common\models\shop\Discount;
use yii\helpers\Html;

?>

<p>
	<button class="btn btn-success" type="button" data-toggle="collapse" data-target="#collapseDiscount" aria-expanded="false" aria-controls="collapseDiscount">
		<i class="fa fa-bookmark-o 2lg" aria-hidden="true"></i> Акции
	</button>
</p>
<div class="collapse" id="collapseDiscount">
	<div class="card card-body">
		<button type="button" class="btn btn-link btn-xs" data-toggle="modal" data-target="#add-discount"><i class="fa fa-plus"></i>  Добавить</button>

		<?=GridView::widget([
			'dataProvider' => $discounts,
			'columns' => [
				'created_at:date',
				'price',
				'date_start',
				'date_finish',
				[
					'attribute' => 'status',
					'format' => 'html',
					'options' => ['width' => '100'],
					'contentOptions' => ['class' => 'text-center'],
					'headerOptions' => ['class' => 'text-center'],
					'value' => function($model){
						$status = $model['status'] == $model::STATUS_ACTIVE ? 'check' : 'ban';
						return '<i class="fa fa-'.$status.'"></i>'; 
					}
				],
				[
					'class' => 'yii\grid\ActionColumn',
					'template' => '{update} {delete}',
					'buttons' => [
						'update' => function($url, $model) {
							return Html::button('', [
								'class' => 'glyphicon glyphicon-pencil btn-link btn-discount',
								'data-id' => $model->id,
							]);
						},
						'delete' => function($url, $model) {
							return Html::a('', ['/shop/discount/delete', 'id' => $model->id], [
								'class' => 'glyphicon glyphicon-trash',
								'data-confirm' => 'Вы уверены, что хотите удалить акцию?'
							]);
						}
					]
				],
			]
		]) ?>

	</div>
</div>


<?php $this->registerJs("
	$('.btn-discount').click(function() {
		let id = $(this).attr('data-id');
		let modal = $('#update-discount-modal');
		modal.find('.modal-body').load('/shop/discount/update?id=' + id);
		modal.modal('show');
		return false;
	});
"); ?>

