<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\shop\Product */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="discount-form">

    <?php $form = ActiveForm::begin([
        'action' => $model->isNewRecord ? '/shop/discount/create' : '/shop/discount/update?id='.$model->id
    ]); ?>
    
    <?=$form->field($model, 'status')->checkbox() ?>

    <div class="row">
        <div class="col-md-4">
            <?=$form->field($model, 'price')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?=$form->field($model, 'date_start')->widget(DatePicker::classname(), [
                'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                'language'=>'ru',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                    'startDate' => date('Y-m-d')
                ]
            ]) ?>
        </div>

        <div class="col-md-6">
            <?=$form->field($model, 'date_finish')->widget(DatePicker::classname(), [
                'type' => DatePicker::TYPE_COMPONENT_PREPEND,
                'language'=>'ru',
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'yyyy-mm-dd',
                    'startDate' => date('Y-m-d')
                ]
            ]) ?>
        </div>
    </div>

    <?php if($model->isNewRecord) { ?>
        <?=$form->field($model, 'product_id')->hiddenInput(['value' => $product_id])->label(false) ?>
    <?php } ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
