<?php

use common\models\shop\RozetkaCategory;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\shop\RozetkaCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории розетки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rozetka-category-index">

    <p>
        <?= Html::a('Добавить категорию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'name',
            [
                'attribute' => 'status',
                'format' => 'html',
                'filter' => [
                    RozetkaCategory::STATUS_ACTIVE => 'Активный',
                    RozetkaCategory::STATUS_INACTIVE => 'Не активный'
                ],
                'options' => ['width' => '100'],
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'value' => function($model){
                    $status = $model['status'] == $model::STATUS_ACTIVE ? 'check' : 'ban';
                    return '<i class="fa fa-'.$status.'"></i>';
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>


</div>
