<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\shop\RozetkaCategory */

$this->title = 'Добавление категории на розетку';
$this->params['breadcrumbs'][] = ['label' => 'Категории розетки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rozetka-category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
