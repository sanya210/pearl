<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\shop\RozetkaCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rozetka-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=$form->field($model, 'status')->checkbox() ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
