<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\shop\RozetkaCategory */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Категории розетки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
?>
<div class="rozetka-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
