<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;
use backend\models\PushForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

	<?php $form = ActiveForm::begin(); ?>

	<div class="row">
		<div class="col-md-3">
			<?= $form->field($model, 'status')->dropDownList([
				User::STATUS_ACTIVE => 'Активный',
				User::STATUS_INACTIVE => 'Не активный',
				User::STATUS_DELETED => 'Удалённый',
			]) ?>
		</div>

		<div class="col-md-4">
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
		</div>

		<div class="col-md-4">
			<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
		</div>
	</div>


	<div class="form-group">
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

	<?php if ($model->pushTokens) { ?>
		<div class="box box-solid box-info">
			<div class="box-header">
				Push-уведомление
			</div>
			<div class="box-body">
				<?php $push_model = new PushForm(); ?>
				<?php $push_form = ActiveForm::begin([
					'action' => '/user/push'
				]); ?>
					<div class="row">
						<div class="col-md-4">
							<?= $push_form->field($push_model, 'title')->textInput(['maxlength' => true]) ?>
						</div>

						<div class="col-md-4">
							<?= $push_form->field($push_model, 'link')->textInput(['maxlength' => true]) ?>
						</div>

						<div class="col-md-4">
							<?= $push_form->field($push_model, 'body')->textInput(['maxlength' => true]) ?>
						</div>
					</div>
					<?= $push_form->field($push_model, 'user_id')->hiddenInput(['value' => $model->id])->label(false) ?>
					<div class="form-group">
						<?= Html::submitButton('Отправить', ['class' => 'btn btn-success btn-sm']) ?>
					</div>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	<?php } ?>
</div>
