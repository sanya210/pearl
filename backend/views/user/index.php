<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\daterange\DateRangePicker;
use common\models\User;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'email:email',
            [
                'attribute' => 'status',
                'format' => 'html',
                'filter' => [
                    User::STATUS_ACTIVE => 'Активный',
                    User::STATUS_INACTIVE => 'Не активный',
                    User::STATUS_DELETED => 'Удалённый'
                ],
                'options' => ['width' => '100'],
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'value' => function($model){
                    $status = $model['status'] == $model::STATUS_ACTIVE ? 'check' : 'ban';
                    return '<i class="fa fa-'.$status.'"></i>'; 
                }
            ],
            'created_at:date',

            [
                'attribute' => 'created_at',
                'vAlign'=>'middle',
                'hAlign'=>'center',
                'format' => ['date', 'php:d-m-Y'],
                'headerOptions' => [
                    'class' => 'col-md-1'
                ],
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => 'поиск...',
                ],
                'filter' => DateRangePicker::widget([
                    'name'=>'created_range',
                    'useWithAddon'=>true,
                    'language'=>'ru',
                    'hideInput'=>true,
                    'convertFormat'=>true,
                    'value' => Yii::$app->getRequest()->getQueryParam('created_range') ? Yii::$app->getRequest()->getQueryParam('created_range') : '',
                    'pluginOptions'=>[
                        'locale' => [
                            'format'=>'d-m-Y',
                            'separator'=>'/',
                            'opens'=>'left',
                            'showDropdowns'=>true,
                        ]
                    ],
                    'pluginEvents' => [
                        "cancel.daterangepicker" => "function(ev, picker) { 
                            var pole = $(picker.element[0]).find('input');
                            $(pole).val('').trigger('change');
                        }",
                    ],
                ])
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {permit}',
                'buttons' => [
                   'permit' => function ($url, $model) {
                       return Html::a('<span class="glyphicon glyphicon-wrench"></span>', Url::to(['/permit/user/view', 'id' => $model->id]), [
                           'title' => Yii::t('yii', 'Change user role')
                       ]); 
                   },
                ]
            ],
        ],
    ]); ?>


</div>
