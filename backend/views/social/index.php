<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use common\models\Social;

/* @var $this yii\web\View */
/* @var $searchModel common\models\SocialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Социальные сети';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="social-index">

    <p>
        <?= Html::a('Добавить сеть', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'icon',
                'filter' => false,
                'format' => 'raw',
                'hAlign' => 'center',
                'width' => '90px',
                'value' => function($model){
                    if($model->icon){
                        return Html::img('/uploads/social/'.$model->icon, ['width' => '50']);
                    } else {
                        return '';
                    }
                }
            ],
            'name',
            [
                'attribute' => 'link',
                'format' => 'html',
                'value' => function($model) {
                    return Html::a($model->link, $model->link);
                }
            ],
            [
                'attribute' => 'priority',
                'width' => '30px',
                'hAlign' => 'center'
            ],
            [
                'attribute' => 'status',
                'format' => 'html',
                'filter' => [
                    Social::STATUS_ACTIVE => 'Активный',
                    Social::STATUS_INACTIVE => 'Не активный'
                ],
                'options' => ['width' => '100'],
                'contentOptions' => ['class' => 'text-center'],
                'headerOptions' => ['class' => 'text-center'],
                'value' => function($model){
                    $status = $model['status'] == $model::STATUS_ACTIVE ? 'check' : 'ban';
                    return '<i class="fa fa-'.$status.'"></i>'; 
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>


</div>
