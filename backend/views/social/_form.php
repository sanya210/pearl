<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use sjaakp\illustrated\Uploader;

/* @var $this yii\web\View */
/* @var $model common\models\Social */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="social-form">

	<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

	<?=$form->field($model, 'status')->checkbox() ?>

	<div class="row">
		<div class="col-md-5">
			<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-5">
			<?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-2">
			<?= $form->field($model, 'priority')->input('number', ['min' => 0]) ?>
		</div>
	</div>

	<label>Иконка в svg</label><br>
		<?php
			if($model->icon){
				echo Html::img('@web/uploads/social/'.$model->icon, ['width' => '100']);
			}
			echo $form->field($model, 'image')->fileInput()->label('');
		?>

	<div class="form-group">
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
