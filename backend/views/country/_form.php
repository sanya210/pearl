<?php

use frontend\models\shop\AddToCartForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Country */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="country-form">

	<?php $form = ActiveForm::begin(); ?>

	<?=$form->field($model, 'status')->checkbox() ?>
	
	<div class="row">
		<div class="col-md-2">
			<?= $form->field($model, 'priority')->textInput() ?>
		</div>

        <div class="col-md-3">
            <?= $form->field($model, 'size_type')->dropDownList(array_combine(AddToCartForm::TYPES, AddToCartForm::TYPES)) ?>
        </div>
	</div>

	<div class="form-group">
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
