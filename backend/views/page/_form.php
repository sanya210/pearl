<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\components\widgets\LanguageWidget;

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">

	<?php $form = ActiveForm::begin(); ?>

	<?=$form->field($model, 'status')->checkbox() ?>

	<?php if(!$model->isNewRecord) { ?>
	<div class="row">
		<div class="col-md-3">
			<?= $form->field($model, 'slug')->textInput(['maxlength' => true, 'disabled' => true]) ?>
		</div>
	</div>
	<?php } ?>

	<?= LanguageWidget::widget([
		'form' => $form,
		'model' => $model,
		'field' => 'name',
		'color' => 'default',
		'label' => 'Название'
	]) ?>

	<?= LanguageWidget::widget([
		'form' => $form,
		'model' => $model,
		'field' => 'text',
		'color' => 'info',
		'label' => 'Текст',
		'text' => true
	]) ?>

	<?= LanguageWidget::widget([
		'form' => $form,
		'model' => $model,
		'field' => 'meta_title',
		'color' => 'success',
		'label' => 'Meta Title'
	]) ?>

	<?= LanguageWidget::widget([
		'form' => $form,
		'model' => $model,
		'field' => 'meta_description',
		'color' => 'primary',
		'label' => 'Meta Description'
	]) ?>

	<?= LanguageWidget::widget([
		'form' => $form,
		'model' => $model,
		'field' => 'meta_keywords',
		'color' => 'warning',
		'label' => 'Meta Keywords'
	]) ?>

	<div class="form-group">
		<?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
