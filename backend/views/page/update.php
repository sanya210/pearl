<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Page */

$this->title = $model->localization[Yii::$app->params['main_language']]['name'];
$this->params['breadcrumbs'][] = ['label' => 'Страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-update">

	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
