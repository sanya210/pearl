<?php
return [
    'Tracking number for order' => 'Sendungsnummer für Bestellung',
    'Good afternoon' => 'Guten Tag',
    'We are pleased to inform you of the dispatch of your order' => 'Wir wollen gerne Sie über Versand Ihrer Bestellung Nummer informieren.',
    'Your order number is' => 'Ihre Bestellnummer lautet',
    'Using this number, you will be able to track the whole process of parcel delivery with your order.' => 'Mit ihr können Sie den gesamten Prozess der Zustellung des Pakets verfolgen.',

    'Sending your order' => 'Sendung Ihrer Bestellnummer',

    'Product' => 'Ware',
    'Size (quantity)' => 'Größe (menge)',
    'Amount' => 'Betrag',

    'Address for delivery' => 'Empfängeradresse',
    'Method of delivery' => 'Versandart',
    'Tracking number' => 'Paketverfolgungsnummer',

    'You can track your parcel follow the link:' => 'Sie können Ihr Paket verfolgen ,indem Sie auf den Link:',
    'and put your tracking' => 'klicken und Ihre Sendungsverfolgungsnummer eingeben',

    'The most important thing for us is for you to be satisfied. Our account manager will be happy to answer all your questions and help you.  You can find all the ways to get in touch with us on <a href="https://pearl.wedding/contacts">the contact page</a>.' => 'Die allerwichtigste Hauptsache für uns ist, dass Sie zufrieden bleiben .Unser Kundendienstmanager beantwortet gerne alle Ihre Fragen und entsprechend hilft Ihnen weiter .Auf der <a href="https://pearl.wedding/de/contacts">Kontaktseite</a> finden Sie alle Möglichkeit uns zu kontaktieren',
    'Thank you for the order!' => 'Danke für Ihre Bestellung',
    'Best regards, the Pearl Fashion Group' => 'Mit freundlichen Grüßen Team Pearl Fashion Group',
];
