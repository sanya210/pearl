<?php
return [
    'Tracking number for order' => 'Номер для отслеживания заказа',
    'Good afternoon' => 'Добрый день',
    'We are pleased to inform you of the dispatch of your order' => 'Мы рады сообщить Вам об отправке Вашего заказа',
    'Your order number is' => 'Номер вашего заказа',
    'Using this number, you will be able to track the whole process of parcel delivery with your order.' => 'По нему вы сможете отследить весь процесс доставки посылки с заказом.',

    'Sending your order' => 'Отправка вашего заказа',

    'Product' => 'Товар',
    'Size (quantity)' => 'Размер (количество)',
    'Amount' => 'Сумма',

    'Address for delivery' => 'Адрес получателя',
    'Method of delivery' => 'Служба доставка',
    'Tracking number' => 'Номер для отслеживания посылки',

    'You can track your parcel follow the link:' => 'Вы можете отследить вашу посылку нажав на ссылку:',
    'and put your tracking' => 'и вставить ваш трэкинг',

    'The most important thing for us is for you to be satisfied. Our account manager will be happy to answer all your questions and help you.  You can find all the ways to get in touch with us on <a href="https://pearl.wedding/contacts">the contact page</a>.' => 'Для нас главное, чтобы вы остались довольны. Наш менеджер по работе с клиентами с радостью ответит на все вопросы и окажет вам помощь. Все способы связи с нами вы найдете на странице <a href="https://pearl.wedding/ru/contacts">контакты</a>.',
    'Thank you for the order!' => 'Спасибо за заказ!',
    'Best regards, the Pearl Fashion Group' => 'С уважением команда Pearl Fashion Group',
];
