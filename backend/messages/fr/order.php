<?php
return [
    'Tracking number for order' => 'Numéro de suivi pour la commande',
    'Good afternoon' => 'Bonjour',
    'We are pleased to inform you of the dispatch of your order' => 'Nous avons le plaisir de vous annoncer l’expédition de votre commande Pearl Fashion Group',
    'Your order number is' => 'Votre numéro de commande est',
    'Using this number, you will be able to track the whole process of parcel delivery with your order.' => 'Vous trouverez ci-dessous le numéro de suivi de votre colis et toutes les informations concernant votre livraison.',

    'Sending your order' => 'Envoi de votre commande',

    'Product' => 'Article',
    'Size (quantity)' => 'Taille (quantité)',
    'Amount' => 'Valeur',

    'Address for delivery' => 'Information Livraison',
    'Method of delivery' => 'Mode de Livraison',
    'Tracking number' => 'Numéro de suivi',

    'You can track your parcel follow the link:' => 'Vous pouvez suivre votre colis en cliquant sur le lien:',
    'and put your tracking' => 'et insérant votre numéro de suivi',

    'The most important thing for us is for you to be satisfied. Our account manager will be happy to answer all your questions and help you.  You can find all the ways to get in touch with us on <a href="https://pearl.wedding/contacts">the contact page</a>.' => 'Votre satisfaction est au coeur de nos priorités. Notre service client est à votre écoute, pour toute question, rendez-vous <a href="https://pearl.wedding/fr/contacts">sur notre page contact</a>.',
    'Thank you for the order!' => 'Merci de votre commande!',
    'Best regards, the Pearl Fashion Group' => 'Cordialement vôtre, l\'équipe du Pearl Fashion Group',
];
