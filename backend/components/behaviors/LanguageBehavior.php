<?php
namespace backend\components\behaviors;

use yii\db\ActiveRecord;
use yii\base\Behavior;
use Yii;
use yii\base\Model;

class LanguageBehavior extends Behavior
{
    public $model;
    public $field;
    public $relation;

    public function initLanguages()
    {
        $models = [];
        $languages = Yii::$app->params['languages'];

        foreach ($languages as $locale => $language) {
            if (!isset($this->owner->{$this->relation}[$locale])) {
                $model = new $this->model;
                $model->{$this->field} = $locale;
                $models[$locale] = $model;
            } else {
                $models[$locale] = $this->owner->{$this->relation}[$locale];
            }
        }
        $this->owner->populateRelation('languages', $models);
    }

    public function saveLanguages()
    {
        $result = true;
        foreach ($this->owner->languages as $language) {
            $this->owner->link($this->relation, $language);
            if(!$language->save()) $result = false;
        }

        return $result;
    }
}