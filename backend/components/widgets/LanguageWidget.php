<?php
namespace backend\components\widgets;

use Yii;
use yii\bootstrap\Dropdown;
use yii\bootstrap\Tabs;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

class LanguageWidget extends Tabs
{
    private $_labels;
    public $form;
    public $model;
    public $field;
    public $color;
    public $text = false;
    public $label;

    public function init()
    {
        $languages = Yii::$app->params['languages'];
        foreach ($languages as $locale => $language) {
            if($this->text) {
                $content = '<div class="col-md-12">'.$this->form->field($this->model->languages[$locale], '[' . $locale . ']'.$this->field)->widget(CKEditor::className(), [
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'full',
                        'inline' => false,
                        'font_names' => 'roboto; playfairdisplaysc',
                        'allowedContent' => true,
                    ]),
                ])->label(false).'</div>';
            } else {
                $content = '<div class="col-md-12">'.$this->form->field($this->model->languages[$locale], '[' . $locale . ']'.$this->field)->textInput()->label(false).'</div>';
            }

            array_push($this->items, [
                'label' => $language,
                'content' => $content,
                'options' => [
                    'class' => 'row',
                    'style' => 'padding: 20px 15px 0'
                ],
            ]);
        }
        parent::init();
    }

    public function run()
    {
        return '<div class="box box-solid box-'.$this->color.'">
                    <div class="box-header">'.$this->label.'</div>
                    <div class="box-body">
                        '.parent::run().'
                    </div>
                </div>';
    }
}