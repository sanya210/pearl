<?php
namespace backend\components\widgets;

use Yii;
use dmstr\widgets\Menu;

class MenuWidget extends Menu
{
    public $data;

    public function init()
    {
        $permissions = Yii::$app->authManager->getPermissionsByUser(Yii::$app->user->identity->id);
        $urls = array_column($permissions, 'name');
        $all_levels = [];
        $second_level = [];

        foreach ($urls as $url) {
            $path = explode('/', $url);
            if(count($path) === 1) $all_levels[] = '/'.$path[0];
            if(count($path) > 1) {
                $second_level['/'.$path[0]][] = '/'.$url;
            }
        }

        foreach ($all_levels as $level) {
            unset($second_level[$level]);
        }
        foreach ($this->data as $item) {
            if( isset($item['url']) ) {
                if( in_array($item['url'], $all_levels) ) {
                    $this->items[] = $item;
                }

                if( array_key_exists($item['url'], $second_level) ) {
                    $sub_items = [];
                    foreach ($item['items'] as $sub_item) {
                        if( in_array($sub_item['url'], $second_level[$item['url']]) ){
                            $sub_items[] = $sub_item;
                        }
                    }
                    $item['items'] = $sub_items;
                    $this->items[] = $item;
                }
            } else {
                $this->items[] = $item;
            }
        }
        parent::init();
    }

    public function run()
    {
        parent::run();
    }
}