<?php
return [
	'adminEmail' => 'admin@example.com',
	'access_roles' => ['admin', 'content-manager']
];
