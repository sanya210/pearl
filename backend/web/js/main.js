$('#createEtsyBtn').click(function() {
	$.ajax({
		type: 'POST',
		url: '/etsy/create',
		data: {
			id: $(this).data('id')
		},
		beforeSend: function() {
			$('.preload').addClass('active');
		},
		success: function (res) {
			$('.preload').removeClass('active');
			if (res) {			
				note('Товар успешно импортирован!');
				$.pjax.reload({
					container: '#pjax-etsy',
					timeout: 5000,
				});
			} else {
				note('Не удалось импортировать товар!');
			}
		},
		error: function() {
			$('.preload').removeClass('active');
			note('Ошибка!');
		},
		timeout: 600000
	});
});

$('#updateEtsyBtn').click(function() {
	$.ajax({
		type: 'POST',
		url: '/etsy/update',
		data: {
			id: $(this).data('id')
		},
		beforeSend: function() {
			$('.preload').addClass('active');
		},
		success: function (res) {
			$('.preload').removeClass('active');
			if (res) {			
				note('Товар успешно обновлён!');
			} else {
				note('Не удалось обновить товар!');
			}
		},
		error: function() {
			$('.preload').removeClass('active');
			note('Ошибка!');
		},
		timeout: 600000
	});
});

function note(message) {
	$('.note').text(message);
	setTimeout(function() {
		$('.note').text('');
	}, 3000);
}



















