<?php

namespace backend\models;


use common\models\Currency;
use common\models\shop\Product;
use Google_Client;
use Google_Service_ShoppingContent;
use Google_Service_ShoppingContent_Price;
use Google_Service_ShoppingContent_Product;
use Google_Service_ShoppingContent_ProductsCustomBatchRequest;
use Google_Service_ShoppingContent_ProductsCustomBatchRequestEntry;
use Google_Service_ShoppingContent_ProductShipping;
use Yii;

class Merchants
{
    const MERCHANT_ID = '143644824';
    const LOCALIZATION = [
        'GB' => [
            'language' => 'en',
            'currency' => 'USD'
        ],
        'US' => [
            'language' => 'en',
            'currency' => 'USD'
        ],
        'RU' => [
            'language' => 'ru',
            'currency' => 'USD'
        ],
        'CA' => [
            'language' => 'en',
            'currency' => 'USD'
        ],
        'DE' => [
            'language' => 'de',
            'currency' => 'EUR'
        ],
        'KZ' => [
            'language' => 'ru',
            'currency' => 'USD'
        ],
        'CH' => [
            'language' => 'de',
            'currency' => 'EUR'
        ],
        'AT' => [
            'language' => 'de',
            'currency' => 'EUR'
        ],
        'FR' => [
            'language' => 'en',
            'currency' => 'EUR'
        ],
    ];
    const BRAND = 'Pearl';
    const DELIVERY_SERVICES = ['DHL', 'Ukrposhta'];

    private $client;

    public function __construct()
    {
        $this->setClinet();
    }

    public function getProducts()
    {
        $service = new Google_Service_ShoppingContent($this->client);
        $products = $service->products->listProducts(self::MERCHANT_ID);
        return $products;
    }

    public function custombatch()
    {
        $service = new Google_Service_ShoppingContent($this->client);

        $models = Product::find()->where(['google_merchants' => 1])->all();

        $products = [];
        $count = 0;
        foreach ($models as $model) {
            if ($model->gtin) {
                foreach (self::LOCALIZATION as $country => $data) {
                    $count++;
                    Yii::$app->language = $data['language'];

                    $product = new Google_Service_ShoppingContent_ProductsCustomBatchRequestEntry();
                    $product->setBatchId($count);
                    $product->setMerchantId(self::MERCHANT_ID);
                    $product->setMethod('insert');
                    $product->setProduct($this->generateProduct($model, $country, $data));

                    $products[] = $product;
                }
            }
        }

        $custom_batch_request = new Google_Service_ShoppingContent_ProductsCustomBatchRequest();
        $custom_batch_request->setEntries($products);

        return $service->products->custombatch($custom_batch_request);
    }

    public function insertProducts()
    {
        $products = Product::find()->where(['google_merchants' => 1])->all();
        $result = [];
        foreach ($products as $product) {
            if ($product->gtin) {
                $result[] = $this->insertProduct($product);
            }
        }
        return $result;
    }

    private function insertProduct($model)
    {
        $service = new Google_Service_ShoppingContent($this->client);
        $model = Product::findOne(2);
        $products = [];
        foreach (self::LOCALIZATION as $country => $data) {
            $service = new Google_Service_ShoppingContent($this->client);
            Yii::$app->language = $data['language'];
            $product = $this->generateProduct($model, $country, $data);
            $products[] = $service->products->insert(self::MERCHANT_ID, $product);
        }
        return $products;
    }

    private function generateCost($currency, Product $product)
    {
        $rate = Currency::getRate($currency);
        $value = $product->getPriceValue($rate)['price'];
        $cost = new Google_Service_ShoppingContent_Price();
        $cost->setCurrency($currency);
        $cost->setValue($value);
        return $cost;
    }

    private function generateProduct($model, $country, $data)
    {
        $product = new Google_Service_ShoppingContent_Product();
        $product->setChannel('online');
        $product->setContentLanguage($data['language']);
        $product->setTargetCountry($country);
        $product->setOfferId($model->id);
        $product->setImageLink($model->mainPhotoLink);
        $product->setAdditionalImageLinks($model->images);
        $product->setAvailability('in stock');
        $product->setColor($model->color->localization[$data['language']]['name']);
        $product->setBrand(self::BRAND);
        $price = $this->generateCost($data['currency'], $model);
        $product->setCostOfGoodsSold($price);
        $product->setPrice($price);
        $product->setDescription($model->localization[$data['language']]->description);
        $product->setGtin($model->gtin);
        $product->setLink($model->link);
        $product->setTitle($model->localization[$data['language']]->name);
        $product->setGender('female');
        $product->setSizes($model->internationalSizes);
        $product->setAgeGroup('adult');
        $product->setShipping($this->generateShipping($data['currency']));

        return $product;
    }

    private function generateShipping($currency)
    {
        $price = new Google_Service_ShoppingContent_Price();
        $price->setValue(0);
        $price->setCurrency($currency);

        $shippings = [];
        foreach (self::DELIVERY_SERVICES as $SERVICE) {
            $shipping = new Google_Service_ShoppingContent_ProductShipping();
            $shipping->setPrice($price);
            $shipping->setService($SERVICE);
            $shippings[] = $shipping;
        }
        return $shippings;
    }

    private function setClinet()
    {
        unset($_SESSION['oauth_access_token']);
        $client = new Google_Client();
        $client->setApplicationName('Pearl shopping');
        $client->setClientId('445832833489-d7bde3b50bkomcscrvt7j2qhtt0pokg4.apps.googleusercontent.com');
        $client->setClientSecret('sNdsUZhaoF7Wq7fP5eCKKoKk');
        $client->setRedirectUri('https://admin.pearl.wedding/merchants/insert-products');
        $client->setScopes('https://www.googleapis.com/auth/content');

        if (isset($_SESSION['oauth_access_token'])) {
            $client->setAccessToken($_SESSION['oauth_access_token']);
        } elseif (isset($_GET['code'])) {
            $token = $client->authenticate($_GET['code']);
            $_SESSION['oauth_access_token'] = $token;
        } else {
            header('Location: ' . $client->createAuthUrl());
            exit;
        }
        $this->client = $client;
    }
}