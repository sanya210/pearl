<?php

namespace backend\models\etsy;

use Yii;

/**
 * This is the model class for table "etsy_token".
 *
 * @property int $id
 * @property string $oauth_token
 * @property string $oauth_token_secret
 * @property string $login_url
 * @property string $oauth_verifier
 * @property string $token
 * @property string $token_secret
 * @property int $date
 */
class Token extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'etsy_token';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['oauth_token', 'oauth_token_secret', 'login_url'], 'required'],
            [['date'], 'integer'],
            [['oauth_token', 'oauth_token_secret', 'login_url', 'oauth_verifier', 'token', 'token_secret'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'oauth_token' => 'Oauth Token',
            'oauth_token_secret' => 'Oauth Token Secret',
            'login_url' => 'Login Url',
            'oauth_verifier' => 'Oauth Verifier',
            'token' => 'Token',
            'token_secret' => 'Token Secret',
            'date' => 'Date',
        ];
    }

    public static function findByRequestToken($request_token)
    {
        return self::find()->where(['oauth_token' => $request_token])->one();
    }

    public static function getToken()
    {
        return self::find()->orderBy(['date' => SORT_DESC])->one();
    }
}
