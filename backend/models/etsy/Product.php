<?php

namespace backend\models\etsy;


use Yii;
use yii\base\DynamicModel;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\ConflictHttpException;

class Product extends \common\models\shop\Product
{
    public $arr_etsy;
    public $properties_etsy;

    public function rules()
    {
        return [
            ['etsy_id', 'integer'],
            ['arr_etsy', 'each', 'rule' => ['string', 'max' => 20]],
            ['etsy_description', 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'etsy_description' => 'Описание Etsy',
        ];
    }

    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert)) {
            if ($this->arr_etsy) {
                $this->etsy_tags = serialize(array_filter($this->arr_etsy));
            }

            return true;
        }
        return false;
    }

    public function afterFind()
    {
        $this->arr_etsy = unserialize($this->etsy_tags);
    }

    public function getMaterials()
    {
        $attr = (new Query())
            ->select('product.id, product_attribute.*, attribute_value.*, attribute_localization.*, attribute_value_localization.*')
            ->from('product')
            ->leftJoin('product_attribute', 'product_attribute.product_id=product.id')
            ->leftJoin('attribute_value', 'attribute_value.id=product_attribute.value_id')
            ->leftJoin('attribute_localization', 'attribute_localization.attribute_id=attribute_value.attribute_id')
            ->leftJoin('attribute_value_localization', 'attribute_value_localization.value_id=attribute_value.id')
            ->where(['product.id' => $this->id])
            ->andWhere(['attribute_localization.name' => 'Fabrics'])
            ->andWhere(['attribute_value_localization.language' => 'en'])
            ->all();

        return array_column($attr, 'name');
    }

    public function getEtsyProperties()
    {
        return $this->etsy_properties ? json_decode($this->etsy_properties, true) : [];
    }

    public function getEtsySizes()
    {
        if ($this->category->etsy_sizes) {
            $sizes = json_decode($this->category->etsy_sizes, true);
            $products = [];
            foreach ($sizes as $size) {
                $products[] = [
                    'property_values' => [$size],
                    'sku' => '',
                    'offerings' => [
                        [
                            'price' => $this->getPriceValue(1)['price'] ?: 1,
                            'quantity' => 1
                        ]
                    ]
                ];
            }

            $inventory = [
                'products' => json_encode($products)
            ];
        } else {
            $inventory = [];
        }
        return $inventory;
    }

    public function createEtsy()
    {
        if (!$this->category->etsy_id) {
            throw new ConflictHttpException('Отсутствует связка категории товара с etsy');
        }
        $texts = $this->getLocalization()->where(['language' => 'en'])->all()['en'];
        if (!$this->etsy_description) {
            throw new ConflictHttpException('Отсутствует описание товара');
        }

        $listing = [
            'title' => $texts->name,
            'who_made' => 'collective',
            'when_made' => '2020_2020',
            'taxonomy_id' => $this->category->etsy_id,
            'should_auto_renew' => 'true',
            'description' => $this->etsy_description,
            'price' => $this->getPriceValue(1)['price'] ?: 1,
            'quantity' => 1,
            'is_supply' => 'false',
            'state' => 'draft',
            'shipping_template_id' => '84755841085',
            'materials' => implode($this->materials, ', '),
            'shop_section_id' => $this->category->etsy_section_id,
            'tags' => $this->arr_etsy ? implode($this->arr_etsy, ', ') : '',
        ];

        $images = $this->getAttachment()->select(['id', 'name'])->orderBy(['sort' => SORT_DESC])->indexBy('id')->asArray()->all();
        $images_path = array_map(function($img){
            return Yii::getAlias('@backend/web/uploads/product/'.$img['name']);
        }, $images);

        $properties = $this->etsyProperties;
        $inventory = $this->etsySizes;

        $etsy = new Etsy();
        $this->etsy_id = $etsy->create($listing, $inventory, $images_path, $properties);
        return $this->save(false);
    }

    public function updateEtsy()
    {
        if (!$this->etsy_id) {
            throw new ConflictHttpException('Товар ещё не импортирован на etsy!');
        }
        $texts = $this->getLocalization()->where(['language' => 'en'])->all()['en'];
        if (!$this->etsy_description) {
            throw new ConflictHttpException('Отсутствует описание товара');
        }

        $listing = [
            'title' => $texts->name,
            'description' => $this->etsy_description,
            'state' => 'draft',
            'materials' => implode($this->materials, ', '),
            'tags' => $this->arr_etsy ? implode($this->arr_etsy, ', ') : '',
        ];

        $properties = $this->etsyProperties;
        $inventory = $this->etsySizes;

        $etsy = new Etsy();
        return $etsy->update($this->etsy_id, $listing, $properties, $inventory);
    }

    public function getPropertiesModel()
    {
        $properties = json_decode($this->category->etsy_properties, true);
        if ($this->etsy_properties) {
            $properties_value = ArrayHelper::index(json_decode($this->etsy_properties, true), 'property_id');
        } else {
            $properties_value = [];
        }

        $model = new DynamicModel();
        foreach ($properties as $property) {
            if (isset($properties_value[$property['property_id']])) {
                $model->defineAttribute($property['property_id'], $properties_value[$property['property_id']]['value_ids'][0]);
            } else {
                $model->defineAttribute($property['property_id']);
            }
        }
        return $model;
    }

    public function load($data, $formName = null)
    {
        if (isset($data['DynamicModel'])) {
            $this->loadProperties($data['DynamicModel']);
        }
        return parent::load($data, $formName);
    }

    private function loadProperties($data)
    {
        $properties = [];
        foreach ($data as $id => $value) {
            $properties[] = [
                'property_id' => $id,
                'value_ids' => [$value],
            ];
        }
        $this->etsy_properties = json_encode($properties);
    }
}