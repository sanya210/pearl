<?php

namespace backend\models\etsy;

use yii\base\Model;
use OAuth;
use common\models\shop\ProductPhoto;
/**
* 
*/
class Etsy extends Model
{
	const OAUTH_CONSUMER_KEY = '4kmtw73egfm2b92qzq9ewdfo';
	const OAUTH_CONSUMER_SECRET = '3in62fw6wg';

	public static function auth()
	{
		$oauth = new OAuth(self::OAUTH_CONSUMER_KEY, self::OAUTH_CONSUMER_SECRET);

		$req_token = $oauth->getRequestToken("https://openapi.etsy.com/v2/oauth/request_token?scope=listings_w%20listings_r%20listings_d", 'https://admin.pearl.wedding/etsy/get-etsy-token', "GET");

		if ($req_token && isset($req_token['login_url'])) {
			$model = new Token();
			$model->oauth_token = $req_token['oauth_token'];
			$model->oauth_token_secret = $req_token['oauth_token_secret'];
			$model->login_url = $req_token['login_url'];
			if ($model->save()) {
				return $req_token['login_url'];
			}
		}

		return false;
	}

	public static function getToken($request_token, $verifier)
	{
		$model = Token::findByRequestToken($request_token);
		if ($model) {
			$oauth = new OAuth(self::OAUTH_CONSUMER_KEY, self::OAUTH_CONSUMER_SECRET);
			$oauth->setVersion('1.2.3');

			$oauth->setToken($request_token, $model->oauth_token_secret);

			try {
			   $acc_token = $oauth->getAccessToken("https://openapi.etsy.com/v2/oauth/access_token", null, $verifier, "GET");
				$model->oauth_verifier = $verifier;
				$model->token = $acc_token['oauth_token'];
				$model->token_secret = $acc_token['oauth_token_secret'];
				$model->date = time();
				if ($model->save()) {
					return true;
				}
			} catch (OAuthException $e) {
			    error_log($e->getMessage());
			}
		}
		return false;
	}

	private static function request($url, $data, $method)
	{
		$token = Token::getToken();
		if ($token) {
			$oauth = new OAuth(self::OAUTH_CONSUMER_KEY, self::OAUTH_CONSUMER_SECRET, OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_URI);
			$oauth->enableDebug();
			$oauth->setToken($token->token, $token->token_secret);

			try {
				$data = $oauth->fetch('https://openapi.etsy.com/v2/'.$url, $data, $method);
				$json = $oauth->getLastResponse();
				return json_decode($json, true);
			} catch (OAuthException $e) {
				error_log($e->getMessage());
				error_log(print_r($oauth->getLastResponse(), true));
				error_log(print_r($oauth->getLastResponseInfo(), true));
			}
		}
	}

	public function addPhoto($listing_id, $id, $image, $rank=false)
	{
		$params = [
			'@image' => '@'.$image.';type=multipart',
			'rank' => $rank ?: 1
		];

		$res = $this->request('listings/'.$listing_id.'/images', $params, OAUTH_HTTP_METHOD_POST);

		$model = ProductPhoto::find()->where(['id' => $id])->one();
		$model->etsy_id = $res['results'][0]['listing_image_id'];
		$model->save(false);
	}

	public function removePhoto($listing_id, $listing_image_id)
	{
		$this->request('listings/'.$listing_id.'/images/'.$listing_image_id, null, OAUTH_HTTP_METHOD_DELETE);
	}

	public function updateRankPhoto($listing_id, $listing_image_id, $rank)
	{
		$data = [
			'listing_image_id' => $listing_image_id,
			'rank' => $rank
		];
		$this->removePhoto($listing_id, $listing_image_id);
		$this->request('listings/'.$listing_id.'/images', $data, OAUTH_HTTP_METHOD_POST);
	}

	public function updateInventory($listing_id, $inventory)
	{
		if ($inventory) {
			$this->request('listings/'.$listing_id.'/inventory', $inventory, OAUTH_HTTP_METHOD_PUT);
		}
	}

	public function updateAttributes($listing_id, $properties)
	{
		foreach ($properties as $property) {
			$this->request('listings/'.$listing_id.'/attributes/'.$property['property_id'], ['value_ids' => $property['value_ids']], OAUTH_HTTP_METHOD_PUT);
		}
	}

	public function create($listing, $sizes, $images, $properties)
	{
		$listing = $this->request('listings', $listing, OAUTH_HTTP_METHOD_POST);
		$listing_id = $listing['results'][0]['listing_id'];

		$this->updateInventory($listing_id, $sizes);
		
		$this->updateAttributes($listing_id, $properties);

		foreach ($images as $id => $image) {
			$this->addPhoto($listing_id, $id, $image);
		}

		return $listing_id;
	}

	public function update($listing_id, $listing, $properties, $sizes)
	{
		$this->request('listings/'.$listing_id, $listing, OAUTH_HTTP_METHOD_PUT);
		$this->updateInventory($listing_id, $sizes);
		$this->updateAttributes($listing_id, $properties);
		return true;
	}

	public function getVariations()
	{
      // $this->request('taxonomy/seller/1656/properties', null, OAUTH_HTTP_METHOD_GET);
      return $this->request('taxonomy/seller/2079/properties', null, OAUTH_HTTP_METHOD_GET);
	}
}















