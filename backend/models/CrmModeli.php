<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "modeli".
 *
 * @property int $id
 * @property string $name
 * @property string $dt
 * @property string $img
 * @property string $notes
 * @property string $pattern
 * @property string $details
 */
class CrmModeli extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'modeli';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('crm');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'dt', 'img', 'notes', 'pattern', 'details'], 'required'],
            [['name', 'dt', 'img', 'notes', 'pattern', 'details'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'dt' => 'Dt',
            'img' => 'Img',
            'notes' => 'Notes',
            'pattern' => 'Pattern',
            'details' => 'Details',
        ];
    }

    public function getTypes()
    {
        return $this->hasMany(CrmModeliType::className(), ['m_id' => 'id']);
    }
}
