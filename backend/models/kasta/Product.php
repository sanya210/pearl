<?php
namespace backend\models\kasta;


use common\models\Currency;
use Yii;
use yii\web\ConflictHttpException;

/**
* @property array $text
* @property string $name
* @property string $brand
* @property string $categoryName
*/

class Product extends \common\models\shop\Product
{
    const BRAND = 'Pearl';
    const COUNTRY = 'Украина';
    const STOCK = 3;

    const GENDER = [
        0 => 'Женщинам',
        1 => 'Мужчинам',
        2 => 'Унисекс',
        3 => 'Девочкам',
        4 => 'Мальчикам',
    ];

    const SEASONS = [
        0 => 'Всесезон',
        1 => 'Демисезон',
        2 => 'Лето',
        3 => 'Зима',
    ];

    private $_text = [];
    public $size;

    private $rate;

    public function __construct(array $config = [])
    {
        Yii::$app->language = 'ru';
        $this->rate = Currency::getRate('UAH');
        parent::__construct($config);
    }

    public static function getActive()
    {
        $products_new = [];
        $products = self::find()->where(['status_kasta' => self::STATUS_ACTIVE])
            ->with(['activeLanguage', 'category', 'category.activeLanguage', 'category.sizes', 'color', 'color.activeLanguage', 'photoKasta'])
            ->all();

        foreach ($products as $product) {
            foreach ($product->sizes as $size) {
                $products_new[] = new ProductChild($size->europe, $product);
            }
        }

        return $products_new;
    }

    public function getChilds()
    {
        $products = [];
        foreach ($this->sizes as $size) {
            $products[] = new ProductChild($size->europe, $this);
        }
        return $products;
    }

    protected function getText()
    {
        if (!$this->_text) {
            $this->_text = $this->translation;
        }
        return $this->_text;
    }

    public function getName()
    {
        return $this->category->name_kasta;
    }

    public function getDescription()
    {
        return strip_tags($this->text['description']);
    }

    public function getBrand()
    {
        return self::BRAND;
    }

    public function getCategoryName()
    {
        return $this->category->name_kasta;
    }

    public function getGenderValue()
    {
        return self::GENDER[$this->category->gender ?: 0];
    }

    public function getSeasonValue()
    {
        return self::SEASONS[$this->season ?: 0];
    }

    public function getCountry()
    {
        return self::COUNTRY;
    }

    public function getStock()
    {
        return self::STOCK;
    }

    public function getProviderPrice()
    {
        return $this->recommendedPrice;
    }

    public function getRecommendedPrice()
    {
        return $this->getPriceValue($this->rate)['data']['old_price'];
    }

    public function getUktzed()
    {
        $uktzed = $this->category->uktzed;
        if (!$uktzed) {
            throw new ConflictHttpException('Отсутствует УКТЗЕД для категории ' . $this->categoryName);
        }
        return $uktzed;
    }

    public function getImages()
    {
        $images = [];
        foreach ($this->photoKasta as $image) {
            $images[] = 'https://pearl.wedding/uploads/product/kasta/'.$image->name;
        }
        return implode('|', $images);
    }
}