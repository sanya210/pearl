<?php
namespace backend\models\kasta;

use \moonland\phpexcel\Excel;

class KastaExel
{
    private $products;

    public function __construct()
    {
        $this->products = Product::getActive();
    }

    protected function headers()
    {
        return [
            'article' => 'Артикул*',
            'name' => 'Назва*',
            'brand' => 'Бренд*',
            'categoryName' => 'Вид',
            'size' => 'Розмір*',
            'color.name' => 'Колір*',
            'country' => 'Країна виробництва*',
            'genderValue' => 'Гендер',
            'seasonValue' => 'Сезонність',
            'composition_kasta' => 'Склад',
            'stock' => 'Сток*',
            'description' => 'Опис',
            'providerPrice' => 'Ціна постачальника*',
            'recommendedPrice' => 'Ціна рекомендована*',
            'sku' => 'Унікальний ключ від постачальника для SKU',
            'images' => 'Зображення',
            'uktzed' => 'Код УКТЗЕД',
            'nomenclature' => 'Номенклатура товарів продавця',
        ];
    }

    protected function columns()
    {
        return array_keys($this->headers());
    }

    public function generate()
    {
        Excel::export([
            'models' => $this->products,
            'columns' => $this->columns(),
            'headers' => $this->headers()
        ]);
    }
}