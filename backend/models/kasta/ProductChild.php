<?php
namespace backend\models\kasta;

class ProductChild
{
    private $size;
    private $product;

    public function __construct($size, Product $product)
    {
        $this->size = $size;
        $this->product = $product;
    }

    public function getArticle()
    {
        return $this->product->article . '.' . $this->size;
    }

    public function getSku()
    {
        return $this->article;
    }

    public function getNomenclature()
    {
        return $this->article;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function __get($name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } else {
            return $this->product->$name;
        }
    }
}