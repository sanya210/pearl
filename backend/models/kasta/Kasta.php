<?php
namespace backend\models\kasta;

use yii\httpclient\Client;

class Kasta extends Client
{
    const TOKEN = 'qbncgkju1lx7emn5fdojcjv9qu2407fsidlpprao';
    const URL = 'https://hub.modnakasta.ua/api/';

    public function sendRequest($action, $method='GET', $data=[])
    {
        $response = $this->createRequest()
            ->setHeaders(['Authorization' => self::TOKEN])
            ->setFormat(Client::FORMAT_JSON)
            ->setMethod($method)
            ->setUrl(self::URL.$action)
            ->setData($data)
            ->send();

        if ($response->isOk) {
            return $response->data;
        }

        $content = json_decode($response->content);
        throw new \Exception($content->message);
    }

    public function getProducts()
    {
        return $this->sendRequest('products/list');
    }

    public function getProductsIds()
    {
        $response = $this->getProducts();
        $products = [];
        if ($response && isset($response['items'])) {
            $products_all = array_column($response['items'], 'code');
            foreach ($products_all as $product) {
                $article = Product::parentArticle($product);
                if ($article && !in_array($article, $products)) {
                    $products[] = $article;
                }
            }
        }
        return $products;
    }

    private function generateDataForUpdate($id)
    {
        $model = Product::findOne($id);
        $data = [];
        foreach ($model->childs as $product) {
            $data[] = [
                'unique_sku_id' => $product->article,
                'supplier_price' => $model->recommendedPrice,
                'old_price' => $model->recommendedPrice,
            ];
        }
        return $data;
    }

    public function productUpdate($id)
    {
        $data = $this->generateDataForUpdate($id);
        return $this->sendRequest('products/update-price/id', 'POST', ['items' => $data]);
    }
}