<?php

namespace backend\models\lamoda;

use RocketLabs\SellerCenterSdk\Core\Client;
use RocketLabs\SellerCenterSdk\Core\Request\GenericRequest;

/**
* 
*/
class FeedStatus extends GenericRequest
{
	const ACTION = 'FeedStatus';

   public function __construct($feedID)
   {
   	parent::__construct(
   		Client::GET,
   		static::ACTION,
   		static::V1,
   		[
   			'FeedID' => $feedID
   		]
   	);
   }

   public function getResponseClassName()
   {
      return FeedStatusResponse::class;
   }
}


















