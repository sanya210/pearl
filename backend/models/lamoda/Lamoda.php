<?php
namespace backend\models\lamoda;

use Yii;
use RocketLabs\SellerCenterSdk\Core\Client;
use RocketLabs\SellerCenterSdk\Core\Configuration;
use RocketLabs\SellerCenterSdk\Endpoint\Endpoints;
use RocketLabs\SellerCenterSdk\Core\Response\ErrorResponse;
use yii\base\DynamicModel;
use yii\helpers\ArrayHelper;
use common\models\Currency;
use common\models\shop\Size;
use yii\web\ConflictHttpException;

/**
* @property Size $sizes
* @property $texts
* @property $priceUAH
* @property $primaryCategory
*/
class Lamoda extends \common\models\shop\Product
{
	const SC_API_URL = 'https://api.sellercenter.lamoda.ua/';
	const SC_API_USER = 'lamoda@pearl.wedding';
	const SC_API_KEY = '72a22be1260b9f086862af587e49706643472fbf';
	const BRAND = 'Pearl Fashion Group';

	private $ignore_properties = ['ModelName', 'SizeBrand', 'SkuSupplierSource', 'TaxClass', 'ShipmentType'];
	private $client;

	public $properties_arr = [];

	public function afterFind()
	{
		$this->client = Client::create(new Configuration(self::SC_API_URL, self::SC_API_USER, self::SC_API_KEY));
		$this->loadProduct();
	}

	public function getPrimaryCategory()
	{
		$primaryCategory = $this->category->lamoda_id;
		if (!$primaryCategory) {
			throw new ConflictHttpException('Отсутствует связка категории товара с lamoda');
		}
		return $primaryCategory;
	}

	public function getTexts()
	{
		return $this->getLocalization()->where(['language' => 'ru'])->all()['ru'];
	}

	public function getPriceUAH()
	{
		$rate = Currency::getRate('UAH');
		return $this->getPriceValue($rate);
	}

	public function getSizes()
	{
		return $this->sizes;
	}

	public function getParentSku()
	{
		$size = $this->sizes[0]->europe;
		return $this->article.'.'.$size;
	}

	public function send($properties)
	{
		if ($this->properties_arr) {
			return $this->updateProduct($properties);
		}
		return $this->createProduct($properties);
	}

	private function createProduct($properties)
	{
		$productCollectionRequest = Endpoints::product()->productCreate();

		$parent_sku = null;
		foreach ($this->sizes as $size) {
			if (!$parent_sku) {
				$parent_sku = $this->article.'.'.$size->europe;
			}
			$product = $productCollectionRequest->newProduct()
				->setName($this->texts->name)
				->setSellerSku($this->article.'.'.$size->europe)
				->setParentSku($parent_sku ?: '')
				->setStatus('inactive')
				->setVariation($size->europe)
				->setPrimaryCategory($this->primaryCategory)
				->setDescription('<![CDATA['.$this->texts->description)
				->setBrand(self::BRAND)
				->setPrice((int)$this->priceUAH['data']['old_price'])
				->setProductData($properties);

			if ($this->priceUAH['isDiscount']) {
				$product->setSalePrice($this->priceUAH['price'])
					->setSaleStartDate($this->priceUAH['start'])
					->setSaleEndDate($this->priceUAH['finish']);
			}
		}

		$response = $productCollectionRequest->build()->call($this->client);

		if ($response instanceof ErrorResponse) {
			return [
				'status' => 'danger',
				'message' => $response->getMessage(),
			];
		} else {
			return [
				'status' => 'success',
				'message' => 'Товар успешно отправлен!',
			];
		}
	}

	public function updateProduct($properties)
	{
		$productCollectionRequest = Endpoints::product()->productUpdate();

		foreach ($this->sizes as $size) {
			$product = $productCollectionRequest
				->updateProduct($this->article.'.'.$size->europe)
				->setName($this->texts->name)
				->setStatus('inactive')
				->setDescription('<![CDATA['.$this->texts->description)
				->setPrice((int)$this->priceUAH['data']['old_price'])
				->setProductData($properties);

			if ($this->priceUAH['isDiscount']) {
				$product->setSalePrice($this->priceUAH['price'])
					->setSaleStartDate($this->priceUAH['start'])
					->setSaleEndDate($this->priceUAH['finish']);
			}
		}

		$response = $productCollectionRequest->build()->call($this->client);
		if ($response instanceof ErrorResponse) {
			return [
				'status' => 'danger',
				'message' => $response->getMessage(),
			];
		}
		return [
			'status' => 'success',
			'message' => 'Изменения успешно отправлены!',
		];
	}

	public function image($sellerSku)
	{
		$response = Endpoints::product()->image($sellerSku);
	
		foreach ($this->attachment as $img) {
			$response->addImage('https://pearl.wedding/uploads/product/'.$img->name);
		}
	
		$response->build()->call($this->client);
	}

	public function getProperties()
	{
		$response = Endpoints::product()->getCategoryAttributes($this->primaryCategory)->call($this->client);
		$attributes = $response->getAttributes();
		$model = new DynamicModel();
		$values = [];
		foreach ($attributes as $attribute) {
			$name = str_replace('_', '', ucwords($attribute->getName(), '_'));
			if (in_array($name, $this->ignore_properties)) {
				continue;
			}
			if ($attribute->getAttributeType() != 'system') {
				$options = [];
				if ($attribute->getOptions()) {
					foreach ($attribute->getOptions() as $option) {
						$options[$option->getName()] = $option->getName();
					}
				}

				$values[] = [
					'name' => $name,
					'label' => $attribute->getLabel() . ($attribute->isMandatory() ? ' *' : ''),
					'options' => $options,
					'description' => $attribute->getDescription(),
					'type' => $attribute->getAttributeType()
				];

				if ($attribute->getAttributeType() == 'multi_option') {
					$value = explode(',', $this->properties_arr[$name] ?? '');
				} else {
					$value = $this->properties_arr[$name] ?? '';
				}
				
				$model->defineAttribute($name, $value);

				if ($attribute->isMandatory()) {
					$model->addRule($name, 'required');
				}
			}
		}
		
		return [
			'model' => $model,
			'values' => $values
		];
	}

	public function loadProduct()
	{
		try {
			$response = \RocketLabs\SellerCenterSdk\Endpoint\Endpoints::product()
				->getProducts()
				->setSearch($this->parentSku)
				->build()
				->call($this->client);

            if (isset($response->getProducts()[0])) {
                $this->properties_arr = $response->getProducts()[0]->getProductData();
            }
		} catch (\Exception $e) {}
	}
}
















