<?php

namespace backend\models\lamoda;

use RocketLabs\SellerCenterSdk\Core\Response\GenericResponse;

/**
*
*/
class FeedStatusResponse extends GenericResponse
{
	const FEED_KEY = 'FeedDetail';

	private $errors;
	private $total_records;
	private $failed_records;

	public function getErrors()
	{
		return $this->errors;
	}

	public function getTotalRecords()
	{
		return $this->total_records;
	}

	public function getFailedRecords()
	{
		return $this->failed_records;
	}

	protected function processDecodedResponse(array $responseData)
	{
		parent::processDecodedResponse($responseData);
		$detail = $this->body[self::FEED_KEY];
		$this->errors = isset($detail['FeedErrors']) ? $detail['FeedErrors']['Error'] : [];
		$this->total_records = $detail['TotalRecords'];
		$this->failed_records = $detail['FailedRecords'];
	}
}
