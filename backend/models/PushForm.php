<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use common\models\PushToken;

/**
 * Push form
 */
class PushForm extends Model
{
    public $user_id;
    public $title;
    public $body;
    public $link;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['user_id', 'integer'],
            [['user_id', 'title', 'body', 'link'], 'required'],
            [['title', 'body'], 'string'],
            ['link', 'url']
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Заголовок',
            'body' => 'Сообщение',
            'link' => 'Ссылка',
        ];
    }

    public function send()
    {
        PushToken::sendToUser($this->user_id, [
            'title' => $this->title,
            'body' => $this->body,
            'link' => $this->link,
        ]);

        return true;
    }
}
