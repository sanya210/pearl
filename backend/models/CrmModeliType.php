<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "modeli_type".
 *
 * @property int $id
 * @property int $m_id
 * @property string $name
 * @property string $artikul
 * @property string $main
 * @property string $pattern
 * @property string $material
 * @property string $norm
 * @property string $dt
 * @property string $note
 * @property string $color
 * @property int $base_size
 * @property string $height
 * @property string $all_size
 */
class CrmModeliType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'modeli_version';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('crm');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['m_id', 'name', 'artikul', 'main', 'pattern', 'material', 'norm', 'dt', 'note', 'color', 'base_size', 'height', 'all_size'], 'required'],
            [['m_id', 'base_size'], 'integer'],
            [['name', 'artikul', 'main', 'pattern', 'material', 'norm', 'dt', 'note', 'color', 'height', 'all_size'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'm_id' => 'M ID',
            'name' => 'Название',
            'artikul' => 'Артикул',
            'main' => 'Main',
            'pattern' => 'Pattern',
            'material' => 'Материалы',
            'norm' => 'Norm',
            'dt' => 'Дата',
            'note' => 'Note',
            'color' => 'Цвет',
            'base_size' => 'Base Size',
            'height' => 'Height',
            'all_size' => 'All Size',
            'img' => 'Картинка'
        ];
    }

    public function getModel()
    {
        return $this->hasOne(CrmModeli::className(), ['id' => 'm_id']);
    }

    public function getImg()
    {
        if ($this->model && $this->model->img) {
            $images = unserialize($this->model->img);
            if (isset($images[0])) {
                return $images[0];
            }
        }
        return null;
    }
}
