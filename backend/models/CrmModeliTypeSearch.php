<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\CrmModeliType;
use common\models\shop\Product;

/**
 * CrmModeliTypeSearch represents the model behind the search form of `backend\models\CrmModeliType`.
 */
class CrmModeliTypeSearch extends CrmModeliType
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'm_id', 'base_size'], 'integer'],
            [['name', 'artikul', 'main', 'pattern', 'material', 'norm', 'dt', 'note', 'color', 'height', 'all_size'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $articles = Product::find()->select('article')->column();
        $query = CrmModeliType::find()->where(['not in', 'artikul', $articles])->with('model');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'm_id' => $this->m_id,
            'base_size' => $this->base_size,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'artikul', $this->artikul])
            ->andFilterWhere(['like', 'main', $this->main])
            ->andFilterWhere(['like', 'pattern', $this->pattern])
            ->andFilterWhere(['like', 'material', $this->material])
            ->andFilterWhere(['like', 'norm', $this->norm])
            ->andFilterWhere(['like', 'dt', $this->dt])
            ->andFilterWhere(['like', 'note', $this->note])
            ->andFilterWhere(['like', 'color', $this->color])
            ->andFilterWhere(['like', 'height', $this->height])
            ->andFilterWhere(['like', 'all_size', $this->all_size]);

        return $dataProvider;
    }
}
