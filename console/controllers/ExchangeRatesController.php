<?php
namespace console\controllers;

use yii\console\Controller;
use common\models\Currency;

class ExchangeRatesController extends Controller
{
	public function actionRecount()
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://api.privatbank.ua/p24api/pubinfo?exchange&json&coursid=11');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$res = curl_exec($ch);
		curl_close($ch);

		$rates = json_decode($res, true);

		$usd = 1;
		$eur = 1;

		foreach ($rates as $rate) {
			switch ($rate['ccy']) {
				case 'USD':
				$usd = $rate['sale'];
				break;
				case 'EUR':
				$eur = $rate['sale'];
				break;
			}
		}

		Currency::setRates([
			'UAH' => $usd,
			'EUR' => $usd / $eur
		]);
	}
}

