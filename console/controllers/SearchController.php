<?php
namespace console\controllers;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use yii\console\Controller;
use common\models\shop\Product;

class SearchController extends Controller
{
	private $client;

	public function __construct($id, $module, Client $client, $config = [])
	{
		parent::__construct($id, $module, $config);
		$this->client = $client;
	}

	public function actionReindex()
	{
		$query = Product::find()
			->where(['status' => Product::STATUS_ACTIVE])
			->with('localization')
			->orderBy('id');

		$this->stdout('Clearing' . PHP_EOL);

		$this->stdout('Indexing of products' . PHP_EOL);

		foreach ($query->each() as $product) {
			$this->stdout('Product #' . $product->id . PHP_EOL);

			$name = '';
			$description = '';
			foreach ($product->localization as $item) {
				$name .= ' '.$item->name;
				$description .= ' '.strip_tags($item->description);
			}

			$this->client->index([
				'index' => 'shop',
				'type' => 'products',
				'id' => $product->id,
				'body' => [
					'name' => $name,
					'description' => $description,
                    'article' => $product->article,
				]
			]);
		}

		$this->stdout('Done!' . PHP_EOL);

	}
}

