<?php
namespace console\controllers;

use yii\console\Controller;
use common\models\Country;

class TestController extends Controller
{
    public function actionCountry()
    {
        Country::updateAll(['size_type' => 'europe']);
    }
}