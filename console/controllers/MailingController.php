<?php
namespace console\controllers;

use yii\console\Controller;
use Yii;
use yii\validators\EmailValidator;
use common\models\Email;
use common\models\PushToken;
use yii\helpers\Inflector;

/**
* 
*/
class MailingController extends Controller
{
	const FILE = '';
	const SUBJECT = '';
	const NAME_LOG = '';

	const PUSH_MESSAGE = [
		'title' => '',
		'body' => '',
		'link' => ''
	];

	public function actionEmail($file=self::FILE, $subject=self::SUBJECT, $name_log=self::NAME_LOG)
	{
		$emails = Email::find()->select('email')->column();

		$validator = new EmailValidator();
		$count = 0;

		foreach ($emails as $email) {
			if ( $validator->validate($email, $error) ) {
				$count++;
				$log = '('.$count.') '.$email;
				$this->sendEmail($email, $log, $file, $name_log, $subject);
				echo '('.$count.') '.$email . "\n";
			}
		}
	}

	private function sendEmail($email, $log, $file, $name_log, $subject)
	{
		$mail = Yii::$app->mailer->compose([
			'html' => $file
		])
		->setFrom(['sales@pearl.wedding'])
		->setTo($email)
		->setSubject($subject)
		->send();

		if ($mail) {
			$this->Loger($log, 'email', $name_log);
		}
	}

	public function actionPush()
	{
		$message = self::PUSH_MESSAGE;

		PushToken::sendToUsers($message);

		$log = "Title: {$message['title']}\nBody: {$message['body']}\nLink: {$message['link']}";

		$this->Loger($log, 'push', Inflector::slug($message['title']));
	}

	private function Loger($msg, $folder, $name_log){        
		$fd = fopen(Yii::getAlias('@console/logs/'.$folder.'/'.$name_log.'_Log_'.date('Y-m-d', time()).'.txt'), 'a');
		fwrite($fd, "\r\n\r\n--------------------------".date('Y-m-d\TH:i:s', time())."-------------------------------\r\n\r\n".$msg);
		fclose($fd);
	}
}
































