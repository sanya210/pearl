<?php

use yii\db\Migration;

/**
 * Class m200220_133618_currency_log
 */
class m200220_133618_currency_log extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%currency_log}}', [
            'id' => $this->primaryKey(),
            'currency_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'rate' => $this->decimal(20, 4)->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk_currency_log', '{{%currency_log}}', 'currency_id', '{{%currency}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%currency_log}}');
    }
}
