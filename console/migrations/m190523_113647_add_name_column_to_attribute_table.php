<?php

use yii\db\Migration;

/**
 * Handles adding name to table `{{%attribute}}`.
 */
class m190523_113647_add_name_column_to_attribute_table extends Migration
{
    public function up()
    {
        $this->addColumn('attribute', 'slug', $this->string(100));
    }

    public function down()
    {
        $this->dropColumn('attribute', 'slug');
    }
}
