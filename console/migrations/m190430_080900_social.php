<?php

use yii\db\Migration;

/**
 * Class m190430_080900_social
 */
class m190430_080900_social extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%social}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100),
            'icon' => $this->string(100),
            'link' => $this->string(100),
            'priority' => $this->integer(),
            'status' => $this->smallInteger()->notNull()
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%social}}');
    }
}
