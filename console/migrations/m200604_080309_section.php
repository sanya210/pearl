<?php

use yii\db\Migration;

/**
 * Class m200604_080309_section
 */
class m200604_080309_section extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%section}}', [
            'id' => $this->primaryKey(),
            'priority' => $this->integer(),
            'status' => $this->smallInteger()->notNull(),
            'categories' => $this->text(),
        ], $tableOptions);

        $this->createTable('{{%section_localization}}', [
            'id' => $this->primaryKey(),
            'section_id' => $this->integer(),
            'language' => $this->string(16)->notNull(),
            'name' => $this->string(100),
        ], $tableOptions);

        $this->addForeignKey('fk_section_localization', '{{%section_localization}}', 'section_id', '{{%section}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%section}}');
        $this->dropTable('{{%section_localization}}');
    }
}
