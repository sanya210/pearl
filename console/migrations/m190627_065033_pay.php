<?php

use yii\db\Migration;

/**
 * Class m190627_065033_pay
 */
class m190627_065033_pay extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%pay}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'user_id' => $this->integer(),
            'amount' => $this->decimal(20, 4),
            'currency' => $this->string(16)->notNull(),
            'date' => $this->integer()->notNull(),
            'system' => $this->string(100)->notNull(),
            'system_id' => $this->integer()->notNull(),
            'status' => $this->string(100)->notNull()
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%pay}}');
    }
}
