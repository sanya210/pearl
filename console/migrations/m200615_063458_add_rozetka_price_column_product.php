<?php

use yii\db\Migration;

/**
 * Class m200615_063458_add_rozetka_price_column_product
 */
class m200615_063458_add_rozetka_price_column_product extends Migration
{
    public function up()
    {
        $this->addColumn('product', 'rozetka_price', $this->decimal(20, 4));
    }

    public function down()
    {
        $this->dropColumn('product', 'rozetka_price');
    }
}
