<?php

use yii\db\Migration;

/**
 * Class m190930_130229_product_rozetka
 */
class m190930_130229_product_rozetka extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%product_rozetka}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'product_id' => $this->integer(),
            'sort' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_product_rozetka', '{{%product_rozetka}}', 'product_id', '{{%product}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%product_rozetka}}');
    }
}
