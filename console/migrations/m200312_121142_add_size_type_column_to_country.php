<?php

use yii\db\Migration;

/**
 * Class m200312_121142_add_size_type_column_to_country
 */
class m200312_121142_add_size_type_column_to_country extends Migration
{
    public function up()
    {
        $this->addColumn('country', 'size_type', $this->string(50));
    }

    public function down()
    {
        $this->dropColumn('country', 'size_type');
    }
}
