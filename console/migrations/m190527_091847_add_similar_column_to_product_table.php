<?php

use yii\db\Migration;

/**
 * Handles adding similar to table `{{%product}}`.
 */
class m190527_091847_add_similar_column_to_product_table extends Migration
{
    public function up()
    {
        $this->addColumn('product', 'similar', $this->text());
    }

    public function down()
    {
        $this->dropColumn('product', 'similar');
    }
}
