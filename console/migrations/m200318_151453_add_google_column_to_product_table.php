<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%product}}`.
 */
class m200318_151453_add_google_column_to_product_table extends Migration
{
    public function up()
    {
        $this->addColumn('product', 'gtin', $this->string());
        $this->addColumn('product', 'google_merchants', $this->smallInteger());
    }

    public function down()
    {
        $this->dropColumn('product', 'gtin');
        $this->dropColumn('product', 'google_merchants');
    }
}
