<?php

use yii\db\Migration;

/**
 * Class m190620_074208_order
 */
class m190620_074208_order extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'user_id' => $this->integer(),
            'user_name' => $this->string(),
            'user_family_name' => $this->string(),
            'user_phone' => $this->string(100),
            'language' => $this->string(16)->notNull(),
            'currency' => $this->string(16)->notNull(),
            'note' => $this->text(),
            'delivery_index' => $this->string(),
            'delivery_address' => $this->string(),
            'status_json' => $this->text(),
            'cost' => $this->decimal(20, 4),
        ], $tableOptions);

        $this->createTable('{{%order_item}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'sizes' => $this->text(),
            'quantity' => $this->text(),
            'price' => $this->decimal(20, 4),
        ], $tableOptions);

        $this->addForeignKey('fk_order_item', '{{%order_item}}', 'order_id', '{{%order}}', 'id', 'CASCADE', 'RESTRICT');

        $this->addForeignKey('fk_order_item_product', '{{%order_item}}', 'product_id', '{{%product}}', 'id', 'RESTRICT');

    }

    public function down()
    {
        $this->dropTable('{{%order}}');
        $this->dropTable('{{%order_item}}');
    }
}
