<?php

use yii\db\Migration;

/**
 * Class m190624_071449_order_log
 */
class m190624_071449_order_log extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%order_log}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'date' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull(),
        ], $tableOptions);

        $this->dropColumn('order', 'status_json');
    }

    public function down()
    {
        $this->dropTable('{{%order_log}}');
        $this->addColumn('order', 'status_json', $this->text());
    }
}
