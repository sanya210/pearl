<?php

use yii\db\Migration;

/**
 * Class m200114_133747_add_etsy_id_column_to_product_photo
 */
class m200114_133747_add_etsy_id_column_to_product_photo extends Migration
{
    public function up()
    {
        $this->addColumn('product_photo', 'etsy_id', $this->string(255));
    }

    public function down()
    {
        $this->dropColumn('product_photo', 'etsy_id');
    }
}
