<?php

use yii\db\Migration;

/**
 * Handles adding status to table `{{%order}}`.
 */
class m190621_101533_add_status_column_to_order_table extends Migration
{
    public function up()
    {
        $this->addColumn('order', 'status', $this->integer()->after('delivery_address'));
    }

    public function down()
    {
        $this->dropColumn('order', 'status');
    }
}
