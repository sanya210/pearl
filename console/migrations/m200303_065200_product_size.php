<?php

use yii\db\Migration;

/**
 * Class m200303_065200_product_size
 */
class m200303_065200_product_size extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%product_size}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'size_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_product_size', '{{%product_size}}', 'product_id', '{{%product}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%product_size}}');
    }
}
