<?php

use yii\db\Migration;

/**
 * Handles adding priority to table `{{%country}}`.
 */
class m190702_060018_add_priority_column_to_country_table extends Migration
{
    public function up()
    {
        $this->addColumn('country', 'priority', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('country', 'priority');
    }
}
