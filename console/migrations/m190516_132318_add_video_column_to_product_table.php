<?php

use yii\db\Migration;

/**
 * Handles adding video to table `{{%product}}`.
 */
class m190516_132318_add_video_column_to_product_table extends Migration
{
    public function up()
    {
        $this->addColumn('product', 'video', $this->string(255));
    }

    public function down()
    {
        $this->dropColumn('product', 'video');
    }
}
