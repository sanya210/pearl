<?php

use yii\db\Migration;

/**
 * Class m190510_165220_discount
 */
class m190510_165220_discount extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%discount}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'price' => $this->decimal(20, 4),
            'created_at' => $this->integer(),
            'date_start' => $this->date(),
            'date_finish' => $this->date(),
            'status' => $this->smallInteger()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk_discount_product', '{{%discount}}', 'product_id', '{{%product}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%discount}}');
    }
}
