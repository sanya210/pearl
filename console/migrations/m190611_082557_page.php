<?php

use yii\db\Migration;

/**
 * Class m190611_082557_page
 */
class m190611_082557_page extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%page}}', [
            'id' => $this->primaryKey(),
            'slug' => $this->string(100)->notNull(),
            'status' => $this->smallInteger()->notNull()
        ], $tableOptions);

        $this->createTable('{{%page_localization}}', [
            'id' => $this->primaryKey(),
            'page_id' => $this->integer(),
            'language' => $this->string(16)->notNull(),
            'name' => $this->string(255),
            'text' => $this->text(),
            'meta_title' => $this->string(150),
            'meta_description' => $this->text(),
            'meta_keywords' => $this->string(255),
        ], $tableOptions);

        $this->addForeignKey('fk_page_localization', '{{%page_localization}}', 'page_id', '{{%page}}', 'id', 'CASCADE', 'RESTRICT');

    }

    public function down()
    {
        $this->dropTable('{{%page}}');
        $this->dropTable('{{%page_localization}}');
    }
}
