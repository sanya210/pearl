<?php

use yii\db\Migration;

/**
 * Class m190610_103700_wishlist
 */
class m190610_103700_wishlist extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%wishlist}}', [
            'user_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('{{%pk-user_wishlist_items}}', '{{%wishlist}}', ['user_id', 'product_id']);
        $this->createIndex('{{%idx-wishlis-user_id}}', '{{%wishlist}}', 'user_id');
        $this->createIndex('{{%idx-wishlist-product_id}}', '{{%wishlist}}', 'product_id');
        $this->addForeignKey('{{%fk-wishlist-user_id}}', '{{%wishlist}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('{{%fk-wishlist-product_id}}', '{{%wishlist}}', 'product_id', '{{%product}}', 'id', 'CASCADE', 'RESTRICT');

    }

    public function down()
    {
        $this->dropTable('{{%wishlist}}');
    }
}
