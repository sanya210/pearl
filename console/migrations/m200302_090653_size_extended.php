<?php

use yii\db\Migration;

/**
 * Class m200302_090653_size_extended
 */
class m200302_090653_size_extended extends Migration
{
    public function up()
    {
        $this->addColumn('size', 'number_usa', $this->integer());
        $this->addColumn('size', 'ru', $this->integer());
        $this->addColumn('size', 'uk', $this->integer());
        $this->addColumn('size', 'de', $this->integer());

        $this->addColumn('size', 'waist_inch', $this->string(10));
        $this->addColumn('size', 'hips_inch', $this->string(10));
        $this->addColumn('size', 'chest_inch', $this->string(10));
    }

    public function down()
    {
        $this->dropColumn('size', 'number_usa');
        $this->dropColumn('size', 'ru');
        $this->dropColumn('size', 'uk');
        $this->dropColumn('size', 'de');
        $this->dropColumn('size', 'waist_inch');
        $this->dropColumn('size', 'hips_inch');
        $this->dropColumn('size', 'chest_inch');
    }
}
