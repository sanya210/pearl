<?php

use yii\db\Migration;

/**
 * Handles adding status_rozetka to table `{{%product}}`.
 */
class m191126_094239_add_status_rozetka_column_to_product_table extends Migration
{
    public function up()
    {
        $this->addColumn('product', 'status_rozetka', $this->smallInteger());
    }

    public function down()
    {
        $this->dropColumn('product', 'status_rozetka');
    }
}
