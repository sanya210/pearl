<?php

use yii\db\Migration;

/**
 * Handles adding address_id to table `{{%order}}`.
 */
class m190701_194018_add_address_id_column_to_order_table extends Migration
{
    public function up()
    {
        $this->addColumn('order', 'address_id', $this->integer());

        $this->dropColumn('order', 'delivery_index');
        $this->dropColumn('order', 'delivery_address');
    }

    public function down()
    {
        $this->dropColumn('order', 'address_id');
        
        $this->addColumn('order', 'delivery_index', $this->string());
        $this->addColumn('order', 'delivery_address', $this->string());
    }
}
