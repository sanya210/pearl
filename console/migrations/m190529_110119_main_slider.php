<?php

use yii\db\Migration;

/**
 * Class m190529_110119_main_slider
 */
class m190529_110119_main_slider extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%main_slider}}', [
            'id' => $this->primaryKey(),
            'img_lg' => $this->string(100)->notNull(),
            'img_xs' => $this->string(100)->notNull(),
            'priority' => $this->integer(),
            'status' => $this->smallInteger()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%main_slider_localization}}', [
            'id' => $this->primaryKey(),
            'slider_id' => $this->integer(),
            'language' => $this->string(16)->notNull(),
            'header' => $this->string(255),
            'text' => $this->text(),
            'link' => $this->string(255),
        ], $tableOptions);

        $this->addForeignKey('fk_main_slider_localization', '{{%main_slider_localization}}', 'slider_id', '{{%main_slider}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%main_slider}}');
        $this->dropTable('{{%main_slider_localization}}');
    }
}
