<?php

use yii\db\Migration;

/**
 * Handles adding price_main to table `{{%order_item}}`.
 */
class m190626_065020_add_price_main_column_to_order_item_table extends Migration
{
    public function up()
    {
        $this->addColumn('order_item', 'price_main', $this->decimal(20, 4));
    }

    public function down()
    {
        $this->dropColumn('order_item', 'price_main');
    }
}
