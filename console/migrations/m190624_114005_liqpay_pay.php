<?php

use yii\db\Migration;

/**
 * Class m190624_114005_liqpay_pay
 */
class m190624_114005_liqpay_pay extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%liqpay_log}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'status' => $this->string(20),
            'payment_id' => $this->string(20),
            'public_key' => $this->string(100),
            'liqpay_order_id' => $this->string(),
            'description' => $this->string(),
            'sender_phone' => $this->string(100),
            'sender_card_mask2' => $this->string(100),
            'sender_card_country' => $this->string(20),
            'ip' => $this->string(50),
            'amount' => $this->decimal(20, 4),
            'currency' => $this->string(20),
            'sender_commission' => $this->decimal(20, 4),
            'receiver_commission' => $this->decimal(20, 4),
            'agent_commission' => $this->decimal(20, 4),
            'amount_debit' => $this->decimal(20, 4),
            'amount_credit' => $this->decimal(20, 4),
            'commission_debit' => $this->decimal(20, 4),
            'commission_credit' => $this->decimal(20, 4),
            'currency_debit' => $this->string(20),
            'currency_credit' => $this->string(20),
            'sender_bonus' => $this->decimal(20, 4),
            'amount_bonus' => $this->decimal(20, 4),
            'language' => $this->string(20),
            'create_date' => $this->integer(20),
            'end_date' => $this->integer(20),
            'transaction_id' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%liqpay_log}}');
    }
}
