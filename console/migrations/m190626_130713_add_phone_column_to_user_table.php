<?php

use yii\db\Migration;

/**
 * Handles adding phone to table `{{%user}}`.
 */
class m190626_130713_add_phone_column_to_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'phone', $this->string(100));
    }

    public function down()
    {
        $this->dropColumn('user', 'phone');
    }
}
