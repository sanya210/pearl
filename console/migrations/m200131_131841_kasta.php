<?php

use yii\db\Migration;

/**
 * Class m200131_131841_kasta
 */
class m200131_131841_kasta extends Migration
{
    public function up()
    {
        $this->addColumn('product', 'status_kasta', $this->smallInteger());
        $this->addColumn('shop_category', 'uktzed', $this->string(50));
    }

    public function down()
    {
        $this->dropColumn('product', 'status_kasta');
        $this->dropColumn('shop_category', 'uktzed');
    }
}
