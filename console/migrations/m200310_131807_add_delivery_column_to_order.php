<?php

use yii\db\Migration;

/**
 * Class m200310_131807_add_delivery_column_to_order
 */
class m200310_131807_add_delivery_column_to_order extends Migration
{
    public function up()
    {
        $this->addColumn('order', 'delivery_number', $this->string(255));
        $this->addColumn('order', 'delivery_type', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('order', 'delivery_number');
        $this->dropColumn('order', 'delivery_type');
    }
}
