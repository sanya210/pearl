<?php

use yii\db\Migration;

/**
 * Class m191213_093748_push_token
 */
class m191213_093748_push_token extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%push_token}}', [
            'id' => $this->primaryKey(),
            'token' => $this->string(255)->notNull(),
            'user_id' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%push_token}}');
    }
}
