<?php

use yii\db\Migration;

/**
 * Handles adding etsy_tags to table `{{%product}}`.
 */
class m200110_125309_add_etsy_tags_column_to_product_table extends Migration
{
	public function up()
	{
		$this->addColumn('product', 'etsy_tags', $this->text());
		$this->addColumn('product', 'etsy_properties', $this->text());
		$this->addColumn('product', 'etsy_id', $this->string(255));
		$this->addColumn('product', 'etsy_description', $this->text());
	}

	public function down()
	{
		$this->dropColumn('product', 'etsy_tags');
		$this->dropColumn('product', 'etsy_properties');
		$this->dropColumn('product', 'etsy_id');
		$this->dropColumn('product', 'etsy_description');
	}
}
