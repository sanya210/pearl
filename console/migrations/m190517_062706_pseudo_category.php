<?php

use yii\db\Migration;

/**
 * Class m190517_062706_pseudo_category
 */
class m190517_062706_pseudo_category extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%pseudo_category}}', [
            'id' => $this->primaryKey(),
            'slug' => $this->string(100)->notNull(),
            'language' => $this->string(16)->notNull(),
            'name' => $this->string(100),
            'category_id' => $this->integer(),
            'meta_title' => $this->string(150),
            'meta_description' => $this->text(),
            'meta_keywords' => $this->string(255),
            'priority' => $this->integer(),
            'status' => $this->smallInteger()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk_pseudo_category_category', '{{%pseudo_category}}', 'category_id', '{{%shop_category}}', 'id', 'RESTRICT');

        $this->createTable('{{%product_pseudo_category}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'pseudo_category_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_product_pseudo_category_product', '{{%product_pseudo_category}}', 'product_id', '{{%product}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_product_pseudo_category_pseudo_category', '{{%product_pseudo_category}}', 'pseudo_category_id', '{{%pseudo_category}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%pseudo_category}}');
        $this->dropTable('{{%product_pseudo_category}}');
    }
}
