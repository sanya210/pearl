<?php

use yii\db\Migration;

/**
 * Class m190930_140721_add_rozetka_column_to_shop_category
 */
class m190930_140721_add_rozetka_column_to_shop_category extends Migration
{
	public function up()
	{
		$this->addColumn('shop_category', 'status_rozetka', $this->smallInteger());
		$this->addColumn('shop_category', 'name_rozetka', $this->string());
	}

	public function down()
	{
		$this->dropColumn('shop_category', 'status_rozetka');
		$this->dropColumn('shop_category', 'name_rozetka');
	}
}
