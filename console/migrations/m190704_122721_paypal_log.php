<?php

use yii\db\Migration;

/**
 * Class m190704_122721_paypal_log
 */
class m190704_122721_paypal_log extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%paypal_log}}', [
            'id' => $this->primaryKey(),
            'mc_gross' => $this->string(),
            'invoice' => $this->string(),
            'protection_eligibility' => $this->string(),
            'payer_id' => $this->string(),
            'payment_date' => $this->string(),
            'payment_status' => $this->string(),
            'charset' => $this->string(),
            'first_name' => $this->string(),
            'mc_fee' => $this->string(),
            'notify_version' => $this->string(),
            'custom' => $this->string(),
            'payer_status' => $this->string(),
            'business' => $this->string(),
            'quantity' => $this->string(),
            'verify_sign' => $this->string(),
            'payer_email' => $this->string(),
            'txn_id' => $this->string(),
            'payment_type' => $this->string(),
            'last_name' => $this->string(),
            'receiver_email' => $this->string(),
            'payment_fee' => $this->string(),
            'shipping_discount' => $this->string(),
            'receiver_id' => $this->string(),
            'insurance_amount' => $this->string(),
            'txn_type' => $this->string(),
            'item_name' => $this->string(),
            'discount' => $this->string(),
            'mc_currency' => $this->string(),
            'item_number' => $this->string(),
            'residence_country' => $this->string(),
            'test_ipn' => $this->string(),
            'shipping_method' => $this->string(),
            'transaction_subject' => $this->string(),
            'payment_gross' => $this->string(),
            'ipn_track_id' => $this->string(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%paypal_log}}');
    }
}
