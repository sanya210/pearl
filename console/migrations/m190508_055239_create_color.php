<?php

use yii\db\Migration;

/**
 * Class m190508_055239_create_color
 */
class m190508_055239_create_color extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%color}}', [
            'id' => $this->primaryKey(),
            'hex' => $this->string(10)->notNull(),
            'sort' => $this->integer(),
            'status' => $this->smallInteger()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%color_localization}}', [
            'id' => $this->primaryKey(),
            'color_id' => $this->integer(),
            'language' => $this->string(16)->notNull(),
            'name' => $this->string(100),
        ], $tableOptions);

        $this->addForeignKey('fk_color_localization', '{{%color_localization}}', 'color_id', '{{%color}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%color}}');
        $this->dropTable('{{%color_localization}}');
    }
}
