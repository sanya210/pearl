<?php

use yii\db\Migration;

/**
 * Handles adding name to table `{{%user}}`.
 */
class m190530_161202_add_name_column_to_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'name', $this->string(255));
    }

    public function down()
    {
        $this->dropColumn('user', 'name');
    }
}
