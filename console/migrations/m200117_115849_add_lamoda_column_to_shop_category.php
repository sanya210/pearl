<?php

use yii\db\Migration;

/**
 * Class m200117_115849_add_lamoda_column_to_shop_category
 */
class m200117_115849_add_lamoda_column_to_shop_category extends Migration
{
    public function up()
    {
        $this->addColumn('shop_category', 'lamoda_id', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('shop_category', 'lamoda_id');
    }
}
