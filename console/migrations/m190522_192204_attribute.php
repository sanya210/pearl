<?php

use yii\db\Migration;

/**
 * Class m190522_192204_attribute
 */
class m190522_192204_attribute extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%attribute}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'priority' => $this->integer(),
            'status' => $this->smallInteger()->notNull()
        ], $tableOptions);

        $this->addForeignKey('fk_attribute_category', '{{%attribute}}', 'category_id', '{{%shop_category}}', 'id', 'RESTRICT');

        $this->createTable('{{%attribute_localization}}', [
            'id' => $this->primaryKey(),
            'attribute_id' => $this->integer(),
            'language' => $this->string(16)->notNull(),
            'name' => $this->string(100)
        ], $tableOptions);

        $this->addForeignKey('fk_attribute_localization', '{{%attribute_localization}}', 'attribute_id', '{{%attribute}}', 'id', 'CASCADE', 'RESTRICT');

        $this->createTable('{{%attribute_value}}', [
            'id' => $this->primaryKey(),
            'attribute_id' => $this->integer(),
            'priority' => $this->integer(),
            'status' => $this->smallInteger()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk_attribute_attribute_value', '{{%attribute_value}}', 'attribute_id', '{{%attribute}}', 'id', 'CASCADE', 'RESTRICT');

        $this->createTable('{{%attribute_value_localization}}', [
            'id' => $this->primaryKey(),
            'value_id' => $this->integer(),
            'language' => $this->string(16)->notNull(),
            'name' => $this->string(100)
        ], $tableOptions);

        $this->addForeignKey('fk_attribute_value_localization', '{{%attribute_value_localization}}', 'value_id', '{{%attribute_value}}', 'id', 'CASCADE', 'RESTRICT');

        $this->createTable('{{%product_attribute}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'value_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_product_attribute_product', '{{%product_attribute}}', 'product_id', '{{%product}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_product_attribute_attribute_value', '{{%product_attribute}}', 'value_id', '{{%attribute_value}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%attribute}}');
        $this->dropTable('{{%attribute_localization}}');
        $this->dropTable('{{%attribute_value}}');
        $this->dropTable('{{%attribute_value_localization}}');
        $this->dropTable('{{%product_attribute}}');
    }
}
