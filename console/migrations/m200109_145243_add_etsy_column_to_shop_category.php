<?php

use yii\db\Migration;

/**
 * Class m200109_145243_add_etsy_column_to_shop_category
 */
class m200109_145243_add_etsy_column_to_shop_category extends Migration
{
    public function up()
    {
        $this->addColumn('shop_category', 'etsy_id', $this->integer());
        $this->addColumn('shop_category', 'etsy_section_id', $this->integer());
        $this->addColumn('shop_category', 'etsy_sizes', $this->text());
        $this->addColumn('shop_category', 'etsy_properties', $this->text());
    }

    public function down()
    {
        $this->dropColumn('shop_category', 'etsy_id');
        $this->dropColumn('shop_category', 'etsy_section_id');
        $this->dropColumn('shop_category', 'etsy_sizes');
        $this->dropColumn('shop_category', 'etsy_properties');
    }
}
