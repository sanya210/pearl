<?php

use yii\db\Migration;

/**
 * Class m190619_140600_currency
 */
class m190619_140600_currency extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%currency}}', [
            'id' => $this->primaryKey(),
            'symbol' => $this->string(100)->notNull(),
            'rate' => $this->decimal(20, 4)->notNull(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%currency}}');
    }
}
