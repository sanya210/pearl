<?php

use yii\db\Migration;

/**
 * Class m190514_071153_size
 */
class m190514_071153_size extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%size}}', [
            'id' => $this->primaryKey(),
            'europe' => $this->integer()->notNull(),
            'international' => $this->string(10)->notNull(),
            'chest' => $this->string(10),
            'waist' => $this->string(10),
            'hips' => $this->string(10),
            'growth' => $this->string(10),
            'status' => $this->smallInteger()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%size}}');
    }
}
