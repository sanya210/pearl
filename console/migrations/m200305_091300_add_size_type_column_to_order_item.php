<?php

use yii\db\Migration;

/**
 * Class m200305_091300_add_size_type_column_to_order_item
 */
class m200305_091300_add_size_type_column_to_order_item extends Migration
{
    public function up()
    {
        $this->addColumn('order_item', 'size_type', $this->string(15));
    }

    public function down()
    {
        $this->dropColumn('order_item', 'size_type');
    }
}
