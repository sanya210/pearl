<?php

use yii\db\Migration;

/**
 * Class m190701_131527_address
 */
class m190701_131527_address extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%address}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'country_id' => $this->integer(),
            'delivery_index' => $this->string(),
            'delivery_address' => $this->string(),
        ], $tableOptions);

        $this->addForeignKey('fk_address_user', '{{%address}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%address}}');
    }
}
