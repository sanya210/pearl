<?php

use yii\db\Migration;

/**
 * Class m200108_085616_etsy_token
 */
class m200108_085616_etsy_token extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%etsy_token}}', [
            'id' => $this->primaryKey(),
            'oauth_token' => $this->string(255)->notNull(),
            'oauth_token_secret' => $this->string(255)->notNull(),
            'login_url' => $this->string(255)->notNull(),
            'oauth_verifier' => $this->string(255),
            'token' => $this->string(255)->notNull(),
            'token_secret' => $this->string(255)->notNull(),
            'date' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%etsy_token}}');
    }
}
