<?php

use yii\db\Migration;

/**
 * Class m190611_135308_news
 */
class m190611_135308_news extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%news}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'slug' => $this->string(255)->notNull(),
            'img' => $this->string(255),
            'img_preview' => $this->string(255),
            'status' => $this->smallInteger()->notNull()
        ], $tableOptions);

        $this->createTable('{{%news_localization}}', [
            'id' => $this->primaryKey(),
            'news_id' => $this->integer(),
            'language' => $this->string(16)->notNull(),
            'name' => $this->string(255),
            'text' => $this->text(),
            'meta_title' => $this->string(150),
            'meta_description' => $this->text(),
            'meta_keywords' => $this->string(255),
        ], $tableOptions);

        $this->addForeignKey('fk_news_localization', '{{%news_localization}}', 'news_id', '{{%news}}', 'id', 'CASCADE', 'RESTRICT');

    }

    public function down()
    {
        $this->dropTable('{{%news}}');
        $this->dropTable('{{%news_localization}}');
    }
}
