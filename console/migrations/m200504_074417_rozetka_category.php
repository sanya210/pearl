<?php

use yii\db\Migration;

/**
 * Class m200504_074417_rozetka_category
 */
class m200504_074417_rozetka_category extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%rozetka_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'status' => $this->smallInteger()->notNull(),
        ], $tableOptions);

        $this->addColumn('product', 'rozetka_id', $this->integer());

        $this->addForeignKey('fk_rozetka_category', '{{%product}}', 'rozetka_id', '{{%rozetka_category}}', 'id', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%rozetka_category}}');
        $this->dropColumn('product', 'rozetka_id');
    }
}
