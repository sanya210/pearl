<?php

use yii\db\Migration;

/**
 * Handles dropping username from table `{{%user}}`.
 */
class m190603_073554_drop_username_column_from_user_table extends Migration
{
    public function up()
    {
        $this->dropColumn('user', 'username');
    }

    public function down()
    {
        $this->addColumn('user', 'username', $this->string()->notNull()->unique());
    }
}
