<?php

use yii\db\Migration;

/**
 * Class m190613_065012_text
 */
class m190613_065012_text extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%text}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
        ], $tableOptions);

        $this->createTable('{{%text_localization}}', [
            'id' => $this->primaryKey(),
            'language' => $this->string(16)->notNull(),
            'text_id' => $this->integer(),
            'text' => $this->text(),
        ], $tableOptions);

        $this->addForeignKey('fk_text_localization', '{{%text_localization}}', 'text_id', '{{%text}}', 'id', 'CASCADE', 'RESTRICT');

    }

    public function down()
    {
        $this->dropTable('{{%text}}');
        $this->dropTable('{{%text_localization}}');
    }
}
