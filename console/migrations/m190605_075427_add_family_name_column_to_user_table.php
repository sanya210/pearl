<?php

use yii\db\Migration;

/**
 * Handles adding family_name to table `{{%user}}`.
 */
class m190605_075427_add_family_name_column_to_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'family_name', $this->string(255));
    }

    public function down()
    {
        $this->dropColumn('user', 'family_name');
    }
}
