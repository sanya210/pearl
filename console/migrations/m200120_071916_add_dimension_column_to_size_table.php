<?php

use yii\db\Migration;

/**
 * Handles adding dimension to table `{{%size}}`.
 */
class m200120_071916_add_dimension_column_to_size_table extends Migration
{
    public function up()
    {
        $this->addColumn('size', 'dimension', $this->string(10));
    }

    public function down()
    {
        $this->dropColumn('size', 'dimension');
    }
}
