<?php

use yii\db\Migration;

/**
 * Class m190502_053911_create_category
 */
class m190502_053911_create_category extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_category}}', [
            'id' => $this->primaryKey(),
            'slug' => $this->string(100)->notNull(),
            'priority' => $this->integer(),
            'status' => $this->smallInteger()->notNull(),
            'show_in_menu' => $this->smallInteger()->notNull(),
        ], $tableOptions);

        $this->createTable('{{%shop_category_localization}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'language' => $this->string(16)->notNull(),
            'name' => $this->string(100),
            'meta_title' => $this->string(150),
            'meta_description' => $this->text(),
            'meta_keywords' => $this->string(255),
        ], $tableOptions);

        $this->addForeignKey('fk_shop_category_localization', '{{%shop_category_localization}}', 'category_id', '{{%shop_category}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%shop_category}}');
        $this->dropTable('{{%shop_category_localization}}');
    }
}
