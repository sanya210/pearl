<?php

use yii\db\Migration;

/**
 * Handles adding name_rozetka to table `{{%product}}`.
 */
class m191003_063743_add_name_rozetka_column_to_product_table extends Migration
{
    public function up()
    {
        $this->addColumn('product', 'name_rozetka', $this->string());
    }

    public function down()
    {
        $this->dropColumn('product', 'name_rozetka');
    }
}
