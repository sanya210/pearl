<?php

use yii\db\Migration;

/**
 * Class m190503_120330_create_product
 */
class m190503_120330_create_product extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'slug' => $this->string(100)->notNull(),
            'article' => $this->string(100)->notNull(),
            'price' => $this->decimal(20, 4),
            'color_id' => $this->integer(),
            'priority' => $this->integer(),
            'number_purchased' => $this->integer(),
            'status' => $this->smallInteger()->notNull(),

            'provider_id' => $this->integer(),
            'provider_name' => $this->string(),
            'provider_article' => $this->string(100)->notNull(),
            'provider_price' => $this->decimal(20, 4),
        ], $tableOptions);

        $this->createTable('{{%product_localization}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'language' => $this->string(16)->notNull(),
            'name' => $this->string(255),
            'description' => $this->text(),
            'meta_title' => $this->string(150),
            'meta_description' => $this->text(),
            'meta_keywords' => $this->string(255),
        ], $tableOptions);

        $this->addForeignKey('fk_product_localization', '{{%product_localization}}', 'product_id', '{{%product}}', 'id', 'CASCADE', 'RESTRICT');

        $this->addForeignKey('fk_product_category', '{{%product}}', 'category_id', '{{%shop_category}}', 'id', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%product}}');
        $this->dropTable('{{%product_localization}}');
    }
}
