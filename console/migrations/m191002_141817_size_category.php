<?php

use yii\db\Migration;

/**
 * Class m191002_141817_size_category
 */
class m191002_141817_size_category extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%size_category}}', [
            'id' => $this->primaryKey(),
            'size_id' => $this->integer(),
            'category_id' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_size_category_size', '{{%size_category}}', 'size_id', '{{%size}}', 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        $this->dropTable('{{%size_category}}');
    }
}
