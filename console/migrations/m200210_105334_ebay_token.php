<?php

use yii\db\Migration;

/**
 * Class m200210_105334_ebay_token
 */
class m200210_105334_ebay_token extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%ebay_token}}', [
            'id' => $this->primaryKey(),
            'access_token' => $this->text()->notNull(),
            'expires_in' => $this->integer()->notNull(),
            'refresh_token' => $this->string(255)->notNull(),
            'refresh_token_expires_in' => $this->integer()->notNull(),
            'token_type' => $this->string(255)->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%ebay_token}}');
    }
}
