<?php

use yii\db\Migration;

/**
 * Handles adding other_colors to table `{{%product}}`.
 */
class m190513_165152_add_other_colors_column_to_product_table extends Migration
{
    public function up()
    {
        $this->addColumn('product', 'other_colors', $this->text());
    }

    public function down()
    {
        $this->dropColumn('product', 'other_colors');
    }
}
