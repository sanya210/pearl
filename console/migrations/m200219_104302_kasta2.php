<?php

use yii\db\Migration;

/**
 * Class m200219_104302_kasta2
 */
class m200219_104302_kasta2 extends Migration
{
    public function up()
    {
        $this->addColumn('shop_category', 'name_kasta', $this->string());
        $this->addColumn('shop_category', 'gender', $this->integer());
        $this->addColumn('product', 'season', $this->integer());
        $this->addColumn('product', 'composition_kasta', $this->string());

    }

    public function down()
    {
        $this->dropColumn('shop_category', 'name_kasta');
        $this->dropColumn('shop_category', 'gender');
        $this->dropColumn('product', 'season');
        $this->dropColumn('product', 'composition_kasta');
    }
}
